---
title: "served_plmn.cfg"
description: "Параметры обслуживаемых сетей"
weight: 20
type: docs
---

В файле задаются сети, обслуживаемые узлом ММЕ. Каждой сети соответствует одна секция **\[Served_PLMN\]**.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload served_plmn.cfg**.

### Описание параметров ###

| Параметр                                               | Описание                                                                                                                                                                                                                                          | Тип               | O/M | P/R | Версия    |
|--------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|-----|-----|-----------|
| <a name="mcc">MCC</a>                                  | Мобильный код страны.                                                                                                                                                                                                                             | int               | M   | R   |           |
| <a name="mnc">MNC</a>                                  | Код мобильной сети.                                                                                                                                                                                                                               | int               | M   | R   |           |
| <a name="mmegi">MMEGI</a>                              | Значение MME Group ID.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                                                                                  | int               | O   | R   |           |
| <a name="mmec">MMEC</a>                                | Значение MME Code.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                                                                                      | int               | O   | R   |           |
| E-UTRAN                                                | Флаг поддержки сети E-UTRAN.<br>По умолчанию: 1.                                                                                                                                                                                                  | bool              | O   | R   |           |
| UTRAN                                                  | Флаг поддержки сети UTRAN.<br>По умолчанию: 1.                                                                                                                                                                                                    | bool              | O   | R   |           |
| GERAN                                                  | Флаг поддержки сети GERAN.<br>По умолчанию: 1.                                                                                                                                                                                                    | bool              | O   | R   |           |
| Origin-Host                                            | Значение `Origin-Host` для протокола Diameter.<br>По умолчанию: mmec[\<MMEC\>](#mmec).mmegi[\<MMEGI\>](#mmegi).<br>mme.epc.mnc[\<MNC\>](#mnc).mcc[\<MCC\>](#mcc).<br>3gppnetwork.org.                                                             | string            | O   | R   |           |
| Origin-Realm                                           | Значение `Origin-Realm` для протокола Diameter.<br>По умолчанию: epc.mnc[\<MNC\>](#mnc).<br>mcc[\<MCC\>](#mcc).3gppnetwork.org.                                                                                                                   | string            | O   | R   |           |
| Authentication                                         | Флаг включения аутентификации.<br>По умолчанию: 1.                                                                                                                                                                                                | bool              | O   | R   |           |
| AuthVectorsNum                                         | Количество запрашиваемых векторов аутентификации.<br>Диапазон: 1-7. По умолчанию: 1.                                                                                                                                                              | int               | O   | R   | 1.41.0.0  |
| CheckIMEI                                              | Флаг включения проверки IMEI на узле EIR.<br>По умолчанию: 0.                                                                                                                                                                                     | bool              | O   | R   |           |
| AllowGreyIMEI                                          | Флаг разрешения подключения телефона с неизвестным IMEI.<br>По умолчанию: 1.                                                                                                                                                                      | bool              | O   | R   |           |
| EIR-Host                                               | Значение `Destination-Host` для узла EIR.                                                                                                                                                                                                         | string            | O   | R   |           |
| EIR-Realm                                              | Значение `Destination-Realm` для узла EIR.                                                                                                                                                                                                        | string            | O   | R   |           |
| <a name="psm">PSM</a>                                  | Флаг разрешения режима Power Saving Mode.<br>См. [3GPP TS 23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/12.02.00_60/ts_123682v120200p.pdf).<br>По умолчанию: 0.                                                               | bool              | O   | R   | 1.22.1.0  |
| T3324                                                  | Таймер T3324, в секундах.<br>См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>**Примечание.** Используется только при `PSM = 1`.<br>По умолчанию: значение T3324 абонента.   | int               | O   | R   |           |
| T3402                                                  | Таймер T3402, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                                               | int               | O   | R   |           |
| T3402_Rej                                              | Таймер T3402 для абонентов, не попавших в белый список IMSI, в секундах.<br>По умолчанию: 0.                                                                                                                                                      | int               | O   | R   |           |
| T3412                                                  | Таймер T3412, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 60.                                                                              | int               | O   | R   |           |
| <a name="t3412-ext">T3412_Ext</a>                      | Таймер T3412 Extended, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: не определено.                                                          | int               | O   | R   |           |
| T3412_Ext_Source                                       | Приоритетный источник таймера [T3412 Extended](#t3412-ext).<br>`mme` / `ue`. По умолчанию: ue.                                                                                                                                                    | string            | O   | R   |           |
| <a name="t3413">T3413</a>                              | Таймер T3413, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 10.                                                                              | int               | O   | R   |           |
| T3413_SGs                                              | Таймер T3413 для процедуры S1AP Paging, вызванного процедурой SGsAP Paging, в секундах.<br>По умолчанию: значение [T3413](#t3413).                                                                                                                | int               | O   | R   |           |
| T3422                                                  | Таймер T3422, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                               | int               | O   | R   | 1.36.0.0  |
| <a name="t3450">T3450</a>                              | Таймер T3450, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                               | int               | O   | R   | 1.36.0.0  |
| T3450_RepeatCount                                      | Количество повторных отправок по таймеру [T3450](#t3450).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                      | int               | O   | R   | 1.36.0.0  |
| <a name="t3460">T3460</a>                              | Таймер T3460, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                               | int               | O   | R   | 1.36.0.0  |
| T3460_RepeatCount                                      | Количество повторных отправок по таймеру [T3460](#t3460).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                      | int               | O   | R   | 1.36.0.0  |
| <a name="t3470">T3470</a>                              | Таймер T3470, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                               | int               | O   | R   | 1.36.0.0  |
| T3470_RepeatCount                                      | Количество повторных отправок по таймеру [T3470](#t3470).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                      | int               | O   | R   | 1.36.0.0  |
| PagingRepeatCount                                      | Количество отправок запросов Paging на каждую станцию eNodeB в рамках текущего шага используемой модели пейджинга.<br>Диапазон: 1-5.<br>По умолчанию: 3.                                                                                          | int               | O   | R   |           |
| PagingModel                                            | Индикатор используемой модели для процедуры Paging.<br>`0` — по умолчанию;<br>`1` — начало с последней базовой станции.<br>По умолчанию: 0.                                                                                                       | int               | O   | R   | 1.40.0.0  |
| UE_ActivityTimeout                                     | Время ожидания активности от устройства UE, в секундах.<br>По умолчанию: 10.                                                                                                                                                                      | int               | O   | R   |           |
| <a name="ims-vops">IMS_VoPS</a>                        | Значение `IMS Voice over PS Session Indicator`.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                         | bool              | O   | R   |           |
| VoPS_APN_NI                                            | Перечень значений APN-NI, которые могут быть использованы для VoPS.<br>По умолчанию: ims.                                                                                                                                                         | \[int\]           | O   | R   | 1.40.0.0  |
| P_CSCF_Restoration                                     | Флаг поддержки процедуры P-CSCF Restoration.<br>По умолчанию: 1.                                                                                                                                                                                  | bool              | O   | R   | 1.44.2.0  |
| ResetOnSetup                                           | Флаг отправки сообщения S1AP: RESET после запроса S1AP: S1 SETUP REQUEST.<br>По умолчанию: 0.                                                                                                                                                     | bool              | O   | R   |           |
| AllowDefaultAPN                                        | Флаг разрешения использования APN по умолчанию.<br>По умолчанию: 1.                                                                                                                                                                               | bool              | O   | R   |           |
| AllowOperatorQCI                                       | Флаг разрешения использования <abbr title="QoS Class Identifier">QCI</abbr> из диапазона 128-254.<br>По умолчанию: 0.                                                                                                                             | bool              | O   | R   |           |
| AllowExtDRX                                            | Флаг разрешения использования Extended DRX.<br>См. [3GPP TS 25.300](https://www.etsi.org/deliver/etsi_ts/125300_125399/125300/17.00.00_60/ts_125300v170000p.pdf).<br>По умолчанию: 0.                                                             | bool              | O   | R   |           |
| PTW                                                    | Значение Paging Transmission Window для Extended DRX.<br>По умолчанию: не определено.                                                                                                                                                             | int               | O   | R   | 1.41.0.0  |
| eDRX                                                   | Значение <abbr title="Extended Discontinuous Reception">eDRX</abbr>.<br>По умолчанию: не определено.                                                                                                                                              | int               | O   | R   | 1.41.0.0  |
| RelativeMME_Capacity                                   | Относительная емкость узла MME, Relative MME Capacity.<br>См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).<br>По умолчанию: 255.                                                | int               | O   | R   |           |
| RelativeMME_CapacityForTAC                             | Перечень связей Relative MME Capacity и TAC. Формат:<br>`<tac>-<capacity>,<tac>-<capacity>`                                                                                                                                                       | \[{int,int}\]     | O   | R   | 1.35.0.0  |
| NotifyOnPGW_Alloc                                      | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении динамического IP-адреса узла PGW для APN.<br>По умолчанию: 1.                                                                                               | bool              | O   | R   |           |
| NotifyOnUE_SRVCC                                       | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении флага поддержки SRVCC на устройстве UE.<br>По умолчанию: 1.                                                                                                 | bool              | O   | R   |           |
| EMM_Info                                               | Флаг отправки сообщения EMM Information.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                                | bool              | O   | R   |           |
| ForceIdentityReq                                       | Флаг отправки Identity Req вне зависимости от наличия сохраненного ранее IMSI.<br>По умолчанию: 0.                                                                                                                                                | bool              | O   | R   |           |
| CP_CIoT_opt                                            | Флаг разрешения оптимизации Control Plane CIoT EPS.<br>См. [3GPP TS 36.300](https://www.etsi.org/deliver/etsi_ts/136300_136399/136300/14.10.00_60/ts_136300v141000p.pdf).<br>По умолчанию: 0.                                                     | bool              | O   | R   | 1.15.19.0 |
| IndirectFwd                                            | Флаг разрешения создания Indirect Data Forwarding Tunnel при хэндовере.<br>По умолчанию: 0.                                                                                                                                                       | bool              | O   | R   | 1.19.0.0  |
| PurgeDelay                                             | Время задержки отправки сообщения Diameter: Purge-Request на узел HSS, в секундах.<br>**Примечание.** По умолчанию сообщение не отправляется, а данные не удаляются.<br>По умолчанию: не определено.                                              | int               | O   | R   |           |
| RequestIMEISV                                          | Флаг требования IMEISV от абонента.<br>По умолчанию: 1.                                                                                                                                                                                           | bool              | O   | R   | 1.15.18.0 |
| UseVLR                                                 | Флаг использования VLR.<br>По умолчанию: 0.                                                                                                                                                                                                       | bool              | O   | R   |           |
| RejectOnLU_Fail                                        | Флаг отправки сообщения SGsAP-LOCATION-UPDATE-REJECT в случае неуспеха процедуры SGsAP-Location-Update.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 0. | bool              | O   | R   |           |
| MO_CSFB_Ind                                            | Флаг отправки сообщения SGsAP-MO-CSFB-Indication при получении запроса Extended Service Request.<br>По умолчанию: 0.                                                                                                                              | bool              | O   | R   |           |
| SRVCC                                                  | Флаг поддержки <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>.<br>См. [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf).<br>По умолчанию: 0.                           | bool              | O   | R   |           |
| NetFullname                                            | Полное название сети.<br>По умолчанию: Protei Network.                                                                                                                                                                                            | string            | O   | R   |           |
| NetName                                                | Краткое название сети.<br>По умолчанию: Protei.                                                                                                                                                                                                   | string            | O   | R   |           |
| LDN                                                    | <abbr title="Local Distinguished Name">LDN</abbr>, отправляемый при активированном SRVCC.<br>По умолчанию: Protei_MME.                                                                                                                            | string            | O   | R   |           |
| AllowEmptyMSISDN                                       | Флаг разрешения регистрации абонентов без MSISDN.<br>По умолчанию: 0.                                                                                                                                                                             | bool              | O   | R   |           |
| AllowUnknownIMEI                                       | Флаг обслуживания устройств, неизвестных для EIR.<br>По умолчанию: 0.                                                                                                                                                                             | bool              | O   | R   | 1.40.0.0  |
| S11UliAlwaysSent                                       | Флаг отправки User Location Information в запросе GTP: Modify Bearer Request.<br>См. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf).<br>По умолчанию: 0.                           | bool              | O   | R   | 1.40.0.0  |
| EquivalentPLMNs                                        | Перечень эквивалентных PLMN, который указывается в поле `ICS Request`. Формат:<br>`<mcc>-<mnc>,<mcc>-<mnc>`<br>См. [3GPP TS 29.292](https://www.etsi.org/deliver/etsi_ts/129200_129299/129292/17.00.00_60/ts_129292v170000p.pdf).                 | \[string\]        | O   | R   | 1.36.0.0  |
| <a name="emergency-numbers">EmergencyNumbers</a>       | Перечень [типов и правил номеров экстренных служб](../emergency_numbers/).                                                                                                                                                                        | \[string\]        | O   | R   |           |
| EmergencyAttach_wo_auth                                | Флаг разрешения процедуры Emergency Attach без аутентификации.<br>По умолчанию: 0.                                                                                                                                                                | bool              | O   | R   |           |
| <a name="emergency-pdn">EmergencyPDN</a>               | Перечень имен PDN Connectivity, применяемых в случае Emergency Attach.<br>**Примечание.** Именованные описания PDN Connectivity хранятся в файле [emergency_pdn.cfg](../emergency_pdn).                                                           | \[string\]        | O   | R   |           |
| [Timezone](#timezone)                                  | Часовые пояса.<br>По умолчанию: UTC+3.                                                                                                                                                                                                            | timezone          | O   | R   |           |
| <a name="apn-rules">APN_rules</a>                      | Перечень [правил APN](../apn_rules/).                                                                                                                                                                                                             | \[string\]        | O   | R   |           |
| <a name="tac_rules">TAC_rules</a>                      | Перечень [правил TAC](../tac_rules/).                                                                                                                                                                                                             | \[string\]        | O   | R   |           |
| <a name="rac-rules">RAC_rules</a>                      | Перечень [правил RAC](../rac_rules/).                                                                                                                                                                                                             | \[string\]        | O   | R   |           |
| ForbiddenLAC                                           | Перечень кодов LAC, из которых переход абонентов запрещен.                                                                                                                                                                                        | \[int\]           | O   | R   | 1.33.0.0  |
| <a name="pgw-ip">PGW_IP</a>                            | IP-адрес и FQDN узла PGW по умолчанию. Формат:<br>`"(<fqdn>)<ip>"`<br>**Примечание.** FQDN может отсутствовать.                                                                                                                                   | {string,ip}       | O   | R   |           |
| <a name="sgw-ip">SGW_IP</a>                            | IP-адрес и FQDN узла SGW по умолчанию. Формат:<br>`"(<fqdn>)<ip>"`<br>**Примечание.** FQDN может отсутствовать.                                                                                                                                   | {string,ip}       | O   | R   |           |
| MME_IP                                                 | IP-адреса других узлов MME с соответствующими значениями MMEGI и MMEC. Формат:<br>`<mmegi>-<mmec>-<ip>,<mmec>-<ip>`<br>**Примечание.** Если MMEGI не задан, то используется значение, связанное с этой PLMN.                                      | [{int,int,ip}\]   | O   | R   |           |
| SGSN_IP                                                | IP-адреса узлов SGSN с соответствующими контроллерами RNC. Формат:<br>`<rnc>-<ip>,<rnc>-<ip>`.                                                                                                                                                    | \[{string,ip}\]   | O   | R   |           |
| NRI_length                                             | Количество используемых бит идентификатора <abbr title="Network Resource Identifier">NRI</abbr> узла SGSN.<br>**Примечание.** Если не задано, то не используется в рамках соответствующей сети PLMN.                                              | int               | O   | R   | 1.44.1.0  |
| <a name="imsi-wl">IMSI_whitelist</a>                   | Перечень [правил TAC и IMSI](../imsi_rules/).<br>Если TAC не указан, то набор масок применяется для всей PLMN.                                                                                                                                    | \[string\]        | O   | R   |           |
| UE_Usage_Types                                         | Перечень UE Usage Type, связанных с текущим MMEGI.<br>См. [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.08.00_60/ts_123401v170800p.pdf).                                                                          | \[int\]           | O   | R   | 1.35.0.0  |
| Foreign_UE_Usage_Types                                 | Перечень связей между UE Usage Type и другими MMEGI. Формат:<br>`<ue_usage_type>,<ue_usage_type>-<mmegi>`.                                                                                                                                        | \[{\[int\],int}\] | O   | R   |           |
| <a name="zc-rules">ZC_rules</a>                        | Перечень [правил ZoneCodes](../zc_rules/).                                                                                                                                                                                                        | \[string\]        | O   | R   | 1.36.0.0  |
| <a name="qos-rules">QoS_rules</a>                      | Перечень [правил QoS](../qos_rules/).                                                                                                                                                                                                             | \[string\]        | O   | R   | 1.37.0.0  |
| <a name="code-mapping-rules">CodeMapping_rules</a>     | Перечень [правил Diameter: Result-Code и EMM Cause](../code_mapping_rules/).                                                                                                                                                                      | \[string\]        | O   | R   | 1.37.2.0  |
| <a name="forbidden-imei-rules">ForbiddenIMEI_rules</a> | Перечень [правил запрещённых IMEI](../forbidden_imei_rules/).                                                                                                                                                                                     | \[string\]        | O   | R   |           |
| \<TAC\> = \<LAI\>                                      | Перечень соответствий TAC к LAI. Формат:<br>`<tac> = [<plmn>-]<lac>;`<br>**Примечание.** Один TAC может быть связан только с одним LAI, один LAI может быть связан со многими TAC.                                                                | \[{int,int,int}\] | O   | R   |           |
| TAC                                                    | Код зоны отслеживания.                                                                                                                                                                                                                            | int               | O   | R   |           |
| \<plmn\>                                               | Код сети PLMN.<br>По умолчанию: значение [\<MCC\>](#mcc)[\<MNC\>](#mnc).                                                                                                                                                                          | int               | O   | R   |           |
| \<lac\>                                                | Код локальной области LAC.                                                                                                                                                                                                                        | int               | M   | R   |           |

#### Формат Timezone {#timezone}

Формат записи часового пояса: `[TACX_]UTC<sign>HH:MM[_DST+Y]`

* Необязательная часть `TAC<X>_` — задает ассоциацию между часовым поясом и TAC, при отсутствии ассоциируется со всеми TAC в данной PLMN:
  * X — код TAC;
* Обязательная часть `UTC<sign><HH>:<MM>` — задает часовой пояс через отклонение от UTC:
  * sign — знак отклонения от UTC, `+` или `-`;
  * HH — количество часов;
  * MM — количество минут;
* Необязательная часть `_DST+<Y>` — задает дополнительное отклонение при переводе часов на летнее время:
  * Y — количество добавленных часов, диапазон: 0-2;

#### Адреса PGW и SGW ####

**Примечание.** Адреса SGW и PGW ([SGW_IP](#sgw-ip)/[PGW_IP](#pgw-ip)) определяются в нескольких конфигурационных файлах.
Приоритеты значений в порядке убывания:

- [apn_rules.cfg](../apn_rules/);
- [dns.cfg](../dns/);
- served_plmn.cfg;
- [gtp_c.cfg](../gtp_c/).

#### Пример ####

```
[Served_PLMN]
{ 
  MCC = 999;
  MNC = 99;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  T3413 = 1;
  T3412 = 1800;
  T3402 = 10;
  T3402_Rej = 10;
  T3450 = 1;
  T3460 = 1;
  T3470 = 1;
  CheckIMEI = 0;
  EmergencyNumbers = "911";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  UseVLR = 0;
  IMS_VoPS = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 0;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6;
  RelativeMME_Capacity = 255;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_999";
  NetName = "test99";
  Timezone = "UTC+3";
}
{
  MCC = 001;
  MNC = 01;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  MME_IP = 1-2-192.168.125.182;
  T3324 = 60;
  T3413 = 1;
  T3412 = 120;
  T3412_Ext = 14400;
  T3412_Ext_Source = "mme";
  T3402 = 5;
  T3402_Rej = 10;
  T3450 = 5;
  T3450_RepeatCount = 4;
  T3460 = 6;
  T3460_RepeatCount = 3;
  T3470 = 1;
  UE_ActivityTimeout = 600;
  CheckIMEI = 0;
  RequestIMEISV = 1;
  AuthVectorsNum = 2
  EmergencyNumbers = "112,911";
  EmergencyPDN = "EMRG";
  Origin-Host = "mme1.epc.mnc001.mcc001.3gppnetwork.org";
  Origin-Realm = "epc.mnc001.mcc001.3gppnetwork.org";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  VoPS_APN_NI = ims;
  AllowGreyIMEI = 0;
  UseVLR = 0;
  RejectOnLU_Fail = 0;
  ResetOnSetup = 0;
  PurgeDelay = 0;
  PagingModel = 1;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1,Rule_2,Rule_3;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6,Rule_7;
  CodeMapping_rules = Rule1, Rule2;
  RelativeMME_Capacity = 100;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_001";
  NetName = "protei";
  Timezone = "TAC1_UTC+2";
  CP_CIoT_opt = 1;
  PSM = 1;
  1 = 1;
  7 = 1;
  9 = 25048-5;
  10 = 25048-7;
  UE_Usage_Types = 0,1;
  Foreign_UE_Usage_Types = 2-2;
  QoS_rules = Rules;
  AllowDefaultAPN = 1;
}
{
  MMEGI = 3;
  MMEC = 4;
}
{
  MCC = 208;
  MNC = 93;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  T3413 = 5;
  T3412_Ext = 600;
  RequestIMEISV = 0;
  CheckIMEI = 0;
  AllowEmptyMSISDN = 1;
  EIR-Host ="eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  PSM = 1;
  AllowExtDRX = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 1;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_4;
  SGW_IP = "192.168.125.95";
  2 = 3;
  1 = 1;
  5 = 25048-4;
  CP_CIoT_opt = 1;
}
```
