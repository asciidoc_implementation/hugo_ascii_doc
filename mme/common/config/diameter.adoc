---
title: diameter.cfg
description: Файл настройки Diameter-соединений
type: docs
weight: 20
---
= diameter.cfg
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

В файле задаются настройки подключений по протоколу Diameter.

=== Используемые секции

 *<<general,[General]>>*:: основные параметры протокола Diameter
 *<<local_address,[LocalAddress]>>*:: параметры локального интерфейса
 *<<local_peer_capabilities,[LocalPeerCapabilities]>>*:: параметры локальных peer
 *<<specific_local_peer_capabilities,[SpecificLocalPeerCapabilities]>>*:: параметры списка LocalPeerCapabilities, специфичных для отдельного PCSM
 *<<timers,[Timers]>>*:: параметры таймеров
 *<<resend,[Resend]>>*:: параметры повторной отправки сообщения

.Описание параметров
[options="header",cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[general]][General]*
| Основные параметры протокола Diameter.
| object
| O
| R
|

| Case-Sensetive
| Флаг сохранения регистра в строковых AVP вместо приведения к нижнему регистру. +
По умолчанию: 1.
| bool
| O
| R
|

| ReceivingFromAnyHost
| Флаг обработки запросов, если *_Destination-Host_* не совпадает с *_Local-Host_*. +
По умолчанию: 0.
| bool
| O
| R
|

| MaxTimeoutCount
| Количество истекших таймеров ожидания ответа, после которого хост считается занятым. +
По умолчанию: 10.
| int
| O
| R
|

| UseResend
| Флаг использования перепосылки по истечении времени ожидания. +
По умолчанию: 0.
| bool
| O
| R
|

| Necromancy
| Флаг отправки запросов на занятый хост, если свободные не найдены. +
По умолчанию: 0.
| bool
| O
| R
|

| *[[local_address]][LocalAddress]*
| Параметры локального интерфейса.
| object
| M
| P
|

| [[local_host]]LocalHost
| Адрес локального хоста. +
По умолчанию: 0.0.0.0.
| ip
| M
| P
|

| [[local_port]]LocalPort
| Номер локального порта.
| int
| M
| P
|

| Transport
| Используемый транспортный протокол. +
*_tcp_*/*_sctp_*. По умолчанию: tcp.
| string
| O
| P
|

| [[local_interfaces]]local_interfaces
| Перечень IP-адресов для мультихоуминга. Формат: +
*_{ <ip>:<port>; <ip>:<port>; }_*
| [ip:port]
| O
| P
|

| InStreams
| Количество входящих SCTP-потоков. +
Диапазон: 1-65&nbsp;536. По умолчанию: 1.
| int
| O
| P
|

| OutStreams
| Количество исходящих SCTP-потоков. +
Диапазон: 1-65&nbsp;536. По умолчанию: 1.
| int
| O
| P
|

| MaxInitRetransmits
| Количество попыток отправки сообщения INIT, прежде чем хост считать недоступным. +
По умолчанию: 10.
| int
| O
| R
|

| InitTimeout
| Время ожидания сообщения INIT_ACK, в миллисекундах. +
По умолчанию: 1000.
| int
| O
| R
|

| RtoMax
| Максимальное значение RTO, в миллисекундах. +
По умолчанию: 60&nbsp;000.
| int
| O
| R
|

| RtoMin
| Минимальное значение RTO, в миллисекундах. +
По умолчанию: 1000.
| int
| O
| R
|

| RtoInitial
| Начальное значение RTO, в миллисекундах. +
По умолчанию: 3000.
| int
| O
| R
|

| HbInterval
| Период посылки сигнала heartbeat, в миллисекундах. +
По умолчанию: 30&nbsp;000.
| int
| O
| R
|

| dscp
| Значение поля заголовка IP DSCP/ToS. +
По умолчанию: 0.
| int
| O
| R
|

| AssociationMaxRetrans
| Максимальное количество повторных отправок, после которых маршрут считается недоступным. +
По умолчанию: 10.
| int
| O
| R
|

| SackDelay
| Время ожидания отправки сообщения SACK.
| int
| O
| R
|

| SndBuf
| Размер буфера сокета для отправки, *_net.core.wmem_default_*. +
CAUTION: Значение удваивается, удвоенный размер не может превышать *_net.core.wmem_max_*.
| int
| O
| R
|

| ShutdownEvent
| Флаг включения индикации о событии SHUTDOWN от ядра.
| bool
| O
| R
|

| AssocChangeEvent
| Флаг включения индикации об изменении состояния ассоциации от ядра.
| bool
| O
| R
|

| PeerAddrChangeEvent
| Флаг включения индикации об изменении состояния peer в ассоциации от ядра.
| bool
| O
| R
|

| *[[local_peer_capabilities]][LocalPeerCapabilities]*
| Параметры локальных peer.
| object
| O
| R
|

| [[origin_host_diam]]Origin-Host
| Идентификатор хоста, *_Origin-Host_*.
| string
| M
| R
|

| [[origin_realm_diam]]Origin-Realm
| Realm хоста, *_Origin-Realm_*.
| string
| M
| R
|

| Vendor-ID
| Идентификатор производителя, *_Vendor-Id_*.
| int
| M
| R
|

| Product-Name
| Название системы, *_Product-Name_*.
| string
| M
| R
|

| Firmware-Revision
| Версия программного обеспечения, *_Firmware-Revision_*.
| int
| O
| R
|

| Origin-State-Id
| Идентификатор состояния, *_Origin-State-Id_*. +
NOTE: Если не задан, то каждый раз при перезагрузке *_Origin-State-Id_* принимает уникальное значение. Задается конкретное значение, чтобы удаленные пиры не инициировали сброс сессий при перезагрузке программного обеспечения.
| int
| O
| R
|

| Host-IP-Address
| Перечень локальных адресов, *_Host-IP-Address_*. Формат: +
*_{ <ip>;<ip>; }_*
| [ip]
| M
| R
|

| Auth-Application-Id
| Перечень идентификаторов поддерживаемых приложений, *_Auth-Application-Id_*. Формат: +
*_{ <int>;<int>; }_*
| [int]
| O
| R
|

| Acct-Application-Id
| Перечень идентификаторов поддерживаемых аккаутинговых приложений, *_Acct-Application-Id_*. Формат: +
*_{ <int>;<int>; }_*
| [int]
| O
| R
|

| Vendor-Specific-Application-Id
| Перечень идентификаторов приложений, определяемых вендором, *_Vendor-Specific-Application-Id_*. Формат см. <<vendor_specific_application_id,ниже>>.
| [object]
| O
| R
|

| Inband-Security-Id
| Перечень идентификаторов поддерживаемых механизмов обеспечения безопасности, *_Inband-Security-Id_*. Формат: +
*_{ <int>;<int>; }_* +
NOTE: Поддерживается только *_0, NO_SECURITY_*.
| [int]
| O
| R
|

| Supported-Vendor-Id
| Перечень идентификаторов поддерживаемых производителей, *_Supported-Vendor-Id_*. Формат: +
*_{ <int>;<int>; }_* +
NOTE: Используется только для формирования сообщения Diameter: Capabilities-Exchange-Request.
| [int]
| O
| R
|

| ForceDestinationHost
| Код формата заполнением AVP *_Destination-Host_*. +
*_0_* - режим обратной совместимости, очищение *_Destination-Host_* только при повторной отправке запроса; +
*_1_* - режим без изменения *_Destination-Host_*; +
*_2_* - режим, при котором очищается значение *_Destination-Host_* при не совпадении со значением из PCSM; +
*_3_* - режим, при котором значение *_Destination-Host_* заменяется значением из PCSM, если значения не совпадают или AVP отсутствует. +
По умолчанию: 0.
| int
| O
| R
| 4.1.11.8

| *[[specific_local_peer_capabilities]][SpecificLocalPeerCapabilities]*
| Перечень LocalPeerCapabilities, специфичных для отдельного PCSM. Формат: +
*_<pcsm_address> = { <LocalPeerCapabilities> };_*. +
NOTE: Формат <LocalPeerCapabilities> аналогичен <<local_peer_capabilities,[LocalPeerCapabilities]>>.
| [object]
| O
| R
|

| *[[timers]][Timers]*
| Параметры таймеров.
| object
| O
| R
|

| [[appl_timeout]]Appl_Timeout
| Время ожидания установления Diameter-соединения, в миллисекундах. +
NOTE: Отсчитывается с момента посылки запроса на установление TCP-соединения до получения запроса Diameter: Capabilities-Exchange-Answer. +
По умолчанию: 40&nbsp;000.
| int
| O
| R
|

| [[watchdog_timeout]]Watchdog_Timeout
| Время ожидания посылки сообщений Diameter: Device-Watchdog-Request/Answer, контроль состояния соединения, в миллисекундах. +
NOTE: Отсчитывается с момента посылки последнего сообщения, не обязательно Diameter: DWR. +
По умолчанию: 10&nbsp;000.
| int
| O
| R
|

| [[reconnect_timeout]]Reconnect_Timeout
| Время ожидания переустановление соединения, в миллисекундах. +
NOTE: Отсчитывается с момента разрушения соединения до очередной попытки восстановления. +
По умолчанию: 30&nbsp;000.
| int
| O
| R
|

| [[on_busy_reconnect_timeout]]OnBusyReconnect_Timeout
| Время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной *_DisconnectCause = BUSY (1)_*, в миллисекундах. +
По умолчанию: 60&nbsp;000. +
NOTE: Если 0, то соединение не переустанавливается.
| int
| O
| R
| 4.1.8.43

| [[on_shutdown_reconnect_timeout]]OnShutdownReconnect_Timeout
| Время ожидания переустановления соединения после получения Diameter: Disconnect-Peer-Request с причиной *_DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU (2)_*, в миллисекундах. +
По умолчанию: 0. +
NOTE: Если 0, то соединение не переустанавливается.
| int
| O
| R
| 4.1.8.43

| [[response_timeout]]Response_Timeout
| Время ожидания ответа, в миллисекундах. +
По умолчанию: 10&nbsp;000.
| int
| O
| R
|

| [[breakdown_timeout]]Breakdown_Timeout
| Период временной недоступности узла PCSM, в миллисекундах. +
По умолчанию: 30&nbsp;000.
| int
| O
| R
|

| [[statistic_timeout]]Statistic_Timeout
| Период вывода статистики в лог-файлы, в миллисекундах. +
По умолчанию: 60&nbsp;000.
| int
| O
| R
|

| *[[resend]][Resend]*
| Параметры повторной отправки сообщения.
| object
| O
| R
| 4.1.8.47

| ResetCountForSetBusy_Timeout
| Период сброса счетчиков <<count_for_set_busy,CountForSetBusy>>, в миллисекундах. +
По умолчанию: 10&nbsp;000.
| int
| O
| R
| 4.1.8.47

| ResendInfo
| Параметры соответствия кодов *_Result-Code_* и деактивации узла PCSM.
| [object]
| O
| R
| 4.1.8.47

| [[result_code]]ResultCode
| Значение поля *_Result-Code_*, при котором совершается повторная отправка сообщений на альтернативный узел PCSM.
| int
| M
| R
| 4.1.8.47

| [[count_for_set_busy]]CountForSetBusy
| Количество ответов с указанным значением <<result_code,ResultCode>>, при котором PCSM становится неактивным. +
NOTE: Счетчик сбрасывается при получении ответа с не указанным в перечне *_ResultCode_*.
| int
| M
| R
| 4.1.8.47
|===

NOTE: *_ResultCode = 3004 (TOO_BUSY)_* и *_CountForSetBusy = 1_* добавляется автоматически, если не задан явно.

[#vendor_specific_application_id]
==== Формат Vendor-Specific-Application-Id

Поле Vendor-Specific-Application-Id может имеет эначения формата

----
{
  Vendor-Id = "<vendor_id>";
  Auth-Application-Id = "<auth_application_id>";
}
----

или

----
{
  Vendor-Id = "<vendor_id>";
  Acct-Application-Id = "<acct_application_id>";
}
----

.Пример
[source,console]
----
[SpecificLocalPeerCapabilities]
Sg.DIAM.PCSM.2 = {
  Origin-Realm = "protei.iot1.com";
  Host-IP-Address = { "192.168.108.36"; };
  Auth-Application-Id = { 16777217; };
  Vendor-Specific-Application-Id = {
    {
      Vendor-Id = 10415;
      Auth-Application-Id = 16777217;
    };
  }
  Supported-Vendor-Id = { 10415; };
  Inband-Security-Id = { 0; };
};
Sg.DIAM.PCSM.4 = {
  Origin-Realm = "protei.ru";
  Host-IP-Address = { "192.168.112.165"; };
  Auth-Application-Id = { 16777216; };
  Vendor-Specific-Application-Id = {
    {
      Vendor-Id = 10415;
      Auth-Application-Id = 16777216;
    };
  }
  Supported-Vendor-Id = { 10415; };
  Inband-Security-Id = { 0; };
};
----