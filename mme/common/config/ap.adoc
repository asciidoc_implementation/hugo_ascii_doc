---
title: ap.cfg
description: Параметры подсистемы сбора аварий
type: docs
weight: 20
---
= ap.cfg
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

В файле задаются настройки подсистемы аварийной индикации, Alarm Processor, SNMP-соединений и правил преобразования компонентных адресов в SNMP-адреса.

CAUTION: Крайне не рекомендуется менять параметры в этом файле.

.Используемые секции
[horizontal]
<<general,General>>:: основные параметры
<<dynamic,Dynamic>>:: переменные и их значения, при которых удаляются динамические объекты
<<snmp,SNMP>>:: параметры SNMP
<<standardmib,StandardMib>>:: параметры объектов из MIB-файла
<<atepath2objname,AtePath2ObjName>>:: правила преобразования АТЕ-пути в SNMP-путь
<<snmptrap,SNMPTrap>>:: правила отправки траповых переменных
<<filter,Filter>>:: параметры фильтрации аварий
<<st_ca_object,SpecificTrapCA_Object>>:: параметры базы SpecificTrap для компонентных адресов
<<st_ct_object,SpecificTrapCT_Object>>:: параметры базы SpecificTrap для компонентных типов
<<st_ca_var,SpecificTrapCA_Var>>:: параметры смещения SpecificTrap
<<logs,Logs>>:: параметры ведения лог-файла
<<filterlevel,FilterLevel>>:: правила фильтрации аварий по журналам
<<snmpv3,SNMPv3>>:: настройки SNMPv3

.Описание параметров
[options="header",cols="1,4,4,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[general]][General]*
| Основные параметры.
|
|
|
|

| Root
| Корень дерева. +
По умолчанию: PROTEI(1.3.6.1.4.1.20873).
| string
| O
| P
|

| [[application_address]]ApplicationAddress
| Адрес приложения. +
По умолчанию: <application_name>.
| string
| M
| R
|

| [[max_connection_count]]MaxConnectionCount
| Максимальное количество одновременных подключений. +
По умолчанию: 10.
| int
| O
| R
|

| ManagerThread
| Флаг запуска встроенного менеджера в отдельном потоке. +
По умолчанию: 0.
| bool
| O
| P
|

| [[cyclic_walk_tree]]CyclicWalkTree
| Флаг циклического обхода деревьев. +
По умолчанию: 0.
| bool
| O
| R
|

| *[[dynamic]][Dynamic]*
| Переменные и их значения, при которых динамические объекты следует удалять.
|
|
|
|

| caVar
| Компонентный адрес переменной.
| string
| O
| P
|

| value
| Значение переменной.
| string
| O
| P
|

| *[[snmp]][SNMP]*
| Настройки SNMP.
| object
| O
| P
|

| ListenIP
| IP-адрес, с которым будет устанавливать соединение система обработки сообщений AlarmProcessor. +
По умолчанию: 0.0.0.0.
| ip
| O
| P
|

| ListenPort
| Прослушиваемый порт. +
Диапазон: 0-65535. +
По умолчанию: 161.
| int
| O
| P
|

| OwnEnterprise
| SNMP-адрес приложения. +
По умолчанию: 1.3.6.1.4.1.20873.
| string
| O
| P
|

| [[check_community]]CheckCommunity
| Флаг проверки сообщества. +
По умолчанию: 0.
| bool
| O
| P
|

| *[[standardmib]][StandardMib]*
| Параметры объектов из стандартного MIB-файла.
| object
| O
| R
|

| addrSNMP
| Адрес SNMP переменной.
| string
| O
| R
|

| typeVar
| Тип переменной.
| string
| O
| R
|

| value
| Значение переменной.
| string
| O
| R
|

| *[[atepath2objname]][AtePath2ObjName]*
| Правила преобразования АТЕ-пути в SNMP-путь.
| object
| O
| P
|

| ctObject
| Компонентный тип объекта.
| regex
| O
| P
|

| caVar
| Компонентный адрес переменной.
| string
| O
| P
|

| *[[snmptrap]][SNMPTrap]*
| Правила посылки трапов.
| object
| O
| R
|

| ipManagerSNMP
| IP-адрес SNMP-менеджера.
| ip
| O
| R
|

| portManagerSNMP
| Порт SNMP-менеджера. +
Диапазон: 0-65535.
| int
| O
| R
|

| caObjectFilter
| Фильтр по адресу объекта.
| regex
| O
| R
|

| ctObjectFilter
| Фильтр по типу объекта.
| regex
| O
| R
|

| caVarFilter
| Фильтр по адресу переменной.
| regex
| O
| R
|

| *[[filter]][Filter]*
| Параметры фильтрации аварий.
| object
| O
| P
|

| caObject
| Фильтр по адресу объекта. +
По умолчанию: *_.*_*.
| regex
| O
| P
|

| ctObject
| Фильтр по типу объекта. +
По умолчанию: *_.*_*.
| regex
| O
| P
|

| caVar
| Фильтр по адресу переменной. +
По умолчанию: *_.*_*.
| regex
| O
| P
|

| TrapIndicator
| Фильтр по индикатору трапа. +
По умолчанию: 1.
| string
| O
| P
|

| DynamicIndicator
| Фильтр по индикатору динамического объекта. +
По умолчанию: 0.
| string
| O
| P
|

| *[[st_ca_object]][SpecificTrapCA_Object]*
| Параметры базы SpecificTrap для компонентных адресов.
| object
| O
| R
|

| caVar
| Компонентный адрес переменной.
| regex
| O
| R
|

| specificTrapBase
| Значение для задания базы SP.
| int
| O
| R
|

| *[[st_ct_object]][SpecificTrapCT_Object]*
| Параметры базы SpecificTrap для компонентных типов.
| object
| O
| R
|

| ctObject
| Компонентный тип объекта.
| string
| O
| R
|

| specificTrapBase
| Значение для задания базы SP.
| int
| O
| R
|

| *[[st_ca_var]][SpecificTrapCA_Var]*
| Параметры смещения SpecificTrap.
| object
| O
| R
|

| caObject
| Компонентный адрес объекта.
| regex
| O
| R
|

| specificTrapOffset
| Смещение для задания SP.
| int
| O
| R
|

| *[[logs]][Logs]*
| Параметры записи текущего состояния объектов в файл.
| object
| O
| R
|

| TreeTimerPeriod
| Период сохранения текущего состояния объектов в лог-файлах, в миллисекундах. +
По умолчанию: 60&nbsp;000.
| int
| O
| R
|

| *[[filterlevel]][FilterLevel]*
| Правила фильтрации аварий по журналам.
| list<object>
| O
| P
|

| caObject
| Компонентный адрес объекта.
| regex
| O
| P
|

| ctObject
| Компонентный тип объекта.
| regex
| O
| P
|

| caVar
| Компонентный адрес переменной.
| regex
| O
| P
|

| nLevel
| Уровень журнала.
| int
| O
| P
|

| *[[snmpv3]][SNMPv3]*
| Настройки SNMPv3 для корректного формирования <<snmp_engine_id,SNMP_EngineID>> и <<context_engine_id,ContextEngineID>>. См. <<https://datatracker.ietf.org/doc/html/rfc1910,RFC 1910>>, <<https://datatracker.ietf.org/doc/html/rfc3411,RFC 3411>>.
| object
| O
| P
|

| [[engine_id_conformance]]EngineIdConformance
| Флаг поддержки SNMPv3. +
По умолчанию: 1.
| bool
| O
| P
|

| [[engine_id_enterprise]]EngineIdEnterprise
| Идентификатор организации. +
По умолчанию: 20873.
| string
| O
| P
|

| <<engine_id_format,EngineIdFormat>>
| Индикатор форматирования 6 октета и далее, является 5 октетом. См. https://datatracker.ietf.org/doc/html/rfc3411[RFC 3411]. +
По умолчанию: 4.
| int
| O
| P
|

| MaxMessageSize
| Максимальный размер обрабатываемого сообщения в октетах. +
NOTE: Используется только для SNMPv3. +
По умолчанию: 484.
| int
| O
| P
|

| EngineBoots
| Количество загрузок модуля с момента последнего конфигурирования. +
NOTE: Используется только для SNMPv3. +
По умолчанию: 0.
| int
| O
| P
|

| TimeWindow
| Интервал обновления. +
По умолчанию: 0.
| int
| O
| P
|

| Community
| Имя сообщества. +
NOTE: Используется для SNMPv1 и SNMPv2, если <<check_community,CheckCommunity>> = 1. +
По умолчанию: public.
| string
| O
| P
|

| [[snmp_engine_id]]SNMP_EngineID
| Идентификатор движка SNMP. +
Правила генерации см. <<snmp_engine_id_section,ниже>>. +
По умолчанию: protei.
| string
| O
| P
|

| [[context_engine_id]]ContextEngineID
| Идентификатор реализации контекста SNMP. +
По умолчанию: значение <<snmp_engine_id,SNMP_EngineID>>.
| string
| O
| P
|

| ContextName
| Имя контекста SNMP. +
По умолчанию: protei.
| string
| O
| P
|

| *[[agents_v3]]Agents_v3*
| Перечень адресов для отправки трапов по протоколу SNMPv3.
| list<object>
| O
| P
|

| addr
| IP-адрес назначения. Формат: +
*_{ "<addr>";"<port>" }_*. +
По умолчанию: 0.0.0.0.
| ip
| O
| P
|

| Port
| Порт назначения. +
Диапазон: 0-65535.
| int
| O
| P
|

| User
| Имя пользователя. +
По умолчанию: protei.
| string
| O
| P
|

| Protocol
| Используемый транспортный протокол. +
*_TCP_*/*_UDP_*. +
По умолчанию: TCP.
| string
| O
| P
|

| *[[agents_v2c]]Agents_v2c*
| Перечень адресов для отправки трапов по протоколу SNMPv2c.
| list<object>
| O
| P
|

| Address
| IP-адрес назначения. +
По умолчанию: 0.0.0.0.
| ip
| O
| P
|

| Port
| Порт назначения. +
Диапазон: 0-65535.
| int
| O
| P
|

| User
| Имя пользователя. +
NOTE: Поле не используется, значение может быть любым.
| string
| O
| P
|

| Protocol
| Используемый транспортный протокол. +
*_TCP_*/*_UDP_*. +
По умолчанию: TCP.
| string
| O
| P
|

| *[[users]]Users*
| Перечень пользователей для протокола SNMPv3.
| list<object>
| O
| P
|

| Name
| Имя пользователя. +
NOTE: Имя должно совпадать с одним из значений в из <<agents_v3,Agents_v3>>. +
По умолчанию: protei.
| string
| O
| P
|

| AuthProtocol
| Протокол авторизации. +
NOTE: Допускается только значение none, авторизация не используется. +
По умолчанию: none.
| string
| O
| P
|

| PrivProtocol
| Протокол шифрования. +
NOTE: Допускается только значение none, шифрование не используется. +
По умолчанию: none.
| string
| O
| P
|
|===

[#engine_id_format]
.Значения EngineIdFormat
[horizontal]
0:: reserved, unused; зарезервированы
1:: IPv4 address (4 octets), lowest non-special IP address; IP-адрес формата IPv4, 4 октета, наименьший не особый
IP-адрес
2:: IPv6 address (16 octets), lowest non-special IP address; IP-адрес формата IPv6, 16 октетов, наименьший не особый
IP-адрес
3:: MAC address (6 octets), lowest IEEE MAC address, canonical order; MAC-адрес, 6 октетов, наименьший IEEE MAC-адрес;
4:: Text, administratively assigned, maximum remaining length 27; текст, максимальный размер оставшейся части -- 27;
5:: Octets, administratively assigned, maximum remaining length 27; октеты, максимальный размер оставшейся части --
27;
6-127:: reserved, unused; зарезервированы;
128-255:: as defined by the enterprise, maximum remaining length 27; определяется организацией, максимальный размер
оставшейся части -- 27;

[#snmp_engine_id_section]
== Правила генерации snmpEngineID

Значение идентификатора snmpEngineID зависит от значения <<engine_id_conformance,EngineIdConformance>>.

* при *_EngineIdConformance = 0_*:

*_SNMP_EngineID = 80 00 + 51 89 + <EngineID>_*

80 00:: значение *_EngineIdConformance = 1_*
51 89:: значение *_EngineIdEnterprise_* (20873 в формате hex)

NOTE: Длина SNMP_EngineID корректируется строго до 12 байт.

* при *_EngineIdConformance = 1_*:

*_SNMP_EngineID = 00 00 + 51 89 + 04 + <EngineID>_*

00 00:: значение *_EngineIdConformance = 1_*
51 89:: значение *_EngineIdEnterprise_* (20873 в формате hex)
04:: значение *_EngineIdFormat_*

=== Форматы

* формат <<dynamic,[Dynamic]>>:

*_{ <caVar>;<value>; };_*

* формат <<standardmib,[StandardMib]>>:

*_{ <addrSNMP>;<typeVar>;<value>; };_*

* формат <<atepath2objname,[AtePath2ObjName]>>:

*_{ <ctObject>;<caVar>; }_*;

Для каждого типа необходимо прописать переменную CA(1) -- адрес объекта, иначе объекты в SNMP-дерево добавляться не будут.

* формат <<snmptrap,[SNMPTrap]>>:

*_{ <ipManagerSNMP>;<portManagerSNMP>;<caObjectFilter>;<ctObjectFilter>;<caVarFilter>; };_*

* формат <<st_ca_object,[SpecificTrapCA_Object]>>:

*_{ <caVar>;<specificTrapOffset>; }_*

* формат <<st_ct_object,[SpecificTrapCT_Object]>>:

*_{ <ctObject>;<specificTrapBase>; }_*

* формат <<st_ca_var,[SpecificTrapCA_Var]>>:

*_{ <caObject>;<specificTrapOffset>; }_*

* формат <<filterlevel,[FilterLevel]>>:

*_{ <caObject>;<ctObject>;<caVar>;<nLevel> }_*

* формат <<agents_v3,Agents_v3>>:

*_{ <address>;<port>;<user>;<protocol> };_*

* формат <<agents_v2c,Agents_v2c>>:

*_{ <address>;<port>;<user>;<protocol> };_*

== Правила вычисления идентификатора трапа (SpecificTrap)

. Значение, заданное для *_caObject_* или *_ctObject_*, является базой (*_base_*) для формирования идентификатора. +
Если указан *_caObject_*, то *_ctObject_* игнорируется. Если подходящий *_caObject_* не найден, за основу берется *_ctObject_*. +
Если не найден ни *_caObject_*, ни *_ctObject_*, то *_base = 0_*.
. Полученное значение умножается на 1000.
. Значение, заданное для *_caVar_* и являющееся смещением (*_offset_*), прибавляется к базе.

NOTE: Идентификаторы для *_caObject_* и *_ctObject_* должны быть больше 0.

NOTE: Идентификаторы для *_caVar_* должны быть больше 100.

NOTE: Идентификаторы для *_caVar_* в диапазоне 1-100 зарезервированы.

[options="header",cols="4,4,1"]
|===
| Переменная | Значение | SP_Offset

| OSTATE
| ACTIVATE
| 1

| OSTATE
| FAIL
| 2

| ASTATE
| UNBLOCKED
| 3

| ASTATE
| BLOCKED
| 4

| HSTATE
| ON
| 5

| HSTATE
| OFF
| 6
|===

.Пример
[source,ini]
----
[Dynamic]
{ "OSTATE";"0"; };

[StandardMib]
#sysDescr
{ 1.3.6.1.2.1.1.1.0;STRING;"Protei_CPE"; };
#sysObjectID
{ 1.3.6.1.2.1.1.2.0;OBJECT_ID;1.3.6.1.4.1.501; };
#get-request SNMPv3 for EngineID
{ "1.3.6.1.6.3.15.1.1.4.0";"COUNTER";"2335"; };

[AtePath2ObjName]
{ Ph(2).Trunk(1,1);CA(1); };
{ Ph(2).Trunk(1,1);ASTATE(2); };
{ Ph(2).Trunk(1,1);OSTATE(3); };
{ Ph(2).Trunk(1,1);Alrm(4).LOS(1).EPH(1); };
{ Ph(2).Trunk(1,1);Alrm(4).LOS(1).LASTDT(2); };
----

После АТЕ-названия в скобках через запятую указывается последовательность цифр, которой соответствует это название в SNMP-пути.

[source,ini]
----
[SNMPTrap]
{ "192.168.100.151";162;"Ph.*"; };
{ "192.168.100.154";162;"Sg.TPI.CDIR"; };

[SpecificTrapCA_Object]
{ Sg.SMPP.Dir.1; 1; }
{ Sg.SMPP.Dir.*; 2; }

[SpecificTrapCT_Object]
{ Sg.SMPP.Dir; 1; }
{ SMSC.GENERAL; 3; }

[SpecificTrapCA_Var]
{ STAT.*; 101; }
{ RESULT.*; 102; }

[FilterLevel]
{ Ph.Card.1; Ph.Card; OSTATE; 4 };
{ .*; .*; OSTATE; 5 };
----

Для переменной *_OSTATE_* в объекте *_Ph.Card.1_* типа *_Ph.Card_* задать уровень 4.
Для переменной *_OSTATE_* во всех объектах задать уровень 5.

[source,ini]
----
[SNMPv3]
EngineIdConformance = 1;
EngineIdEnterprise = 20873;
EngineIdFormat = 4;
MaxMessageSize = 484;
EngineBoots = 21;
SNMP_EngineID = "protei";
ContextEngineID = "protei";
ContextName = "protei";
Community = "bookmark";

Agents_v3 = {
  { "0.0.0.0";3162;"protei";"UDP" };
}

Users = {
  {
    Name = "protei";
    AuthProtocol = "none";
    PrivProtocol = "none";
  }
}
----

== Перегрузка параметров

Параметры конфигурации делятся на две группы:

. Параметры AP_Agent. Формат перегрузки:

[source,bash]
----
$ ./reload ap_agent.di
$ ./reload ap_agent.di "config/alarm/ap.cfg"
----

Если параметры не указаны, путь к ap.cfg по умолчанию: *_config/ap.cfg_*.

[start=2]
. Параметры AP_Manager. Формат перегрузки:

[source,bash]
----
$ ./reload ap_manager.di
$ ./reload ap_manager.di "config/alarm/ap.cfg"
----

Перегрузить можно только приведенный ниже список параметров/секций:

. AP_Agent:

* <<general,[General]>>:
 ** <<application_address,ApplicationAddress>>;
 ** <<max_connection_count,MaxConnectionCount>>;
* <<logs,[Logs]>>.

. AP_Manager:

* <<general,[General]>>:
 ** <<cyclic_walk_tree,CyclicWalkTree>>;
* <<standardmib,[StandardMib]>>;
* <<snmptrap,[SNMPTrap]>>;
* <<st_ca_object,[SpecificTrapCA_Object]>>;
* <<st_ct_object,[SpecificTrapCT_Object]>>;
* <<st_ca_var,[SpecificTrapCA_Var]>>.