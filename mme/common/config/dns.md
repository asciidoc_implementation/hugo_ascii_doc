---
title: "dns.cfg"
description: "Параметры DNS"
weight: 20
type: docs
---

В файле задаются настройки DNS.

**Примечание.** Наличие файла обязательно.

### Используемые секции ###

* [\[Client\]](#client) - параметры клиентской части;
* [\[Server\]](#server) - параметры серверной части;
* [\[ClientPool\]](#client-pool) - параметры пула клиентов.

### Описание параметров ###

| Параметр                                     | Описание                                                                                                                                                        | Тип     | O/M | P/R | Version |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|-----|-----|---------|
| **<a name="client">\[Client\]</a>**          | Параметры клиентской части.                                                                                                                                     | object  | M   | P   |         |
| ID                                           | Идентификатор направления.<br>Должно быть 0.                                                                                                                    | int     | M   | P   |         |
| Transport                                    | Транспортный протокол.<br>`UDP` / `TCP`.<br>По умолчанию: UDP.                                                                                                  | string  | O   | P   |         |
| DestAddress                                  | IP-адрес сервера, куда направляются запросы.                                                                                                                    | ip      | M   | P   |         |
| DestPort                                     | Прослушиваемый порт сервера.<br>По умолчанию: 53.                                                                                                               | int     | O   | P   |         |
| SrcAddress                                   | Локальный IP-адрес.<br>По умолчанию: 0.0.0.0.                                                                                                                   | ip      | O   | P   |         |
| SrcPort                                      | Локальный порт.<br>По умолчанию: 0.                                                                                                                             | int     | O   | P   |         |
| ResponseTimeout                              | Время ожидания ответа, в секундах.<br>По умолчанию: 60.                                                                                                         | int     | O   | P   |         |
| ResendOverTcp                                | Флаг перепосылки запроса по TCP для получения полного ответа.<br>По умолчанию: 0.                                                                               | bool    | O   | P   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>.<br>По умолчанию: 0.           | int     | O   | P   | 1.0.1.1 |
| IP_MTU_Discover                              | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: 0. | int     | O   | P   | 1.0.2.9 |
| **<a name="server">\[Server\]</a>**          | Параметры серверной части.                                                                                                                                      | object  | M   | P   |         |
| ID                                           | Идентификатор направления.                                                                                                                                      | int     | M   | P   |         |
| Transport                                    | Транспортный протокол.<br>UDP/TCP.<br>По умолчанию: UDP.                                                                                                        | string  | O   | P   |         |
| Address                                      | Прослушиваемый IP-адрес.<br>По умолчанию: 0.0.0.0.                                                                                                              | ip      | O   | P   |         |
| Port                                         | Прослушиваемый порт.<br>По умолчанию: 53.                                                                                                                       | int     | O   | P   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>.<br>По умолчанию: 0.           | int     | O   | P   | 1.0.1.1 |
| IP_MTU_Discover                              | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: 0. | int     | O   | P   | 1.0.2.9 |
| **<a name="client-pool">\[ClientPool\]</a>** | Параметры пула клиентов.                                                                                                                                        | object  | O   | P   | 1.0.2.0 |
| ID                                           | Идентификатор пула клиентских направлений.                                                                                                                      | int     | M   | P   | 1.0.2.0 |
| Directions                                   | Перечень клиентских направлений, задействованных в балансировке.                                                                                                | \[int\] | M   | P   | 1.0.2.0 |

**Примечание.** Если секция [ClientPool](#client-pool) пуста, по умолчанию создается пул с `ID = 0`, содержащий все сконфигурированные клиентские направления.

#### Пример ####

```
[Client]
{
  ID = 1;
  Transport = TCP;
  DestAddress = 8.8.8.8;
  ResendOverTcp = 1;
  ResponseTimeout = 1000;
}
{
  ID = 2;
  DestAddress = 127.0.0.1;
  DestPort = 1234;
  ResponseTimeout = 10;
}
{
  ID = 3;
  DestAddress = 127.0.0.1;
  DestPort = 53;
  dscp = 56;
}

[Server]
{
  ID = 1;
  Address = 127.0.0.1;
  Port = 1234;
}
{
  ID = 2;
}

[ClientPool]
{
  ID = 1;
  Directions = { 2; 3; };
}
```
