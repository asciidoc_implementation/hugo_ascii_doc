---
title : "diameter.cfg"
description: "Файл настройки компоненты Diameter"
weight: 20
type: docs
---

В файле задаются настройки компонента Diameter.

### Описание секции Params компоненты DIAM

| Name         | Description               | O/M |
|--------------|---------------------------|-----|
| PeerTable    | Таблица хостов.           |  M  |
| RoutingTable | Таблица realm.            |  O  |
| DefaultPCSM  | Список PCSM по умолчанию. |  O  |

### Описание секции PeerTable компоненты DIAM

| Параметр          | Описание                                                                  | Тип           | O/M | P/R | Версия   |
|-------------------|---------------------------------------------------------------------------|---------------|-----|-----|----------|
| Host-Identity     | Идентификатор хоста.                                                      | string        | M   | P   |          |
| PCSM              | Компонентный адрес PCSM.                                                  | string        | M   | P   |          |
| PeerIP            | IP адрес хоста.<br>Рекомендуется использовать параметр remote_interfaces. | ip            | O   | P   |          |
| remote_interfaces | Адрес хоста.                                                              | string/object | O   | P   | 4.1.11.2 |
| Weight            | Вес PCSM.<br>По умолчанию: 1.                                             | int           | O   | P   | 4.1.10.0 |

* Можно задавать несколько секций в PeerTable с одинаковым Host-Identity, поскольку
имеется дополнительный параметр PeerIP, связывающий адрес подключающегося клиента с нужным PCSM.
* Начиная с версии 4.1.11.2, добавлен параметр remote_interfaces, позволяющий указать несколько адресов (ip:port) для связывания с PCSM;
* Вес используется для распределения нагрузки между PCSM с одинаковым Host-Identity.
При отправке сообщения по хосту каждый PCSM с подходящим Host-Identity может быть выбран с вероятностью `Weight / TotalWeight`,
где **TotalWeight** — сумма весов всех PCSM с данным Host-Identity.

#### Пример

```
{
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      { 
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.1";
        PeerIP = "192.168.115.231";
      };
      { 
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.2";
        remote_interfaces = "192.168.115.232:3868";
      };
      { 
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.3";
        remote_interfaces = { "192.168.115.233";"192.168.115.234" };
      };
      { 
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.4";
        remote_interfaces = ":3869";
      };
      { 
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.5";
      };
    };
  };
}
```

* На PCSM.1 принимаются подключения с IP-адреса **192.168.115.231**, с любого порта;
* На PCSM.2 принимаются подключения с IP-адреса **192.168.115.232**, с порта **3868**;
* На PCSM.3 принимаются подключения с IP-адресов **192.168.115.233** и **192.168.115.234**, с любого порта;
* На PCSM.4 принимаются подключения с любого IP-адреса, с порта **3869**;
* На PCSM.5 принимаются все подключения.

#### Описание секции RoutingTable компоненты DIAM

| Параметр | Описание                             | Тип                   | O/M | P/R | Версия |
|----------|--------------------------------------|-----------------------|-----|-----|--------|
| Realm    | Идентификатор направления.           | string/regex          | M   | P   |        |
| Route    | Перечень хостов.                     | list\<string/object\> | M   | P   |        |
| AltRoute | Идентификатор альтернативного хоста. | string                | O   | P   |        |

До версии 4.1.11.0 список хостов (Route) может быть задан только в виде списка строк.

В версии 4.1.11.0 добавлена возможность указать вес и приоритет хоста.

| Параметр | Описание                             | Тип    | O/M | P/R | Версия   |
|----------|--------------------------------------|--------|-----|-----|----------|
| Peer     | Идентификатор хоста.                 | string | M   | P   | 4.1.11.0 |
| Weight   | Вес хоста.<br>По умолчанию: 1.       | int    | O   | P   | 4.1.11.0 |
| Priority | Приоритет хоста.<br>По умолчанию: 1. | int    | O   | P   | 4.1.11.0 |

* Чем меньше значение **Priority**, тем более приоритетным является хост.
Сначала маршрут выбирается из хостов с наивысшим приоритетом.
При их недоступности выбор производится из хостов со следующим приоритетом и т.д.
* **Weight** используется для распределения нагрузки между хостами с одинаковым приоритетом.
Каждый хост может быть выбран с вероятностью `Weight / TotalWeight`,
где **TotalWeight** - сумма весов всех хостов с данным приоритетом.

#### Пример

```
RoutingTable = {
  {
    Realm = "example1.realm";
    Route = {
      "host1";
      "host2";
      "host3";
    };
    AltRoute = "alt.host";
  };
}
```

До версии 4.1.11.0 поддерживается только такой формат.
Нагрузка равномерно распределяется между тремя хостами.
В случае их недоступности используется `AltRoute`.

```
RoutingTable = {
  {
    Realm = "example2.realm";
    Route = {
      {
        Peer = "host1";
        Weight = 2;
      };
      {
        Peer = "host2";
        Weight = 3;
      };
    };
  };
}
```

**Примечание.** Распределение нагрузки между `host1` и `host2` в соотношении 2:3.

```
RoutingTable = {
  {
    Realm = "example3.realm";
    Route = {
      {
        Peer = "host1";
        Priority = 1;
      };
      {
        Peer = "host2";
        Priority = 2;
      };
    };
  };
}
```

**Примечание.** Отправка сообщений на `host2` выполняется только в случае недоступности `host1`.

```
RoutingTable = {
  {
    Realm = "example4.realm";
    Route = {
      {
        Peer = "host1";
        Priority = 1;
      };
      {
        Peer = "host2";
        Priority = 2;
      };
    };
    AltRoute = "alt.host"
  };
};
```

**Примечание.** `AltRoute` оставлен для обратной совместимости и является наименее приоритетным хостом: 

`Priority = INT_MAX = 2147483647`

#### Описание секции DefaultPCSM компоненты DIAM


| Параметр | Описание                             | Тип    | O/M | P/R | Версия   |
|----------|--------------------------------------|--------|-----|-----|----------|
| PCSM     | Компонентный адрес PCSM.             | string | M   | P   | 4.1.12.0 |
| Weight   | Вес хоста.<br>По умолчанию: 1.       | int    | O   | P   | 4.1.12.0 |
| Priority | Приоритет хоста.<br>По умолчанию: 1. | int    | O   | P   | 4.1.12.0 |

* Чем меньше значение **Priority**, тем более приоритетным является PCSM.
  Сначала маршрут выбирается из PCSM с наивысшим приоритетом.
  При их недоступности выбор производится из хостов со следующимм приоритетом и т.д.
* **Weight** используется для распределения нагрузки между хостами с одинаковым приоритетом.
  Каждый хост может быть выбран с вероятностью `Weight / TotalWeight`,
  где **TotalWeight** — сумма весов всех хостов с данным приоритетом.

#### Пример для версий до 4.1.12.0

```
DefaultPCSM = {
  "Sg.DIAM.PCSM.0";
  "Sg.DIAM.PCSM.1";
};
```

**Примечание.** До версии 4.1.12.0 поддерживается только такой формат.

#### Пример для версий после 4.1.12.0

```
DefaultPCSM = {
  { PCSM = "Sg.DIAM.PCSM.0"; Priority = 1; Weight = 2 }
  { PCSM = "Sg.DIAM.PCSM.1"; Priority = 1; Weight = 3 }
  { PCSM = "Sg.DIAM.PCSM.0"; Priority = 2; }
  { PCSM = "Sg.DIAM.PCSM.1"; Priority = 2; }
};
```

**Примечание.** Начиная с версии 4.1.12.0, допускается указывать веса и приоритеты.<br>
Используемый ранее формат поддерживается.

#### Описание секции Params компоненты DIAM.PCSM

| Параметр                                                    | Описание                                                                                                                                                                                                                                                                                                                                                                           | Применимость            | Тип         | O/M | P/R | Версия   |
|-------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|-------------|-----|-----|----------|
| PeerIP                                                      | Адрес сервера.<br>По умолчанию: 0.0.0.0.                                                                                                                                                                                                                                                                                                                                           | tcp/sctp, client        | ip          | M   | P   |          |
| PeerPort                                                    | Порт сервера.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                                                                  | tcp/sctp, client        | int         | M   | P   |          |
| SrcIP                                                       | Локальный адрес.                                                                                                                                                                                                                                                                                                                                                                   | tcp/sctp, client        | ip          | O   | P   |          |
| SrcPort                                                     | Локальный порт.                                                                                                                                                                                                                                                                                                                                                                    | tcp/sctp, client        | int         | O   | P   |          |
| Origin-State                                                | Идентификатор состояния, `Origin-State`.                                                                                                                                                                                                                                                                                                                                           | tcp/sctp, client/server | int         | O   | P   |          |
| Origin-Host                                                 | Идентификатор хоста, `Origin-Host`.<br>По умолчанию: значение [diameter.cfg::\[LocalPeerCapabilities\]::Origin-Host](../../diameter/#origin-host-diam/).                                                                                                                                                                                                                           | tcp/sctp, client/server | string      | O   | P   |          |
| Origin-Realm                                                | Realm хоста, `Origin-Realm`.<br>По умолчанию: значение [diameter.cfg::\[LocalPeerCapabilities\]::Origin-Realm](../../diameter/#origin-realm-diam/).                                                                                                                                                                                                                                | tcp/sctp, client/server | string      | O   | P   |          |
| Transport                                                   | Протокол транспортного уровня.<br>`tcp` / `sctp`. По умолчанию: tcp.                                                                                                                                                                                                                                                                                                               | tcp/sctp, client        | string      | O   | P   |          |
| InStreams                                                   | Количество входящих потоков.<br>По умолчанию: 2.                                                                                                                                                                                                                                                                                                                                   | sctp, client            | int         | O   | P   |          |
| OutStreams                                                  | Количество исходящих потоков.<br>По умолчанию: 2.                                                                                                                                                                                                                                                                                                                                  | sctp, client            | int         | O   | P   |          |
| MaxInitRetransmits                                          | Максимальное количество попыток отправить сообщение INIT/COOKIE ECHO.                                                                                                                                                                                                                                                                                                              | sctp, client            | int         | O   | P   |          |
| InitTimeout                                                 | Время ожидания сообщения INIT.                                                                                                                                                                                                                                                                                                                                                     | sctp, client            | int         | O   | P   |          |
| RtoMax                                                      | Максимальное значение RTO.                                                                                                                                                                                                                                                                                                                                                         | sctp, client            | int         | O   | P   |          |
| RtoMin                                                      | Минимальное значение RTO.                                                                                                                                                                                                                                                                                                                                                          | sctp, client            | int         | O   | P   |          |
| RtoInitial                                                  | Начальное значение RTO.                                                                                                                                                                                                                                                                                                                                                            | sctp, client            | int         | O   | P   |          |
| HbInterval                                                  | Периодичность отправки сигнала heartbeat.                                                                                                                                                                                                                                                                                                                                          | sctp, client            | int         | O   | P   |          |
| dscp                                                        | Значение поля заголовка IP DSCP/ToS.                                                                                                                                                                                                                                                                                                                                               | tcp/sctp, client        | int         | O   | P   | 4.1.8.55 |
| AssociationMaxRetrans                                       | Максимальное количество повторных отправок, при превышении которого маршрут считается недоступным.                                                                                                                                                                                                                                                                                 | sctp, client            | int         | O   | P   |          |
| SackDelay                                                   | Время ожидания отправки сообщения SACK.                                                                                                                                                                                                                                                                                                                                            | sctp, client            | int         | O   | P   |          |
| SndBuf                                                      | Размер буфера сокета для отправки, параметр `net.core.wmem_default` Linux Kernel.<br>**Внимание.** Значение удваивается. Удвоенный размер не может превышать значение `net.core.wmem_max`.                                                                                                                                                                                         | sctp, client            | int         | O   | P   |          |
| ShutdownEvent                                               | Флаг включения индикации о событии `SHUTDOWN` от ядра.                                                                                                                                                                                                                                                                                                                             | sctp, client            | bool        | O   | P   |          |
| AssocChangeEvent                                            | Флаг включения индикации об изменении состояния ассоциации от ядра.                                                                                                                                                                                                                                                                                                                | sctp, client            | bool        | O   | P   |          |
| PeerAddrChangeEvent                                         | Флаг включения индикации об изменении состояния peer в ассоциации от ядра.                                                                                                                                                                                                                                                                                                         | sctp, client            | bool        | O   | P   |          |
| local_interfaces                                            | Перечень локальных интерфейсов. Дополнительную информацию по конфигурированию см. [LocalAddress](#localandremoteaddress). Формат:<br>`{ <ip:port>; <ip:port>; }`                                                                                                                                                                                                                   | sctp, client            | \[ip:port\] | O   | P   |          |
| remote_interfaces                                           | Перечень удаленных интерфейсов. Дополнительную информацию по конфигурированию см. [RemoteAddress](#localandremoteaddress). Формат:<br>`{ <ip:port>; <ip:port>; }`                                                                                                                                                                                                                  | sctp, client            | \[ip:port\] | O   | P   |          |
| Appl_Timeout                                                | Максимальное время ожидания установления Diameter-соединения.<br>**Примечание.** Отсчитывается с момента отправки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Appl_Timeout](../../diameter/#appl-timeout/).                                                                   | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| Watchdog_Timeout                                            | Максимальное время ожидания сообщений Diameter: Device-Watchdog-Request/Answer.<br>**Примечание.** Учитывается время прошедшее с момента посылки последнего сообщения, не обязательно Diameter: DWR.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Watchdog_Timeout](../../diameter/#watchdog-timeout/).                                                                    | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| Reconnect_Timeout                                           | Максимальное время ожидания переустановления соединения.<br>**Примечание.** Учитывается время от разрушения соединения до очередной попытки восстановления.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Reconnect_Timeout](../../diameter/#reconnect-timeout/).                                                                                                           | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| OnBusyReconnect_Timeout                                     | Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = BUSY`.<br>**Примечание.** Если 0, то соединение не переустанавливается.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::OnBusyReconnect_Timeout](../../diameter/#on-busy-reconnect-timeout/).                               | tcp/sctp, client/server | int         | O   | P   | 4.1.8.43 |
| OnShutdownReconnect_Timeout                                 | Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU`.<br>**Примечание.** Если 0, то соединение не переустанавливается.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::OnShutdownReconnect_Timeout](../../diameter/#on-shutdown-reconnect-timeout/). | tcp/sctp, client/server | int         | O   | P   | 4.1.8.43 |
| Response_Timeout                                            | Максимальное время ожидания ответа.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Response_Timeout](../../diameter/#response-timeout/).                                                                                                                                                                                                                                     | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| Breakdown_Timeout                                           | Продолжительность временной недоступности PCSM.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Breakdown_Timeout](../../diameter/#breakdown-timeout/).                                                                                                                                                                                                                       | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| Statistic_Timeout                                           | Периодичность записи статистики в лог-файлы.<br>По умолчанию: значение [diameter.cfg::\[Timers\]::Statistic_Timeout](../../diameter/#statistic-timeout/).                                                                                                                                                                                                                          | tcp/sctp, client/server | int         | O   | P   | 4.1.8.39 |
| <a name="TrafficManagerInterval">TrafficManagerInterval</a> | Период подсчета количества входящих и исходящих запросов.<br>По умолчанию: 1000.                                                                                                                                                                                                                                                                                                   | tcp/sctp, client/server | int         | O   | P   | 4.1.8.60 |
| MaxTransactions                                             | Максимальное количество запросов за период подсчета запросов, [TrafficManagerInterval](#TrafficManagerInterval).<br>**Примечание.** Если 0, то ограничение не проверяется.<br>На входящие запросы сверх лимита отправляется ответ с `Result-Code = TOO_BUSY (3002)`. На исходящие запросы сверх лимита в логику отправляется Pr_DIAM_SEND_DATA_REJ.<br>По умолчанию: 0.            | tcp/sctp, client/server | int         | O   | P   | 4.1.8.60 |

#### Конфигурация local и remote адресов {#localandremoteaddress}

Для серверных компонент IP-адрес, порт и `local_interfaces` используются соответствующие значения из файла [diameter.cfg](../../diameter/):
* [diameter.cfg::\[LocalAddress\]::LocalHost](../../diameter/#local_host/)
* [diameter.cfg::\[LocalAddress\]::LocalPort](../../diameter/#local_port/)
* [diameter.cfg::\[LocalAddress\]::local_interfaces](../../diameter/#local_interfaces/)

Параметры из этого файла для них игнорируются.

Компонента является клиентом, если:

* указан **PeerIP** для tcp;
* указан **PeerIP** или `remote_interfaces` для sctp.

Адреса клиентских компонент составляются следующим образом для TCP в порядке приоритетности:

* Для удаленных адресов, **RemoteAddr**:

  * ip - PeerIP;
  * port - PeerPort;

* Для локальных адресов, **LocalAddr**:

  * ip - SrcIP, [diameter.cfg::\[LocalAddress\]::LocalHost](../../diameter/#local_host/);
  * port - SrcPort;

Для клиентских SCTP-ассоциаций `remote_interfaces` и `local_interfaces` являются набором 1 основной + дополнительные
адреса.

Основной адрес для SCTP определяется следующим образом в порядке приоритетности:

* Для удаленных адресов, **RemoteAddr**:

  * ip - PeerIP, первый адрес в списке `remote_interfaces`;
  * port - PeerPort, первый порт в `remote_interfaces`;

* Для локальных адресов, **LocalAddr**:

  * ip - SrcIP, первый адрес в списке `local_interfaces`, [diameter.cfg::\[LocalAddress\]::LocalHost](../../diameter/#local_host/);
  * port - SrcPort, первый порт в `local_interfaces`;

#### Обязательность параметров клиентских компонент

| Параметр          | Клиент TCP      | Клиент SCTP                                               |
|-------------------|-----------------|-----------------------------------------------------------|
| PeerIP            | Обязательный    | Обязательный, если не указан `ip` в `remote_interfaces`   |
| PeerPort          | Обязательный    | Обязательный, если не указан `port` в `remote_interfaces` |
| SrcIP             | Опциональный    | Опциональный                                              |
| SrcPort           | Опциональный    | Опциональный                                              |
| local_interfaces  | Не используется | Опциональный                                              |
| remote_interfaces | Не используется | Обязательный, если не указан `PeerIP`/`PeerPort`          |

#### Команды по работе с компонентами PCSM

* Добавление новой компоненты:

```bash
$ echo "com_console_di
{
  Command = "Add";
  ComponentAddr = Sg.DIAM.PCSM.N;
  ComponentType = Sg.DIAM.PCSM;
  Params = {
    PeerIP = %s;
    PeerPort= %d;
    Transport=%s;
  };
}" > <path>/logs/reload.req
```

**Примечание.** Начиная с версии DiameterInterface 4.1.8.31, после успешного создания компоненты происходит ее разблокировка.

* Блокировка компоненты:

```bash
$ echo "com_console_di
{
  Command = "Block";
  ComponentAddr = Sg.DIAM.PCSM.3;
  Params = {}
}" > <path>/logs/reload.req
```

* Разблокировка компоненты:

```bash
$ echo "com_console_di
{
  Command = "Unblock";
  ComponentAddr = Sg.DIAM.PCSM.3;
  Params = {}
}" > <path>/logs/reload.req
```

**Внимание.** Компонента должна находиться в состоянии `CLOSED`.

После разблокировки:

* Административное состояние изменяется на `CM_UNBLOCK`;
* Компонента переходит в состояние `WAIT_CONN_ACK`;
* Клиентский PCSM пытается установить соединение.

**Примечание.** Разблокированный PCSM, работающий в серверном режиме, будет возвращать ответы с `ResultCode = 3010`, пока не будет добавлен в PeerTable.

* Модификация компоненты:

```bash
$ echo "com_console_di
{
    Command = "Mod";
    ComponentAddr = Sg.DIAM.PCSM.N;
    Params = {
        PeerIP = %s;
        PeerPort = %d;
        Transport=%s;
    };
}" > <path>/logs/reload.req
```

**Внимание.** Компонента должна находиться в состоянии `CM_UNBLOCK`.

* Перезагрузка компоненты:

```bash
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM.PCSM.N;
  Params = {};
}" > <path>/logs/reload.req
```

**Внимание.** Компонента должна находиться в состоянии `CM_UNBLOCK`.

* Закрытие соединения:

```bash
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM.PCSM.N;
  Params = {
    Action="Close";
  };
}" > <path>/logs/reload.req
```

**Внимание.** Компонента должна находиться в состоянии `CM_UNBLOCK`.

* Удаление компоненты:

```bash
echo "com_console_di
{
    Command = "Del";
    ComponentAddr = Sg.DIAM.PCSM.N;
    Params = {};
}" > <path>/logs/reload.req
```

**Внимание.** Соединение должно находиться в состоянии `CLOSED` или `WAIT_CONN_ACK`.

До версии Diameter 4.1.8.61 после удаления компоненты записи, связанные с ней, удаляются также DefaultPCSM из PeerTable.

#### Команды по работе с компонентами DIAM

* Если в таблице маршрутизации существует запись с заданным ключом, то запись модифицируется;
* Если в таблице маршрутизации отсутствует запись с заданным ключом, то добавляется новая запись;
* Ключ для **PeerTable** - **PCSM**, ключ для **RoutingTable** - **Realm**;
* При модификации RoutingTable, список Route полностью заменяется новыми значениями;

Добавление/удаление записей из DefaultPCSM:

* Для версий 4.1.8.57: если значение в команде уже присутствует в таблице, то оно удаляется, если нет - добавляется;
* Начиная с версии 4.1.8.57: список DefaultPCSM полностью заменяется на указанный в команде перезагрузки.

* Добавление/Модификация таблиц маршрутизации:

```bash
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      {
        Host-Identity = %s;
        PCSM = "Sg.DIAM.PCSM.N";
        PeerIP = %s;
      };
    };
    RoutingTable = {
      {
        Realm = %s;
        Route = { %s; }
        AltRoute = %s;
      };
    };
    DefaultPCSM = {
      "Sg.DIAM.PCSM.N";
    };
  };
}" > <path>/logs/reload.req
```

* Удаление из таблиц маршрутизации:

```bash
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      {
        PCSM = "Sg.DIAM.PCSM.N";
        Delete = 1;
      };
    };
    RoutingTable = {
      {
        Realm = %s;
        Delete = 1;
      };
    };
    DefaultPCSM = {
      "Sg.DIAM.PCSM.N";
    };
  };
}" > <path>/logs/reload.req
```