---
title: diameter.cfg
description: Файл настройки компоненты Diameter
type: docs
weight: 20
---
= diameter.cfg
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

В файле задаются настройки компонента Diameter.

=== Описание секции Params компоненты DIAM

|===
| Name | Description | O/M

| PeerTable
| Таблица хостов.
| M

| RoutingTable
| Таблица realm.
| O

| DefaultPCSM
| Список PCSM по умолчанию.
| O
|===

=== Описание секции PeerTable компоненты DIAM

|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| Host-Identity
| Идентификатор хоста.
| string
| M
| P
|

| PCSM
| Компонентный адрес PCSM.
| string
| M
| P
|

| PeerIP
| IP адрес хоста. +
Рекомендуется использовать параметр remote_interfaces.
| ip
| O
| P
|

| remote_interfaces
| Адрес хоста.
| string/object
| O
| P
| 4.1.11.2

| Weight
| Вес PCSM. +
По умолчанию: 1.
| int
| O
| P
| 4.1.10.0
|===

* Можно задавать несколько секций в PeerTable с одинаковым Host-Identity, поскольку
имеется дополнительный параметр PeerIP, связывающий адрес подключающегося клиента с нужным PCSM.
* Начиная с версии 4.1.11.2, добавлен параметр remote_interfaces, позволяющий указать несколько адресов (ip:port) для связывания с PCSM;
* Вес используется для распределения нагрузки между PCSM с одинаковым Host-Identity.
При отправке сообщения по хосту каждый PCSM с подходящим Host-Identity может быть выбран с вероятностью *_Weight / TotalWeight_*,
где *TotalWeight* -- сумма весов всех PCSM с данным Host-Identity.

.Пример
[source,console]
----
{
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      {
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.1";
        PeerIP = "192.168.115.231";
      };
      {
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.2";
        remote_interfaces = "192.168.115.232:3868";
      };
      {
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.3";
        remote_interfaces = { "192.168.115.233";"192.168.115.234" };
      };
      {
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.4";
        remote_interfaces = ":3869";
      };
      {
        Host-Identity = "mme.vlr.com";
        PCSM = "Sg.DIAM.PCSM.5";
      };
    };
  };
}
----

* На PCSM.1 принимаются подключения с IP-адреса *192.168.115.231*, с любого порта;
* На PCSM.2 принимаются подключения с IP-адреса *192.168.115.232*, с порта *3868*;
* На PCSM.3 принимаются подключения с IP-адресов *192.168.115.233* и *192.168.115.234*, с любого порта;
* На PCSM.4 принимаются подключения с любого IP-адреса, с порта *3869*;
* На PCSM.5 принимаются все подключения.

==== Описание секции RoutingTable компоненты DIAM

|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| Realm
| Идентификатор направления.
| string/regex
| M
| P
|

| Route
| Перечень хостов.
| list<string/object>
| M
| P
|

| AltRoute
| Идентификатор альтернативного хоста.
| string
| O
| P
|
|===

До версии 4.1.11.0 список хостов (Route) может быть задан только в виде списка строк.

В версии 4.1.11.0 добавлена возможность указать вес и приоритет хоста.

|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| Peer
| Идентификатор хоста.
| string
| M
| P
| 4.1.11.0

| Weight
| Вес хоста. +
По умолчанию: 1.
| int
| O
| P
| 4.1.11.0

| Priority
| Приоритет хоста. +
По умолчанию: 1.
| int
| O
| P
| 4.1.11.0
|===

* Чем меньше значение *Priority*, тем более приоритетным является хост.
Сначала маршрут выбирается из хостов с наивысшим приоритетом.
При их недоступности выбор производится из хостов со следующим приоритетом и т.д.
* *Weight* используется для распределения нагрузки между хостами с одинаковым приоритетом.
Каждый хост может быть выбран с вероятностью *_Weight / TotalWeight_*,
где *TotalWeight* - сумма весов всех хостов с данным приоритетом.

.Пример
[source,console]
----
RoutingTable = {
  {
    Realm = "example1.realm";
    Route = {
      "host1";
      "host2";
      "host3";
    };
    AltRoute = "alt.host";
  };
}
----

До версии 4.1.11.0 поддерживается только такой формат.
Нагрузка равномерно распределяется между тремя хостами.
В случае их недоступности используется *_AltRoute_*.

----
RoutingTable = {
  {
    Realm = "example2.realm";
    Route = {
      {
        Peer = "host1";
        Weight = 2;
      };
      {
        Peer = "host2";
        Weight = 3;
      };
    };
  };
}
----

NOTE: Распределение нагрузки между *_host1_* и *_host2_* в соотношении 2:3.

----
RoutingTable = {
  {
    Realm = "example3.realm";
    Route = {
      {
        Peer = "host1";
        Priority = 1;
      };
      {
        Peer = "host2";
        Priority = 2;
      };
    };
  };
}
----

NOTE: Отправка сообщений на *_host2_* выполняется только в случае недоступности *_host1_*.

----
RoutingTable = {
  {
    Realm = "example4.realm";
    Route = {
      {
        Peer = "host1";
        Priority = 1;
      };
      {
        Peer = "host2";
        Priority = 2;
      };
    };
    AltRoute = "alt.host"
  };
};
----

NOTE: *_AltRoute_* оставлен для обратной совместимости и является наименее приоритетным хостом:

*_Priority = INT_MAX = 2147483647_*

==== Описание секции DefaultPCSM компоненты DIAM

|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| PCSM
| Компонентный адрес PCSM.
| string
| M
| P
| 4.1.12.0

| Weight
| Вес хоста. +
По умолчанию: 1.
| int
| O
| P
| 4.1.12.0

| Priority
| Приоритет хоста. +
По умолчанию: 1.
| int
| O
| P
| 4.1.12.0
|===

* Чем меньше значение *Priority*, тем более приоритетным является PCSM.
Сначала маршрут выбирается из PCSM с наивысшим приоритетом.
При их недоступности выбор производится из хостов со следующимм приоритетом и т.д.
* *Weight* используется для распределения нагрузки между хостами с одинаковым приоритетом.
Каждый хост может быть выбран с вероятностью *_Weight / TotalWeight_*,
где *TotalWeight* -- сумма весов всех хостов с данным приоритетом.

.Пример
[source,console]
----
DefaultPCSM = {
  "Sg.DIAM.PCSM.0";
  "Sg.DIAM.PCSM.1";
};
----

NOTE: До версии 4.1.12.0 поддерживается только такой формат.

.Пример
[source,console]
----
DefaultPCSM = {
  { PCSM = "Sg.DIAM.PCSM.0"; Priority = 1; Weight = 2 }
  { PCSM = "Sg.DIAM.PCSM.1"; Priority = 1; Weight = 3 }
  { PCSM = "Sg.DIAM.PCSM.0"; Priority = 2; }
  { PCSM = "Sg.DIAM.PCSM.1"; Priority = 2; }
};
----

NOTE: Начиная с версии 4.1.12.0, допускается указывать веса и приоритеты. +
Используемый ранее формат поддерживается.

==== Описание секции Params компоненты DIAM.PCSM

|===
| Параметр | Описание | Применимость | Тип | O/M | P/R | Версия

| PeerIP
| Адрес сервера. +
По умолчанию: 0.0.0.0.
| tcp/sctp, client
| ip
| M
| P
|

| PeerPort
| Порт сервера. +
По умолчанию: 0.
| tcp/sctp, client
| int
| M
| P
|

| SrcIP
| Локальный адрес.
| tcp/sctp, client
| ip
| O
| P
|

| SrcPort
| Локальный порт.
| tcp/sctp, client
| int
| O
| P
|

| Origin-State
| Идентификатор состояния, *_Origin-State_*.
| tcp/sctp, client/server
| int
| O
| P
|

| Origin-Host
| Идентификатор хоста, *_Origin-Host_*. +
По умолчанию: значение link:diameter.adoc#origin-host-diam.adoc[diameter.cfg::[LocalPeerCapabilities\]::Origin-Host].
| tcp/sctp, client/server
| string
| O
| P
|

| Origin-Realm
| Realm хоста, *_Origin-Realm_*. +
По умолчанию: значение link:diameter.adoc#origin-realm-diam.adoc[diameter.cfg::[LocalPeerCapabilities\]::Origin-Realm].
| tcp/sctp, client/server
| string
| O
| P
|

| Transport
| Протокол транспортного уровня. +
*_tcp_* / *_sctp_*. По умолчанию: tcp.
| tcp/sctp, client
| string
| O
| P
|

| InStreams
| Количество входящих потоков. +
По умолчанию: 2.
| sctp, client
| int
| O
| P
|

| OutStreams
| Количество исходящих потоков. +
По умолчанию: 2.
| sctp, client
| int
| O
| P
|

| MaxInitRetransmits
| Максимальное количество попыток отправить сообщение INIT/COOKIE ECHO.
| sctp, client
| int
| O
| P
|

| InitTimeout
| Время ожидания сообщения INIT.
| sctp, client
| int
| O
| P
|

| RtoMax
| Максимальное значение RTO.
| sctp, client
| int
| O
| P
|

| RtoMin
| Минимальное значение RTO.
| sctp, client
| int
| O
| P
|

| RtoInitial
| Начальное значение RTO.
| sctp, client
| int
| O
| P
|

| HbInterval
| Периодичность отправки сигнала heartbeat.
| sctp, client
| int
| O
| P
|

| dscp
| Значение поля заголовка IP DSCP/ToS.
| tcp/sctp, client
| int
| O
| P
| 4.1.8.55

| AssociationMaxRetrans
| Максимальное количество повторных отправок, при превышении которого маршрут считается недоступным.
| sctp, client
| int
| O
| P
|

| SackDelay
| Время ожидания отправки сообщения SACK.
| sctp, client
| int
| O
| P
|

| SndBuf
| Размер буфера сокета для отправки, параметр *_net.core.wmem_default_* Linux Kernel. +
CAUTION: Значение удваивается. Удвоенный размер не может превышать значение *_net.core.wmem_max_*.
| sctp, client
| int
| O
| P
|

| ShutdownEvent
| Флаг включения индикации о событии *_SHUTDOWN_* от ядра.
| sctp, client
| bool
| O
| P
|

| AssocChangeEvent
| Флаг включения индикации об изменении состояния ассоциации от ядра.
| sctp, client
| bool
| O
| P
|

| PeerAddrChangeEvent
| Флаг включения индикации об изменении состояния peer в ассоциации от ядра.
| sctp, client
| bool
| O
| P
|

| local_interfaces
| Перечень локальных интерфейсов. Дополнительную информацию по конфигурированию см. <<localandremoteaddress,LocalAddress>>. Формат: +
*_{ <ip:port>; <ip:port>; }_*
| sctp, client
| [ip:port]
| O
| P
|

| remote_interfaces
| Перечень удаленных интерфейсов. Дополнительную информацию по конфигурированию см. <<localandremoteaddress,RemoteAddress>>. Формат: +
*_{ <ip:port>; <ip:port>; }_*
| sctp, client
| [ip:port]
| O
| P
|

| Appl_Timeout
| Максимальное время ожидания установления Diameter-соединения. +
NOTE: Отсчитывается с момента отправки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer. +
По умолчанию: значение link:diameter.adoc#appl-timeout.adoc[diameter.cfg::[Timers\]::Appl_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| Watchdog_Timeout
| Максимальное время ожидания сообщений Diameter: Device-Watchdog-Request/Answer. +
NOTE: Учитывается время прошедшее с момента посылки последнего сообщения, не обязательно Diameter: DWR. +
По умолчанию: значение link:diameter.adoc#watchdog-timeout.adoc[diameter.cfg::[Timers\]::Watchdog_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| Reconnect_Timeout
| Максимальное время ожидания переустановления соединения. +
NOTE: Учитывается время от разрушения соединения до очередной попытки восстановления. +
По умолчанию: значение link:diameter.adoc#reconnect-timeout.adoc[diameter.cfg::[Timers\]::Reconnect_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| OnBusyReconnect_Timeout
| Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной *_DisconnectCause = BUSY_*. +
NOTE: Если 0, то соединение не переустанавливается. +
По умолчанию: значение link:diameter.adoc#on-busy-reconnect-timeout.adoc[diameter.cfg::[Timers\]::OnBusyReconnect_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.43

| OnShutdownReconnect_Timeout
| Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной *_DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU_*. +
NOTE: Если 0, то соединение не переустанавливается. +
По умолчанию: значение link:diameter.adoc#on-shutdown-reconnect-timeout.adoc[diameter.cfg::[Timers\]::OnShutdownReconnect_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.43

| Response_Timeout
| Максимальное время ожидания ответа. +
По умолчанию: значение link:diameter.adoc#response-timeout.adoc[diameter.cfg::[Timers\]::Response_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| Breakdown_Timeout
| Продолжительность временной недоступности PCSM. +
По умолчанию: значение link:diameter.adoc#breakdown-timeout.adoc[diameter.cfg::[Timers\]::Breakdown_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| Statistic_Timeout
| Периодичность записи статистики в лог-файлы. +
По умолчанию: значение link:diameter.adoc#statistic-timeout.adoc[diameter.cfg::[Timers\]::Statistic_Timeout].
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.39

| [[TrafficManagerInterval]]TrafficManagerInterval
| Период подсчета количества входящих и исходящих запросов. +
По умолчанию: 1000.
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.60

| MaxTransactions
| Максимальное количество запросов за период подсчета запросов, <<trafficmanagerinterval,trafficmanagerinterval>>. +
NOTE: Если 0, то ограничение не проверяется. +
На входящие запросы сверх лимита отправляется ответ с *_Result-Code = TOO_BUSY (3002)_*. На исходящие запросы сверх лимита в логику отправляется Pr_DIAM_SEND_DATA_REJ. +
По умолчанию: 0.
| tcp/sctp, client/server
| int
| O
| P
| 4.1.8.60
|===

[#localandremoteaddress]
==== Конфигурация local и remote адресов

Для серверных компонент IP-адрес, порт и *_local_interfaces_* используются соответствующие значения из файла xref:diameter.adoc[diameter.cfg]:

* {blank}
* {blank}
* {blank}

Параметры из этого файла для них игнорируются.

Компонента является клиентом, если:

* указан *PeerIP* для tcp;
* указан *PeerIP* или *_remote_interfaces_* для sctp.

Адреса клиентских компонент составляются следующим образом для TCP в порядке приоритетности:

* Для удаленных адресов, *RemoteAddr*:
 ** ip - PeerIP;
 ** port - PeerPort;
* Для локальных адресов, *LocalAddr*:
 ** ip - SrcIP, link:diameter.adoc#local_host.adoc[diameter.cfg::[LocalAddress\]::LocalHost];
 ** port - SrcPort;

Для клиентских SCTP-ассоциаций *_remote_interfaces_* и *_local_interfaces_* являются набором 1 основной + дополнительные
адреса.

Основной адрес для SCTP определяется следующим образом в порядке приоритетности:

* Для удаленных адресов, *RemoteAddr*:
 ** ip - PeerIP, первый адрес в списке *_remote_interfaces_*;
 ** port - PeerPort, первый порт в *_remote_interfaces_*;
* Для локальных адресов, *LocalAddr*:
 ** ip - SrcIP, первый адрес в списке *_local_interfaces_*, link:diameter.adoc#local_host.adoc[diameter.cfg::[LocalAddress\]::LocalHost];
 ** port - SrcPort, первый порт в *_local_interfaces_*;

==== Обязательность параметров клиентских компонент

|===
| Параметр | Клиент TCP | Клиент SCTP

| PeerIP
| Обязательный
| Обязательный, если не указан *_ip_* в *_remote_interfaces_*

| PeerPort
| Обязательный
| Обязательный, если не указан *_port_* в *_remote_interfaces_*

| SrcIP
| Опциональный
| Опциональный

| SrcPort
| Опциональный
| Опциональный

| local_interfaces
| Не используется
| Опциональный

| remote_interfaces
| Не используется
| Обязательный, если не указан *_PeerIP_*/*_PeerPort_*
|===

==== Команды по работе с компонентами PCSM

* Добавление новой компоненты:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Add";
  ComponentAddr = Sg.DIAM.PCSM.N;
  ComponentType = Sg.DIAM.PCSM;
  Params = {
    PeerIP = %s;
    PeerPort= %d;
    Transport=%s;
  };
}" > <path>/logs/reload.req
----

NOTE: Начиная с версии DiameterInterface 4.1.8.31, после успешного создания компоненты происходит ее разблокировка.

* Блокировка компоненты:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Block";
  ComponentAddr = Sg.DIAM.PCSM.3;
  Params = {}
}" > <path>/logs/reload.req
----

* Разблокировка компоненты:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Unblock";
  ComponentAddr = Sg.DIAM.PCSM.3;
  Params = {}
}" > <path>/logs/reload.req
----

CAUTION: Компонента должна находиться в состоянии *_CLOSED_*.

После разблокировки:

* Административное состояние изменяется на *_CM_UNBLOCK_*;
* Компонента переходит в состояние *_WAIT_CONN_ACK_*;
* Клиентский PCSM пытается установить соединение.

NOTE: Разблокированный PCSM, работающий в серверном режиме, будет возвращать ответы с *_ResultCode = 3010_*, пока не будет добавлен в PeerTable.

* Модификация компоненты:

[source,bash]
----
$ echo "com_console_di
{
    Command = "Mod";
    ComponentAddr = Sg.DIAM.PCSM.N;
    Params = {
        PeerIP = %s;
        PeerPort = %d;
        Transport=%s;
    };
}" > <path>/logs/reload.req
----

CAUTION: Компонента должна находиться в состоянии *_CM_UNBLOCK_*.

* Перезагрузка компоненты:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM.PCSM.N;
  Params = {};
}" > <path>/logs/reload.req
----

CAUTION: Компонента должна находиться в состоянии *_CM_UNBLOCK_*.

* Закрытие соединения:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM.PCSM.N;
  Params = {
    Action="Close";
  };
}" > <path>/logs/reload.req
----

CAUTION: Компонента должна находиться в состоянии *_CM_UNBLOCK_*.

* Удаление компоненты:

[source,bash]
----
echo "com_console_di
{
    Command = "Del";
    ComponentAddr = Sg.DIAM.PCSM.N;
    Params = {};
}" > <path>/logs/reload.req
----

CAUTION: Соединение должно находиться в состоянии *_CLOSED_* или *_WAIT_CONN_ACK_*.

До версии Diameter 4.1.8.61 после удаления компоненты записи, связанные с ней, удаляются также DefaultPCSM из PeerTable.

==== Команды по работе с компонентами DIAM

* Если в таблице маршрутизации существует запись с заданным ключом, то запись модифицируется;
* Если в таблице маршрутизации отсутствует запись с заданным ключом, то добавляется новая запись;
 Ключ для *PeerTable* - *PCSM*, ключ для *RoutingTable*:: *Realm*
* При модификации RoutingTable, список Route полностью заменяется новыми значениями;

Добавление/удаление записей из DefaultPCSM:

 Для версий 4.1.8.57: если значение в команде уже присутствует в таблице, то оно удаляется, если нет:: добавляется
* Начиная с версии 4.1.8.57: список DefaultPCSM полностью заменяется на указанный в команде перезагрузки.
* Добавление/Модификация таблиц маршрутизации:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      {
        Host-Identity = %s;
        PCSM = "Sg.DIAM.PCSM.N";
        PeerIP = %s;
      };
    };
    RoutingTable = {
      {
        Realm = %s;
        Route = { %s; }
        AltRoute = %s;
      };
    };
    DefaultPCSM = {
      "Sg.DIAM.PCSM.N";
    };
  };
}" > <path>/logs/reload.req
----

* Удаление из таблиц маршрутизации:

[source,bash]
----
$ echo "com_console_di
{
  Command = "Mod";
  ComponentAddr = Sg.DIAM;
  ComponentType = Sg.DIAM;
  Params = {
    PeerTable = {
      {
        PCSM = "Sg.DIAM.PCSM.N";
        Delete = 1;
      };
    };
    RoutingTable = {
      {
        Realm = %s;
        Delete = 1;
      };
    };
    DefaultPCSM = {
      "Sg.DIAM.PCSM.N";
    };
  };
}" > <path>/logs/reload.req
----