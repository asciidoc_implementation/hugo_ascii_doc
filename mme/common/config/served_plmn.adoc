---
title: served_plmn.cfg
description: Параметры обслуживаемых сетей
type: docs
weight: 20
---
= served_plmn.cfg
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

В файле задаются сети, обслуживаемые узлом ММЕ. Каждой сети соответствует одна секция *[Served_PLMN]*.

NOTE: Наличие файла обязательно.

Ключ для перегрузки -- *reload served_plmn.cfg*.

.Описание параметров
[options="header",cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| [[mcc]]MCC
| Мобильный код страны.
| int
| M
| R
|

| [[mnc]]MNC
| Код мобильной сети.
| int
| M
| R
|

| [[mmegi]]MMEGI
| Значение MME Group ID. +
См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf[3GPP TS 23.003]. +
По умолчанию: 0.
| int
| O
| R
|

| [[mmec]]MMEC
| Значение MME Code. +
См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf[3GPP TS 23.003]. +
По умолчанию: 0.
| int
| O
| R
|

| E-UTRAN
| Флаг поддержки сети E-UTRAN. +
По умолчанию: 1.
| bool
| O
| R
|

| UTRAN
| Флаг поддержки сети UTRAN. +
По умолчанию: 1.
| bool
| O
| R
|

| GERAN
| Флаг поддержки сети GERAN. +
По умолчанию: 1.
| bool
| O
| R
|

| Origin-Host
| Значение *_Origin-Host_* для протокола Diameter. +
По умолчанию: mmec<<mmec,<MMEC>>>.mmegi<<mmegi,<MMEGI>>>. +
mme.epc.mnc<<mnc,<MNC>>>.mcc<<mcc,<MCC>>>. +
3gppnetwork.org.
| string
| O
| R
|

| Origin-Realm
| Значение *_Origin-Realm_* для протокола Diameter. +
По умолчанию: epc.mnc<<mnc,<MNC>>>. +
mcc<<mcc,<MCC>>>.3gppnetwork.org.
| string
| O
| R
|

| Authentication
| Флаг включения аутентификации. +
По умолчанию: 1.
| bool
| O
| R
|

| AuthVectorsNum
| Количество запрашиваемых векторов аутентификации. +
Диапазон: 1-7. По умолчанию: 1.
| int
| O
| R
| 1.41.0.0

| CheckIMEI
| Флаг включения проверки IMEI на узле EIR. +
По умолчанию: 0.
| bool
| O
| R
|

| AllowGreyIMEI
| Флаг разрешения подключения телефона с неизвестным IMEI. +
По умолчанию: 1.
| bool
| O
| R
|

| EIR-Host
| Значение *_Destination-Host_* для узла EIR.
| string
| O
| R
|

| EIR-Realm
| Значение *_Destination-Realm_* для узла EIR.
| string
| O
| R
|

| [[psm]]PSM
| Флаг разрешения режима Power Saving Mode. +
См. https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/12.02.00_60/ts_123682v120200p.pdf[3GPP TS 23.682]. +
По умолчанию: 0.
| bool
| O
| R
| 1.22.1.0

| T3324
| Таймер T3324, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf[3GPP TS 24.008]. +
NOTE: Используется только при *_PSM = 1_*. +
По умолчанию: значение T3324 абонента.
| int
| O
| R
|

| T3402
| Таймер T3402, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 0.
| int
| O
| R
|

| T3402_Rej
| Таймер T3402 для абонентов, не попавших в белый список IMSI, в секундах. +
По умолчанию: 0.
| int
| O
| R
|

| T3412
| Таймер T3412, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 60.
| int
| O
| R
|

| [[t3412_ext]]T3412_Ext
| Таймер T3412 Extended, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: не определено.
| int
| O
| R
|

| T3412_Ext_Source
| Приоритетный источник таймера <<t3412_ext,T3412 Extended>>. +
*_mme_* / *_ue_*. По умолчанию: ue.
| string
| O
| R
|

| [[t3413]]T3413
| Таймер T3413, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 10.
| int
| O
| R
|

| T3413_SGs
| Таймер T3413 для процедуры S1AP Paging, вызванного процедурой SGsAP Paging, в секундах. +
По умолчанию: значение <<t3413,T3413>>.
| int
| O
| R
|

| T3422
| Таймер T3422, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 6.
| int
| O
| R
| 1.36.0.0

| [[t3450]]T3450
| Таймер T3450, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 6.
| int
| O
| R
| 1.36.0.0

| T3450_RepeatCount
| Количество повторных отправок по таймеру <<t3450,T3450>>. +
Диапазон: 0-5. По умолчанию: 4.
| int
| O
| R
| 1.36.0.0

| [[t3460]]T3460
| Таймер T3460, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 6.
| int
| O
| R
| 1.36.0.0

| T3460_RepeatCount
| Количество повторных отправок по таймеру <<t3460,T3460>>. +
Диапазон: 0-5. По умолчанию: 4.
| int
| O
| R
| 1.36.0.0

| [[t3470]]T3470
| Таймер T3470, в секундах. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 6.
| int
| O
| R
| 1.36.0.0

| T3470_RepeatCount
| Количество повторных отправок по таймеру <<t3470,T3470>>. +
Диапазон: 0-5. По умолчанию: 4.
| int
| O
| R
| 1.36.0.0

| PagingRepeatCount
| Количество отправок запросов Paging на каждую станцию eNodeB в рамках текущего шага используемой модели пейджинга. +
Диапазон: 1-5. +
По умолчанию: 3.
| int
| O
| R
|

| PagingModel
| Индикатор используемой модели для процедуры Paging. +
*_0_* -- по умолчанию; +
*_1_* -- начало с последней базовой станции. +
По умолчанию: 0.
| int
| O
| R
| 1.40.0.0

| UE_ActivityTimeout
| Время ожидания активности от устройства UE, в секундах. +
По умолчанию: 10.
| int
| O
| R
|

| [[ims_vops]]IMS_VoPS
| Значение *_IMS Voice over PS Session Indicator_*. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 0.
| bool
| O
| R
|

| VoPS_APN_NI
| Перечень значений APN-NI, которые могут быть использованы для VoPS. +
По умолчанию: ims.
| [int]
| O
| R
| 1.40.0.0

| P_CSCF_Restoration
| Флаг поддержки процедуры P-CSCF Restoration. +
По умолчанию: 1.
| bool
| O
| R
| 1.44.2.0

| ResetOnSetup
| Флаг отправки сообщения S1AP: RESET после запроса S1AP: S1 SETUP REQUEST. +
По умолчанию: 0.
| bool
| O
| R
|

| AllowDefaultAPN
| Флаг разрешения использования APN по умолчанию. +
По умолчанию: 1.
| bool
| O
| R
|

| AllowOperatorQCI
| Флаг разрешения использования {QCI} из диапазона 128-254. +
По умолчанию: 0.
| bool
| O
| R
|

| AllowExtDRX
| Флаг разрешения использования Extended DRX. +
См. https://www.etsi.org/deliver/etsi_ts/125300_125399/125300/17.00.00_60/ts_125300v170000p.pdf[3GPP TS 25.300]. +
По умолчанию: 0.
| bool
| O
| R
|

| PTW
| Значение Paging Transmission Window для Extended DRX. +
По умолчанию: не определено.
| int
| O
| R
| 1.41.0.0

| eDRX
| Значение {eDRX}. +
По умолчанию: не определено.
| int
| O
| R
| 1.41.0.0

| RelativeMME_Capacity
| Относительная емкость узла MME, Relative MME Capacity. +
См. https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf[3GPP TS 36.413]. +
По умолчанию: 255.
| int
| O
| R
|

| RelativeMME_CapacityForTAC
| Перечень связей Relative MME Capacity и TAC. Формат: +
*_<tac>-<capacity>,<tac>-<capacity>_*
| [{int,int}]
| O
| R
| 1.35.0.0

| NotifyOnPGW_Alloc
| Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении динамического IP-адреса узла PGW для APN. +
По умолчанию: 1.
| bool
| O
| R
|

| NotifyOnUE_SRVCC
| Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении флага поддержки SRVCC на устройстве UE. +
По умолчанию: 1.
| bool
| O
| R
|

| EMM_Info
| Флаг отправки сообщения EMM Information. +
См. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301]. +
По умолчанию: 0.
| bool
| O
| R
|

| ForceIdentityReq
| Флаг отправки Identity Req вне зависимости от наличия сохраненного ранее IMSI. +
По умолчанию: 0.
| bool
| O
| R
|

| CP_CIoT_opt
| Флаг разрешения оптимизации Control Plane CIoT EPS. +
См. https://www.etsi.org/deliver/etsi_ts/136300_136399/136300/14.10.00_60/ts_136300v141000p.pdf[3GPP TS 36.300]. +
По умолчанию: 0.
| bool
| O
| R
| 1.15.19.0

| IndirectFwd
| Флаг разрешения создания Indirect Data Forwarding Tunnel при хэндовере. +
По умолчанию: 0.
| bool
| O
| R
| 1.19.0.0

| PurgeDelay
| Время задержки отправки сообщения Diameter: Purge-Request на узел HSS, в секундах. +
NOTE: По умолчанию сообщение не отправляется, а данные не удаляются. +
По умолчанию: не определено.
| int
| O
| R
|

| RequestIMEISV
| Флаг требования IMEISV от абонента. +
По умолчанию: 1.
| bool
| O
| R
| 1.15.18.0

| UseVLR
| Флаг использования VLR. +
По умолчанию: 0.
| bool
| O
| R
|

| RejectOnLU_Fail
| Флаг отправки сообщения SGsAP-LOCATION-UPDATE-REJECT в случае неуспеха процедуры SGsAP-Location-Update. +
См. https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf[3GPP TS 29.118]. +
По умолчанию: 0.
| bool
| O
| R
|

| MO_CSFB_Ind
| Флаг отправки сообщения SGsAP-MO-CSFB-Indication при получении запроса Extended Service Request. +
По умолчанию: 0.
| bool
| O
| R
|

| SRVCC
| Флаг поддержки {SRVCC}. +
См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf[3GPP TS 29.280]. +
По умолчанию: 0.
| bool
| O
| R
|

| NetFullname
| Полное название сети. +
По умолчанию: Protei Network.
| string
| O
| R
|

| NetName
| Краткое название сети. +
По умолчанию: Protei.
| string
| O
| R
|

| LDN
| {LDN}, отправляемый при активированном SRVCC. +
По умолчанию: Protei_MME.
| string
| O
| R
|

| AllowEmptyMSISDN
| Флаг разрешения регистрации абонентов без MSISDN. +
По умолчанию: 0.
| bool
| O
| R
|

| AllowUnknownIMEI
| Флаг обслуживания устройств, неизвестных для EIR. +
По умолчанию: 0.
| bool
| O
| R
| 1.40.0.0

| S11UliAlwaysSent
| Флаг отправки User Location Information в запросе GTP: Modify Bearer Request. +
См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf[3GPP TS 29.274]. +
По умолчанию: 0.
| bool
| O
| R
| 1.40.0.0

| EquivalentPLMNs
| Перечень эквивалентных PLMN, который указывается в поле *_ICS Request_*. Формат: +
*_<mcc>-<mnc>,<mcc>-<mnc>_* +
См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129292/17.00.00_60/ts_129292v170000p.pdf[3GPP TS 29.292].
| [string]
| O
| R
| 1.36.0.0

| [[emergency_numbers]]EmergencyNumbers
| Перечень link:emergency_numbers.adoc[типов и правил номеров экстренных служб].
| [string]
| O
| R
|

| EmergencyAttach_wo_auth
| Флаг разрешения процедуры Emergency Attach без аутентификации. +
По умолчанию: 0.
| bool
| O
| R
|

| [[emergency_pdn]]EmergencyPDN
| Перечень имен PDN Connectivity, применяемых в случае Emergency Attach. +
NOTE: Именованные описания PDN Connectivity хранятся в файле xref:emergency_pdn[emergency_pdn.cfg].
| [string]
| O
| R
|

| <<timezone,Timezone>>
| Часовые пояса. +
По умолчанию: UTC+3.
| timezone
| O
| R
|

| [[apn_rules]]APN_rules
| Перечень link:apn_rules.adoc[правил APN].
| [string]
| O
| R
|

| [[tac_rules]]TAC_rules
| Перечень link:tac_rules.adoc[правил TAC].
| [string]
| O
| R
|

| [[rac_rules]]RAC_rules
| Перечень link:rac_rules.adoc[правил RAC].
| [string]
| O
| R
|

| ForbiddenLAC
| Перечень кодов LAC, из которых переход абонентов запрещен.
| [int]
| O
| R
| 1.33.0.0

| [[pgw_ip]]PGW_IP
| IP-адрес и FQDN узла PGW по умолчанию. Формат: +
*_"(<fqdn>)<ip>"_* +
NOTE: FQDN может отсутствовать.
| {string,ip}
| O
| R
|

| [[sgw_ip]]SGW_IP
| IP-адрес и FQDN узла SGW по умолчанию. Формат: +
*_"(<fqdn>)<ip>"_* +
NOTE: FQDN может отсутствовать.
| {string,ip}
| O
| R
|

| MME_IP
| IP-адреса других узлов MME с соответствующими значениями MMEGI и MMEC. Формат: +
*_<mmegi>-<mmec>-<ip>,<mmec>-<ip>_* +
NOTE: Если MMEGI не задан, то используется значение, связанное с этой PLMN.
| [{int,int,ip}]
| O
| R
|

| SGSN_IP
| IP-адреса узлов SGSN с соответствующими контроллерами RNC. Формат: +
*_<rnc>-<ip>,<rnc>-<ip>_*.
| [{string,ip}]
| O
| R
|

| NRI_length
| Количество используемых бит идентификатора {NRI} узла SGSN. +
NOTE: Если не задано, то не используется в рамках соответствующей сети PLMN.
| int
| O
| R
| 1.44.1.0

| [[imsi_wl]]IMSI_whitelist
| Перечень link:imsi_rules.adoc[правил TAC и IMSI]. +
Если TAC не указан, то набор масок применяется для всей PLMN.
| [string]
| O
| R
|

| UE_Usage_Types
| Перечень UE Usage Type, связанных с текущим MMEGI. +
См. https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.08.00_60/ts_123401v170800p.pdf[3GPP TS 23.401].
| [int]
| O
| R
| 1.35.0.0

| Foreign_UE_Usage_Types
| Перечень связей между UE Usage Type и другими MMEGI. Формат: +
*_<ue_usage_type>,<ue_usage_type>-<mmegi>_*.
| [{[int],int}]
| O
| R
|

| [[zc_rules]]ZC_rules
| Перечень link:zc_rules.adoc[правил ZoneCodes].
| [string]
| O
| R
| 1.36.0.0

| [[qos_rules]]QoS_rules
| Перечень link:qos_rules.adoc[правил QoS].
| [string]
| O
| R
| 1.37.0.0

| [[code_mapping_rules]]CodeMapping_rules
| Перечень link:code_mapping_rules.adoc[правил Diameter: Result-Code и EMM Cause].
| [string]
| O
| R
| 1.37.2.0

| [[forbidden_imei_rules]]ForbiddenIMEI_rules
| Перечень link:forbidden_imei_rules.adoc[правил запрещённых IMEI].
| [string]
| O
| R
|

| <TAC> = <LAI>
| Перечень соответствий TAC к LAI. Формат: +
*_<tac> = [<plmn>-]<lac>;_* +
NOTE: Один TAC может быть связан только с одним LAI, один LAI может быть связан со многими TAC.
| [{int,int,int}]
| O
| R
|

| TAC
| Код зоны отслеживания.
| int
| O
| R
|

| <plmn>
| Код сети PLMN. +
По умолчанию: значение <<mcc,<MCC>>><<mnc,<MNC>>>.
| int
| O
| R
|

| <lac>
| Код локальной области LAC.
| int
| M
| R
|
|===

[#timezone]
==== Формат Timezone

Формат записи часового пояса: *_[TACX_]UTC<sign>HH:MM[_DST+Y]_*

* Необязательная часть *_TAC<X>__* -- задает ассоциацию между часовым поясом и TAC, при отсутствии ассоциируется со всеми TAC в данной PLMN:
 ** X -- код TAC;
* Обязательная часть *_UTC<sign><HH>:<MM>_* -- задает часовой пояс через отклонение от UTC:
 ** sign -- знак отклонения от UTC, *_+_* или *_-_*;
 ** HH -- количество часов;
 ** MM -- количество минут;
* Необязательная часть *__DST+<Y>_* -- задает дополнительное отклонение при переводе часов на летнее время:
 ** Y -- количество добавленных часов, диапазон: 0-2;

==== Адреса PGW и SGW

NOTE: Адреса SGW и PGW (<<sgw_ip,SGW_IP>>/<<pgw-ip,PGW_IP>>) определяются в нескольких конфигурационных файлах.
Приоритеты значений в порядке убывания:

* link:apn_rules.adoc[apn_rules.cfg];
* link:dns.adoc[dns.cfg];
* served_plmn.cfg;
* link:gtp_c.adoc[gtp_c.cfg].

.Пример
[source,console]
----
[Served_PLMN]
{
  MCC = 999;
  MNC = 99;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  T3413 = 1;
  T3412 = 1800;
  T3402 = 10;
  T3402_Rej = 10;
  T3450 = 1;
  T3460 = 1;
  T3470 = 1;
  CheckIMEI = 0;
  EmergencyNumbers = "911";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  UseVLR = 0;
  IMS_VoPS = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 0;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6;
  RelativeMME_Capacity = 255;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_999";
  NetName = "test99";
  Timezone = "UTC+3";
}
{
  MCC = 001;
  MNC = 01;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  MME_IP = 1-2-192.168.125.182;
  T3324 = 60;
  T3413 = 1;
  T3412 = 120;
  T3412_Ext = 14400;
  T3412_Ext_Source = "mme";
  T3402 = 5;
  T3402_Rej = 10;
  T3450 = 5;
  T3450_RepeatCount = 4;
  T3460 = 6;
  T3460_RepeatCount = 3;
  T3470 = 1;
  UE_ActivityTimeout = 600;
  CheckIMEI = 0;
  RequestIMEISV = 1;
  AuthVectorsNum = 2
  EmergencyNumbers = "112,911";
  EmergencyPDN = "EMRG";
  Origin-Host = "mme1.epc.mnc001.mcc001.3gppnetwork.org";
  Origin-Realm = "epc.mnc001.mcc001.3gppnetwork.org";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  VoPS_APN_NI = ims;
  AllowGreyIMEI = 0;
  UseVLR = 0;
  RejectOnLU_Fail = 0;
  ResetOnSetup = 0;
  PurgeDelay = 0;
  PagingModel = 1;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1,Rule_2,Rule_3;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6,Rule_7;
  CodeMapping_rules = Rule1, Rule2;
  RelativeMME_Capacity = 100;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_001";
  NetName = "protei";
  Timezone = "TAC1_UTC+2";
  CP_CIoT_opt = 1;
  PSM = 1;
  1 = 1;
  7 = 1;
  9 = 25048-5;
  10 = 25048-7;
  UE_Usage_Types = 0,1;
  Foreign_UE_Usage_Types = 2-2;
  QoS_rules = Rules;
  AllowDefaultAPN = 1;
}
{
  MMEGI = 3;
  MMEC = 4;
}
{
  MCC = 208;
  MNC = 93;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  T3413 = 5;
  T3412_Ext = 600;
  RequestIMEISV = 0;
  CheckIMEI = 0;
  AllowEmptyMSISDN = 1;
  EIR-Host ="eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  PSM = 1;
  AllowExtDRX = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 1;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_4;
  SGW_IP = "192.168.125.95";
  2 = 3;
  1 = 1;
  5 = 25048-4;
  CP_CIoT_opt = 1;
}
----