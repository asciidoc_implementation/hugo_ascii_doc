---
title: "gtp_c.cfg"
description: "Параметры компонента GTP-C"
weight: 20
type: docs
---

В файле задаются настройки компонента GTP-C.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload gtp_c.cfg**.

### Используемые секции ###

* [\[LocalAddress\]](#local-address) - параметры локального хоста;
* [\[S11\]](#s11) - параметры интерфейса S11;
* [\[S11-U\]](#s11-u) - параметры интерфейса S11-U;
* [\[S10\]](#s10) - параметры интерфейса S10;
* [\[S3\]](#s3) - параметры интерфейса S3;
* [\[Sv\]](#sv) - параметры интерфейса Sv;
* [\[GnGp\]](#gngp) - параметры интерфейса GnGp;
* [\[Timers\]](#timers) - параметры таймеров;
* [\[Repeat\]](#repeat) - параметры повторов;
* [\[Overload\]](#overload) - параметры перегрузки;

### Описание параметров ###

| Параметр                                         | Описание                                                                                                                                                                                                                     | Тип    | O/M | P/R | Версия    |
|--------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|-----------|
| **<a name="local-address">\[LocalAddress\]</a>** | Параметры локального хоста.                                                                                                                                                                                                  | object | M   | P   |           |
| <a name="local-host">LocalHost</a>               | IP-адрес локального хоста.                                                                                                                                                                                                   | ip     | M   | P   |           |
| <a name="local-port">LocalPort</a>               | Прослушиваемый порт.<br>По умолчанию: 2123.                                                                                                                                                                                  | int    | O   | P   |           |
| <a name="dscp">DSCP</a>                          | Используемое значение DSCP.                                                                                                                                                                                                  | int    | O   | P   |           |
| <a name="ip-mtu-discover">IP_MTU_Discover</a>    | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: 0.                                                              | int    | O   | P   |           |
| **<a name="s11">\[S11\]</a>**                    | Параметры интерфейса S11.                                                                                                                                                                                                    | object | O   | P   |           |
| LocalHost                                        | IP-адрес хоста для интерфейса S11.<br>По умолчанию: значение [\[LocalAddress\]::LocalHost](#local-host).                                                                                                                     | ip     | O   | P   |           |
| LocalPort                                        | Прослушиваемый порт для интерфейса S11.<br>По умолчанию: значение [\[LocalAddress\]::LocalPort](#local-port).                                                                                                                | int    | O   | P   |           |
| DSCP                                             | Используемое значение DSCP.<br>По умолчанию: значение [\[LocalAddress\]::DSCP](#dscp).                                                                                                                                       | int    | O   | P   |           |
| IP_MTU_Discover                                  | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[LocalAddress\]::IP_MTU_Discover](#ip-mtu-discover). | int    | O   | P   |           |
| **<a name="s11-u">\[S11_U\]</a>**                | Параметры интерфейса S11-U.                                                                                                                                                                                                  | object | O   | P   | 1.15.22.0 |
| LocalHost                                        | IP-адрес хоста для интерфейса S11-U.<br>По умолчанию: значение [\[LocalAddress\]::LocalHost](#local-host).                                                                                                                   | ip     | O   | P   | 1.15.22.0 |
| LocalPort                                        | Прослушиваемый порт для интерфейса S11-U.<br>По умолчанию: 2152.                                                                                                                                                             | int    | O   | P   | 1.15.22.0 |
| DSCP                                             | Используемое значение DSCP.<br>По умолчанию: значение [\[LocalAddress\]::DSCP](#dscp).                                                                                                                                       | int    | O   | P   |           |
| IP_MTU_Discover                                  | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[LocalAddress\]::IP_MTU_Discover](#ip-mtu-discover). | int    | O   | P   |           |
| **<a name="s10">\[S10\]</a>**                    | Параметры интерфейса S10.                                                                                                                                                                                                    | object | O   | P   |           |
| LocalHost                                        | IP-адрес хоста для интерфейса S10.<br>По умолчанию: значение [\[LocalAddress\]::LocalHost](#local-host).                                                                                                                     | ip     | O   | P   |           |
| LocalPort                                        | Прослушиваемый порт для интерфейса S10.<br>По умолчанию: значение [\[LocalAddress\]::LocalPort](#local-port).                                                                                                                | int    | O   | P   |           |
| DSCP                                             | Используемое значение DSCP.<br>По умолчанию: значение [\[LocalAddress\]::DSCP](#dscp).                                                                                                                                       | int    | O   | P   |           |
| IP_MTU_Discover                                  | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[LocalAddress\]::IP_MTU_Discover](#ip-mtu-discover). | int    | O   | P   |           |
| **<a name="s3">\[S3\]</a>**                      | Параметры интерфейса S3.                                                                                                                                                                                                     | object | O   | P   |           |
| <a name="s3-local-host">LocalHost</a>            | IP-адрес хоста для интерфейса S3.<br>По умолчанию: значение [\[LocalAddress\]::LocalHost](#local-host).                                                                                                                      | ip     | O   | P   |           |
| <a name="s3-local-port">LocalPort</a>            | Прослушиваемый порт для интерфейса S3.<br>По умолчанию: значение [\[LocalAddress\]::LocalPort](#local-port).                                                                                                                 | int    | O   | P   |           |
| <a name="s3-dscp">DSCP</a>                       | Используемое значение DSCP.<br>По умолчанию: значение [\[LocalAddress\]::DSCP](#dscp).                                                                                                                                       | int    | O   | P   |           |
| <a name="s3-ip-mtu-discover">IP_MTU_Discover</a> | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[LocalAddress\]::IP_MTU_Discover](#ip-mtu-discover). | int    | O   | P   |           |
| **<a name="sv">\[Sv\]</a>**                      | Параметры интерфейса Sv.                                                                                                                                                                                                     | object | O   | P   |           |
| <a name="sv-local-host">LocalHost</a>            | IP-адрес хоста для интерфейса Sv.<br>По умолчанию: значение [\[LocalAddress\]::LocalHost](#local-host).                                                                                                                      | ip     | O   | P   |           |
| <a name="sv-local-port">LocalPort</a>            | Прослушиваемый порт для интерфейса Sv.<br>По умолчанию: значение [\[LocalAddress\]::LocalPort](#local-port).                                                                                                                 | int    | O   | P   |           |
| <a name="sv-dscp">DSCP</a>                       | Используемое значение DSCP.<br>По умолчанию: значение [\[LocalAddress\]::DSCP](#dscp).                                                                                                                                       | int    | O   | P   |           |
| <a name="sv-ip-mtu-discover">IP_MTU_Discover</a> | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[LocalAddress\]::IP_MTU_Discover](#ip-mtu-discover). | int    | O   | P   |           |
| **<a name="gngp">\[GnGp\]</a>**                  | Параметры интерфейса GnGp.                                                                                                                                                                                                   | object | O   | P   |           |
| LocalHost                                        | IP-адрес хоста для интерфейса GnGp.<br>По умолчанию: значение [\[S3\]::LocalHost](#s3-local-host).                                                                                                                           | ip     | O   | P   |           |
| LocalPort                                        | Прослушиваемый порт для интерфейса GnGp.<br>По умолчанию: значение [\[S3\]::LocalPort](#s3-local-port).                                                                                                                      | int    | O   | P   |           |
| DSCP                                             | Используемое значение DSCP.<br>По умолчанию: значение [\[S3\]::DSCP](#s3-dscp).                                                                                                                                              | int    | O   | P   |           |
| IP_MTU_Discover                                  | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: значение [\[S3\]::IP_MTU_Discover](#s3-ip-mtu-discover).        | int    | O   | P   |           |
| **<a name="timers">\[Timers\]</a>**              | Параметры таймеров.                                                                                                                                                                                                          | object | O   | R   |           |
| <a name="resp-timeout">Response_Timeout</a>      | Время ожидания ответного сообщения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                           | int    | O   | R   |           |
| **<a name="repeat">\[Repeat\]</a>**              | Параметры повторов запросов.                                                                                                                                                                                                 | object | O   | R   |           |
| Count                                            | Количество повторных запросов, отправляемых по истечении времени [Response_Timeout](#resp-timeout).<br>По умолчанию: 4.                                                                                                      | int    | O   | R   |           |
| **<a name="overload">\[Overload\]</a>**          | Параметры перегрузки.                                                                                                                                                                                                        | object | O   | R   | 1.1.0.0   |
| Enable                                           | Флаг детектирования перегрузки.<br>По умолчанию: 0.                                                                                                                                                                          | bool   | O   | R   | 1.1.0.0   |
| DDN                                              | Максимальное количество сообщений GTP-C: Downlink Data Notification в секунду.<br>По умолчанию: 1000.                                                                                                                        | int    | O   | R   | 1.1.0.0   |

#### Пример ####

```
[LocalAddress]
LocalHost = 192.168.100.1;
LocalPort = 2123;

[S11]
LocalHost = 192.168.100.2;
LocalPort = 2123;

[S11-U]
LocalHost = 192.168.100.3;
LocalPort = 2152;

[S10]
LocalHost = 192.168.100.2;
LocalPort = 2123;

[S3]
LocalHost = 192.168.100.1;
LocalPort = 2123;

[Timers]
Response_Timeout = 30000;

[Repeat]
Count = 4;

[Overload]
Enable = 1;
DDN = 500;
```
