---
title: "Reject EDR"
description: "Журнал reject_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле     | Описание                                                | Тип      |
|-----|----------|---------------------------------------------------------|----------|
| 1   | DateTime | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |
| 2   | Event    | Событие или процедура, сформировавшая запись.           | string   |
| 3   | IMSI     | Номер IMSI абонента.                                    | string   |
| 4   | PLMN     | Идентификатор PLMN.                                     | string   |
| 5   | TAC      | Код зоны отслеживания.                                  | string   |
| 6   | CellID   | Идентификатор соты.                                     | int/hex  |

```log
DateTime,Event,IMSI,PLMN,TAC,CellID
```