---
title: "EDR"
description: "Описание журналов EDR"
weight: 10
type: docs
---

В данном разделе приведено описание журналов EDR.
По умолчанию журналы EDR хранятся в директории `/usr/protei/cdr/Protei_MME`.