---
title: S1AP Context EDR
description: Журнал s1ap_context_cdr
type: docs
weight: 20
---
= S1AP Context EDR
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

=== Описание используемых полей

|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| eNodeB UE ID
| Идентификатор S1-контекста на стороне eNodeB.
| int

| 4
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 5
| IMSI
| Номер IMSI абонента.
| string

| 6
| MSISDN
| Номер MSISDN абонента.
| string

| 7
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 8
| IMEI
| Номер IMEI устройства.
| string

| 9
| PLMN
| Идентификатор PLMN.
| string

| 10
| TAC
| Код области отслеживания.
| string

| 11
| CellID
| Идентификатор соты.
| int/hex

| 12
| <<cause,CauseType>>
| Тип события.
| string

| 13
| <<cause,CauseReason>>
| Причина события.
| string

| 14
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 15
| Procedure Type:ErrorCode
| Тип процедуры + Внутренний link:error_code.adoc[код MME] результата. Формат: +
*_<procedure_type>:<error_code>_*. +
*_1_* -- <<service_req,Service Req>>; +
*_2_* -- <<s1_context_release,S1 Context Release>>.
| object
|===

[#event]
==== События и процедуры, Event

* <<service_req,Service Req>>;
* <<s1_context_release,S1 Context Release>>;
* <<reset,Reset>>.

[#service_req]
==== Service Req

[source,log]
----
DateTime,Service Req,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,0,0,Duration,1:ErrorCode
----

Локальные коды ошибок

|===
| Код | Описание

| 1
| Указана недопустимая сеть PLMN в запросе S1 Service Request.

| 2
| У абонента нет подходящих bearer-служб.

| 3
| Запрошена неизвестная bearer-служба.

| 4
| Ошибка модификации bearer-службы.

| 5
| Отсутствует поле *_User Data Container_*.

| 6
| Не удалось декодировать значение ESM Message Container.

| 7
| Не удалось сбросить контекст.

| 8
| Для данного абонента неизвестен VLR GT.

| 9
| Ошибка процедуры S1AP: INITIAL CONTEXT SETUP.

| 10
| Получен повторный запрос S1 Service Request от абонента со смежного узла MME.

| 11
| До завершения обработки текущего S1 Service Request был получен новый.
|===

[#s1_context_release]
==== S1 Context Release

[source,log]
----
DateTime,S1 Context Release,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,CauseType,CauseReason,Duration,2:ErrorCode
----

[#reset]
==== Reset

[source,log]
----
DateTime,Reset,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,PLMN,TAC,CellID,CauseType,CauseReason,Duration,3:ErrorCode
----

[#cause]
==== CauseType и CauseReason

Поля *CauseType* и *CauseReason* описывают причину события S1AP.

*CauseType* задает группу событий. См. https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf[3GPP TS 36.413].

Возможные значения:

* 1 -- <<radio_network_layer_cause,Radio Network Layer cause>>, причины уровня радиосети;
* 2 -- <<transport_layer_cause,Transport Layer cause>>, причины уровня передачи;
* 3 -- <<nas_cause,NAS cause>>, причины уровня NAS;
* 4 -- <<protocol_cause,Protocol cause>>, причины уровня протокола;
* 5 -- <<miscellaneous_cause,Miscellaneous cause>>, остальные причины.

*CauseReason* задает порядковый номер причины события внутри группы *CauseType*. См. https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf[3GPP TS 36.413].

[#radio_network_layer_cause]
===== Radio Network Layer cause

|===
| Код | Причина | Описание

| 0
| Unspecified
| Неописанная ошибка.

| 1
| TX2RELOCOverall Expiry
| Таймер, контролирующий хэндовер X2, нештатно истек.

| 2
| Successful Handover
| Хэндовер осуществлен успешно.

| 3
| Release due to E-UTRAN generated reason
| Причина отбоя вызвана сетью E-UTRAN.

| 4
| Handover Cancelled
| Вызвана отмена хэндовера.

| 5
| Partial Handover
| Вызвана отмена хэндовера, поскольку в сообщении S1AP: HANDOVER COMMAND от MME задано поле E-RABs to Release List, и исходная eNodeB оценила, что непрерывность услуги будет лучше без хэндовера к указанному eNodeB назначения.

| 6
| Handover Failure In Target EPC/eNB Or Target System
| Хэндовер завершился неуспешно ввиду ошибки EPC/eNodeB назначения.

| 7
| Handover Target not allowed
| Хэндовер в указанную соту назначения не разрешен.

| 8
| TS1RELOCoverall Expiry
| Вызван истечением таймера TS1RELOCoverall.

| 9
| TS1RELOCprep Expiry
| Процедура Handover Preparation отменяется по истечении таймера TS1RELOCprep.

| 10
| Cell not available
| Соответствующая сота недоступна.

| 11
| Unknown Target ID
| Хэндовер не разрешен, поскольку идентификатор назначения не известен EPC.

| 12
| No radio resources available in target cell
| Нагрузка на соту назначения слишком высока.

| 13
| Unknown or already allocated MME UE S1AP ID
| Запрошен, поскольку либо идентификатор MME UE S1AP ID неизвестен, либо (для первого сообщения, полученного на eNodeB) известен и привязан к уже существующему контексту.

| 14
| Unknown or already allocated eNB UE S1AP ID
| Запрошен, поскольку либо идентификатор eNodeB UE S1AP ID неизвестен, либо (для первого сообщения, полученного узлом MME) известен и привязан к уже существующему контексту.

| 15
| Unknown or inconsistent pair of UE S1AP ID
| Запрошен, поскольку либо идентификаторы UE S1AP ID неизвестны, либо известны, но связаны с различными контекстами.

| 16
| Handover Desirable for Radio Reasons
| Причина запроса хэндовера связана с радиосетью.

| 17
| Time Critical Handover
| Запрошен хэндовер по причине, которая критична по времени, т.е. значение зарезервировано для всех критических случаев, когда соединение наверняка будет потеряно при невыполнении хэндовера.

| 18
| Resource Optimisation Handover
| Запрошен хэндовер для оптимизации распределения нагрузки на соседние соты.

| 19
| Reduce Load in Serving Cell
| Необходимо снизить нагрузку на обслуживающие соты. При подготовке к хэндоверу указывает на активацию хэндовера ввиду балансировки нагрузки.

| 20
| User Inactivity
| Запрошен ввиду неактивности пользователя на всех E-{RAB}, например, разрыв S1-соединения, для оптимизации использования ресурсов.

| 21
| Radio Connection With UE Lost
| Запрошен ввиду потери радиосоединения с UE.

| 22
| Load Balancing TAU Required
| Запрашивается для всех случаев балансировки и разгрузки узла MME.

| 23
| CS Fallback triggered
| Запрошен ввиду процедуры CS Fallback. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS.

| 24
| UE Not Available for PS Service
| Запрошен ввиду процедуры CS Fallback для сети GERAN. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS ввиду отсутствия поддержки {DTM} GERAN-сотой назначения или UE.

| 25
| Radio resources not available
| Запрашиваемые радиоресурсы недоступны.

| 26
| Invalid QoS combination
| Вызван некорректными значениями параметров QoS.

| 27
| Inter-RAT Redirection
| Запрошен ввиду переадресации между различными сетями RAT или LTE. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, дальнейшие действия EPC должны следовать https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.07.00_60/ts_123401v170700p.pdf[3GPP TS 23.401].

| 28
| Failure in the Radio Interface Procedure
| Процедура Radio Interface Procedure завершилась неуспешно.

| 29
| Interaction with other procedure
| Вызван продолжающимся взаимодействием с другой процедурой.

| 30
| Unknown E-RAB ID
| Вызван отсутствием идентификатора E-RAB на eNodeB.

| 31
| Multiple E-RAB ID Instances
| Вызван передачей нескольких одинаковых E-RAB на eNodeB.

| 32
| Encryption and/or integrity protection algorithms not supported
| eNodeB не поддерживает ни один из алгоритмов шифрования/защиты целостности, которые поддерживаются UE.

| 33
| S1 Intra system Handover triggered
| Вызван внутрисистемным хэндовером S1.

| 34
| S1 Inter system Handover triggered
| Вызван межсистемным хэндовером S1.

| 35
| X2 Handover triggered
| Вызван хэндовером X2.

| 36
| Redirection towards 1xRTT
| Запрошен разрыв логического, связанного с UE S1-соединения ввиду переадресации на 1xRTT-систему, например, CS fallback или SRVCC, когда необходима приостановка обслуживания в PS-сетях. Для процедуры сообщение может, но не обязано, содержать данные о переадресации.

| 37
| Not supported QCI Value
| Установление E-RAB завершилось ошибкой, поскольку указанный {QCI} не поддерживается.

| 38
| Invalid CSG Id
| Идентификатор CSG, переданный eNodeB назначения, некорректен.

| 39
| Release due to Pre-Emption
| Вызван приоритетным использованием службы.

| 40
| N26 interface not available
| Вызван временной ошибкой интерфейса N26.

| 41
| Insufficient UE Capabilities
| Вызван недостаточными возможностями UE.

| 42
| Maximum bearer pre-emption rate exceeded
| Количество запросов превысило максимально допустимое для bearer-службы.

| 43
| UP integrity protection not possible
| E-RAB не может быть принят ввиду политики обеспечения целостности плоскости пользователя.
|===

[#transport_layer_cause]
===== Transport Layer cause

|===
| Код | Причина | Описание

| 0
| Transport Resource Unavailable
| Требуемые ресурсы для передачи недоступны.

| 1
| Unspecified
| Неописанная ошибка, принадлежащая к группе Transport Network Layer.
|===

[#nas_cause]
===== NAS cause

|===
| Код | Причина | Описание

| 0
| Normal Release
| Вызван нормальным сценарием.

| 1
| Authentication Failure
| Вызван неуспешной аутентификацией.

| 2
| Detach
| Вызвано отключением от сети.

| 3
| Unspecified
| Неописанная ошибка, принадлежащая к группе NAS.

| 4
| CSG Subscription Expiry
| Вызвано тем, что UE перестает быть членом используемой {CSG}.
|===

[#protocol_cause]
===== Protocol cause

|===
| Код | Причина | Описание

| 0
| Transfer Syntax Error
| Полученное сообщение содержит синтаксическую ошибку передачи.

| 1
| Abstract Syntax Error (Reject)
| Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'reject'.

| 2
| Abstract Syntax Error (Ignore And Notify)
| Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'ignore and notify'.

| 3
| Message Not Compatible With Receiver State
| Полученное сообщение не совместимо с текущим состоянием отправителя.

| 4
| Semantic Error
| Полученное сообщение содержит семантическую ошибку.

| 5
| Abstract Syntax Error (Falsely Constructed Message)
| Полученное сообщение содержит IE в неверном порядке или указанные слишком много раз.

| 6
| Unspecified
| Неописанная ошибка, принадлежащая к группе Protocol cause.
|===

[#miscellaneous_cause]
===== Miscellaneous cause

|===
| Код | Причина | Описание

| 0
| Control Processing Overload
| Перегрузка при обработке управлящего трафика.

| 1
| Not Enough User Plane Processing Resources Available
| Недостаточно ресурсов для обработки пользовательского трафика.

| 2
| Hardware Failure
| Вызван аппаратным сбоем.

| 3
| O&M Intervention
| Вызван вмешательством OM.

| 4
| Unspecified Failure
| Неописанная ошибка, не связанная с категориями Radio Network Layer, Transport Network Layer, NAS или Protocol.

| 5
| Unknown PLMN
| Узел MME не обнаружил какую-либо сеть PLMN, предоставленную eNodeB.
|===