---
title: "S1AP Overload"
description: "Журнал s1ap_overload_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле        | Описание                                                 | Тип      |
|-----|-------------|----------------------------------------------------------|----------|
| 1   | DateTime    | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.  | datetime |
| 2   | Processed   | Количество обработанных сообщений.                       | int      |
| 3   | Ignored     | Количество проигнорированных сообщений.                  | int      |
| 4   | MessageType | Тип сообщений.<br>`Initial UE Message`/`Attach Request`. | string   |

```log
DateTime,Processed,Ignored,MessageType
```