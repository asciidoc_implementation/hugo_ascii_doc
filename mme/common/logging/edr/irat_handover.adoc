---
title: Inter-RAT Handover EDR
description: Журнал irat_handover_cdr
type: docs
weight: 20
---
= Inter-RAT Handover EDR
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

=== Описание используемых полей

|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| eNodeB UE ID
| Идентификатор S1-контекста на стороне исходного eNodeB.
| int

| 4
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 5
| IMSI
| Номер IMSI абонента.
| string

| 6
| MSISDN
| Номер MSISDN абонента.
| string

| 7
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 8
| IMEI
| Номер IMEI устройства.
| string

| 9
| Source PLMN
| Исходная PLMN.
| string

| 10
| Source TAC
| Исходный код области отслеживания.
| string

| 11
| Source CellID
| Идентификатор исходной соты.
| string

| 12
| eNodeB IP
| IP-адрес исходного eNodeB.
| ip

| 13
| Target PLMN
| Идентификатор PLMN назначения.
| string

| 14
| Target TAC
| Код области отслеживания назначения.
| int

| 15
| Target LAC
| Код локальной области назначения.
| int

| 16
| Target CellID
| Идентификатор соты назначения.
| int/hex

| 17
| Target RAC
| Код зоны маршрутизации назначения.
| int

| 18
| Target RNC Id
| Идентификатор {RNC} назначения.
| int

| 19
| PS supported
| Флаг поддержки хэндовера для PS-домена при исходящем {SRVCC}. +
NOTE: См. https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.04.00_60/ts_136413v170400p.pdf[3GPP TS 36.413].
| bool

| 20
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 21
| <<event,Event Code>>:ErrorCode
| Код ошибки + Внутренний link:error_code.adoc[код MME] результата.
| object
|===

[#event]
==== События и процедуры, Event

 1:: <<ho_to_geran,HO to GERAN>>
 2:: <<ho_to_utran,HO to UTRAN>>
 3:: <<srvcc_ho_to_geran,SRVCC HO to GERAN>>
 4:: <<srvcc_ho_from_e_utran_to_utran,SRVCC HO to UTRAN>>
 5:: <<ho_from_utran,HO from UTRAN>>
 6:: <<ho_from_geran,HO from GERAN>>
 7:: <<srvcc_ho_from_utran_geran,SRVCC HO from UTRAN/GERAN>>

[#ho_to_geran]
==== HO to GERAN

[source,log]
----
DateTime,HO to GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,1:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не удалось определить IP-адрес узла SGSN.

| 2
| Узел SGSN отказал в подготовке хэндовера.

| 3
| Узел SGSN отказал в переносе данных абонента.

| 4
| Получен отказ в хэндовере.

| 5
| Параметр *_Access Restriction Data_* запрещает использование сетей GERAN.

| 6
| Попытка хэндовера из области {TAC} сети {NB-IoT}.
|===

[#ho_to_utran]
==== HO to UTRAN

[source,log]
----
DateTime,HO to UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,2:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не удалось определить IP-адрес узла SGSN.

| 2
| Узел SGSN отказал в подготовке хэндовера.

| 3
| Узел SGSN отказал в переносе данных абонента.

| 4
| Получен отказ в хэндовере.

| 5
| Параметр *_Access Restriction Data_* запрещает использование сетей GERAN.

| 6
| Попытка хэндовера из области {TAC} сети {NB-IoT}.

| 7
| Слишком длинный UTRAN Transparent Container.
|===

[#srvcc_ho_to_geran]
==== SRVCC HO to GERAN

[source,log]
----
DateTime,SRVCC HO to GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,3:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не найдена голосовая bearer-служба.

| 2
| Не удалось определить IP-адрес сервера MSC.

| 3
| Не удалось определить IP-адрес узла SGSN.

| 4
| Параметр Access Restriction Data запрещает использование сетей GERAN.

| 5
| Получен отказ от сервера MSC.

| 6
| Отсутствует сообщение GTPv2-C: Suspend Notification.

| 7
| Отсутствует сообщение {SRVCC} PS to CS Complete.

| 8
| Получен отказ в хэндовере.

| 9
| Узел SGSN отказал в подготовке хэндовера.
|===

[#srvcc_ho_to_utran]
==== SRVCC HO to UTRAN

[source,log]
----
DateTime,SRVCC HO to UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC, Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,4:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не найдена голосовая bearer-служба.

| 2
| Не удалось определить IP-адрес сервера MSC.

| 3
| Не удалось определить IP-адрес узла SGSN.

| 4
| Параметр *_Access Restriction Data_* запрещает использование сетей UTRAN.

| 5
| Получен отказ от сервера MSC.

| 7
| Не найдено сообщение {SRVCC} PS to CS Complete Notification.

| 8
| Получен отказ в хэндовере.

| 9
| Узел SGSN отказал в подготовке хэндовера.
|===

[#ho_from_utran]
==== HO from UTRAN

[source,log]
----
DateTime,HO from UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,5:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Получен отказ в хэндовере.

| 2
| SGW назначения не принял создание сессии.

| 3
| Нет данных о eNodeB назначения.

| 4
| Получено сообщение S1AP: HANDOVER FAILURE.

| 5
| eNodeB не приняла ни одной bearer-службы по умолчанию.

| 6
| Не найдено сообщение S1AP: HANDOVER NOTIFY.

| 7
| Не найдено сообщение GTPv2-C: Forward Relocation Complete Acknowledge.

| 8
| Ошибка процедуры Modify-Bearer.

| 9
| Не найдено сообщение TAU-Request.

| 10
| Не удалось извлечь идентификаторы IMSI, {IMEISV}.

| 11
| Попытка хэндовера из области {TAC} сети {NB-IoT}.
|===

[#ho_from_geran]
==== HO from GERAN

[source,log]
----
DateTime,HO from GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,6:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Получен отказ в хэндовере.

| 2
| SGW назначения не принял создание сессии.

| 3
| Нет данных о eNodeB назначения.

| 4
| Получено сообщение S1AP: HANDOVER FAILURE.

| 5
| eNodeB не приняла ни одной bearer-службы по умолчанию.

| 6
| Не найдено сообщение S1AP: HANDOVER NOTIFY.

| 7
| Не найдено сообщение GTPv2-C: Forward Relocation Complete Acknowledge.

| 8
| Ошибка процедуры Modify-Bearer.

| 9
| Не найдено сообщение TAU-Request.

| 10
| Не удалось извлечь идентификаторы IMSI, {IMEISV}.
|===

[#srvcc_ho_from_utran_geran]
==== SRVCC HO from UTRAN/GERAN

[source,log]
----
DateTime,SRVCC HO from UTRAN/GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,7:ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не удалось определить адрес предыдущего узла MME/SGSN.

| 2
| Нет данных о eNodeB назначения.

| 3
| Ошибка получения предыдущего контекста.

| 4
| Ошибка на новом узле SGW.

| 5
| eNodeB назначения не приняла ни одной bearer-службы.

| 6
| Не получено сообщение S1AP: HANDOVER NOTIFY.

| 7
| Не получено сообщение {SRVCC} CS to PS Complete Acknowledgement.

| 8
| Ошибка модификации bearer-служб.

| 9
| В сообщении {SRVCC} CS to PS Request отсутствуют данные об eNodeB назначения.

| 10
| Получено сообщение S1AP: HANDOVER FAILURE.

| 11
| Не получено сообщение S1AP: HANDOVER REQUEST ACKNOWLEDGE.

| 12
| Получено сообщение {SRVCC} CS to PS Cancel Notification.
|===