---
title: HTTP EDR
description: Журнал http_cdr
type: docs
weight: 20
---
= HTTP EDR
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

=== Описание используемых полей

|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| MME UE ID
| Идентификатор S1-контекста на узле MME.
| int

| 4
| IMSI
| Номер IMSI абонента.
| string

| 5
| MSISDN
| Номер MSISDN абонента.
| string

| 6
| GUTI
| Глобальный уникальный временный идентификатор абонента.
| string

| 7
| PLMN
| Идентификатор PLMN.
| string

| 8
| CellID
| Идентификатор соты.
| int/hex

| 9
| IMEI
| Номер IMEI устройства.
| string

| 10
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 11
| ErrorCode
| Внутренний link:error_code.adoc[код MME] результата.
| object
|===

[#event]
==== События и процедуры, Event

* <<deact_bearer,Deact bearer>>;
* <<detach,Detach>>;
* <<disconnect_enodeb,Disconnect eNodeB>>;
* <<get_profile,Get Profile>>;
* <<get_db_status,Get DB Status>>;
* <<get_gtp_peers,Get GTP Peers>>;
* <<get_s1_peers,Get S1 Peers>>;
* <<get_sgs_peers,Get SGs Peers>>;
* <<clear_dns_cache,Clear DNS Cache>>;
* <<ue_enable_trace,UE enable trace>>;
* <<ue_disable_trace,UE disable trace>>;
* <<get_metrics,Get Metrics>>;
* <<reset_metrics,Reset Metrics>>.

[#deact_bearer]
==== Deact Bearer

[source,log]
----
DateTime,Deact bearer,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Не удалось получить идентификатор bearer-службы.

| 2
| Неизвестный идентификатор bearer-службы.

| 3
| Ошибка процедуры Paging.

| 4
| Ошибка деактивации bearer-службы.
|===

[#detach]
==== Detach

[source,log]
----
DateTime,Detach,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Ошибка процедуры Paging.

| 2
| Ошибка процедуры Detach.
|===

[#disconnect_enodeb]
==== Disconnect eNodeB

[source,log]
----
DateTime,Disconnect eNodeB,IP,Duration,ErrorCode
----

[#get_profile]
==== Get Profile

[source,log]
----
DateTime,GetProfile,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
----

[#get_db_status]
==== Get DB Status

[source,log]
----
DateTime,Get DB Status,Duration,ErrorCode
----

[#get_gtp_peers]
==== Get GTP Peers

[source,log]
----
DateTime,Get GTP Peers,Duration,ErrorCode
----

[#get_s1_peers]
==== Get S1 Peers

[source,log]
----
DateTime,Get S1 Peers,Duration,ErrorCode
----

[#get_sgs_peers]
==== Get SGs Peers

[source,log]
----
DateTime,Get SGS Peers,Duration,ErrorCode
----

[#clear_dns_cache]
==== Clear DNS Cache

[source,log]
----
DateTime,Clear DNS Cache,Duration,ErrorCode
----

[#ue_enable_trace]
==== UE enable trace

[source,log]
----
DateTime,UE enable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Абонент уже находится в списке отслеживания.

| 2
| Не указан IP-адрес узла [[Trace Collection Entity]]TCE.

| 3
| Ошибка процедуры Paging.

| 4
| Не найдены свободные идентификаторы Trace ID.
|===

[#ue_disable_trace]
==== UE disable trace

[source,log]
----
DateTime,UE disable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
----

Локальные коды ошибок:

|===
| N | Описание

| 1
| Ошибка процедуры Paging.

| 2
| Идентификатор E-UTRAN Trace ID не найден для абонента.
|===

[#get_metrics]
==== Get Metrics

[source,log]
----
DateTime,Get Metrics,Duration,ErrorCode
----

[#reset_metrics]
==== Reset Metrics

[source,log]
----
DateTime,Reset Metrics,Duration,ErrorCode
----