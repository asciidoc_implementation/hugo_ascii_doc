---
title: "S11 Interface"
description: "Статистика процедур S11"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s11Interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S11.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf).

| Tx/Rx | Метрика                                                                                                 | Описание                                                                                                                                                                                  | Группа |
|-------|---------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| Rx    | <a name="createBearerRequest">createBearerRequest</a>                                                   | Количество сообщений GTPv2-C: Create Bearer Request в процедуре активации выделенной bearer-службы.                                                                                       |        |
| Tx    | <a name="createBearerResponse">createBearerResponse</a>                                                 | Количество сообщений GTPv2-C: Create Bearer Response.                                                                                                                                     |        |
| Tx    | <a name="createBearerResponse16">createBearerResponse16</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#16 Request accepted".                                                                                                   |        |
| Tx    | <a name="createBearerResponse17">createBearerResponse17</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#17 Request accepted partially".                                                                                         |        |
| Tx    | <a name="createBearerResponse64">createBearerResponse64</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#64 Context Not Found".                                                                                                  |        |
| Tx    | <a name="createBearerResponse70">createBearerResponse70</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#70 Mandatory IE missing".                                                                                               |        |
| Tx    | <a name="createBearerResponse72">createBearerResponse72</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#72 System failure".                                                                                                     |        |
| Tx    | <a name="createBearerResponse73">createBearerResponse73</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#73 No resources available".                                                                                             |        |
| Tx    | <a name="createBearerResponse87">createBearerResponse87</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#87 UE not responding".                                                                                                  |        |
| Tx    | <a name="createBearerResponse88">createBearerResponse88</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#88 UE refuses".                                                                                                         |        |
| Tx    | <a name="createBearerResponse90">createBearerResponse90</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#90 Unable to page UE".                                                                                                  |        |
| Tx    | <a name="createBearerResponse94">createBearerResponse94</a>                                             | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#94 Request rejected (reason not specified)".                                                                            |        |
| Tx    | <a name="createBearerResponse102">createBearerResponse102</a>                                           | Количество сообщений GTPv2-C: Create Bearer Response с причиной "#102 Unable to page UE due to Suspension".                                                                               |        |
| Tx    | <a name="modifyBearerRequest">modifyBearerRequest</a>                                                   | Количество сообщений GTPv2-C: Modify Bearer Request.                                                                                                                                      |        |
| Rx    | <a name="modifyBearerResponse">modifyBearerResponse</a>                                                 | Количество сообщений GTPv2-C: Modify Bearer Response.                                                                                                                                     |        |
| Rx    | <a name="modifyBearerResponse16">modifyBearerResponse16</a>                                             | Количество сообщений GTPv2-C: Modify Bearer Response с причиной "#16 Request accepted".                                                                                                   |        |
| Rx    | <a name="modifyBearerResponse64">modifyBearerResponse64</a>                                             | Количество сообщений GTPv2-C: Modify Bearer Response с причиной "#64 Context Not Found".                                                                                                  |        |
| Rx    | <a name="modifyBearerResponse68">modifyBearerResponse68</a>                                             | Количество сообщений GTPv2-C: Modify Bearer Response с причиной "#68 Service not supported".                                                                                              |        |
| Rx    | <a name="modifyBearerResponse70">modifyBearerResponse70</a>                                             | Количество сообщений GTPv2-C: Modify Bearer Response с причиной "#70 Mandatory IE missing".                                                                                               |        |
| Rx    | <a name="modifyBearerResponse72">modifyBearerResponse72</a>                                             | Количество сообщений GTPv2-C: Modify Bearer Response с причиной "#72 System failure".                                                                                                     |        |
| Rx    | <a name="deleteBearerRequest">deleteBearerRequest</a>                                                   | Количество сообщений GTPv2-C: Delete Bearer Request.                                                                                                                                      |        |
| Tx    | <a name="deleteBearerResponse">deleteBearerResponse</a>                                                 | Количество сообщений GTPv2-C: Delete Bearer Response.                                                                                                                                     |        |
| Tx    | <a name="deleteBearerResponse16">deleteBearerResponse16</a>                                             | Количество сообщений GTPv2-C: Delete Bearer Response с причиной "#16 Request accepted".                                                                                                   |        |
| Tx    | <a name="createSessionRequest">createSessionRequest</a>                                                 | Количество сообщений GTPv2-C: Create Session Request.                                                                                                                                     |        |
| Rx    | <a name="createSessionResponse">createSessionResponse</a>                                               | Количество сообщений GTPv2-C: Create Session Response.                                                                                                                                    |        |
| Rx    | <a name="createSessionResponse16">createSessionResponse16</a>                                           | Количество сообщений GTPv2-C: Create Session Response с причиной "#16 Request accepted".                                                                                                  |        |
| Rx    | <a name="createSessionResponse100">createSessionResponse100</a>                                         | Количество сообщений GTPv2-C: Create Session Response с причиной "#100 Remote peer not responding".                                                                                       |        |
| Tx    | <a name="deleteSessionRequest">deleteSessionRequest</a>                                                 | Количество сообщений GTPv2-C: Delete Session Request.                                                                                                                                     |        |
| Rx    | <a name="deleteSessionResponse">deleteSessionResponse</a>                                               | Количество сообщений GTPv2-C: Delete Session Response.                                                                                                                                    |        |
| Rx    | <a name="deleteSessionResponse16">deleteSessionResponse16</a>                                           | Количество сообщений GTPv2-C: Delete Session Response с причиной "#16 Request accepted".                                                                                                  |        |
| Rx    | <a name="deleteSessionResponse64">deleteSessionResponse64</a>                                           | Количество сообщений GTPv2-C: Delete Session Response с причиной "#64 Context Not Found".                                                                                                 |        |
| Tx    | <a name="bearerResourceCommand">bearerResourceCommand</a>                                               | Количество сообщений GTPv2-C: Bearer Resource Command.                                                                                                                                    |        |
| Rx    | <a name="bearerResourceFailureIndication">bearerResourceFailureIndication</a>                           | Количество сообщений GTPv2-C: Bearer Resource Failure Indication.                                                                                                                         |        |
| Rx    | <a name="downlinkDataNotification">downlinkDataNotification</a>                                         | Количество сообщений GTPv2-C: Downlink Data Notification.                                                                                                                                 |        |
| Tx    | <a name="downlinkDataNotificationAck">downlinkDataNotificationAck</a>                                   | Количество сообщений GTPv2-C: Downlink Data Notification ACK.                                                                                                                             |        |
| Tx    | <a name="downlinkDataNotificationAck16">downlinkDataNotificationAck16</a>                               | Количество сообщений GTPv2-C: Downlink Data Notification ACK с причиной "#16 Request accepted".                                                                                           |        |
| Rx    | <a name="downlinkDataNotificationFailureInd">downlinkDataNotificationFailureInd</a>                     | Количество сообщений GTPv2-C: Downlink Data Notification Failure Indication.                                                                                                              |        |
| Tx    | <a name="downlinkDataNotificationFailureInd87">downlinkDataNotificationFailureInd87</a>                 | Количество сообщений GTPv2-C: Downlink Data Notification Failure Indication с причиной "#87 UE not responding".                                                                           |        |
| Tx    | <a name="downlinkDataNotificationFailureInd89">downlinkDataNotificationFailureInd89</a>                 | Количество сообщений GTPv2-C: Downlink Data Notification Failure Indication с причиной "#89 Service denied".                                                                              |        |
| Rx    | <a name="updateBearerRequest">updateBearerRequest</a>                                                   | Количество сообщений GTPv2-C: Update Bearer Request.                                                                                                                                      |        |
| Tx    | <a name="updateBearerResponse">updateBearerResponse</a>                                                 | Количество сообщений GTPv2-C: Update Bearer Response.                                                                                                                                     |        |
| Tx    | <a name="updateBearerResponse16">updateBearerResponse16</a>                                             | Количество сообщений GTPv2-C: Update Bearer Response  с причиной "#16 Request accepted".                                                                                                  |        |
| Tx    | <a name="deleteBearerCommand">deleteBearerCommand</a>                                                   | Количество сообщений GTPv2-C: Delete Bearer Command.                                                                                                                                      |        |
| Rx    | <a name="deleteBearerFailureIndication">deleteBearerFailureIndication</a>                               | Количество сообщений GTPv2-C: Delete Bearer Failure Indication.                                                                                                                           |        |
| Tx    | <a name="createIndirectDataForwardingTunnelRequest">createIndirectDataForwardingTunnelRequest</a>       | Количество сообщений GTPv2-C: Create Indirect Data Forwarding Tunnel Request.                                                                                                             |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse">createIndirectDataForwardingTunnelResponse</a>     | Количество сообщений GTPv2-C: Create Indirect Data Forwarding Tunnel Response.                                                                                                            |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse16">createIndirectDataForwardingTunnelResponse16</a> | Количество сообщений GTPv2-C: Create Indirect Data Forwarding Tunnel Response с причиной "#16 Request accepted".                                                                          |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse17">createIndirectDataForwardingTunnelResponse17</a> | Количество сообщений GTPv2-C: Create Indirect Data Forwarding Tunnel Response с причиной "#17 Request accepted partially".                                                                |        |
| Tx    | <a name="deleteIndirectDataForwardingTunnelRequest">deleteIndirectDataForwardingTunnelRequest</a>       | Количество сообщений GTPv2-C: Delete Indirect Data Forwarding Tunnel Request.                                                                                                             |        |
| Rx    | <a name="deleteIndirectDataForwardingTunnelResponse">deleteIndirectDataForwardingTunnelResponse</a>     | Количество сообщений GTPv2-C: Delete Indirect Data Forwarding Tunnel Response.                                                                                                            |        |
| Rx    | <a name="deleteIndirectDataForwardingTunnelResponse16">deleteIndirectDataForwardingTunnelResponse16</a> | Количество сообщений GTPv2-C: Delete Indirect Data Forwarding Tunnel Response с причиной "#16 Request accepted".                                                                          |        |
| Tx    | <a name="modifyAccessBearersRequest">modifyAccessBearersRequest</a>                                     | Количество сообщений GTPv2-C: Modify Access Bearers Request.                                                                                                                              |        |
| Rx    | <a name="modifyAccessBearersResponse">modifyAccessBearersResponse</a>                                   | Количество сообщений GTPv2-C: Modify Access Bearers Response.                                                                                                                             |        |
| Tx    | <a name="releaseAccessBearersRequest">releaseAccessBearersRequest</a>                                   | Количество сообщений GTPv2-C: Release Access Bearers Request.                                                                                                                             |        |
| Rx    | <a name="releaseAccessBearersResponse">releaseAccessBearersResponse</a>                                 | Количество сообщений GTPv2-C: Release Access Bearers Response в процедурах S1 Release.                                                                                                    |        |
| Rx    | <a name="releaseAccessBearersResponse16">releaseAccessBearersResponse16</a>                             | Количество сообщений GTPv2-C: Release Access Bearers Response в процедурах S1 Release с причиной "#16 Request accepted".                                                                  |        |
| Rx    | <a name="releaseAccessBearersResponse64">releaseAccessBearersResponse64</a>                             | Количество сообщений GTPv2-C: Release Access Bearers Response в процедурах S1 Release с причиной "#64 Context Not Found".                                                                 |        |
| Tx    | <a name="modifyBearerCommand">modifyBearerCommand</a>                                                   | Количество сообщений GTPv2-C: Modify Bearer Command в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) или при `SQCI = 1` в сообщении Context Response. |        |
| Rx    | <a name="modifyBearerFailureIndication">modifyBearerFailureIndication</a>                               | Количество сообщений GTPv2-C: Modify Bearer Failure Indication, отправленных при ошибке в процедуре HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD).              |        |
| Tx    | <a name="suspendNotification">suspendNotification</a>                                                   | Количество сообщений GTPv2-C: Suspend Notification в процедурах CSFB.                                                                                                                     |        |
| Rx    | <a name="suspendAcknowledge">suspendAcknowledge</a>                                                     | Количество сообщений GTPv2-C: Suspend Notification ACK в процедурах CSFB.                                                                                                                 |        |

**Примечание.** Описание причин ошибок GTP см. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf).

#### Пример файла ####

```csv
rx,createBearerRequest,,0
tx,createBearerResponse,,0
tx,bearerResourceCommand,,0
rx,bearerResourceFailureIndication,,0
tx,modifyBearerRequest,,67
rx,modifyBearerResponse,,67
rx,modifyBearerResponse16,,67
rx,deleteBearerRequest,,9
tx,deleteBearerResponse,,9
tx,deleteBearerResponse16,,1
tx,deleteBearerResponse64,,8
tx,modifyBearerCommand,,0
rx,modifyBearerFailureIndication,,0
rx,updateBearerRequest,,0
tx,updateBearerResponse,,0
tx,deleteBearerCommand,,0
rx,deleteBearerFailureIndication,,0
tx,releaseAccessBearersRequest,,89
rx,releaseAccessBearersResponse,,89
rx,releaseAccessBearersResponse16,,89
tx,createSessionRequest,,53
rx,createSessionResponse,,53
rx,createSessionResponse16,,53
tx,deleteSessionRequest,,40
rx,deleteSessionResponse,,40
rx,deleteSessionResponse16,,40
rx,downlinkDataNotification,,65
tx,downlinkDataNotificationAck,,64
tx,downlinkDataNotificationAck16,,64
tx,downlinkDataNotificationFailureInd,,8
tx,downlinkDataNotificationFailureInd87,,8
tx,createIndirectDataForwardingTunnelRequest,,0
rx,createIndirectDataForwardingTunnelResponse,,0
tx,deleteIndirectDataForwardingTunnelRequest,,0
rx,deleteIndirectDataForwardingTunnelResponse,,0
tx,suspendNotification,,0
```