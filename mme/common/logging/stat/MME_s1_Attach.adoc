---
title: S1 Attach
description: Статистика процедур S1 Attach
type: docs
weight: 20
---
= S1 Attach
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

Файл **<node_name>\_MME-s1Attach\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S1 по процедурам Attach.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="1,4,4,2"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Rx
| [[attachRequest]]attachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach Type = EPS attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachSuccess]]attachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach Type = EPS attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail]]attachFail
| Количество сообщений S1 Attach Reject.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail2]]attachFail2
| Количество сообщений S1 Attach Reject с причиной "#2 IMSI unknown in HSS".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail3]]attachFail3
| Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail6]]attachFail6
| Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail7]]attachFail7
| Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail8]]attachFail8
| Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail11]]attachFail11
| Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail12]]attachFail12
| Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail13]]attachFail13
| Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail14]]attachFail14
| Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail15]]attachFail15
| Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail17]]attachFail17
| Количество сообщений S1 Attach Reject с причиной "#17 Network failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail19]]attachFail19
| Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail25]]attachFail25/a>
| Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail111]]attachFail111
| Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".
| TAI:Value, IMSIPLMN:Value

| Rx
| [[combinedAttachRequest]]combinedAttachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachSuccess]]combinedAttachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail]]combinedAttachFail
| Количество сообщений S1 Attach Reject в ответ на  Attach Request с IE *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail3]]combinedAttachFail3
| Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail6]]combinedAttachFail6
| Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail7]]combinedAttachFail7
| Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail8]]combinedAttachFail8
| Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail11]]combinedAttachFail11
| Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail12]]combinedAttachFail12
| Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail13]]combinedAttachFail13
| Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail14]]combinedAttachFail14
| Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail15]]combinedAttachFail15
| Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail19]]combinedAttachFail19
| Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail25]]combinedAttachFail25
| Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail111]]combinedAttachFail111
| Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".
| TAI:Value, IMSIPLMN:Value

| Rx
| [[emergencyAttachRequest]]emergencyAttachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[emergencyAttachSuccess]]emergencyAttachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[emergencyAttachFail]]emergencyAttachFail
| Количество сообщений S1 Attach Reject с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value
|===

NOTE: Описание причин ошибок EMM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| TAI
| Идентификатор области отслеживания. Формат: +
*_<plmn_id><tac>_*.
| hex

| <plmn_id>
| Идентификатор сети PLMN.
| int

| <tac>
| Код области отслеживания.
| hex

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
TAI:2500103E8
IMSIPLMN:25099
----

.Пример

[source,csv]
----
rx,attachRequest,,288
rx,attachRequest,IMSIPLMN:20893,287
rx,attachRequest,TAI:208930001,288
tx,attachSuccess,,276
tx,attachSuccess,IMSIPLMN:20893,276
tx,attachSuccess,TAI:208930001,276
tx,attachFail17,,15
tx,attachFail17,IMSIPLMN:20893,14
tx,attachFail17,TAI:208930001,15
rx,combinedAttachRequest,,7
rx,combinedAttachRequest,IMSIPLMN:00101,2
rx,combinedAttachRequest,IMSIPLMN:25020,1
rx,combinedAttachRequest,IMSIPLMN:25060,2
rx,combinedAttachRequest,IMSIPLMN:99999,2
rx,combinedAttachRequest,TAI:001010001,7
tx,combinedAttachSuccess,,5
tx,combinedAttachSuccess,IMSIPLMN:00101,2
tx,combinedAttachSuccess,IMSIPLMN:99999,3
tx,combinedAttachSuccess,TAI:001010001,4
tx,combinedAttachSuccess,TAI:999990001,1
tx,combinedAttachFail12,,3
tx,combinedAttachFail12,IMSIPLMN:25020,1
tx,combinedAttachFail12,IMSIPLMN:25060,2
tx,combinedAttachFail12,TAI:001010001,3
rx,emergencyAttachRequest,,0
tx,emergencyAttachSuccess,,0
----