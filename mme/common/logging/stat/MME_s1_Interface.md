---
title: "S1AP Interface"
description: "Статистика процедур S1AP"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур S1AP.

### Описание параметров ###

Подробную информацию см. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf). 

| Tx/Rx | Метрика                                                                   | Описание                                                                                                            | Группа                 |
|-------|---------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------|------------------------|
| Rx    | <a name="s1SetupRequest">s1SetupRequest</a>                               | Количество сообщений S1 Setup Request.                                                                              | TAI:Value              |
| Tx    | <a name="s1SetupSuccess">s1SetupSuccess</a>                               | Количество сообщений S1 Setup Response.                                                                             | TAI:Value              |
| Rx    | <a name="eNodeBInitS1ResetRequest">eNodeBInitS1ResetRequest</a>           | Количество сообщений S1 Reset Request (eNB-initiated).                                                              | TAI:Value              |
| Tx    | <a name="eNodeBInitS1ResetSuccess">eNodeBInitS1ResetSuccess</a>           | Количество сообщений S1 Reset Acknowledge (eNB-initiated).                                                          | TAI:Value              |
| Rx    | <a name="eNbConfigurationUpdateRequest">eNbConfigurationUpdateRequest</a> | Количество сообщений S1AP: ENB CONFIGURATION UPDATE REQUEST.                                                        | TAI:Value              |
| Tx    | <a name="eNbConfigurationUpdateSuccess">eNbConfigurationUpdateSuccess</a> | Количество сообщений S1AP: ENB CONFIGURATION UPDATE ACKNOWLEDGE.                                                    | TAI:Value              |
| Tx    | <a name="mmeConfigurationUpdateRequest">mmeConfigurationUpdateRequest</a> | Количество сообщений S1AP: MME CONFIGURATION UPDATE REQUEST.                                                        | TAI:Value              |
| Rx    | <a name="mmeConfigurationUpdateSuccess">mmeConfigurationUpdateSuccess</a> | Количество сообщений S1AP: MME CONFIGURATION UPDATE ACKNOWLEDGE.                                                    | TAI:Value              |
| Tx    | <a name="eRabSetupRequest">eRabSetupRequest</a>                           | Количество сообщений S1AP: E-RAB SETUP REQUEST.                                                                     | TAI:Value              |
| Rx    | <a name="eRabSetupResponse">eRabSetupResponse</a>                         | Количество сообщений S1AP: E-RAB SETUP RESPONSE.                                                                    | TAI:Value              |
| Rx    | <a name="eRabSetupResponse0_25">eRabSetupResponse0_25</a>                 | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Radio resources not available".                         | TAI:Value              |
| Rx    | <a name="eRabSetupResponse0_28">eRabSetupResponse0_28</a>                 | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Failure in Radio Interface Procedure".                  | TAI:Value              |
| Rx    | <a name="eRabSetupResponse1_0">eRabSetupResponse1_0</a>                   | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Transport Resource Unavailable".                        | TAI:Value              |
| Rx    | <a name="eRabSetupResponse0_26">eRabSetupResponse0_26</a>                 | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Invalid QoS combination".                               | TAI:Value              |
| Rx    | <a name="eRabSetupResponse0_37">eRabSetupResponse0_37</a>                 | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Not supported QCI Value".                               | TAI:Value              |
| Tx    | <a name="eRabModifyRequest">eRabModifyRequest</a>                         | Количество сообщений S1AP: E-RAB MODIFY REQUEST.                                                                    | TAI:Value              |
| Rx    | <a name="eRabModifyResponse">eRabModifyResponse</a>                       | Количество сообщений S1AP: E-RAB MODIFY RESPONSE.                                                                   | TAI:Value              |
| Tx    | <a name="eRabReleaseCommand">eRabReleaseCommand</a>                       | Количество сообщений S1AP: E-RAB RELEASE COMMAND.                                                                   | TAI:Value              |
| Rx    | <a name="eRabReleaseResponse">eRabReleaseResponse</a>                     | Количество сообщений S1AP: E-RAB RELEASE RESPONSE.                                                                  | TAI:Value              |
| Rx    | <a name="eRabReleaseIndication">eRabReleaseIndication</a>                 | Количество сообщений S1AP: E-RAB RELEASE INDICATION.                                                                | TAI:Value              |
| Rx    | <a name="eRabModificationIndication">eRabModificationIndication</a>       | Количество сообщений S1AP: E-RAB MODIFICATION INDICATION.                                                           | TAI:Value              |
| Tx    | <a name="eRabModificationConfirm">eRabModificationConfirm</a>             | Количество сообщений S1AP: E-RAB MODIFICATION CONFIRM.                                                              | TAI:Value              |
| Tx    | <a name="initialContextSetupRequest">initialContextSetupRequest</a>       | Количество сообщений S1AP: INITIAL CONTEXT SETUP REQUEST.                                                           | TAI:Value              |
| Rx    | <a name="initialContextSetupResponse">initialContextSetupResponse</a>     | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE.                                                          | TAI:Value              |
| Rx    | <a name="initialContextSetupFailure">initialContextSetupFailure</a>       | Количество сообщений S1AP: INITIAL CONTEXT SETUP FAILURE.                                                           | TAI:Value, Cause:Value |
| Rx    | <a name="ueContextReleaseRequest">ueContextReleaseRequest</a>             | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST.                                                              | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest0_20">ueContextReleaseRequest0_20</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "User Inactivity".                                 | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest0_21">ueContextReleaseRequest0_21</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "Radio Connection With UE Lost".                   | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest0_23">ueContextReleaseRequest0_23</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "CS Fallback Triggered".                           | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest0_24">ueContextReleaseRequest0_24</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "UE Not Available for PS Service".                 | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest1_0">ueContextReleaseRequest1_0</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "Transport resource unavailable".                  | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequest2_0">ueContextReleaseRequest2_0</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "Normal Release".                                  | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand">ueContextReleaseCommand</a>             | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND.                                                              | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand0_2">ueContextReleaseCommand0_2</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "Radio Network:Successful Handover".             | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand0_20">ueContextReleaseCommand0_20</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "Radio Network:User Inactivity".                 | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand0_23">ueContextReleaseCommand0_23</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "Radio Network:CS Fallback triggered".           | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand0_24">ueContextReleaseCommand0_24</a>     | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "Radio Network:UE Not Available for PS Service". | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand2_0">ueContextReleaseCommand2_0</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "NAS:Normal Release".                            | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand2_1">ueContextReleaseCommand2_1</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "NAS:Authentication Failure".                    | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand2_2">ueContextReleaseCommand2_2</a>       | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с S1AP Cause "NAS:Deregister".                                | TAI:Value              |
| Rx    | <a name="ueContextReleaseComplete">ueContextReleaseComplete</a>           | Количество сообщений S1AP: UE CONTEXT RELEASE COMPLETE.                                                             | TAI:Value              |
| Tx    | <a name="ueContextModificationRequest">ueContextModificationRequest</a>   | Количество сообщений S1AP: UE CONTEXT MODIFICATION REQUEST.                                                         | TAI:Value              |
| Rx    | <a name="ueContextModificationResponse">ueContextModificationResponse</a> | Количество сообщений S1AP: UE CONTEXT MODIFICATION RESPONSE.                                                        | TAI:Value              |
| Rx    | <a name="ueContextModificationFailure">ueContextModificationFailure</a>   | Количество сообщений S1AP: UE CONTEXT MODIFICATION FAILURE.                                                         | TAI:Value              |
| Tx    | <a name="overloadStart">overloadStart</a>                                 | Количество сообщений S1AP: OVERLOAD START.                                                                          | TAI:Value              |
| Tx    | <a name="overloadStop">overloadStop</a>                                   | Количество сообщений S1AP: OVERLOAD STOP.                                                                           | TAI:Value              |
|       | <a name="numberOfEnodeb">numberOfEnodeb</a>                               | Количество станций eNodeB.                                                                                          | TAI:Value              |

**Примечание.** В названии метрики также может быть указана группа причин неуспешного запроса и сама причина.

Формат записи названия метрики: `<metrics_name><cause_type>_<cause_code>`.

Пример: `eRabSetupResponse0_41` - метрика [eRabSetupResponse](#eRabSetupResponse), группа причин 
[Radio Network Layer cause](#radio-network-layer-cause) - 0, причина [Insufficient UE Capabilities](#example) - 41.

Подробную информацию см. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf) и
раздел [Коды групп причин и коды причины](#causes)

### Группа ###

| Название    | Описание                                                         | Тип    |
|-------------|------------------------------------------------------------------|--------|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex    |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int    |
| \<tac\>     | Код области отслеживания.                                        | hex    |
| Cause       | Имя ошибки.                                                      | string |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
rx,s1SetupRequest,,7
rx,s1SetupRequest,TAI:001010001,1
rx,s1SetupRequest,TAI:208930001,5
rx,s1SetupRequest,TAI:460000001,1
tx,s1SetupSuccess,,7
tx,s1SetupSuccess,TAI:001010001,1
tx,s1SetupSuccess,TAI:208930001,5
tx,s1SetupSuccess,TAI:460000001,1
rx,eNodeBInitS1ResetRequest,,1
rx,eNodeBInitS1ResetRequest,TAI:001010001,1
rx,eNodeBInitS1ResetRequest,TAI:001010006,1
tx,eNodeBInitS1ResetSuccess,,1
tx,eNodeBInitS1ResetSuccess,TAI:001010001,1
tx,eNodeBInitS1ResetSuccess,TAI:001010006,1
rx,eNbConfigurationUpdateRequest,,0
tx,eNbConfigurationUpdateSuccess,,0
tx,mmeConfigurationUpdateRequest,,0
rx,mmeConfigurationUpdateSuccess,,0
tx,eRabSetupRequest,,9
tx,eRabSetupRequest,TAI:001010001,9
rx,eRabSetupResponse,,9
rx,eRabSetupResponse,TAI:001010001,9
rx,eRabSetupResponse0_0,,3
rx,eRabSetupResponse0_0,TAI:001010001,3
tx,eRabModifyRequest,,0
rx,eRabModifyResponse,,0
tx,eRabReleaseCommand,,2
tx,eRabReleaseCommand,TAI:001010001,2
rx,eRabReleaseResponse,,2
rx,eRabReleaseResponse,TAI:001010001,2
rx,eRabModificationIndication,,0
tx,eRabModificationConfirm,,0
tx,initialContextSetupRequest,,151
tx,initialContextSetupRequest,TAI:001010001,71
tx,initialContextSetupRequest,TAI:999990001,80
rx,initialContextSetupResponse,,151
rx,initialContextSetupResponse,TAI:001010001,71
rx,initialContextSetupResponse,TAI:999990001,80
rx,initialContextSetupFailure,,0
rx,ueContextReleaseRequest,,153
rx,ueContextReleaseRequest,TAI:001010001,68
rx,ueContextReleaseRequest,TAI:208930001,4
rx,ueContextReleaseRequest,TAI:999990001,81
rx,ueContextReleaseRequest0_20,,153
rx,ueContextReleaseRequest0_20,TAI:001010001,68
rx,ueContextReleaseRequest0_20,TAI:208930001,4
rx,ueContextReleaseRequest0_20,TAI:999990001,81
tx,ueContextReleaseCommand,,165
tx,ueContextReleaseCommand,TAI:001010001,73
tx,ueContextReleaseCommand,TAI:001010051,1
tx,ueContextReleaseCommand,TAI:208930001,10
tx,ueContextReleaseCommand,TAI:999990001,81
tx,ueContextReleaseCommand0_20,,153
tx,ueContextReleaseCommand0_20,TAI:001010001,68
tx,ueContextReleaseCommand0_20,TAI:208930001,4
tx,ueContextReleaseCommand0_20,TAI:999990001,81
tx,ueContextReleaseCommand2_0,,6
tx,ueContextReleaseCommand2_0,TAI:208930001,6
tx,ueContextReleaseCommand2_1,,1
tx,ueContextReleaseCommand2_1,TAI:001010051,1
tx,ueContextReleaseCommand2_2,,5
tx,ueContextReleaseCommand2_2,TAI:001010001,5
rx,ueContextReleaseComplete,,165
rx,ueContextReleaseComplete,TAI:001010001,73
rx,ueContextReleaseComplete,TAI:001010051,1
rx,ueContextReleaseComplete,TAI:208930001,10
rx,ueContextReleaseComplete,TAI:999990001,81
tx,ueContextModificationRequest,,0
rx,ueContextModificationResponse,,0
,numberOfEnodeb,,8
,numberOfEnodeb,TAI:001010001,5
,numberOfEnodeb,TAI:001010006,2
,numberOfEnodeb,TAI:001010051,2
,numberOfEnodeb,TAI:001060001,1
,numberOfEnodeb,TAI:208930001,1
,numberOfEnodeb,TAI:250510001,1
,numberOfEnodeb,TAI:466920051,2
,numberOfEnodeb,TAI:999120001,1
,numberOfEnodeb,TAI:999990001,3
,numberOfEnodeb,TAI:999990006,1
```

### Коды групп причин и коды причин {#causes}

Группы причин:

* 0 — [Radio Network Layer cause](#radio-network-layer-cause), причины уровня радиосети;
* 1 — [Transport Layer cause](#transport-layer-cause), причины уровня передачи;
* 2 — [NAS cause](#nas-cause), причины уровня NAS;
* 3 — [Protocol cause](#protocol-cause), причины уровня протокола;
* 4 — [Miscellaneous cause](#miscellaneous-cause), остальные причины.

#### Radio Network Layer cause {#radio-network-layer-cause}

| Код | Причина                                                         | Описание                                                                                                                                                                                                                                                                                                         |
|-----|-----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0   | Unspecified                                                     | Неописанная ошибка.                                                                                                                                                                                                                                                                                              |
| 1   | TX2RELOCOverall Expiry                                          | Таймер, контролирующий хэндовер X2, нештатно истек.                                                                                                                                                                                                                                                              |
| 2   | Successful Handover                                             | Хэндовер осуществлен успешно.                                                                                                                                                                                                                                                                                    |
| 3   | Release due to E-UTRAN generated reason                         | Причина отбоя вызвана сетью E-UTRAN.                                                                                                                                                                                                                                                                             |
| 4   | Handover Cancelled                                              | Вызвана отмена хэндовера.                                                                                                                                                                                                                                                                                        |
| 5   | Partial Handover                                                | Вызвана отмена хэндовера, поскольку в сообщении S1AP: HANDOVER COMMAND от MME задано поле E-RABs to Release List, и исходная eNodeB оценила, что непрерывность услуги будет лучше без хэндовера к указанному eNodeB назначения.                                                                                  |
| 6   | Handover Failure In Target EPC/eNB Or Target System             | Хэндовер завершился неуспешно ввиду ошибки EPC/eNodeB назначения.                                                                                                                                                                                                                                                |
| 7   | Handover Target not allowed                                     | Хэндовер в указанную соту назначения не разрешен.                                                                                                                                                                                                                                                                |
| 8   | TS1RELOCoverall Expiry                                          | Вызван истечением таймера TS1RELOCoverall.                                                                                                                                                                                                                                                                       |
| 9   | TS1RELOCprep Expiry                                             | Процедура Handover Preparation отменяется по истечении таймера TS1RELOCprep.                                                                                                                                                                                                                                     |
| 10  | Cell not available                                              | Соответствующая сота недоступна.                                                                                                                                                                                                                                                                                 |
| 11  | Unknown Target ID                                               | Хэндовер не разрешен, поскольку идентификатор назначения не известен EPC.                                                                                                                                                                                                                                        |
| 12  | No radio resources available in target cell                     | Нагрузка на соту назначения слишком высока.                                                                                                                                                                                                                                                                      |
| 13  | Unknown or already allocated MME UE S1AP ID                     | Запрошен, поскольку либо идентификатор MME UE S1AP ID неизвестен, либо (для первого сообщения, полученного на eNodeB) известен и привязан к уже существующему контексту.                                                                                                                                         |
| 14  | Unknown or already allocated eNB UE S1AP ID                     | Запрошен, поскольку либо идентификатор eNodeB UE S1AP ID неизвестен, либо (для первого сообщения, полученного узлом MME) известен и привязан к уже существующему контексту.                                                                                                                                      |
| 15  | Unknown or inconsistent pair of UE S1AP ID                      | Запрошен, поскольку либо идентификаторы UE S1AP ID неизвестны, либо известны, но связаны с различными контекстами.                                                                                                                                                                                               |
| 16  | Handover Desirable for Radio Reasons                            | Причина запроса хэндовера связана с радиосетью.                                                                                                                                                                                                                                                                  |
| 17  | Time Critical Handover                                          | Запрошен хэндовер по причине, которая критична по времени, т.е. значение зарезервировано для всех критических случаев, когда соединение наверняка будет потеряно при невыполнении хэндовера.                                                                                                                     |
| 18  | Resource Optimisation Handover                                  | Запрошен хэндовер для оптимизации распределения нагрузки на соседние соты.                                                                                                                                                                                                                                       |
| 19  | Reduce Load in Serving Cell                                     | Необходимо снизить нагрузку на обслуживающие соты. При подготовке к хэндоверу указывает на активацию хэндовера ввиду балансировки нагрузки.                                                                                                                                                                      |
| 20  | User Inactivity                                                 | Запрошен ввиду неактивности пользователя на всех E-<abbr title="Radio Access Bearer">RAB</abbr>, например, разрыв S1-соединения, для оптимизации использования ресурсов.                                                                                                                                         |
| 21  | Radio Connection With UE Lost                                   | Запрошен ввиду потери радиосоединения с UE.                                                                                                                                                                                                                                                                      |
| 22  | Load Balancing TAU Required                                     | Запрашивается для всех случаев балансировки и разгрузки узла MME.                                                                                                                                                                                                                                                |
| 23  | CS Fallback triggered                                           | Запрошен ввиду процедуры CS Fallback. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS.                                                                                                                     |
| 24  | UE Not Available for PS Service                                 | Запрошен ввиду процедуры CS Fallback для сети GERAN. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, то указывает на отсутствие необходимости для EPC приостанавливать обслуживание в сети PS ввиду отсутствия поддержки <abbr title="Dual Transfer Mode">DTM</abbr> GERAN-сотой назначения или UE. |
| 25  | Radio resources not available                                   | Запрашиваемые радиоресурсы недоступны.                                                                                                                                                                                                                                                                           |
| 26  | Invalid QoS combination                                         | Вызван некорректными значениями параметров QoS.                                                                                                                                                                                                                                                                  |
| 27  | Inter-RAT Redirection                                           | Запрошен ввиду переадресации между различными сетями RAT или LTE. Если находится в сообщении S1AP: UE CONTEXT RELEASE REQUEST, дальнейшие действия EPC должны следовать [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.07.00_60/ts_123401v170700p.pdf).                           |
| 28  | Failure in the Radio Interface Procedure                        | Процедура Radio Interface Procedure завершилась неуспешно.                                                                                                                                                                                                                                                       |
| 29  | Interaction with other procedure                                | Вызван продолжающимся взаимодействием с другой процедурой.                                                                                                                                                                                                                                                       |
| 30  | Unknown E-RAB ID                                                | Вызван отсутствием идентификатора E-<abbr title="Radio Access Bearer">RAB</abbr> на eNodeB.                                                                                                                                                                                                                      |
| 31  | Multiple E-RAB ID Instances                                     | Вызван передачей нескольких одинаковых E-RAB на eNodeB.                                                                                                                                                                                                                                                          |
| 32  | Encryption and/or integrity protection algorithms not supported | eNodeB не поддерживает ни один из алгоритмов шифрования/защиты целостности, которые поддерживаются UE.                                                                                                                                                                                                           |
| 33  | S1 Intra system Handover triggered                              | Вызван внутрисистемным хэндовером S1.                                                                                                                                                                                                                                                                            |
| 34  | S1 Inter system Handover triggered                              | Вызван межсистемным хэндовером S1.                                                                                                                                                                                                                                                                               |
| 35  | X2 Handover triggered                                           | Вызван хэндовером X2.                                                                                                                                                                                                                                                                                            |
| 36  | Redirection towards 1xRTT                                       | Запрошен разрыв логического, связанного с UE S1-соединения ввиду переадресации на 1xRTT-систему, например, CS fallback или SRVCC, когда необходима приостановка обслуживания в PS-сетях. Для процедуры сообщение может, но не обязано, содержать данные о переадресации.                                         |
| 37  | Not supported QCI Value                                         | Установление E-RAB завершилось ошибкой, поскольку указанный <abbr title="QoS Class Identifier">QCI</abbr> не поддерживается.                                                                                                                                                                                     |
| 38  | Invalid CSG Id                                                  | Идентификатор <abbr title="Closed Subscriber Group">CSG</abbr>, переданный eNodeB назначения, некорректен.                                                                                                                                                                                                       |
| 39  | Release due to Pre-Emption                                      | Вызван приоритетным использованием службы.                                                                                                                                                                                                                                                                       |
| 40  | N26 interface not available                                     | Вызван временной ошибкой интерфейса N26.                                                                                                                                                                                                                                                                         |
| 41  | <a name="example">Insufficient UE Capabilities</a>              | Вызван недостаточными возможностями UE.                                                                                                                                                                                                                                                                          |
| 42  | Maximum bearer pre-emption rate exceeded                        | Количество запросов превысило максимально допустимое для bearer-службы.                                                                                                                                                                                                                                          |
| 43  | UP integrity protection not possible                            | E-RAB не может быть принят ввиду политики обеспечения целостности плоскости пользователя.                                                                                                                                                                                                                        |

#### Transport Layer cause {#transport-layer-cause}

| Код | Причина                        | Описание                                                            |
|-----|--------------------------------|---------------------------------------------------------------------|
| 0   | Transport Resource Unavailable | Требуемые ресурсы для передачи недоступны.                          |
| 1   | Unspecified                    | Неописанная ошибка, принадлежащая к группе Transport Network Layer. |

#### NAS cause {#nas-cause}

| Код | Причина                 | Описание                                                                                                 |
|-----|-------------------------|----------------------------------------------------------------------------------------------------------|
| 0   | Normal Release          | Вызван нормальным сценарием.                                                                             |
| 1   | Authentication Failure  | Вызван неуспешной аутентификацией.                                                                       |
| 2   | Detach                  | Вызвано отключением от сети.                                                                             |
| 3   | Unspecified             | Неописанная ошибка, принадлежащая к группе NAS.                                                          |
| 4   | CSG Subscription Expiry | Вызвано тем, что UE перестает быть членом используемой <abbr title="Closed Subscriber Group">CSG</abbr>. |

#### Protocol cause {#protocol-cause}

| Код | Причина                                             | Описание                                                                                                           |
|-----|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 0   | Transfer Syntax Error                               | Полученное сообщение содержит синтаксическую ошибку передачи.                                                      |
| 1   | Abstract Syntax Error (Reject)                      | Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'reject'.            |
| 2   | Abstract Syntax Error (Ignore And Notify)           | Полученное сообщение содержит абстрактную синтаксическую ошибку, чья критичность указывает на 'ignore and notify'. |
| 3   | Message Not Compatible With Receiver State          | Полученное сообщение не совместимо с текущим состоянием отправителя.                                               |
| 4   | Semantic Error                                      | Полученное сообщение содержит семантическую ошибку.                                                                |
| 5   | Abstract Syntax Error (Falsely Constructed Message) | Полученное сообщение содержит IE в неверном порядке или указанные слишком много раз.                               |
| 6   | Unspecified                                         | Неописанная ошибка, принадлежащая к группе Protocol cause.                                                         |

#### Miscellaneous cause {#miscellaneous-cause}

| Код | Причина                                              | Описание                                                                                                       |
|-----|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| 0   | Control Processing Overload                          | Перегрузка при обработке управлящего трафика.                                                                  |
| 1   | Not Enough User Plane Processing Resources Available | Недостаточно ресурсов для обработки пользовательского трафика.                                                 |
| 2   | Hardware Failure                                     | Вызван аппаратным сбоем.                                                                                       |
| 3   | O&M Intervention                                     | Вызван вмешательством OM.                                                                                      |
| 4   | Unspecified Failure                                  | Неописанная ошибка, не связанная с категориями Radio Network Layer, Transport Network Layer, NAS или Protocol. |
| 5   | Unknown PLMN                                         | Узел MME не обнаружил какую-либо сеть PLMN, предоставленную eNodeB.                                            |
