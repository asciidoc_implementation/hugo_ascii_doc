---
title: "S1 Service"
description: "Статистика процедур S1/S1AP Service"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Service\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур S1/S1AP Service.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf) и
[3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

| Tx/Rx | Метрика                                                                               | Описание                                                                                                                                                          | Группа    |
|-------|---------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Rx    | <a name="ExtendedServiceRequest">ExtendedServiceRequest</a>                           | Количество сообщений S1 Extended Service Request.                                                                                                                 | TAI:Value |
| Rx    | <a name="ExtendedServiceSuccess">ExtendedServiceSuccess</a>                           | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE или S1AP: UE CONTEXT MODIFICATION RESPONSE.                                                             | TAI:Value |
| Rx    | <a name="csfbMoUeContextModificationResponse">csfbMoUeContextModificationResponse</a> | Количество сообщений S1AP: UE CONTEXT MODIFICATION RESPONSE в ответ на запросы S1AP: UE CONTEXT MODIFICATION REQUEST с заполненным полем `CS Fallback Indicator`. | TAI:Value |
| Rx    | <a name="csfbMoInitialContextSetupResponse">csfbMoInitialContextSetupResponse</a>     | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE в ответ на запросы S1AP: INITIAL CONTEXT SETUP REQUEST с заполненным полем `CS Fallback Indicator`.     | TAI:Value |
| Rx    | <a name="ServiceRequest">ServiceRequest</a>                                           | Количество сообщений S1 Service Request.                                                                                                                          | TAI:Value |
| Rx    | <a name="ServiceSuccess">ServiceSuccess</a>                                           | Количество сообщений S1AP: INITIAL CONTEXT SETUP COMPLETE в ответ на запросы S1 Service Request.                                                                  | TAI:Value |
| Tx    | <a name="ServiceReject">ServiceReject</a>                                             | Количество сообщений S1 Service Reject в ответ на запросы S1 Service Request или S1 Extended Service Request.                                                     | TAI:Value |

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
rx,ExtendedServiceRequest,,0
rx,ExtendedServiceSuccess,,0
rx,csfbMoUeContextModificationResponse,,0
rx,csfbMoInitialContextSetupResponse,,0
rx,ServiceRequest,,133
rx,ServiceRequest,TAI:001010001,127
rx,ServiceRequest,TAI:001010010,6
rx,ServiceSuccess,,128
rx,ServiceSuccess,TAI:001010001,127
rx,ServiceSuccess,TAI:001010010,1
```