---
title: "S1 Bearer Modification"
description: "Статистика процедур S1 Bearer Modification"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1BearerModification\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур 
S1 Bearer Modification.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Метрика                                                                       | Описание                                                                                                                                        | Группа         |
|-------|-------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| Tx    | <a name="pgwInitBearerModRequest">pgwInitBearerModRequest</a>                 | Количество сообщений S1 Modify EPS Bearer Context Request в процедурах PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD).                 | IMSIPLMN:Value |
| Rx    | <a name="pgwInitBearerModSuccess">pgwInitBearerModSuccess</a>                 | Количество сообщений S1 Modify EPS Bearer Context Accept в процедурах PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD).                  | IMSIPLMN:Value |
| Tx    | <a name="hssInitBearerModRequest">hssInitBearerModRequest</a>                 | Количество сообщений S1 Modify EPS Bearer Context Request в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD). | IMSIPLMN:Value |
| Rx    | <a name="hssInitBearerModSuccess">hssInitBearerModSuccess</a>                 | Количество сообщений S1 Modify EPS Bearer Context Accept в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD).  | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject">modifyEpsBearerContextReject</a>       | Количество сообщений S1 Modify EPS Bearer Context Reject.                                                                                       | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject26">modifyEpsBearerContextReject26</a>   | Количество сообщений S1 Modify EPS Bearer Context Reject с причиной "#26 Insufficient resources".                                               | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject111">modifyEpsBearerContextReject111</a> | Количество сообщений S1 Modify EPS Bearer Context Reject с причиной "#111 Protocol error, unspecified".                                         | IMSIPLMN:Value |
| Rx    | <a name="ueInitBearerResModRequest">ueInitBearerResModRequest</a>             | Количество сообщений S1 Bearer Resource Modification Request.                                                                                   | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject">ueInitBearerResModReject</a>               | Количество сообщений S1 Bearer Resource Modification Reject.                                                                                    | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject26">ueInitBearerResModReject26</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#26 Insufficient resources".                                            | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject30">ueInitBearerResModReject30</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#30 Request rejected by Serving GW or PDN GW".                          | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject31">ueInitBearerResModReject31</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#31 Request rejected, unspecified".                                     | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject32">ueInitBearerResModReject32</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#32 Service option not supported".                                      | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject33">ueInitBearerResModReject33</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#33 Requested service option not subscribed".                           | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject37">ueInitBearerResModReject37</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#37 EPS QoS not accepted".                                              | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject59">ueInitBearerResModReject59</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#59 Unsupported QCI value".                                             | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject96">ueInitBearerResModReject96</a>           | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#96 Invalid mandatory information".                                     | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject111">ueInitBearerResModReject111</a>         | Количество сообщений S1 Bearer Resource Modification Reject с причиной "#111 Protocol error, unspecified".                                      | IMSIPLMN:Value | 

**Примечание.** Описание причин ошибок ESM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Группа

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
IMSIPLMN:25099
```

#### Пример файла ####

```csv
tx,pgwInitBearerModRequest,,0
rx,pgwInitBearerModSuccess,,0
tx,hssInitBearerModRequest,,0
rx,hssInitBearerModSuccess,,0
rx,modifyEpsBearerContextReject,,0
rx,ueInitBearerResModRequest,,0
tx,ueInitBearerResModReject,,0
```