---
title: S6a Interface
description: Статистика процедур S6a
type: docs
weight: 20
---
= S6a Interface
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

Файл **<node_name>\_MME-S6a-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S6a.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272]
и https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="1,4,4,2"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Tx
| [[authenticationInformationRequest]]authenticationInformationRequest
| Количество сообщений Diameter: Authentication-Information-Request.
| IMSIPLMN:Value

| Rx
| [[authenticationInformationAnswer]]authenticationInformationAnswer
| Количество сообщений Diameter: Authentication-Information-Answer.
| IMSIPLMN:Value

| Tx
| [[updateLocationRequest]]updateLocationRequest
| Количество сообщений Diameter: Update-Location-Request.
| IMSIPLMN:Value

| Rx
| [[updateLocationAnswer]]updateLocationAnswer
| Количество сообщений Diameter: Update-Location-Answer.
| IMSIPLMN:Value

| Rx
| [[resetRequest]]resetRequest
| Количество сообщений Diameter: Reset-Request.
| IMSIPLMN:Value

| Tx
| [[resetAnswer]]resetAnswer
| Количество сообщений Diameter: Reset-Answer.
| IMSIPLMN:Value

| Rx
| [[cancelLocationRequest]]cancelLocationRequest
| Количество сообщений Diameter: Cancel-Location-Request.
| IMSIPLMN:Value

| Rx
| [[cancelLocationAnswer]]cancelLocationAnswer
| Количество сообщений Diameter: Cancel-Location-Answer.
| IMSIPLMN:Value

| Rx
| [[insertSubscriberDataRequest]]insertSubscriberDataRequest
| Количество сообщений Diameter: Insert-Subscriber-Data-Request.
| IMSIPLMN:Value

| Tx
| [[insertSubscriberDataAnswer]]insertSubscriberDataAnswer
| Количество сообщений Diameter: Insert-Subscriber-Data-Answer.
| IMSIPLMN:Value

| Tx
| [[purgeUeRequest]]purgeUeRequest
| Количество сообщений Diameter: Purge-UE-Request.
| IMSIPLMN:Value

| Rx
| [[purgeUeAnswer]]purgeUeAnswer
| Количество сообщений Diameter: Purge-UE-Answer.
| IMSIPLMN:Value

| Tx
| [[notifyRequest]]notifyRequest
| Количество сообщений Diameter: Notify-Request.
| IMSIPLMN:Value

| Rx
| [[notifyAnswer]]notifyAnswer
| Количество сообщений Diameter: Notify-Answer.
| IMSIPLMN:Value

| Rx
| [[deleteSubscriberDataRequest]]deleteSubscriberDataRequest
| Количество сообщений Diameter: Delete-Subscriber-Data-Request.
| IMSIPLMN:Value

| Tx
| [[deleteSubscriberDataAnswer]]deleteSubscriberDataAnswer
| Количество сообщений Diameter: Delete-Subscriber-Data-Answer.
| IMSIPLMN:Value
|===

В таблице ниже приведены возможные значения AVP *_Result-Code_* и AVP *_Experimental-Result-Code_* из перечисленных в
https://www.etsi.org/deliver/etsi_ts/129200_129299/129229/17.02.00_60/ts_129229v170200p.pdf[3GPP TS 29.229] и
https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272].

NOTE: Коды AVP *_Experimental-Result-Code_* отмечены буквой *_e_*.

|===
| Код | Описание

| 2001
| DIAMETER_SUCCESS

| 4181e
| DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE

| 5001e
| DIAMETER_ERROR_USER_UNKNOWN

| 5003e
| DIAMETER_ERROR_IDENTITY_NOT_REGISTERED

| 5004e
| DIAMETER_ERROR_ROAMING_NOT_ALLOWED

| 5012e
| DIAMETER_ERROR_SERVING_NODE_FEATURE_UNSUPPORTED

| 5420e
| DIAMETER_ERROR_UNKNOWN_EPS_SUBSCRIPTION

| 5421e
| DIAMETER_ERROR_RAT_NOT_ALLOWED

| 5423e
| DIAMETER_ERROR_UNKNOWN_SERVING_NODE
|===

При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP *_Experimental-Result-Code_* отмечены дополнительной буквой *_e_* в конце.

Пример: *_updateLocationAnswer5420e_*.

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
IMSIPLMN:25099
----

.Пример

[source,csv]
----
rx,updateLocationAnswer,,71
rx,updateLocationAnswer2001,,71
rx,cancelLocationRequest,,2
rx,authenticationInformationAnswer,,134
rx,authenticationInformationAnswer2001,,134
rx,authenticationInformationAnswer4181e,,0
rx,insertSubscriberDataRequest,,0
rx,deleteSubscriberDataRequest,,0
rx,purgeUeAnswer,,47
rx,purgeUeAnswer2001,,47
rx,resetRequest,,0
rx,notifyAnswer,,118
rx,notifyAnswer2001,,90
rx,notifyAnswer5423e,,28
tx,updateLocationRequest,,71
tx,cancelLocationAnswer,,2
tx,cancelLocationAnswer2001,,2
tx,authenticationInformationRequest,,134
tx,insertSubscriberDataAnswer,,0
tx,deleteSubscriberDataAnswer,,0
tx,purgeUeRequest,,47
tx,resetAnswer,,0
tx,notifyRequest,,118
rx,updateLocationAnswer2001,IMSIPLMN:00101,59
rx,updateLocationAnswer,IMSIPLMN:00101,59
rx,cancelLocationRequest,IMSIPLMN:00101,2
rx,authenticationInformationAnswer2001,IMSIPLMN:00101,129
rx,authenticationInformationAnswer,IMSIPLMN:00101,129
rx,purgeUeAnswer2001,IMSIPLMN:00101,46
rx,purgeUeAnswer,IMSIPLMN:00101,46
rx,notifyAnswer2001,IMSIPLMN:00101,86
rx,notifyAnswer5423e,IMSIPLMN:00101,28
rx,notifyAnswer,IMSIPLMN:00101,114
rx,updateLocationAnswer2001,IMSIPLMN:20893,12
rx,updateLocationAnswer,IMSIPLMN:20893,12
rx,authenticationInformationAnswer2001,IMSIPLMN:20893,5
rx,authenticationInformationAnswer4181e,IMSIPLMN:20893,0
rx,authenticationInformationAnswer,IMSIPLMN:20893,5
rx,purgeUeAnswer2001,IMSIPLMN:20893,1
rx,purgeUeAnswer,IMSIPLMN:20893,1
rx,notifyAnswer2001,IMSIPLMN:20893,4
rx,notifyAnswer,IMSIPLMN:20893,4
rx,cancelLocationRequest,IMSIPLMN:99999,0
tx,updateLocationRequest,IMSIPLMN:00101,59
tx,cancelLocationAnswer2001,IMSIPLMN:00101,2
tx,cancelLocationAnswer,IMSIPLMN:00101,2
tx,authenticationInformationRequest,IMSIPLMN:00101,129
tx,purgeUeRequest,IMSIPLMN:00101,46
tx,notifyRequest,IMSIPLMN:00101,114
tx,updateLocationRequest,IMSIPLMN:20893,12
tx,authenticationInformationRequest,IMSIPLMN:20893,5
tx,purgeUeRequest,IMSIPLMN:20893,1
tx,notifyRequest,IMSIPLMN:20893,4
tx,cancelLocationAnswer2001,IMSIPLMN:99999,0
tx,cancelLocationAnswer,IMSIPLMN:99999,0
----