---
title: Журналы
description: Описание журналов и статистики
type: docs
weight: 50
---
= Журналы
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

Узел Protei_MME ведет следующие журналы:

* EDR:
 ** xref:edr/connect/[connect_cdr] -- журнал событий с PDN-соединениями;
 ** xref:edr/dedicated_bearer/[dedicated_bearer_cdr] -- журнал событий с выделенной bearer-службой;
 ** xref:edr/diam/[diam_cdr] -- журнал событий Diameter;
 ** xref:edr/enodeb/[enodeb_cdr] -- журнал событий eNodeB;
 ** xref:edr/gtp_c/[gtp_c_cdr] -- журнал событий GTP-C;
 ** xref:edr/gtp_c_overload/[gtp_c_overload] -- журнал событий перегрузки интерфейса GTP-C;
 ** xref:edr/http/[http_cdr] -- журнал событий HTTP;
 ** xref:edr/irat_handover/[irat_handover_cdr] -- журнал межсетевого хэндовера;
 ** xref:edr/lte_handover/[lte_handover_cdr] -- журнал хэндовера в сети LTE;
 ** xref:edr/paging/[paging_cdr] -- журнал событий Paging;
 ** xref:edr/reject/[reject_cdr] -- журнал событий отбоя сообщений;
 ** xref:edr/s1ap/[s1ap_cdr] -- журнал событий S1AP;
 ** xref:edr/s1ap_context/[s1ap_context_cdr] -- журнал событий с контекстом S1AP;
 ** xref:edr/s1ap_overload/[s1ap_overload_cdr] -- журнал событий перегрузки интерфейса S1AP;
 ** xref:edr/sgsap/[sgsap_cdr] -- журнал событий SGsAP;
 ** xref:edr/tau/[tau_cdr] -- журнал событий Tracking-Area-Update;
* log:
 ** alarm -- общий журнал аварий системы;
 ** alarm_cdr -- журнал CDR подсистемы сбора аварий;
 ** alarm_trace -- журнал действий подсистемы сбора аварий;
 ** bc_trace -- журнал действий базовой компоненты;
 ** bc_warning -- журнал предупреждений базовой компоненты;
 ** COM_trace -- журнал действий подсистемы конфигурирования компонент;
 ** COM_warning -- журнал предупреждений подсистемы конфигурирования компонент;
 ** config -- журнал загрузок конфигурационных файлов
 ** db_trace -- журнал действий базы данных MME;
 ** diam_info -- журнал событий протокола Diameter;
 ** diam_trace -- журнал действий протокола Diameter;
 ** diam_warning -- журнал предупреждений протокола Diameter;
 ** dns_trace -- журнал действий протокола DNS;
 ** dns_warning -- журнал предупреждений протокола DNS;
 ** GTP_C_trace -- журнал действий протокола GTP Control Plane: GTPv1-C и GTPv2;
 ** GTP_C_warning -- журнал предупреждений протокола GTP Control Plane: GTPv1-C и GTPv2;
 ** http_trace -- журнал действий HTTP-интерфейса;
 ** mme_config -- журнал чтения конфигурационных файлов MME: *mme.cfg*, *served_plmn.cfg* и связанных с ним **_rules*;
 ** profilers -- журнал использования физических ресурсов и логик приложения;
 ** S1AP_trace -- журнал действий протокола S1AP;
 ** S1AP_warning -- журнал предупреждений протокола S1AP;
 ** sctp_binary -- дамп SCTP-соединений;
 ** Sg_info -- журнал событий системы сигнализации;
 ** Sg_trace -- журнал действий системы сигнализации;
 ** Sg_warning -- журнал предупреждений системы сигнализации;
 ** SGsAP_trace -- журнал действий протокола SGsAP;
 ** SGsAP_warning -- журнал предупреждений протокола SGsAP;
 ** si -- журнал действий сокет-интерфейса;
 ** si_info -- журнал событий сокет-интерфейса;
 ** si_warning -- журнал предупреждений сокет-интерфейса;
 ** trace -- общий журнал действий;
 ** ue_trace -- журнал отслеживания устройств.

Узел Protei_MME ведет следующие файлы со статистиками по метрикам:

* xref:stat/MME_Diameter/[MME_Diameter.csv] -- статистическая информация по метрикам MME для процедур протокола Diameter: Base;
* xref:stat/MME_handover/[MME_handover.csv] -- статистическая информация по метрикам MME для процедур хэндовера;
* xref:stat/MME_paging/[MME_paging.csv] -- статистическая информация по метрикам MME для процедур Paging;
* xref:stat/MME_resource/[MME_resource.csv] -- статистическая информация по метрикам MME для использования ресурсов сервера;
* xref:stat/MME_s11_Interface/[MME_s11_Interface.csv] -- статистическая информация по метрикам MME для процедур интерфейса S11;
* xref:stat/MME_s1_Attach/[MME_s1_Attach.csv] -- статистическая информация по метрикам MME для интерфейса S1 по процедурам Attach;
* xref:stat/MME_s1_Bearer_Activation/[MME_s1_Bearer_Activation.csv] -- статистическая информация по метрикам MME для процедур S1 Bearer Activation;
* xref:stat/MME_s1_Bearer_Deactivation/[MME_s1_Bearer_Deactivation.csv] -- статистическая информация по метрикам MME для процедур S1 Bearer Deactivation;
* xref:stat/MME_s1_Bearer_Modification/[MME_s1_Bearer_Modification.csv] -- статистическая информация по метрикам MME для процедур S1 Bearer Modification;
* xref:stat/MME_s1_Detach/[MME_s1_Detach.csv] -- статистическая информация по метрикам MME для интерфейса S1 по процедурам Detach;
* xref:stat/MME_s1_Interface/[MME_s1_Interface.csv] -- статистическая информация по метрикам MME для процедур протокола S1AP;
* xref:stat/MME_s1_Security/[MME_s1_Security.csv] -- статистическая информация по метрикам MME для процедур протокола NAS;
* xref:stat/MME_s1_Service/[MME_s1_Service.csv] -- статистическая информация по метрикам MME для процедур S1/S1AP Service;
* xref:stat/MME_S6a_interface/[MME_S6a_interface.csv] -- статистическая информация по метрикам MME для процедур интерфейса S6a;
* xref:stat/MME_sgs_Interface/[MME_sgs_Interface.csv] -- статистическая информация по метрикам MME для процедур интерфейса SGs;
* xref:stat/MME_sv_Interface/[MME_sv_Interface.csv] -- статистическая информация по метрикам MME для процедур интерфейса Sv;
* xref:stat/MME_tau/[MME_tau.csv] -- статистическая информация по метрикам MME для процедур TRACKING AREA UPDATE;
* xref:stat/MME_users/[MME_users.csv] -- статистическая информация по метрикам MME для количества абонентов и сессий.