---
title: Установка
description: Инструкция по развертыванию узла PROTEI MME
type: docs
weight: 10
---
= Установка
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

=== Установка программного обеспечения

. Cкопировать на целевой сервер дистрибутив Protei_MME.
. Создать локальную виртуальную машину от лица суперпользователя.

[source,bash]
----
$ su -
$ pvcreate /dev/sdb
$ vgcreate protei /dev/sdb
----

. Разметить дисковое пространство.

[source,bash]
----
$ lvcreate --name protei_app -l 5%FREE protei
$ lvcreate --name protei_cdr -l 45%FREE protei
$ lvcreate --name protei_log -l 100%FREE protei
----

. Создать файловую систему.

[source,bash]
----
$ mkfs.xfs /dev/mapper/protei-app
$ mkfs.xfs /dev/mapper/protei-log
$ mkfs.xfs /dev/mapper/protei-cdr
----

. Создать директории.

[source,bash]
----
$ mkdir -p /usr/protei/
$ mkdir -p /usr/protei/log
$ mkdir -p /usr/protei/cd
----

. Открыть файл /etc/fstab/ и добавить строки.

[source,bash]
----
$ /dev/mapper/protei-app /usr/protei xfs defaults 0 0
$ /dev/mapper/protei-log /usr/protei/log xfs defaults 0 0
$ /dev/mapper/protei-cdr /usr/protei/cdr xfs defaults 0 0
----

. Применить изменения из файла fstab к текущей системе.

[source,bash]
----
$ mount -a
----

. Обеспечить доступ к репозиторию для разрешения зависимостей.
. Установить необходимые библиотеки.

[source,bash]
----
$ apt-get install build-essential cmake gcc gdb git g++ iputils-ping libboost-all-dev libmariadb-dev libpcap-dev libsctp-dev libssl-dev libz-dev make pkg-config python3 rsync ssh ta
----

. Установить пакет Protei_MME.

[source,bash]
----
$ apt-get install MME.<version>.rpm
----

. Запустить приложение и проверить статус.

[source,bash]
----
$ systemctl start mme
$ systemctl status mme
----

=== Настройка VRF

NOTE: Данная настройка VRF описана для OEL8 (Oracle). +
Перед настройкой необходимо обновить OEL-ядро до версии UEK не ниже 5.

. Проверить поддержку *vrf* операционной системой с помощью команды *_modinfo vrf_*.

[source,console]
----
$ modinfo vrf
filename:       /lib/modules/5.15.0-103.114.4.el8uek.x86_64/kernel/drivers/net/vrf.ko.xz
version:        1.1
alias:          rtnl-link-vrf
license:        GPL
description:    Device driver to instantiate VRF domains
author:         Shrijeet Mukherjee, David Ahern
srcversion:     E0CE385C0CEC58F13E31D08
depends:
retpoline:      Y
intree:         Y
name:           vrf
vermagic:       5.15.0-103.114.4.el8uek.x86_64 SMP mod_unload modversions
sig_id:         PKCS#7
signer:         Oracle CA Server
sig_key:        30:71:5B:FF:7A:CF:B6:65:96:05:D8:9B:D5:C1:5E:CE:83:D5:76:A3
sig_hashalgo:   sha512
signature:      0C:A4:39:6E:49:FC:33:32:06:FE:95:EC:57:90:24:EB:15:DE:94:6E:
                2E:4C:01:B5:01:37:AA:8F:5D:2C:B1:89:38:6B:6E:B6:B7:F4:9B:BC:
                AE:47:92:22:60:5B:47:FA:0E:29:C7:C5:0E:6F:D0:42:2A:B5:01:5B:
                2C:E4:37:71:45:A0:50:05:9F:EA:46:62:27:23:C8:09:0D:F6:4B:1A:
                6A:6B:09:9B:AD:15:6D:E1:F0:80:DE:4F:D1:95:01:28:0C:CF:AA:2B:
                E9:98:8C:8C:38:35:53:AC:9D:47:70:CE:08:C2:E3:2D:28:32:D8:45:
                C5:A3:5C:6B:0D:B5:D0:5C:5E:8C:91:1E:6D:C3:9A:55:C7:C7:12:1C:
                06:90:9D:89:05:B5:B8:92:4D:12:EC:2B:96:CA:42:9C:39:05:62:87:
                0E:A6:7E:11:C0:14:82:51:C4:CC:7E:B3:2B:88:9E:54:3A:A2:F6:48:
                00:9F:BA:8C:36:10:CA:54:B4:58:FE:49:0A:06:15:97:8C:95:18:77:
                E5:D8:0F:84:25:68:D9:BB:19:09:AD:5C:93:68:CA:13:C4:B7:0E:07:
                26:D3:12:26:FE:27:D6:C9:0F:B0:14:36:1E:CB:C2:94:A2:F1:90:9B:
                46:24:C5:6F:9F:02:05:E9:4F:F8:47:01:53:23:8F:4D:9B:61:97:76:
                1C:D0:80:8F:9B:55:4C:92:4F:C4:D6:7C:B6:13:2A:99:12:44:97:4F:
                97:43:66:E9:9D:C8:E9:B8:58:F1:BF:81:9A:7A:E8:5C:D7:74:94:C4:
                AF:A4:E2:76:D7:B4:05:7B:0D:1C:A0:78:41:94:04:1B:25:13:0D:ED:
                18:31:C5:47:E8:96:5D:AA:5C:84:28:71:0F:DF:22:0C:32:1B:2F:7B:
                A5:F6:14:DD:4C:75:ED:15:D4:D9:38:7B:0C:5C:CF:B7:90:E0:E4:11:
                BF:88:4E:B0:C6:DE:FE:4F:17:A4:AB:28:F9:44:DD:17:30:D0:5F:50:
                AC:C4:E4:92:78:B9:D8:22:71:A0:D5:E2:E2:6C:C7:9B:19:A3:CA:1A:
                88:21:0F:30:23:17:F2:C1:5D:52:02:22:DF:F3:F6:AD:58:9A:A9:90:
                09:A7:4C:3E:8E:55:16:5E:B3:DE:6D:56:86:60:CF:EB:58:4E:45:12:
                E7:69:79:DF:D1:95:8B:26:48:CB:AD:9F:44:2D:3E:25:EB:2A:F1:CB:
                70:ED:DC:37:AA:56:BA:F7:7D:1A:42:5A:C1:95:0C:9B:A7:03:96:7F:
                18:3E:E9:DC:08:AC:A0:78:13:B3:51:81:56:F8:14:26:C1:43:C2:10:
                A1:DC:51:5E:C7:1A:0B:E9:7D:D1:5C:E8
----

. Если результат команды пуст, то необходимо установить пакет *kernel-uek-modules-extra.x86_64*.

[source,bash]
----
$ apt install kernel-uek-modules-extra.x86_64
----

или

[source,bash]
----
$ yum install kernel-uek-modules-extra.x86_64
----

. Создать S1-соединение с виртуальным устройством с именем *S1* и прикрепить его к
таблице маршрутизации 5 с помощью утилиты *_nmcli_*, NetworkManager CLI.

[source,bash]
----
$ nmcli connection add type vrf ifname S1 con-name S1 table 5 ipv4.method disabled ipv6.method disabled
----

. Активировать S1-соединение.

[source,bash]
----
$ nmcli con up S1
----

. Создать виртуальный интерфейс *_bond0.2001_* для VLAN 2001 на интерфейсе *_bond0_* и задать IP-адрес.

[source,bash]
----
$ nmcli conn add type vlan con-name bond0.2001 ifname bond0.2001 dev bond0 id 2001 master S1 ipv4.method manual ipv4.address 10.159.10.105/28 ipv6.method disabled
----

. Активировать виртуальный интерфейс.

[source,bash]
----
$ nmcli con up bond0.2001
----

Далее необходимо выполнить подготовку конфигурационных файлов.

. Добавить следующие настройки в файл *_/etc/sysctl.conf_*:

[source,console]
----
net.ipv4.ip_nonlocal_bind = 1
net.ipv4.conf.all.arp_ignore = 1
kernel.core_pattern = /usr/protei/log/core/core.%e
kernel.core_uses_pid = 1
net.ipv4.tcp_l3mdev_accept = 1
net.ipv4.udp_l3mdev_accept = 1
net.ipv4.conf.all.rp_filter = 0
----

. Применить новые настройки с помощью команды *_sysctl -p_*.
. Создать директорию *_/usr/protei/log/core/_*.

[source,bash]
----
$ mkdir -p /usr/protei/log/core/
----

. Добавить следующие настройки в файл *_/lib/systemd/system/protei_network.service_*:

[source,console]
----
[Unit]
Description = Add VRFs and dummy loopback interfaces
DefaultDependencies = false
After = NetworkManager.service

[Service]
User = root
Type = oneshot
RemainAfterExit = yes
ExecStart = /etc/network/protei_network

[Install]
WantedBy = multi-user.target
----

. Создать файл *_/etc/network/protei_network_*.

[source,bash]
----
$ touch /etc/network/protei_network
----

. Записать в файл следующий скрипт:

[source,console]
----
#!/bin/bash

# CONFIGURATION SECTION
#Enter Sig physical interfaces
if_Sig_1 = bond0.2007

#Enter S1 physical interfaces:
if_S1_1 = bond0.2001

#END CONFIGURATION SECTION
#############################################
date > /var/log/protei_network.log
echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "add VRFs" >> /var/log/protei_network.log

/usr/sbin/ip link show type vrf >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
echo "add dummy loopback interfaces" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

sleep 1
echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "show master table" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip link sh master Sig >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip link sh master S1 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "show IP addresses in VRFs" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip addr sh vrf Sig >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip addr sh vrf S1 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "adding PBR" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

/usr/sbin/ip rule add from $(/usr/sbin/ip add show $if_Sig_1 | grep -Po 'inet \K[\d\.]+')\/32 preference 10 lookup 6 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
sleep 1

/usr/sbin/ip rule add from $(/usr/sbin/ip add show $if_S1_1 | grep -Po 'inet \K[\d\.]+')\/32 preference 10 lookup 5 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
sleep 1

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "show PBR" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip rule sh >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "add default unreachable routes in VRFs" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

/usr/sbin/ip route add table 6 unreachable default metric 4278198272 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip route add table 5 unreachable default metric 4278198272 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

/usr/sbin/ip route add table 6 0.0.0.0/0 via 10.159.10.113 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip route add table 5 0.0.0.0/0 via 10.159.10.97 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
echo "show routes in VRFs" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "ip route sh table 6" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip route sh table 6 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log

echo "#####" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
echo "ip route sh table 5" >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
/usr/sbin/ip route sh table 5 >> /var/log/protei_network.log 2 >> /var/log/protei_network.log
----

. Сделать файл *_protei_network_* исполняемым.

[source,bash]
----
$ chmod +x /etc/network/protei_network
----

. Запустить файл.

[source,bash]
----
$ /etc/network/protei_network
----

==== Управляющие команды

* показать адреса для VRF S1:

[source,console]
----
$ ip addr show vrf S1
13: bond0.2001@bond0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master S1 state UP group default qlen 1000
    link/ether 48:df:37:95:8c:70 brd ff:ff:ff:ff:ff:ff
    inet 10.159.10.105/28 brd 10.159.10.111 scope global noprefixroute bond0.2001
       valid_lft forever preferred_lft forever
----

* показать устройства, назначенные VRF S1:

[source,console]
----
$ ip link show vrf S1
13: bond0.2001@bond0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master S1 state UP mode DEFAULT group default qlen 1000
    link/ether 48:df:37:95:cd:a0 brd ff:ff:ff:ff:ff:ff
----

* показать маршруты для VRF S1:

[source,console]
----
$ ip route show vrf S1
default via 10.159.10.97 dev bond0.2001
unreachable default metric 4278198272
10.159.10.96/28 dev bond0.2001 proto kernel scope link src 10.159.10.104 metric 400
----

NOTE: Допускается также сокращенная форма команды:

[source,bash]
----
$ ip ro sh vrf S1
----

* отобразить таблицу маршрутизации 5:

[source,console]
----
$ ip route show table 5
default via 10.159.10.97 dev bond0.2001
unreachable default metric 4278198272
10.159.10.96/28 dev bond0.2001 proto kernel scope link src 10.159.10.105 metric 400
local 10.159.10.105 dev bond0.2001 proto kernel scope host src 10.159.10.105
broadcast 10.159.10.111 dev bond0.2001 proto kernel scope link src 10.159.10.105
----

NOTE: Допускается также сокращенная форма команды:

[source,bash]
----
$ ip ro sh ta 5
----

* проверить состояние соединения:

[source,bash]
----
$ ping -I 10.159.10.105 10.159.10.97
----

NOTE: Запрос отправляется через source, 10.159.10.105, до роутера.

или

[source,bash]
----
$ ip vrf exec S1 ping 10.159.10.97
----

NOTE: Запускается командная строка для указанного VRF и отправляется запрос до роутера.