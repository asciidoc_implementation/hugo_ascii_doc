---
title: "Статистика"
description: "Описание формата файлов статистики"
type: docs
weight: 30
---
= Статистика
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

== Имя файла

Файлу статистики присваивается имя следующего формата: *__<node_name>\_<PM-Group>_<datetime>\_<granularity>.csv__*.

Ниже приведены параметры, которые могут использоваться в имени файла при его создании.

[options="header",cols="4,4,1"]
|===
| Параметр | Описание | Тип

| node_name
| Имя узла.
| string

| PM-Group
| Имя запроса или интерфейса с заменой пробелов на дефисы, *-*.
| string

| datetime
| Дата и время. Формат: +
*_<YYYY>-<MM>-<DD>T<hh>:<mm><offset>_*. +
NOTE: Время задается в 24-часовом формате.
| datetime

| <YYYY>
| Год в формате четырехзначного числа.
| int

| <MM>
| Месяц в формате двухзначного числа с добавлением нулей слева.
| int

| <DD>
| День месяца в формате двухзначного числа с добавлением нулей слева.
| int

| <hh>
| Час в формате двухзначного числа с добавлением нулей слева.
| int

| <mm>
| Минута в формате двухзначного числа с добавлением нулей слева.
| int

| <offset>
| Отклонение от времени UTC со знаком. Формат: +
*_+hhmm_* / *_-hhmm_*.
| string

| *granularity*
| Период гранулярности метрик. +
*_5min_* / *_15min_* / *_30min_* / *_60min_*.
| units
|===

.Пример
----
NSK-SGW01_SGW-users_2023-08-08T15:00-0300_5min.csv
MSK-SGW002_SGW-throughput_2023-07-06T05:15+0300_15min.csv
----

== Содержимое файла

Файл статистики имеет следующую структуру:

[source,csv]
----
<direction>,<metrics_name>,<metrics_group>:<metrics_group_value>,<metrics_value>
----

[options="header",cols="4,4,1"]
|===
| Параметр | Описание | Тип

| direction
| Направление передачи сообщения, по которому считается метрика. +
*_rx_* -- входящее сообщение; +
*_tx_* -- исходящее сообщение. +
NOTE: Для метрик, где невозможно определить направление, значение отсутствует.
| string

| metrics_name
| Название метрики.
| string

| metrics_group
| Название группы метрик. +
NOTE: Для агрегированных метрик по всем группам значение отсутствует. +
Возможно деление только по одной группе метрик в строке.
| string

| metrics_group_value
| Значение группы метрик.
| string

| metrics_value
| Значение метрики.
| Any
|===

.Пример
[source,csv]
----
rx,createBearerRequest,,1000
rx,createBearerRequest,TAI:2500103E8,1000
rx,createBearerRequest,APN:internet.mnc01.mcc250.gprs,800
rx,createBearerRequest,APN:data.mnc01.mcc250.gprs,200
rx,createBearerRequest,IMSIPLMN:99999,1000
tx,createBearerResponse,,900
tx,createBearerResponse,TAI:2500103E8,900
tx,createBearerResponse,APN:internet.mnc01.mcc250.gprs,800
tx,createBearerResponse,APN:data.mnc01.mcc250.gprs,100
tx,createBearerResponse,IMSIPLMN:99999,900
----