---
title: "S11 interface"
description: "Статистика по метрикам S11 интерфейса в части сигнальных сообщений"
type: docs
weight: 30
---
= S11 interface
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

Файл *__<node_name>\_SGW-S11-interface_<datetime>\_<granularity>.csv__* содержит статистическую информацию по метрикам S11-интерфейса SGW в части сигнальных сессионых сообщений.

.Описание параметров
[options="header",cols="1,4,4,2"]
|===
| Rx/Tx | Метрика | Описание | Группа

| Rx
| [[createSessionRequest]]createSessionRequest
| Количество сообщений GTP: Create Session Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createSessionResponse16]]createSessionResponse16
| Количество сообщений GTP: Create Session Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createSessionResponse17]]createSessionResponse17
| Количество сообщений GTP: Create Session Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[bearerResourceCommand]]bearerResourceCommand
| Количество сообщений GTP: Bearer Resource Command в процедурах eNodeB Requested Bearer Release или MME Initiated Dedicated Bearer Deactivation.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[bearerResourceFailureIndication]]bearerResourceFailureIndication
| Количество сообщений GTP: Bearer Resource Failure Indication.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[modifyBearerRequest]]modifyBearerRequest
| Количество сообщений GTP: Modify Bearer Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[modifyBearerResponse16]]modifyBearerResponse16
| Количество сообщений GTP: Modify Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[modifyBearerResponse17]]modifyBearerResponse17
| Количество сообщений GTP: Modify Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerRequestPgwInit]]deleteBearerRequestPgwInit
| Количество сообщений GTP: Delete Bearer Request, инициированных PGW.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerRequestUeInit]]deleteBearerRequestUeInit
| Количество сообщений GTP: Delete Bearer Request, инициированных UE в части Bearer Resource Modification.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerResponsePgwInit16]]deleteBearerResponsePgwInit16
| Количество сообщений GTP: Delete Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerResponsePgwInit17]]deleteBearerResponsePgwInit17
| Количество сообщений GTP: Delete Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerResponseUeInit16]]deleteBearerResponsePgwInit17
| Количество сообщений GTP: Delete Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[deleteSessionRequest]]deleteSessionRequest
| Количество сообщений GTP: Delete Session Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteSessionResponse16]]deleteSessionResponse16
| Количество сообщений GTP: Delete Session Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteSessionResponse17]]deleteSessionResponse17
| Количество сообщений GTP: Delete Session Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[downlinkDataNotification]]downlinkDataNotification
| Количество сообщений GTP: Downlink Data Notification.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[downlinkDataNotificationAck]]downlinkDataNotificationAck
| Количество сообщений GTP: Downlink Data Notification Acknowledge.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[downlinkDataNotificationFailureInd]]downlinkDataNotificationFailureInd
| Количество сообщений GTP: Downlink Data Notification Failure Indication.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[deleteIndirectDataForwardingTunReq]]deleteIndirectDataForwardingTunReq
| Количество сообщений GTP: Delete Indirect Data Forwarding Tunnel Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteIndirectDataForwardingTunRes]]deleteIndirectDataForwardingTunRes
| Количество сообщений GTP: Delete Indirect Data Forwarding Tunnel Response.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[modifyBearerCommand]]modifyBearerCommand
| Количество сообщений GTP: Modify Bearer Command в процедурах HSS Initiated subscribed QoS modification или при *_SQCI = 1_* в ответе Context Response.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[modifyBearerFailureIndication]]modifyBearerFailureIndication
| Количество сообщений GTP: Modify Bearer Failure Indication в процедуре HSS Initiated subscribed QoS modification.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[updateBearerRequest]]updateBearerRequest
| Количество сообщений GTP: Update Bearer Request.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[updateBearerResponse16]]updateBearerResponse16
| Количество сообщений GTP: Update Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[updateBearerResponse17]]updateBearerResponse17
| Количество сообщений GTP: Update Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[deleteBearerCommand]]deleteBearerCommand
| Количество сообщений GTP: Delete Bearer Command в процедурах eNodeB Requested Bearer Release или
MME Initiated Dedicated Bearer Deactivation.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[deleteBearerFailureIndication]]deleteBearerFailureIndication
| Количество сообщений GTP: Delete Bearer Failure Indication.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[createIndirectDataForwardingTunnelReq]]createIndirectDataForwardingTunnelReq
| Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createIndirectDataForwardingTunnelRes]]createIndirectDataForwardingTunnelRes
| Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Response.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[releaseAccessBearersRequest]]releaseAccessBearersRequest
| Количество сообщений GTP: Release Access Bearers Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[releaseAccessBearersResponse]]releaseAccessBearersResponse
| Количество сообщений GTP: Release Access Bearers Response.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[suspendNotification]]suspendNotification
| Количество сообщений GTP: Suspend Notification.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[suspendAcknowledge]]suspendAcknowledge
| Количество сообщений GTP: Suspend Acknowledge.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[resumeNotification]]resumeNotification
| Количество сообщений GTP: Resume Notification.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[resumeAcknowledge]]resumeAcknowledge
| Количество сообщений GTP: Resume Acknowledge.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createBearerRequest]]createBearerRequest
| Количество сообщений GTP: Create Bearer Request.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createBearerRequestQci1]]createBearerRequestQci1
| Количество сообщений GTP: Create Bearer Request c *_QCI = 1_*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createBearerRequestQci2]]createBearerRequestQci2
| Количество сообщений GTP: Create Bearer Request c *_QCI = 2_*.
| IMSIPLMN:Value, TAI:Value

| Tx
| [[createBearerRequestQci65]]createBearerRequestQci65
| Количество сообщений GTP: Create Bearer Request c *_QCI = 65_*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[createBearerResponse]]createBearerResponse
| Количество сообщений GTP: Create Bearer Response.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[createBearerResponseQci1]]createBearerResponseQci1
| Количество сообщений GTP: Create Bearer Response c *_QCI = 1_*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[createBearerResponseQci2]]createBearerResponseQci2
| Количество сообщений GTP: Create Bearer Response c *_QCI = 2_*.
| IMSIPLMN:Value, TAI:Value

| Rx
| [[createBearerResponseQci65]]createBearerResponseQci65
| Количество сообщений GTP: Create Bearer Response c *_QCI = 65_*.
| IMSIPLMN:Value, TAI:Value

| Tx/Rx
| [[gtpErrorIndication]]gtpErrorIndication
| Количество сообщений GTP: Error Indication.
|

| Tx/Rx
| [[gtpCv2EchoRequest]]gtpCv2EchoRequest
| Количество сообщений GTPv2-C: Echo Request.
|

| Tx/Rx
| [[gtpCv2EchoResponse]]gtpCv2EchoResponse
| Количество сообщений GTPv2-C: Echo Response.
|
|===

NOTE: Описание значений IE cause приведено в
https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf[3GPP TS 29.274].

.Группа
[cols="1,6,2"]
|===
| Название | Описание | Тип

| TAI
| Идентификатор области отслеживания. Формат: +
*_<mnc><mcc><TAC>_*.
| string

| <mnc>
| Код мобильной сети, {MNC}.
| int

| <mcc>
| Мобильный код страны, {MCC}.
| int

| TAC
| Код области отслеживания (Tracing Area Code).
| hex

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5/6 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
TAI:2500103E8
IMSIPLMN:25099
APN:internet.mnc01.mcc250.gprs
----