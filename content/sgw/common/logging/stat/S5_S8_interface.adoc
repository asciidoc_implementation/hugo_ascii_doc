---
title: "S5/S8 interface"
description: "Статистика по метрикам S5/S8 интерфейса в части сигнальных сообщений"
type: docs
weight: 30
---
= S5/S8 interface
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

Файл *__<node_name>\_SGW-S5-interface_<datetime>\_<granularity>.csv__* содержит статистическую информацию по метрикам S5/S8-интерфейса SGW в части сигнальных сессионых сообщений.

.Описание параметров
[cols="1,4,4,4"]
|===
| Rx/Tx | Метрика | Описание | Группа

| Tx
| [[createSessionRequest]]createSessionRequest
| Количество сообщений GTPv2-C: Create Session Request.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse16]]createSessionResponse16
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse17]]createSessionResponse17
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse64]]createSessionResponse64
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#64 Context not found"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse68]]createSessionResponse68
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#68 Service not supported"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse69]]createSessionResponse69
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#69 Mandatory IE incorrect"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse70]]createSessionResponse70
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#70 Mandatory IE missing"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse72]]createSessionResponse72
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#72 System failure"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse73]]createSessionResponse73
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#73 No resources available"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse78]]createSessionResponse78
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#78 Missing or unknown APN"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse84]]createSessionResponse84
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#84 All dynamic addresses are occupied"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse92]]createSessionResponse92
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#92 User authentication failed"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse93]]createSessionResponse93
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#93 APN access denied -- no subscription"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse94]]createSessionResponse94
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#94 Request rejected (reason not specified)"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse113]]createSessionResponse113
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#113 APN Congestion"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createSessionResponse125]]createSessionResponse125
| Количество сообщений GTPv2-C: Create Session Response с причиной *__"#125 UE not authorised by OCS or external AAA Server"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[updateBearerRequest]]updateBearerRequest
| Количество сообщений GTPv2-C: Update Bearer Request.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[updateBearerResponse16]]updateBearerResponse16
| Количество сообщений GTPv2-C: Update Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[updateBearerResponse17]]updateBearerResponse17
| Количество сообщений GTPv2-C: Update Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[modifyBearerRequest]]modifyBearerRequest
| Количество сообщений GTPv2-C: Modify Bearer Request.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[modifyBearerResponse16]]modifyBearerResponse16
| Количество сообщений GTPv2-C: Modify Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[modifyBearerResponse17]]modifyBearerResponse17
| Количество сообщений GTPv2-C: Modify Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[createBearerRequest]]createBearerRequest
| Количество сообщений GTPv2-C: Create Bearer Request.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createBearerResponse16]]createBearerResponse16
| Количество сообщений GTPv2-C: Create Bearer Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[createBearerResponse17]]createBearerResponse17
| Количество сообщений GTPv2-C: Create Bearer Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteBearerRequestPgwInit]]deleteBearerRequestPgwInit
| Количество сообщений GTPv2-C: Delete Bearer Request в процедуре удаления bearer-службы, инициированой PGW.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[deleteBearerResponsePgwInit16]]deleteBearerResponsePgwInit16
| Количество сообщений GTPv2-C: Delete Bearer Response в процедуре удаления bearer-службы, инициированой PGW с
причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[deleteBearerResponsePgwInit17]]deleteBearerResponsePgwInit17
| Количество сообщений GTPv2-C: Delete Bearer Response в процедуре удаления bearer-службы, инициированой PGW.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[deleteSessionRequestMmeInit]]deleteSessionRequestMmeInit
| Количество сообщений GTPv2-C: Delete Session Request в процедуре удаления bearer-службы, инициированой MME.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteSessionResponseMmeInit16]]deleteSessionResponseMmeInit16
| Количество сообщений GTPv2-C: Delete Session Response в процедуре удаления bearer-службы, инициированой MME с
причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteSessionResponseMmeInit17]]deleteSessionResponseMmeInit17
| Количество сообщений GTPv2-C: Delete Session Response в процедуре удаления bearer-службы, инициированой MME.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[deleteSessionRequest]]deleteSessionRequest
| Количество сообщений GTPv2-C: Delete Session Request.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteSessionResponse16]]deleteSessionResponse16
| Количество сообщений GTPv2-C: Delete Session Response с причиной *__"#16 Request Accepted"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteSessionResponse17]]deleteSessionResponse17
| Количество сообщений GTPv2-C: Delete Session Response с причиной *__"#17 Request Accepted Partially"__*.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[modifyBearerCommand]]modifyBearerCommand
| Количество сообщений GTPv2-C: Modify Bearer Command в части процедуры HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) или при *_SQCI = 1_* в ответе Context Response.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[modifyBearerFailureIndication]]modifyBearerFailureIndication
| Количество сообщений GTPv2-C: Modify Bearer Failure Indication в части процедуры HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD).
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[deleteBearerCommand]]deleteBearerCommand
| Количество сообщений GTPv2-C: Delete Bearer Command в части процедур eNodeB Requested Bearer Release или MME Initiated Dedicated Bearer Deactivation
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[deleteBearerFailureIndication]]deleteBearerFailureIndication
| Количество сообщений GTPv2-C: Delete Bearer Failure Indication.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[bearerResourceCommand]]bearerResourceCommand
| Количество сообщений GTPv2-C: Bearer Resource Command в части процедур eNodeB Requested Bearer Release или MME Initiated Dedicated Bearer Deactivation.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[bearerResourceFailureIndication]]bearerResourceFailureIndication
| Количество сообщений GTPv2-C: Bearer Resource Failure Indication.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[changeNotification]]changeNotification
| Количество сообщений GTPv2-C: Change Notification.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[changeNotificationAck]]changeNotificationAck
| Количество сообщений GTPv2-C: Change Notification Ack.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx/Rx
| [[gtpErrorIndication]]gtpErrorIndication
| Количество сообщений GTPv1-U: Error Indication.
|

| Tx/Rx
| [[gtpCv2EchoRequest]]gtpCv2EchoRequest
| Количество сообщений GTPv2-С: Echo Request Error.
|

| Tx/Rx
| [[gtpCv2EchoResponse]]gtpCv2EchoResponse
| Количество сообщений GTPv2-С: Echo Response Error.
|

| Tx/Rx
| [[gtpUEchoRequest]]gtpUv2EchoRequest
| Количество сообщений GTPv1-U: Echo Request Error.
|

| Tx/Rx
| [[gtpUEchoResponse]]gtpUv2EchoResponse
| Количество сообщений GTPv1-U: Echo Response Error.
|

| Rx
| [[suspendNotification]]suspendNotification
| Количество сообщений GTPv2-C: Suspend Notification.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[suspendAcknowledge]]suspendAcknowledge
| Количество сообщений GTPv2-C: Suspend Acknowledge.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx
| [[resumeNotification]]resumeNotification
| Количество сообщений GTPv2-C: Resume Notification.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Tx
| [[resumeAcknowledge]]resumeAcknowledge
| Количество сообщений GTPv2-C: Resume Acknowledge.
| IMSIPLMN:Value, TAI:Value, APN:Value

| Rx/Tx
| [[EndMarker]]EndMarker
| Количество сообщений GTPv1-U: End Marker.
|
|===

NOTE: Описание значений IE cause приведено в
https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf[3GPP TS 29.274].

.Группа
[cols="1,6,2"]
|===
| Название | Описание | Тип

| TAI
| Идентификатор области отслеживания. Формат: +
*_<mnc><mcc><TAC>_*
| string

| TAC
| Код области отслеживания (Tracing Area Code).
| hex

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5/6 цифр из номера IMSI
| int

| APN
| Идентификатор области отслеживания. Формат: +
*_<name>.mnc<mnc>.mcc<mcc>.<network>_*
| string

| <name>
| Имя точки доступа
| string

| <mnc>
| Код мобильной сети, {MNC}
| int

| <mcc>
| Мобильный код страны, {MCC}
| int

| <network>
| Тип сети
| string
|===

.Пример
----
TAI:2500103E8
IMSIPLMN:25099
APN:internet.mnc01.mcc250.gprs
----