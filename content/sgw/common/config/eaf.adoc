---
title: "eaf.conf"
description: "Конфигурация основных параметров сервиса SGW"
type: docs
weight: 30
---
= eaf.conf
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:ecmp: pass:[<abbr title="Equal Cost Multipath Routing">ECMP</abbr>]
:pci: pass:[<abbr title="Peripheral Component Interconnect">PCI</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:ecmp: ECMP
:pci: PCI
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

В файле задаются параметры внутренних обработчиков приложения и параметры внутреннего сетевого стэка на базе DPDK.

Файл имеет формат JSON.

<<bonds,bonds>>:: параметры bonding-устройств, см. http://doc.dpdk.org/guides/prog_guide/link_bonding_poll_mode_drv_lib.html#link-bonding-modes-overview[описание]

.Секции
[horizontal]
<<nics,nics>>:: параметры портов
<<interfaces,interfaces>>:: параметры интерфейсов
<<forwarders,forwarders>>:: параметры forwarder, логических компонент внутренней архитектуры SGW, выполняющих обработку и передачу частей потока пользовательского трафика
<<routes,routes>>:: таблица маршрутизации default vrf
<<vrfs,vrfs>>:: параметры VRF
<<gtp_path_management,gtp_path_management>>:: параметры настройки функциональности GTPv2 Path Management для обнаружения недоступности узлов PGW, с которыми есть установленные GTP-туннели

.Описание параметров
[cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| max_buffered_pkts
| Максимальное количество пакетов в буфере для bearer-служб. +
NOTE: Используется для хранения пакетов сессий, находящихся в состоянии *_ECM-IDLE_*. +
По умолчанию: 0, без ограничений.
| int
| O
| P
|

| tracers_cpu_id
| Номер ядра CPU, задействованного для обработки потока вывода статистики и работы с log-файлами. +
По умолчанию: 3.
| int
| O
| P
|

| control_plane_cpu_id
| Номер ядра CPU, задействованного для обработки потока плоскости управления. +
По умолчанию: 2.
| int
| O
| P
|

| preferred_numa_node
| Номер узла, предпочтительного для выделения памяти. +
По умолчанию: 0.
| int
| O
| P
|

| instance_id
| Префикс для потока {DPDK} {EAL}, *_--file-prefix_*. +
NOTE: Уникальное значение для каждого объекта DPDK-программ. +
По умолчанию: saegw_dpdk.
| string
| O
| P
|

| http_port
| Прослушиваемый порт HTTP-сервера. +
NOTE: Прослушиваются все сетевые интерфейсы, находящиеся под управлением операционной системы. +
По умолчанию: 9555.
| int
| O
| P
|

| dump_file_size
| Максимальный размер дампа, по достижении которого запись будет продолжена в новый файл, в мегабайтах. +
По умолчанию: 50.
| int
| O
| P
|

| dump_file_count
| Максимальное количество хранимых дампов каждого типа. +
По умолчанию: 0, без ограничений.
| int
| O
| P
|

| write_dump
| Флаг записи дампов трафика. +
По умолчанию: false.
| int
| O
| P
|

| eal_options
| Опции, которые должны быть указаны при инициализации EAL.
См. http://doc.dpdk.org/guides/linux_gsg/linux_eal_parameters.html[описание].
| [string]
| O
| P
|

| arp_cache_timeout
| Максимальное время жизни записи в ARP-таблице, в секундах. +
Диапазон: 10-1200. По умолчанию: 60.
| int
| O
| P
|

| *[[nics]]nics*
| Параметры портов сетевой карты.
| [object]
| M
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;type
| Тип порта. +
*_dpdk_* -- для портов шины {PCI}; +
*_af_packet_* -- в остальных случаях.
| string
| M
| P
|

| &nbsp;&nbsp;port
| Имя порта, необходимое для нахождения его DPDK-стеком. +
Для портов типа *_dpdk_* -- PCI-адрес устройства; +
Для портов типа *__af_packet__* -- имя интерфейса в операционной системе.
| string
| M
| P
|

| &nbsp;&nbsp;name
| Название порта.
| string
| M
| P
|

| &nbsp;&nbsp;mtu
| Максимальный размер пакета, в байтах. +
По умолчанию: 1500.
| int
| O
| P
|

| &nbsp;&nbsp;symmetric_queues
| Флаг создания очередей на прием в количестве, равном количеству очередей на отправку. +
NOTE: Используется для *_virtio-net_* и некоторых других витруальных карт. Дополнительные очереди создаются только на портах, связанных с forwarder. +
По умолчанию: false.
| bool
| O
| P
|

| &nbsp;&nbsp;args
| Дополнительные параметры устройства для DPDK. +
NOTE: Применимо только для типа *__af_packet__*.
См. http://doc.dpdk.org/guides/nics/af_packet.html#options-and-inherent-limitations[описание]. +
NOTE: Передается как строка в формате *__"args": "<parameter_1>=<value_1>,<parameter_N>=<value_N>"__*.
| {string,string}
| O
| P
|

| &nbsp;&nbsp;rx_mbuf_pool_size
| Максимальный объем буфера приема *_mbuf_*, в байтах. +
NOTE: Рекомендуется задать значение, на 1 меньшее степени числа 2. +
По умолчанию: 32&nbsp;767.
| int
| O
| P
|

| &nbsp;&nbsp;nb_rx_desc
| Количество дескрипторов пакетов в очереди на прием. +
По умолчанию: 2048.
| int
| O
| P
|

| *}*
|
|
|
|
|

| *[[bonds]]bonds*
| Параметры bonding-устройств.
| object
| O
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;[[members]]members
| Перечент имен портов, подчиненных bonding-устройству. +
NOTE: Каждый порт из массива <<nics,nics>> может находиться под управлением только одного bonding-устройства.
| list
| M
| P
|

| &nbsp;&nbsp;name
| Название bonding-устройства.
| string
| M
| P
|

| &nbsp;&nbsp;mode
| Режим работы bonding-устройства.
См. http://doc.dpdk.org/guides/prog_guide/link_bonding_poll_mode_drv_lib.html#link-bonding-modes-overview[описание]. +
NOTE: Значение может быть указано в формате строки либо числом. +
*_round_robin = 0_*; +
*_active_backup = 1_*; +
*_balance = 2_*; +
*_broadcast = 3_*; +
*_8023ad = 4_*; +
*_tlb = 5_*; +
*_alb = 6_*.
| string/int
| M
| P
|

| &nbsp;&nbsp;xmit_policy
| Политика передачи, которую применяет соединительное устройство при работе в режиме *_balance_* или *_8023ad_*. Для других режимов параметр игнорируется. +
NOTE: Значение может быть указано в формате строки либо числом. +
*_l2 = 0_* / *_l23 = 1_* / *_l34 = 2_*.
| string/int
| C
| P
|

| &nbsp;&nbsp;mac
| MAC-адрес bonding-устройства. +
По умолчанию: адрес <<primary,primary>>.
| string
| O
| P
|

| &nbsp;&nbsp;mtu
| Максимальный размер пакета для соединительного устройства, в байтах. +
По умолчанию: 1500.
| int
| O
| P
|

| &nbsp;&nbsp;nb_rx_desc
| Количество дескрипторов пакетов в очереди на прием. +
По умолчанию: 2048.
| int
| O
| P
|

| &nbsp;&nbsp;rx_mbuf_pool_size
| Максимальный объем буфера приема *_rte_mbuf_*, в байтах. +
NOTE: Рекомендуется задать значение, на 1 меньшее степени числа 2. +
По умолчанию: 32&nbsp;767.
| int
| O
| P
|

| &nbsp;&nbsp;[[primary]]primary
| Имя основного подчиненного порта. +
По умолчанию: первый элемент <<members,members>>.
| string
| O
| P
|

| &nbsp;&nbsp;symmetric_queues
| Флаг создания очередей на прием в количестве, равном количеству очередей на отправку. +
NOTE: Используется для *_virtio-net_* и некоторых других витруальных карт. +
Дополнительные очереди создаются только на портах, связанных с forwarder. +
По умолчанию: false.
| bool
| O
| P
|

| *}*
|
|
|
|
|

| *[[forwarders]]forwarders*
| Параметры forwarder.
| [object]
| M
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;rx_nic_name
| Имя порта, с которого на forwarder поступает на обработку поток пользовательского трафика. +
Имя порта должно быть указано в <<nics,nics>>.
| int
| M
| P
|

| &nbsp;&nbsp;cpu_id
| Номер ядра CPU, выделенного для обработки трафика через forwarder. +
По умолчанию: 0.
| int
| O
| P
|

| *}*
|
|
|
|
|

| *[[interfaces]]interfaces*
| Параметры интерфейсов портов сетевой карты.
| object
| M
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;name
| Название интерфейса, которое будет использовано для связывания с маршрутом, а также отображаться в HTTP-интерфейсе.
| string
| M
| P
|

| &nbsp;&nbsp;nic_name
| Название устройства, к которому привязан данный интерфейс. Может быть названием bonding устройства или именем порта из массива <<nics,nics>>, где расположен интерфейс. +
NOTE: Используется для ARP-ответов и GARP-ответов.
| int
| M
| P
|

| &nbsp;&nbsp;ip
| IPv4-адрес интерфейса.
| ip
| M
| P
|

| &nbsp;&nbsp;mask
| Битовая маска подсети интерфейса.
| int
| M
| P
|

| &nbsp;&nbsp;vlan
| Идентификатор VLAN для интерфейса. +
По умолчанию: 0, идентификатор VLAN отсутствует.
| int
| O
| P
|

| &nbsp;&nbsp;mtu
| Максимальный размер пакета. +
NOTE: Если 0, то заменяется на 1500.
| int
| O
| P
|

| &nbsp;&nbsp;vrf_name
| Имя VRF, связанного с данным интерфейсом.
| string
| O
| P
|

| *}*
|
|
|
|
|

| *[[routes]]routes*
| Таблица маршрутизации VRF по умолчанию.
| object
| O
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;ip
| Адрес сети.
| ip
| M
| P
|

| &nbsp;&nbsp;gw
| Адрес шлюза маршрута. +
NOTE: Для маршрутов в локальной сети, не через шлюз, рекомендуется указывать значение 0.0.0.0.
| ip
| M
| P
|

| &nbsp;&nbsp;mask
| Битовая маска подсети.
| int
| M
| P
|

| &nbsp;&nbsp;iface_name
| Имя сетевого интерфейса, отправляющего пакеты по маршруту, из массива <<interfaces,interfaces>>.
| int
| M
| P
|

| &nbsp;&nbsp;weight
| Вес маршрута, применяемый при применении {ECMP}. +
Чем меньше значение, тем больше шанс выбора. +
По умолчанию: 10.
| int
| O
| P
|

| &nbsp;&nbsp;track_bfd
| Название связанной с маршрутом BFD-сессии.
| string
| O
| P
|

| *}*
|
|
|
|
|

| *[[vrfs]]vrfs*
| Параметры VRF.
| object
| O
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;name
| Имя VRF маршрутизации.
| string
| M
| P
|

| &nbsp;&nbsp;ipv4_family
| Флаг поддержки адресации IPv4. +
По умолчанию: true.
| bool
| O
| P
|

| &nbsp;&nbsp;ipv6_family
| Флаг поддержки адресации IPv6. +
По умолчанию: true.
| bool
| O
| P
|

| &nbsp;&nbsp;routes
| Перечень маршрутов <<routes,routes>>, входящих в состав таблицы маршрутизации VRF.
| [object]
| O
| P
|

| *}*
|
|
|
|
|

| *[[gtp_path_management]]gtp_path_management*
| Параметры GTPv2 Path Management.
| object
| O
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;check_period
| Время после последнего сообщения от узла PGW до начала процедуры Liveliness Detection: Echo Request / Echo Response. +
По умолчанию: 60s.
| units
| О
| P
|

| &nbsp;&nbsp;N3_requests
| Количество отправленных запросов Echo Request без ответа, по достижении которого узел PGW считается недоступным. +
По умолчанию: 10
| int
| O
| P
|

| &nbsp;&nbsp;T3_ticks
| Время ожидания узлом SGW ответа Echo Response от узла PGW до последующей отправки Echo Request. +
По умолчанию: 10s.
| units
| O
| P
|

| *}*
|
|
|
|
|
|===

.Пример
[source,json]
----
{
  "max_buffered_pkts": 200000,
  "tracers_cpu_id": 3,
  "control_plane_cpu_id": 2,
  "preferred_numa_node": 0,
  "instance_id": "saegw_dpdk",
  "http_port": 9555,
  "dump_file_size": 100,
  "dump_file_count": 100,
  "write_dump": 1,
  "eal_options": [],
  "arp_cache_timeout": "60s",
  "nics": [
    {
      "type": "dpdk",
      "name": "ens0f0",
      "port": "0000:0c:00.1"
    },
    {
      "type": "dpdk",
      "name": "ens0f1",
      "port": "0000:0c:00.0"
    }
  ],
  "bonds": [
    {
      "members": [ "ens0f0", "ens0f1" ],
      "name": "net_bonding0",
      "mode": 4,
      "xmit_policy": 2,
      "mtu": 1500,
      "nb_rx_desc": 2048,
      "rx_mbuf_pool_size": 2000000,
      "symmetric_queues": false
    }
  ],
  "forwarders": [
    {
      "cpu_id": 6,
      "rx_nic_name": "net_bonding0"
    },
    {
      "cpu_id": 8,
      "rx_nic_name": "net_bonding0"
    },
    {
      "cpu_id": 10,
      "rx_nic_name": "net_bonding0"
    }
  ],
  "interfaces": [
    {
      "ip": "10.20.30.52",
      "mask": 29,
      "vlan": 1009,
      "mtu": 1500,
      "name": "S1-u_p2p_SGW1",
      "nic_name": "net_bonding0",
      "vrf_name": "S1"
    },
    {
      "ip": "10.20.30.68",
      "mask": 29,
      "vlan": 1011,
      "mtu": 1500,
      "name": "S5-u_p2p_SGW1",
      "nic_name": "net_bonding0",
      "vrf_name": "S5"
    },
    {
      "ip": "198.51.100.56",
      "mask": 32,
      "vlan": 1009,
      "mtu": 1500,
      "name": "S1-u_SGW1",
      "nic_name": "net_bonding0",
      "vrf_name": "S1"
    },
    {
      "ip": "198.51.100.60",
      "mask": 32,
      "vlan": 1011,
      "mtu": 1500,
      "name": "S5-u_SGW1",
      "nic_name": "net_bonding0",
      "vrf_name": "S5"
    }
  ],
  "vrfs": [
    {
      "name": "S1",
      "ipv4-family": true,
      "ipv6-family": false,
      "routes": [
        {
          "ip": "0.0.0.0",
          "gw": "10.20.30.49",
          "mask": 0,
          "track_bfd": "bfd_S1_u_1",
          "iface_name": "S1-u_p2p_SGW1"
        },
        {
          "ip": "0.0.0.0",
          "gw": "10.20.30.49",
          "mask": 0,
          "track_bfd": "bfd_S1_u_2",
          "iface_name": "S1-u_p2p_SGW1"
        }
      ]
    },
    {
      "name": "S5",
      "ipv4-family": true,
      "ipv6-family": false,
      "routes": [
        {
          "ip": "0.0.0.0",
          "gw": "10.153.151.65",
          "mask": 0,
          "track_bfd": "bfd_S5_u_1",
          "iface_name": "S5-u_p2p_SGW1"
        },
        {
          "ip": "0.0.0.0",
          "gw": "10.153.151.65",
          "mask": 0,
          "track_bfd": "bfd_S5_u_2",
          "iface_name": "S5-u_p2p_SGW1"
        }
      ]
    }
  ],
  "gtp_path_management": {
    "check_period": "70s",
    "N3_requests": 10,
    "T3_ticks": "10s"
  }
}
----