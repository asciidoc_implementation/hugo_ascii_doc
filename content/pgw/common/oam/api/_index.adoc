---
title: "HTTP API"
description: "Описание методов HTTP API"
type: docs
weight: 30
---
= HTTP API
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

В данном разделе приведено описание методов взаимодействия с узлом PROTEI PGW по протоколу HTTP.

== Список запросов

.Управление узлом PROTEI PGW
[horizontal]
link:stop[stop]:: остановить работу узла PROTEI PGW;
link:switch_role[switch_role(master/slave)]:: изменить роль узла на Master (ведущий) или Slave (ведомый);
link:recovery_counter[recovery_counter]:: получить количество перезапусков узла PROTEI PGW с момента установки;
link:master_uptime[master_uptime]:: получить дату и время запуска PROTEI PGW;
link:show_routes[show_routes]:: получить таблицу маршрутизации;
link:show_arp[show_arp]:: получить таблицу соответствия IP-адресов и MAC-адресов интерфейсов;

.Управление конфигурацией
[horizontal]
link:reload_core[reload_core]:: применить изменения в файле link:../../config/core[core.conf];
link:reload_net[reload_net]:: применить изменения в файле link:../../config/net[net.conf];
link:reload_ifaces[reload_ifaces]:: применить изменения в секции link:../../config/net/#ifaces[net.conf::ifaces];
link:reload_trace[reload_trace]:: применить изменения в файле link:../../config/trace[trace.conf];
link:reload_dscp_qos[reload_dscp_qos]:: применить изменения в файле link:../../config/dscp_qos[dscp_qos.conf];
link:show_applyed_config[show_applyed_config]:: получить список текущих значений параметров конфигурации узла;

.Управление APN
[horizontal]
link:set_apn_down[set_apn_down]:: деактивировать APN;
link:set_apn_up[set_apn_up]:: активировать APN;
link:get_apn_offload[get_apn_offload]:: получить информацию о доступности APN, с которыми работает узел PROTEI PGW;

.Управление абонентскими сессиями и запись дампов
[horizontal]
link:drop_connection[drop_connection]:: удалить активное подключение абонента по номеру IMSI или IP-адресу;
link:get_info[get_info]:: получить информацию об абонентской сессии по номеру IMSI или MSISDN;
link:add_to_trace[add_to_trace]:: активировать запись пользовательского трафика в дамп по номеру IMSI или
IP-адресу;
link:remove_from_trace[remove_from_trace]:: отключить запись пользовательского трафика в дамп по номеру IMSI
или IP-адресу;
link:get_trace_list[get_trace_list]:: получить список номеров IMSI и IP-адресов абонентов, для которых ведется
запись пользовательского трафика в дамп;
link:start_write_all_traffic[start_write_all_traffic]:: активировать запись всего пользовательского трафика в
дамп;
link:stop_write_all_traffic[stop_write_all_traffic]:: отключить запись всего пользовательского трафика в дамп;
link:flush_dump[flush_dump]:: записать дамп сетевого трафика;

.Вывод статистики
[horizontal]
link:http2_client_stat[http2_client_stat]:: получить статистику клиентской стороны протокола HTTP2;
link:http2_server_stat[http2_server_stat]:: получить статистику серверной стороны протокола HTTP2;
link:net_stat[net_stat]:: получить статистику сети;
link:radius_client_stat[radius_client_stat]:: получить статистику клиентской стороны протокола RADIUS;
link:diameter_client_stat[diameter_client_stat]:: получить статистику клиентской стороны протокола Diameter;
link:bfd_client_stat[bfd_client_stat]:: получить статистику клиента BFD;
link:oam_stat[oam_stat]:: получить статистику OAM;
link:gtpc_client_stat[gtpc_client_stat]:: получить статистику клиентской стороны протокола GTP-C;
link:core_stat[core_stat]:: получить статистику ядра узла;
link:dhcp_client_stat[dhcp_client_stat]:: получить статистику клиентской стороны протокола DHCP;
link:cntr[cntr]:: получить все доступные статистики;

Все запросы осуществляются с помощью метода HTTP GET. В запросе допускается использовать только одну команду.

.Формат запроса
[source,curl]
----
http://<host>:<port>/<command>?<param>=<value>&<param>=<value>
----

[options="header",cols="2,4,1,1"]
|===
| Поле | Описание |Тип | O/M

| host
| IP-адрес или DNS-имя хоста PROTEI PGW для приема запросов. +
NOTE: Определяется значением link:../../config/common[common.conf::oam::listen::addr].
| string
| M

| port
| Прослушиваемый порт OAM PROTEI PGW для приема запросов. +
NOTE: Определяется значением link:../../config/common[common.conf::oam::listen::port].
| int
| M

| command
| Команда для выполнения.
| string
| M

| param
| Параметр запроса.
| string
| O

| value
| Значение параметра.
| string
| O

|===

Если запрос не требует ответ, система отправляет статус выполнения запроса в формате *_<code><description>_*.

Ответом по умолчанию в случае успешного выполнения запроса является сообщение *_200 OK_*

.Коды ответов HTTP
[options="header",cols="1,4,8"]
|===
| Code | Message | Description

| 200
| Successful operation
| Операция выполнена успешно

| 400
| Bad request
| Ошибка составления запроса

| 401
| Authorization information is missing or invalid
| Параметры авторизации некорректны или отсутствуют

| 404
| A user with the specified ID was not found
| Не найдена информация по указанным данным

| 5XX
| Unexpected error
| Ошибка сервера
|===

Условные обозначения в таблицах описания параметров запросов и ответов

В поле *O/M* возможны следующие значения:

* (*M*)andatory -- обязательный параметр;
* (*O*)ptional -- необязательный (опциональный) параметр;
* (*C*)onditional -- параметр может быть как обязательным, так и опциональным, в зависимости от условий.