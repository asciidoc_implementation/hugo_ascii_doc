---
title: "diameter_client.conf"
description: "Параметры клиента протокола Diameter"
type: docs
weight: 20
---
= diameter_client.conf
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

В файле задаются настройки клиента протокола Diameter.

Файл имеет формат JSON.

.Секции
[horizontal]
<<common,common>>:: общие параметры
<<groups,groups>>:: параметры групп Diameter-пиров
<<common_groups,common>>::: общие параметры групп
<<vendor,vendor_spec_app_id>>::: параметры Vendor-Specific-App
<<listen,listen>>::: параметры удаленного адреса
<<peers,peers>>::: параметры пиров
<<routing_table,routing_table>>::: параметры таблицы маршрутизации

.Описание параметров
[options="header",cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| diameter_client
| Корень файла.
| object
| M
| R
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;[[common]]common
| Общие параметры.
| object
| O
| R
|

| &nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;message_queue_size
| Размер очереди команд.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;core_number
| Номер ядра для запуска потока diameter_client.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;timers
| Параметры таймеров.
| object
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;thread_sleep_time
| Период сна потока при простое.
| units
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;**[[groups]]groups**
| Параметры групп Diameter-пиров.
| [object]
| O
| R
|

| &nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**[[common_groups]]common**
| Общие параметры групп.
| object
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;max_not_replied_message
| Максимальное количество отправленных запросов без ответа до того, как узел будет считаться недоступным.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;max_wd_not_replied
| Максимальное количество отправленных запросов Diameter: Device-Watchdog-Request без ответа до того, как узел будет считаться недоступным.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spurious_connections
| Количество зарезервированных соединений для подключений других пиров.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transactions_per_peer
| Количество транзакций на пира. +
NOTE: Пул транзакций ограничен.
| int
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;timers
| Параметры таймеров.
| object
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;wait_answer
| Время ожидания ответа на запрос.
| units
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;watchdog
| Время ожидания для отправки запроса Diameter: Device-Watchdog-Request.
| units
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;recreate_socket
| Период пересоздания сокета.
| units
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reconnect
| Период переподключения.
| units
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;name
| Уникальное имя группы.
| string
| M
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;send_policy
| Политика выбора портов сервера для отправки сообщений. +
*_seq_* -- последовательный выбор; +
*_fan_* -- одновременный выбор нескольких.
| string
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;acc_app_id
| Значение *_Acct-Application-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 259.
| [int]
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;auth_app_id
| Значение *_Auth-Application-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 258.
| [int]
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;vendor_id
| Значение *_Vendor-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 266.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;product_name
| Значение *_Product-Name_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 269.
| string
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;supported_vendor_id
| Значение *_Supported-Vendor-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 265.
| [int]
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;firmware_rev
| Значение *_Firmware-Revision-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 267.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;sharing_contexts
| Флаг поддержки аварийного переключения внутри группы для запросов Diameter: Credit-Control-Request Initial. +
По умолчанию: true.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;process_cer_from_unknown_peer
| Флаг отправки ответа Diameter: Capabilities-Exchange-Answer c *_Result-Code = DIAMETER_UNKNOWN_PEER_* на запросы от неизвестных пиров. +
По умолчанию: false.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**[[vendor]]vendor_spec_app_id**
| Значения *_Vendor-Specific-Application-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 260.
| [object]
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;acc_app_id
| Значение *_Acct-Application-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 259.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;auth_app_id
| Значение *_Auth-Application-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 258.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;vendor_id
| Значение *_Vendor-Id_* для запросов Diameter: Capabilities-Exchange-Request/Answer. +
NOTE: См. https://tools.ietf.org/html/rfc6733[RFC 6733]. AVP: 266.
| [int]
| M
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**[[listen]]listen**
| Параметры удаленного пользователя.
| [object]
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr +
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addrs
| IPv4-/IPv6-адрес удаленного пользователя. +
Список IPv4- / IPv6-адресов. +
NOTE: Длина списка не должна превышать 2.
| ip/string +
[ip/string]
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;port +
ports
| Порт удаленного пользователя. +
Список портов.
| int
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transport
| Используемый транспортный протокол. +
*_tcp_* / *_sctp_* / *_ssl_tcp_* / *_ssl_sctp_*.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buffer_size
| Размер буфера сокета. +
NOTE: Только для транспортных протоколов *_tcp_* и *_sctp_*.
| int
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssl_options
| Параметры SSL. +
NOTE: Только для транспортных протоколов *_ssl_tcp_* и *_ssl_sctp_*.
| object
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;certificate_path
| Путь до файла сертификата *.pem.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;key_path
| Путь до ключа сертификата.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;disable_sslv2
| Флаг исключения SSLv2 из списка поддерживаемых версий. +
По умолчанию: true.
| bool
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;read_ahead
| Флаг включения опции *_read_ahead_* для SSL. +
По умолчанию: true.
| bool
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_options
| Параметры SCTP. +
NOTE: Только для транспортных протоколов *_sctp_* и *_ssl_sctp_*. +
См. https://datatracker.ietf.org/doc/html/rfc4895[RFC 4895].
| object
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;srto_max
| Максимальное время передачи сообщения, в миллисекундах.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;srto_min
| Минимальный порог времени передачи сообщения, в миллисекундах.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sasoc_asocmaxrxt
| Максимальное количество попыток передачи для ассоциации.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sinit_num_ostreams
| Максимальное количество исходящих потоков.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sinit_num_istreams
| Максимальное количество входящих потоков.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sinit_max_attempts
| Максимальное количество попыток передачи запросов *_INIT_* конечному узлу.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sinit_max_init_timeo
| Время ожидания между попытками передачи запроса *_INIT_*.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reset_linger
| Флаг использования *_SO_LINGER_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: true.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;use_mapped_v4_address
| Флаг *_SCTP_I_WANT_MAPPED_V4_ADDR_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: true.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_data_io_event
| Флаг *_sctp_data_io_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_association_event
| Флаг *_sctp_association_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_address_event
| Флаг *_sctp_address_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_send_failure_event
| Флаг *_sctp_send_failure_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_peer_error_event
| Флаг *_sctp_peer_error_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_shutdown_event
| Флаг *_sctp_shutdown_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_partial_delivery_event
| Флаг *_sctp_partial_delivery_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_authentication_event
| Флаг *_sctp_authentication_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sctp_adaptation_layer_event
| Флаг *_sctp_adaptation_layer_event_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 0.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;disable_frag
| Флаг *_SCTP_DISABLE_FRAGMENTS_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: false.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;enable_heart_beats
| Флаг *_SCTP_PEER_ADDR_PARAMS_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: false.
| bool
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;spp_hbinterval
| Период посылки сообщения *_heartbeat_*, в миллисекундах. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6458[RFC 6458]. +
По умолчанию: 6000.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ppid
| Значение *_PPID_* для фрагментов открытого текста SCTP DATA. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733]. +
По умолчанию: 46.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**[[peers]]peers**
| Параметры пиров.
| object
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;application_id
| Значение *_Application-Id_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733].
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;origin_host
| Значение *_Origin-Host_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733]. AVP: 264.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;origin_realm
| Значение *_Origin-Realm_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733]. AVP: 296.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination_host
| Значение *_Destination-Host_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733]. AVP: 293.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;destination_realm
| Значение *_Destination-Realm_*. +
NOTE: См. https://www.rfc-editor.org/rfc/rfc6733[RFC 6733]. AVP: 283.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;weight
| Вес, приоритет пира.
| int
| M
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr +
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addrs
| IPv4- / IPv6-адрес пира. +
Список IPv4- / IPv6-адресов пира. +
NOTE: Обязательный, если доступен без DRA. +
NOTE: Длина списка не должна превышать 2.
| ip/string +
[ip/string]
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;port +
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ports
| Прослушиваемый порт пира. +
Список прослушиваемых портов пира. +
NOTE: Обязательный, если доступен без DRA. +
NOTE: Длина списка не должна превышать 2.
| int
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transport
| Используемый транспортный протокол. +
*_tcp_* / *_sctp_* / *_ssl_tcp_* / *_ssl_sctp_*. +
NOTE: Обязательный, если доступен без DRA.
| string
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;buffer_size
| Размер буфера сокета. +
NOTE: Только для транспортных протоколов *_tcp_* и *_sctp_*.
| int
| C
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;limiters
| Параметры нагрузки.
| object
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ccr_i
| Максимальное количество запросов Diameter: Credit-Control-Request Initial. +
NOTE: При превышении осуществляется попытка аварийного переключения. При неудаче возвращение в логику.
| object
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bind
| Параметры узла для отправки.
| object
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr +
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addrs
| IPv4-/IPv6-адрес узла отправления. +
Список IPv4-/IPv6-адресов узла отправления. +
NOTE: Длина списка не должна превышать 2.
| ip/string +
[ip/string]
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;port +
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ports
| Порт узла отправления. +
Список портов узла отправления. +
NOTE: Длина списка не должна превышать 2.
| int
| O
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;**[[routing_table]]routing_table**
| Параметры таблицы маршрутизации.
| [object]
| C
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[host]]host
| Хост узла. +
NOTE: Должен быть задан этот параметр или <<realm,realm>>.
| string
| C
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[realm]]realm
| Realm узла. +
NOTE: Должен быть задан этот параметр или <<host,host>>.
| string
| C
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;route_peers_weight
| Вес / номер группы пира для получения host / realm.
| int
| M
| R
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;**}**
|
|
|
|
|

| *}*
|
|
|
|
|
|===

.Пример
[source,json]
----
{
  "diameter_client": {
    "common": {
      "message_queue_size": 40000,
      "core_number": 0,
      "timers": { "thread_sleep_time": "1ms" }
    },
    "groups": [
      {
        "common": {
          "max_not_replied_message": 1,
          "max_wd_not_replied": 3,
          "spurious_connections": 3,
          "transactions_per_peer": 65536,
          "timers": {
            "wait_answer": "3s",
            "watchdog": "30s",
            "recreate_socket": "5s",
            "reconnect": "30s"
          }
        },
        "name": "gx",
        "send_policy": "seq",
        "acc_app_id": [],
        "auth_app_id": [],
        "vendor_id": 0,
        "product_name": "",
        "supported_vendor_id": [],
        "firmware_rev": 0,
        "sharing_contexts": true,
        "process_cer_from_unknown_peer": false,
        "vendor_spec_app_id": [
          {
            "vendor_id": [],
            "acc_app_id": 1,
            "auth_app_id": 1
          }
        ],
        "listen": [
          {
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "tcp",
            "buffer_size": 65000
          },
          {
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "ssl_tcp",
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "disable_sslv2": true
            }
          },
          {
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "sctp",
            "buffer_size": 65000,
            "sctp_options": {
              "srto_max": 5000,
              "srto_min": 1000,
              "sasoc_asocmaxrxt": 4,
              "sinit_num_ostreams": 30,
              "sinit_num_istreams": 0,
              "sinit_max_attempts": 2,
              "sinit_max_init_timeo": 10000,
              "reset_linger": true,
              "use_mapped_v4_address": true,
              "sctp_data_io_event": 0,
              "sctp_association_event": 0,
              "sctp_address_event": 0,
              "sctp_send_failure_event": 1,
              "sctp_peer_error_event": 0,
              "sctp_shutdown_event": 1,
              "sctp_partial_delivery_event": 1,
              "sctp_authentication_event": 0,
              "sctp_adaptation_layer_event": 0,
              "disable_frag": false,
              "enable_heart_beats": true,
              "spp_hbinterval": 6000,
              "ppid": 46
            }
          },
          {
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "ssl_sctp",
            "sctp_options": {},
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "read_ahead": true,
              "disable_sslv2": true
            }
          }
        ],
        "peers": [
          {
            "application_id": 1,
            "origin_host": "P-GW1.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi1.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 10,
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "tcp",
            "buffer_size": 65000,
            "bind": {
              "addr": "192.168.108.31",
              "port": 3869
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW2.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi2.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 10,
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "ssl_tcp",
            "ssl_options": {
              "disable_sslv2": true
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW3.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi3.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 11,
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "sctp",
            "buffer_size": 65000,
            "bind": {
              "addrs": [ "192.168.108.31", "192.168.108.31" ],
              "ports": [ 3869, 3890 ]
            },
            "sctp_options": {}
          },
          {
            "application_id": 1,
            "origin_host": "P-GW4.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi4.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 11,
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "ssl_sctp",
            "buffer_size": 65000,
            "bind": {
              "addrs": [ "192.168.108.33", "192.168.108.34" ],
              "ports": [ 3869, 3890 ]
            },
            "sctp_options": {},
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "read_ahead": true,
              "disable_sslv2": true
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW1.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi1.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 1
          },
          {
            "application_id": 1,
            "origin_host": "P-GW3.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi3.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 2,
            "limiters": {
              "ccr_i": 1
            }
          }
        ],
        "routing_table": [
          {
            "host": "dpi1.dpi.ru",
            "route_peers_weight": 10
          },
          {
            "realm": "dpi",
            "route_peers_weight": 11
          }
        ]
      }
    ]
  }
}
----