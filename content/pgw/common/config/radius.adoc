---
title: "radius.conf"
description: "Конфигурация протокола RADIUS"
type: docs
weight: 20
---
= radius.conf
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

В файле задаются настройки протокола RADIUS: группы RADIUS-пиров, а также общие настройки протокола.

Файл имеет формат JSON.

.Секции
[horizontal]
<<common,common>>:: общие параметры
<<groups,groups>>:: параметры групп RADIUS-пиров

.Описание параметров
[options="header",cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[[common]]common*
| Общие параметры протокола RADIUS.
| object
| O
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;max_retransmit
| Максимальное количество перепосылок для пира. +
NOTE: При превышении сообщения отправляются следующему пиру. +
По умолчанию: 10.
| int
| O
| P
|

| &nbsp;&nbsp;max_try_send
| Максимальное количество попыток отправки. +
NOTE: При превышении сообщения отправляются следующему пиру. +
По умолчанию: 1&nbsp;000.
| int
| O
| P
|

| &nbsp;&nbsp;max_not_replied_message
| Максимальное количество сообщений без ответа. +
NOTE: При превышении сообщения отправляются следующему пиру.
| int
| O
| P
|

| &nbsp;&nbsp;message_queue_size
| Размер внутренней очереди сообщений, в битах.
| int
| O
| P
|

| &nbsp;&nbsp;socket_buffer_size
| Размер буфера сокета, в битах. +
По умолчанию: 65&nbsp;536.
| int
| O
| P
|

| &nbsp;&nbsp;core_number
| Номер ядра, на котором будет работать поток radius_client. +
По умолчанию: 0
| int
| O
| P
|

| &nbsp;&nbsp;dscp
| Код DSCP, который используется для марикровки IP-пакетов протокола RADIUS. +
По умолчанию: 0
| int
| O
| P
|

| &nbsp;&nbsp;timers
| Параметры таймеров.
| object
| O
| P
|

| &nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;transaction
| Время жизни сообщения с ответом, в миллисекундах. +
По умолчанию: 1.
| int
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;retransmit
| Время ожидания перепосылки сообщения, в миллисекундах. +
По умолчанию: 100.
| int
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;check_inactive
| Период проверки появления активности узла, в миллисекундах. +
По умолчанию: 1&nbsp;000.
| int
| O
| P
|

| &nbsp;&nbsp;**}**
|
|
|
|
|

| *}*
|
|
|
|
|

| *[[groups]]groups*
| Параметры групп протокола RADIUS.
| [object]
| M
| P
|

| *{*
|
|
|
|
|

| &nbsp;&nbsp;group_name
| Уникальное имя группы.
| string
| M
| P
|

| &nbsp;&nbsp;[[bind_addr]]bind_addr
| Привязанный IP-адрес.
| ip
| M
| P
|

| &nbsp;&nbsp;[[bind_port]]bind_port
| Привязанные порты.
| [int]
| M
| P
|

| &nbsp;&nbsp;port_policy
| Политика выбора порта сервера. +
*_seq_* -- последовательный выбор / *_fan_* -- одновременный выбор нескольких. +
По умолчанию: seq.
| string
| O
| P
|

| &nbsp;&nbsp;peer_policy
| Политика выбора пира. +
*_seq_* -- последовательный выбор / *_fan_* -- одновременный выбор нескольких. +
По умолчанию: seq.
| string
| O
| P
|

| &nbsp;&nbsp;listen_port
| Порт для ожидания запроса RADIUS Disconnect от AAA-сервера. +
По умолчанию: 1812.
| int
| O
| P
|

| &nbsp;&nbsp;peers
| Параметры пиров.
| [object]
| M
| P
|

| &nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;name
| Уникальное имя RADIUS-сервера.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;addr
| Удаленный IP-адрес RADIUS-сервера.
| ip
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;secret
| Секретный код RADIUS-сервера. +
NOTE: Должен совпадать с таковым на сервере.
| string
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;weight
| Приоритет пира. +
NOTE: Чем меньше значение, тем выше приоритет.
| int
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;auth_port
| Порт для процедуры авторизации. +
По умолчанию: 1812.
| int
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;acct_port
| Порт для процедуры тарификации. +
По умолчанию: 1813.
| int
| O
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;mirroring
| Параметры зеркалирования.
| [object]
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;addr
| IP-адрес зеркала.
| ip
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;port
| Порт зеркала. +
По умолчанию: <<bind_port,bind_port>>.
| ip
| M
| P
|

| &nbsp;&nbsp;&nbsp;&nbsp;**}**
|
|
|
|
|

| &nbsp;&nbsp;**}**
|
|
|
|
|

| *}*
|
|
|
|
|
|===

.Пример
[source,json]
----
{
  "radius": {
    "common": {
      "message_queue_size": 1000,
      "socket_buffer_size": 65536,
      "max_retransmit": 10,
      "max_try_send": 1000,
      "max_not_replied_message": 1,
      "timers": {
        "transaction": 1,
        "retransmit": 100,
        "check_inactive": 1000
      }
    },
    "groups": [
      {
        "group_name": "name",
        "bind_addr": "192.168.102.10",
        "bind_port": "1-4, 5",
        "port_policy": "seq",
        "peer_policy": "seq",
        "listen_port": 1812,
        "peers": [
          {
            "name": "auth",
            "addr": "192.168.108.45",
            "secret": "secret",
            "weight": 1,
            "auth_port": 1812,
            "acct_port": 1813,
            "mirroring": [
              {
                "addr": "192.168.100.231",
                "port": 1834
              }
            ]
          },
          {
            "name": "acct",
            "addr": "192.168.102.22",
            "secret": "testing123",
            "weight": 1,
            "auth_port": 1812,
            "acct_port": 1813
          }
        ]
      }
    ]
  }
}
----