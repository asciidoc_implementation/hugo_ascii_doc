---
title: "dscp_qos.conf"
description: "Параметры DSCP-маркировки пакетов с пользовательским трафиком на основании параметров QoS"
type: docs
weight: 20
---
= dscp_qos.conf
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

В файле задаются настройки назначения DSCP-меток для маркировки пользовательского трафика на основании параметров QoS.

Файл имеет формат JSON.

Ключ для reload -- *reload_dscp_qos*.

.Описание параметров
[options="header",cols="2,4,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *dscp_qos*
| Корневая секция параметров назначения DSCP-меток.
| object
| O
| R
|

| default
| DSCP-метка по умолчанию.
| int
| O
| R
|

| v1
| Массив значений параметров, по которым определяется DSCP-метка для пользовательских пакетов сессий 2G/3G, управление сессией по протоколу GTPv1.
| [object]
| O
| R
|

| arp
| Приоритет назначения и удержания каналов.
| int
| O
| R
|

| traffic_class
| Класс трафика.
| int
| O
| R
|

| thp
| Приоритет обработки трафика.
| int
| O
| R
|

| dscp
| Назначаемая DSCP-метка для указанных значений параметров.
| int
| O
| R
|

| v2
| Массив значений параметров, по которым определяется DSCP-метка для пользовательских пакетов сессий 4G, управляющий протокол GTPv2.
| [object]
| O
| R
|

| arp
| Приоритет назначения и удержания каналов.
| int
| O
| R
|

| qci
| Идентификатор класса качества обслуживания.
| int
| O
| R
|

| dscp
| Назначаемая DSCP-метка для указанных значений параметров.
| int
| O
| R
|

| translation_qos_value
| Правила для трансляции QoS между форматами R99 и IP QoS.
| object
| O
| P
|

| &nbsp;&nbsp;**{**
|
|
|
|
|

| &nbsp;&nbsp;**}**
|
|
|
|
|
|===

.Пример
[source,json]
----
{
  "dscp_qos": {
    "default": { "dscp": 8 },
    "v1": [
      { "arp": 1, "traffic_class": 1, "thp": 0, "dscp": 10 },
      { "arp": 2, "traffic_class": 1, "thp": 0, "dscp": 10 },
      { "arp": 3, "traffic_class": 1, "thp": 0, "dscp": 10 },

      { "arp": 1, "traffic_class": 2, "thp": 0, "dscp": 10 },
      { "arp": 2, "traffic_class": 2, "thp": 0, "dscp": 10 },
      { "arp": 3, "traffic_class": 2, "thp": 0, "dscp": 10 },

      { "arp": 1, "traffic_class": 3, "thp": 1, "dscp": 10 },
      { "arp": 2, "traffic_class": 3, "thp": 1, "dscp": 10 },
      { "arp": 3, "traffic_class": 3, "thp": 1, "dscp": 10 },
      { "arp": 1, "traffic_class": 3, "thp": 2, "dscp": 12 },
      { "arp": 2, "traffic_class": 3, "thp": 2, "dscp": 12 },
      { "arp": 3, "traffic_class": 3, "thp": 2, "dscp": 12 },
      { "arp": 1, "traffic_class": 3, "thp": 3, "dscp": 14 },
      { "arp": 2, "traffic_class": 3, "thp": 3, "dscp": 14 },
      { "arp": 3, "traffic_class": 3, "thp": 3, "dscp": 14 },

      { "arp": 1, "traffic_class": 4, "thp": 0, "dscp": 10 },
      { "arp": 2, "traffic_class": 4, "thp": 0, "dscp": 10 },
      { "arp": 3, "traffic_class": 4, "thp": 0, "dscp": 10 }
    ],
    "v2": [
      { "arp": 1, "qci": 1, "dscp": 46 },
      { "arp": 2, "qci": 1, "dscp": 46 },
      { "arp": 3, "qci": 1, "dscp": 46 },
      { "arp": 4, "qci": 1, "dscp": 46 },
      { "arp": 5, "qci": 1, "dscp": 46 },
      { "arp": 6, "qci": 1, "dscp": 46 },
      { "arp": 7, "qci": 1, "dscp": 46 },
      { "arp": 8, "qci": 1, "dscp": 46 },
      { "arp": 9, "qci": 1, "dscp": 46 },
      { "arp": 10, "qci": 1, "dscp": 46 },
      { "arp": 11, "qci": 1, "dscp": 46 },
      { "arp": 12, "qci": 1, "dscp": 46 },
      { "arp": 13, "qci": 1, "dscp": 46 },
      { "arp": 14, "qci": 1, "dscp": 46 },
      { "arp": 15, "qci": 1, "dscp": 46 },

      { "arp": 1, "qci": 5, "dscp": 34 },
      { "arp": 2, "qci": 5, "dscp": 34 },
      { "arp": 3, "qci": 5, "dscp": 34 },
      { "arp": 4, "qci": 5, "dscp": 34 },
      { "arp": 5, "qci": 5, "dscp": 34 },
      { "arp": 6, "qci": 5, "dscp": 34 },
      { "arp": 7, "qci": 5, "dscp": 34 },
      { "arp": 8, "qci": 5, "dscp": 34 },
      { "arp": 9, "qci": 5, "dscp": 34 },
      { "arp": 10, "qci": 5, "dscp": 34 },
      { "arp": 11, "qci": 5, "dscp": 34 },
      { "arp": 12, "qci": 5, "dscp": 34 },
      { "arp": 13, "qci": 5, "dscp": 34 },
      { "arp": 14, "qci": 5, "dscp": 34 },
      { "arp": 15, "qci": 5, "dscp": 34 },

      { "arp": 1, "qci": 69, "dscp": 34 },
      { "arp": 2, "qci": 69, "dscp": 34 },
      { "arp": 3, "qci": 69, "dscp": 34 },
      { "arp": 4, "qci": 69, "dscp": 34 },
      { "arp": 5, "qci": 69, "dscp": 34 },
      { "arp": 6, "qci": 69, "dscp": 34 },
      { "arp": 7, "qci": 69, "dscp": 34 },
      { "arp": 8, "qci": 69, "dscp": 34 },
      { "arp": 9, "qci": 69, "dscp": 34 },
      { "arp": 10, "qci": 69, "dscp": 34 },
      { "arp": 11, "qci": 69, "dscp": 34 },
      { "arp": 12, "qci": 69, "dscp": 34 },
      { "arp": 13, "qci": 69, "dscp": 34 },
      { "arp": 14, "qci": 69, "dscp": 34 },
      { "arp": 15, "qci": 69, "dscp": 34 },

      { "arp": 1, "qci": 65, "dscp": 46 },
      { "arp": 2, "qci": 65, "dscp": 46 },
      { "arp": 3, "qci": 65, "dscp": 46 },
      { "arp": 4, "qci": 65, "dscp": 46 },
      { "arp": 5, "qci": 65, "dscp": 46 },
      { "arp": 6, "qci": 65, "dscp": 46 },
      { "arp": 7, "qci": 65, "dscp": 46 },
      { "arp": 8, "qci": 65, "dscp": 46 },
      { "arp": 9, "qci": 65, "dscp": 46 },
      { "arp": 10, "qci": 65, "dscp": 46 },
      { "arp": 11, "qci": 65, "dscp": 46 },
      { "arp": 12, "qci": 65, "dscp": 46 },
      { "arp": 13, "qci": 65, "dscp": 46 },
      { "arp": 14, "qci": 65, "dscp": 46 },
      { "arp": 15, "qci": 65, "dscp": 46 },

      { "arp": 1, "qci": 6, "dscp": 26 },
      { "arp": 2, "qci": 6, "dscp": 26 },
      { "arp": 3, "qci": 6, "dscp": 26 },
      { "arp": 4, "qci": 6, "dscp": 26 },
      { "arp": 5, "qci": 6, "dscp": 26 },
      { "arp": 6, "qci": 6, "dscp": 26 },
      { "arp": 7, "qci": 6, "dscp": 26 },
      { "arp": 8, "qci": 6, "dscp": 26 },
      { "arp": 9, "qci": 6, "dscp": 26 },
      { "arp": 10, "qci": 6, "dscp": 26 },
      { "arp": 11, "qci": 6, "dscp": 26 },
      { "arp": 12, "qci": 6, "dscp": 26 },
      { "arp": 13, "qci": 6, "dscp": 26 },
      { "arp": 14, "qci": 6, "dscp": 26 },
      { "arp": 15, "qci": 6, "dscp": 26 },

      { "arp": 1, "qci": 7, "dscp": 24 },
      { "arp": 2, "qci": 7, "dscp": 24 },
      { "arp": 3, "qci": 7, "dscp": 24 },
      { "arp": 4, "qci": 7, "dscp": 24 },
      { "arp": 5, "qci": 7, "dscp": 24 },
      { "arp": 6, "qci": 7, "dscp": 24 },
      { "arp": 7, "qci": 7, "dscp": 24 },
      { "arp": 8, "qci": 7, "dscp": 24 },
      { "arp": 9, "qci": 7, "dscp": 24 },
      { "arp": 10, "qci": 7, "dscp": 24 },
      { "arp": 11, "qci": 7, "dscp": 24 },
      { "arp": 12, "qci": 7, "dscp": 24 },
      { "arp": 13, "qci": 7, "dscp": 24 },
      { "arp": 14, "qci": 7, "dscp": 24 },
      { "arp": 15, "qci": 7, "dscp": 24 },

      { "arp": 1, "qci": 8, "dscp": 12 },
      { "arp": 2, "qci": 8, "dscp": 12 },
      { "arp": 3, "qci": 8, "dscp": 12 },
      { "arp": 4, "qci": 8, "dscp": 12 },
      { "arp": 5, "qci": 8, "dscp": 12 },
      { "arp": 6, "qci": 8, "dscp": 12 },
      { "arp": 7, "qci": 8, "dscp": 12 },
      { "arp": 8, "qci": 8, "dscp": 12 },
      { "arp": 9, "qci": 8, "dscp": 12 },
      { "arp": 10, "qci": 8, "dscp": 12 },
      { "arp": 11, "qci": 8, "dscp": 12 },
      { "arp": 12, "qci": 8, "dscp": 12 },
      { "arp": 13, "qci": 8, "dscp": 12 },
      { "arp": 14, "qci": 8, "dscp": 12 },
      { "arp": 15, "qci": 8, "dscp": 12 },

      { "arp": 1, "qci": 9, "dscp": 10 },
      { "arp": 2, "qci": 9, "dscp": 10 },
      { "arp": 3, "qci": 9, "dscp": 10 },
      { "arp": 4, "qci": 9, "dscp": 10 },
      { "arp": 5, "qci": 9, "dscp": 10 },
      { "arp": 6, "qci": 9, "dscp": 10 },
      { "arp": 7, "qci": 9, "dscp": 10 },
      { "arp": 8, "qci": 9, "dscp": 10 },
      { "arp": 9, "qci": 9, "dscp": 10 },
      { "arp": 10, "qci": 9, "dscp": 10 },
      { "arp": 11, "qci": 9, "dscp": 10 },
      { "arp": 12, "qci": 9, "dscp": 10 },
      { "arp": 13, "qci": 9, "dscp": 10 },
      { "arp": 14, "qci": 9, "dscp": 10 },
      { "arp": 15, "qci": 9, "dscp": 10 }
    ]
  },
  "translation_qos_value": {
    "pre_emption": {
      "pci": 0,
      "pvi": 0
    },
    "arp_mapping_value": {
      "arp_mapping_H_value": 4,
      "arp_mapping_M_value": 9
    }
  }
}
----