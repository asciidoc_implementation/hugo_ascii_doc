---
title: "cdr"
description: "Журнал CDR пользовательских сессий"
type: docs
weight: 30
---
= cdr
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
:msisdn: pass:[<abbr title="Mobile Subscriber Integrated Services Digital Number">MSISDN</abbr>]
:teid: pass:[<abbr title="Tunnel Endpoint Identifier">TEID</abbr>]
:pdp: pass:[<abbr title="Packet Data Protocol">PDP</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:msisdn: MSISDN
:teid: TEID
:pdp: PDP
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: left
endif::[]

== Назначение

Журнал *EDR* (внутреннее название CDR) предназначен для фиксации ключевых событий, связанных с обработкой пользовательских сесси. Для каждого события создается по одной записи на каждый канал передачи данных, которые затрагивает событие. Для событий, относящихся ко всей сессии, создается по одной записи на каждый канал передачи данных в этой сессии.

Для активации ведения журналов EDR необходимо задать параметры логирования в секции *cdr* конфигурационного файла
link:../../../config/trace/#journal_name[trace.conf].

== Описание формата

Формат по умолчанию имени файла CDR -- *__cdr_<YYYY><MM><DD>_<hh><mm>.cdr__*.

Каждая запись располагается на отдельной строке. Значения полей разделяются символом *_;_*.

[source,text]
----
DT; EventType; IMSI; MSISDN; IMEI; SubscriberIP; SubscriberIpPoolID; FinalAPN; InitAPN; SessionCreationDT; RAT; MCC; MNC; LAC; CellID; RAC; TAC; SAC; CGI; ECGI; ChargingID; NSAPI; SGSN; PackagesDL; BytesDL; DroppedDL; PackagesUL; BytesUL; DroppedUL; PGW; Peer; DeactivationCause; GxResult; GyResult
----

.Используемые поля
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| DT
| Дата и время создания записи.
| datetime

| 2
| EventType
| Тип события, вызвавший создание записи. +
*_Create_* -- создание PDP Context по запросу _GTPv1 Create PDP Context_ или _GTPv2 Create Session Request_; +
*_Update_* -- обновление PDP Context по запросу _GTPv1 Update PDP Context_ или _GTPv2 Update Bearer Request_; +
*_Modify_* -- изменение EPS Bearer по запросу _GTPv2 Modify Bearer Request_; +
*_radius_timer_*, *_gx_timer_*, *_gy_timer_* -- истечение таймера; +
*_Quota_* -- исчерпание выделенной квоты; +
*_Delete_* -- удаление PDP Context или EPS Bearer по запросу _GTPv1 Delete PDP Context_ или _GTPv2 Delete Session_; +
*_Reject_Create_* -- неудачное создание PDP Context по запросу _GTPv1 Create PDP Context_ или _GTPv2 Create Session Request_.
| string

| 3
| IMSI
| Номер {IMSI}.
| string

| 4
| MSISDN
| Номер {MSISDN}.
| string

| 5
| IMEI
| Номер {IMEI}.
| string

| 6
| SubscriberIP
| IP-адрес пользователя.
| ip

| 7
| SubscriberIpPoolID
| Идентификатор пула IP-адресов абонента.
| int

| 8
| FinalAPN
| Итоговое {apn}.
| string

| 9
| InitAPN
| Первоначальное APN.
| string

| 10
| SessionCreationDT
| Метка времени Unix создания сессии.
| timestamp

| 11
| RAT
| Код типа сети. +
*_1_* -- {UTRAN}; +
*_2_* -- {GERAN}; +
*_3_* -- WLAN; +
*_4_* -- {gan}; +
*_5_* -- {HSPA} Evolution; +
*_6_* -- {E-UTRAN}; +
*_7_* -- Virtual; +
*_8_* -- E-UTRAN-{NB-IoT}.
| int

| 12
| MCC
| Мобильный код страны.
| int

| 13
| MNC
| Код мобильной сети.
| int

| 14
| LAC
| Код локальной зоны.
| int

| 15
| CellID
| Идентификатор соты.
| int

| 16
| RAC
| Код зоны маршрутизации.
| int

| 17
| TAC
| Код зоны отслеживания.
| int

| 18
| SAC
| Код зоны обслуживания.
| int

| 19
| CGI
| Глобальный идентификатор соты.
| int

| 20
| ECGI
| Глобальный идентификатор соты в сети E-UTRAN.
| int

| 21
| ChargingID
| Идентификатор плательщика.
| int

| 22
| NSAPI_BearerID
| Идентификатор PDP-контекста (2G / 3G) или EPS bearer-службы (4G).
| int

| 23
| SGSN_SGW
| IP-адрес {SGSN} / {SGW}.
| ip/string

| 24
| PackagesDL
| Количество переданных пакетов по нисходящим направлениям к абоненту.
| int

| 25
| BytesDL
| Количество переданных байтов по нисходящим направлениям к абоненту.
| int

| 26
| DroppedDL
| Количество отброшенных пакетов по нисходящим направлениям.
| int

| 27
| PackagesUL
| Количество переданных пакетов по восходящим направлениям от абонента.
| int

| 28
| BytesUL
| Количество переданных байтов по восходящим направлениям от абонента.
| int

| 29
| DroppedUL
| Количество отброшенных пакетов по восходящим направлениям.
| int

| 30
| PGW_CP_TEID
| {teid} плоскости управления {PGW}.
| int

| 31
| Peer_CP_TEID
| {TEID} плоскости управления SGW / SGSN пира.
| int

| 32
| DeactivationCause
| Причина деактивации {PDP}-контекста службы передачи данных.
| int

| 33
| GxResult
| Diameter код результата выполнения запроса по Gx-интерфейсу.
| int

| 34
| GyResult
| Diameter код результата выполнения запроса по Gy-интерфейсу.
| int
|===

.Пример
[source,log]
----
2023-08-21 16:45:00.979;modify;466920100001205;76000000205;8654690603093949;198.51.100.161;0;internet;internet.mnc092.mcc466.gprs;1692624031285;6;466;92;;;;81;;;28675;439;5;192.168.125.96;265;71914;0;293;96123;0;439;83886178;;;
----