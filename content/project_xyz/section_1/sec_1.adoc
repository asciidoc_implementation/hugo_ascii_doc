---
title: "Section 1"
---

= Section 1 Page
:doctype: book

You will hardly find anything interesting here.

image::wallpaper.jpg[]

xref:section_2/sec_2.adoc[Section 2]