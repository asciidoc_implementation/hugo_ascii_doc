---
title: Tracking Area Update
description: Статистика процедур TAU
type: docs
weight: 20
---

= Tracking Area Update

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-tau\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур TRACKING AREA UPDATE.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Rx
| [[intraTauRequest]]intraTauRequest
| Количество сообщений NAS TRACKING AREA UPDATE REQUEST c *_Update Type = TA updating_*.
| TAI:Value

| Tx
| [[intraTauSuccess]]intraTauSuccess
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_*.
| TAI:Value

| Tx
| [[intraTauReject]]intraTauReject
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_*.
|

| Tx
| [[intraTauReject3]]intraTauReject3
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#3 Illegal UE".
| TAI:Value

| Tx
| [[intraTauReject6]]intraTauReject6
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#6 Illegal ME".
| TAI:Value

| Tx
| [[intraTauReject7]]intraTauReject7
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#7 EPS services not allowed".
| TAI:Value

| Tx
| [[intraTauReject9]]intraTauReject9
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#9 UE identity cannot be derived by the network".
| TAI:Value

| Tx
| [[intraTauReject10]]intraTauReject10
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#10 Implicitly detached".
| TAI:Value

| Tx
| [[intraTauReject11]]intraTauReject11
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#11 PLMN not allowed".
| TAI:Value

| Tx
| [[intraTauReject12]]intraTauReject12
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#12 Tracking Area not allowed".
| TAI:Value

| Tx
| [[intraTauReject13]]intraTauReject13
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value

| Tx
| [[intraTauReject14]]intraTauReject14
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value

| Tx
| [[intraTauReject15]]intraTauReject15
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#15 No Suitable Cells In tracking area".
| TAI:Value

| Tx
| [[intraTauReject40]]intraTauReject40
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#40 No EPS bearer context activated".
| TAI:Value

| Tx
| [[intraTauReject111]]intraTauReject111
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#111 Protocol error, unspecified".
| TAI:Value

| Rx
| [[periodTauRequest]]periodTauRequest
| Количество сообщений NAS TRACKING AREA UPDATE REQUEST с *_Update Type = periodic updating_*.
| TAI:Value

| Tx
| [[periodTauSuccess]]periodTauSuccess
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = periodic updating_*.
| TAI:Value

| Tx
| [[periodTauReject]]periodTauReject
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_*.
|

| Tx
| [[periodTauReject3]]periodTauReject3
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#3 Illegal UE".
| TAI:Value

| Tx
| [[periodTauReject6]]periodTauReject6
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#6 Illegal ME".
| TAI:Value

| Tx
| [[periodTauReject7]]periodTauReject7
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#7 EPS services not allowed".
| TAI:Value

| Tx
| [[periodTauReject9]]periodTauReject9
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#9 UE identity cannot be derived by the network".
| TAI:Value

| Tx
| [[periodTauReject10]]periodTauReject10
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#10 Implicitly detached".
| TAI:Value

| Tx
| [[periodTauReject11]]periodTauReject11
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#11 PLMN not allowed".
| TAI:Value

| Tx
| [[periodTauReject12]]periodTauReject12
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#12 Tracking Area not allowed".
| TAI:Value

| Tx
| [[periodTauReject13]]periodTauReject13
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value

| Tx
| [[periodTauReject14]]periodTauReject14
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value

| Tx
| [[periodTauReject15]]periodTauReject15
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#15 No Suitable Cells In tracking area".
| TAI:Value

| Tx
| [[periodTauReject40]]periodTauReject40
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#40 No EPS bearer context activated".
| TAI:Value

| Tx
| [[periodTauReject111]]periodTauReject111
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = periodic updating_* и причиной "#111 Protocol error, unspecified".
| TAI:Value

| Rx
| [[interTauRequest]]interTauRequest
| Количество сообщений inter-MME NAS TRACKING AREA UPDATE REQUEST c *_Update Type = TA updating_*.
| TAI:Value

| Tx
| [[interTauSuccess]]interTauSuccess
| Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_*.
| TAI:Value

| Tx
| [[interTauReject]]interTauReject
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_*.
| TAI:Value

| Tx
| [[interTauReject3]]interTauReject3
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#3 Illegal UE".
| TAI:Value

| Tx
| [[interTauReject6]]interTauReject6
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#6 Illegal ME".
| TAI:Value

| Tx
| [[interTauReject7]]interTauReject7
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#7 EPS services not allowed".
| TAI:Value

| Tx
| [[interTauReject9]]interTauReject9
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#9 UE identity cannot be derived by the network".
| TAI:Value

| Tx
| [[interTauReject10]]interTauReject10
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#10 Implicitly detached".
| TAI:Value

| Tx
| [[interTauReject11]]interTauReject11
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#11 PLMN not allowed".
| TAI:Value

| Tx
| [[interTauReject12]]interTauReject12
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#12 Tracking Area not allowed".
| TAI:Value

| Tx
| [[interTauReject13]]interTauReject13
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value

| Tx
| [[interTauReject14]]interTauReject14
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value

| Tx
| [[interTauReject15]]interTauReject15
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#15 No Suitable Cells In tracking area".
| TAI:Value

| Tx
| [[interTauReject40]]interTauReject40
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#40 No EPS bearer context activated".
| TAI:Value

| Tx
| [[interTauReject111]]interTauReject111r
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = TA updating_* и причиной "#111 Protocol error, unspecified".
| TAI:Value

| Rx
| [[intraCombinedTauRequest]]intraCombinedTauRequest
| Количество сообщений NAS TRACKING AREA UPDATE REQUEST c *_Update Type = combined TA/LA updating_* или *_Update Type = combined TA/LA updating with IMSI attach_*.
| TAI:Value

| Tx
| [[intraCombinedTauSuccess]]intraCombinedTauSuccess
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = combined TA/LA updating_*.
| TAI:Value

| Tx
| [[intraCombinedTauSuccessSmsOnly]]intraCombinedTauSuccessSmsOnly
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = combined TA/LA updating_* и *_Additional update result_*= *_SMS only_*
| TAI:Value

| Tx
| [[intraCombinedTauSuccess2]]intraCombinedTauSuccess2
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_* и причиной "#2 IMSI unknown in HSS".
| TAI:Value

| Tx
| [[intraCombinedTauSuccess18]]intraCombinedTauSuccess18
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_* и причиной "#18 CS domain not available".
| TAI:Value

| Tx
| [[intraCombinedTauReject]]intraCombinedTauReject
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_*.
| TAI:Value

| Tx
| [[intraCombinedTauReject3]]intraCombinedTauReject3
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#3 Illegal UE".
| TAI:Value

| Tx
| [[intraCombinedTauReject6]]intraCombinedTauReject6
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#6 Illegal ME".
| TAI:Value

| Tx
| [[intraCombinedTauReject7]]intraCombinedTauReject7
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#7 EPS services not allowed".
| TAI:Value

| Tx
| [[intraCombinedTauReject9]]intraCombinedTauReject9
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#9 UE identity cannot be derived by the network".
| TAI:Value

| Tx
| [[intraCombinedTauReject10]]intraCombinedTauReject10/a>
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#10 Implicitly detached".
| TAI:Value

| Tx
| [[intraCombinedTauReject11]]intraCombinedTauReject11
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#11 PLMN not allowed".
| TAI:Value

| Tx
| [[intraCombinedTauReject12]]intraCombinedTauReject12
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#12 Tracking Area not allowed".
| TAI:Value

| Tx
| [[intraCombinedTauReject13]]intraCombinedTauReject13
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value

| Tx
| [[intraCombinedTauReject14]]intraCombinedTauReject14
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value

| Tx
| [[intraCombinedTauReject15]]intraCombinedTauReject15
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#15 No Suitable Cells In tracking area".
| TAI:Value

| Tx
| [[intraCombinedTauReject40]]intraCombinedTauReject40
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#40 No EPS bearer context activated".
| TAI:Value

| Tx
| [[intraCombinedTauReject111]]intraCombinedTauReject111
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#111 Protocol error, unspecified".
| TAI:Value

| Rx
| [[interCombinedTauRequest]]interCombinedTauRequest
| Количество сообщений inter-MME NAS TRACKING AREA UPDATE REQUEST c *_Update Type = combined TA/LA updating_* или *_Update Type = combined TA/LA updating with IMSI attach_*.
| TAI:Value

| Tx
| [[interCombinedTauSuccess]]interCombinedTauSuccess
| Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = combined TA/LA updating_*.
| TAI:Value

| Tx
| [[interCombinedTauSuccessSmsOnly]]interCombinedTauSuccessSmsOnly
| Количество сообщений inter-MME NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = combined TA/LA updating_* и *_Additional update result = SMS only_*.
| TAI:Value

| Tx
| [[interCombinedTauSuccess2]]interCombinedTauSuccess2
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_* и причиной "#2 IMSI unknown in HSS".
| TAI:Value

| Tx
| [[interCombinedTauSuccess18]]interCombinedTauSuccess18
| Количество сообщений NAS TRACKING AREA UPDATE ACCEPT с *_Update Type = TA updating_* и причиной "#18 CS domain not available".
| TAI:Value

| Tx
| [[interCombinedTauReject]]interCombinedTauReject
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_*.
| TAI:Value

| Tx
| [[interCombinedTauReject3]]interCombinedTauReject3
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#3 Illegal UE".
| TAI:Value

| Tx
| [[interCombinedTauReject6]]interCombinedTauReject6
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#6 Illegal ME".
| TAI:Value

| Tx
| [[interCombinedTauReject7]]interCombinedTauReject7
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#7 EPS services not allowed".
| TAI:Value

| Tx
| [[interCombinedTauReject9]]interCombinedTauReject9
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#9 UE identity cannot be derived by the network".
| TAI:Value

| Tx
| [[interCombinedTauReject10]]interCombinedTauReject10
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#10 Implicitly detached".
| TAI:Value

| Tx
| [[interCombinedTauReject11]]interCombinedTauReject11
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#11 PLMN not allowed".
| TAI:Value

| Tx
| [[interCombinedTauReject12]]interCombinedTauReject12
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#12 Tracking Area not allowed".
| TAI:Value

| Tx
| [[interCombinedTauReject13]]interCombinedTauReject13
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value

| Tx
| [[interCombinedTauReject14]]interCombinedTauReject14
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value

| Tx
| [[interCombinedTauReject15]]interCombinedTauReject15
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#15 No Suitable Cells In tracking area".
| TAI:Value

| Tx
| [[interCombinedTauReject40]]interCombinedTauReject40
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#40 No EPS bearer context activated".
| TAI:Value

| Tx
| [[interCombinedTauReject111]]interCombinedTauReject111
| Количество сообщений NAS TRACKING AREA UPDATE REJECT с *_Update Type = combined TA/LA updating_* и причиной "#111 Protocol error, unspecified".
| TAI:Value
|===

NOTE: Описание причин ошибок EMM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| TAI
| Идентификатор области отслеживания. Формат: +
*_<plmn_id><tac>_*.
| hex

| <plmn_id>
| Идентификатор сети PLMN.
| int

| <tac>
| Код области отслеживания.
| hex
|===

.Пример
[source,console]
----
TAI:2500103E8
----

.Пример
[source,csv]
----
rx,intraTauRequest,,0
tx,intraTauSuccess,,0
tx,intraTauReject,,0
rx,periodTauRequest,,0
tx,periodTauSuccess,,0
tx,periodTauReject,,0
rx,interTauRequest,,0
tx,interTauSuccess,,0
tx,interTauReject,,0
rx,intraCombinedTauRequest,,4
rx,intraCombinedTauRequest,TAI:001010001,2
rx,intraCombinedTauRequest,TAI:001010010,2
tx,intraCombinedTauSuccess,,3
tx,intraCombinedTauSuccess,TAI:001010001,1
tx,intraCombinedTauSuccess,TAI:001010010,2
tx,intraCombinedTauReject9,,1
tx,intraCombinedTauReject9,TAI:001010001,1
rx,interCombinedTauRequest,,0
tx,interCombinedTauSuccess,,0
tx,interCombinedTauReject,,0
----