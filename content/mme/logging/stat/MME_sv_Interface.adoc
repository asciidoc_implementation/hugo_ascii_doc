---
title: Sv interface
description: Статистика процедур Sv
type: docs
weight: 20
---

= Sv interface

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-svInterface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса Sv.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf[3GPP TS 29.280].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Tx
| [[srvccPsToCsRequest]]srvccPsToCsRequest
| Количество сообщений GTPv2-C: SRVCC PS to CS Request.
|

| Rx
| [[srvccPsToCsResponse]]srvccPsToCsResponse
| Количество сообщений GTPv2-C: SRVCC PS to CS Response.
|

| Rx
| [[pagingReqsrvccPsToCsCompleteNotificationuest]]srvccPsToCsCompleteNotification
| Количество сообщений GTPv2-C: SRVCC PS to CS Complete Notification.
|

| Tx
| [[srvccPsToCsCompleteAcknowledge]]srvccPsToCsCompleteAcknowledge
| Количество сообщений GTPv2-C: SRVCC PS to CS Complete Acknowledge.
|
|===

.Пример
[source,csv]
----
tx,srvccPsToCsRequest,,0
rx,srvccPsToCsResponse,,0
rx,srvccPsToCsCompleteNotification,,0
tx,srvccPsToCsCompleteAcknowledge,,0
----