---
title: S1 Security
description: Статистика процедур NAS
type: docs
weight: 20
---

= S1 Security

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-s1Security\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для протокола NAS.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301] и
https://www.etsi.org/deliver/etsi_ts/124500_124599/124501/17.10.01_60/ts_124501v171001p.pdf[3GPP TS 24.501].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Tx
| [[securityModeCommand]]securityModeCommand
| Количество сообщений NAS SECURITY MODE COMMAND.
|

| Rx
| [[securityModeCommandComplete]]securityModeCommandComplete
| Количество сообщений NAS SECURITY MODE COMPLETE.
|

| Rx
| [[securityModeCommandReject]]securityModeCommandReject
| Количество сообщений NAS SECURITY MODE REJECT.
|

| Tx
| [[authenticationRequest]]authenticationRequest
| Количество сообщений NAS AUTHENTICATION REQUEST.
|

| Rx
| [[authenticationResponse]]authenticationResponse
| Количество сообщений NAS AUTHENTICATION RESPONSE.
|

| Tx
| [[authenticationReject]]authenticationReject
| Количество сообщений NAS AUTHENTICATION REJECT.
|

| Rx
| [[authenticationFailure]]authenticationFailure
| Количество сообщений NAS AUTHENTICATION Failure
|

| Rx
| [[authenticationFailure21]]authenticationFailure21
| Количество сообщений NAS AUTHENTICATION FAILURE c причиной "#21 Synch failure".
|
|===

NOTE: Описание причин ошибок EMM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Пример
[source,csv]
----
tx,securityModeCommand,,8
rx,securityModeCommandComplete,,8
rx,securityModeCommandReject,,0
tx,authenticationRequest,,14
rx,authenticationResponse,,7
tx,authenticationReject,,0
rx,authenticationFailure,,7
rx,authenticationFailure21,,7
----