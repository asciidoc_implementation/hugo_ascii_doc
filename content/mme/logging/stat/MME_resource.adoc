---
title: HW Resource
description: Статистика использования ресурсов
type: docs
weight: 20
---

= HW Resource

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-resource\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для использования ресурсов сервера.

NOTE: Подсчитывается для ресурсов, используемых только MME.

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

|
| [[averageCpuUtilization]]averageCpuUtilization
| Средняя загрузка CPU за период измерения в процентах.
|

|
| [[maxCpuUtilization]]maxCpuUtilization
| Максимальная загрузка CPU за период измерения в процентах.
|
|===

.Пример
[source,csv]
----
,averageCpuUtilization,,1.12436
,maxCpuUtilization,,1.8
----