---
title: S1 Attach
description: Статистика процедур S1 Attach
type: docs
weight: 20
---

= S1 Attach

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Файл **<node_name>\_MME-s1Attach\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S1 по процедурам Attach.

Подробную информацию см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf[3GPP TS 24.301].

.Описание параметров
[options="header",cols="2,6,8,4"]
|===
| Tx/Rx | Метрика | Описание | Группа

| Rx
| [[attachRequest]]attachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach Type = EPS attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachSuccess]]attachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach Type = EPS attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail]]attachFail
| Количество сообщений S1 Attach Reject.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail2]]attachFail2
| Количество сообщений S1 Attach Reject с причиной "#2 IMSI unknown in HSS".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail3]]attachFail3
| Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail6]]attachFail6
| Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail7]]attachFail7
| Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail8]]attachFail8
| Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail11]]attachFail11
| Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail12]]attachFail12
| Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail13]]attachFail13
| Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail14]]attachFail14
| Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail15]]attachFail15
| Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail17]]attachFail17
| Количество сообщений S1 Attach Reject с причиной "#17 Network failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail19]]attachFail19
| Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail25]]attachFail25/a>
| Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[attachFail111]]attachFail111
| Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".
| TAI:Value, IMSIPLMN:Value

| Rx
| [[combinedAttachRequest]]combinedAttachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachSuccess]]combinedAttachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail]]combinedAttachFail
| Количество сообщений S1 Attach Reject в ответ на  Attach Request с IE *_EPS Attach type = combined EPS/IMSI attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail3]]combinedAttachFail3
| Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail6]]combinedAttachFail6
| Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail7]]combinedAttachFail7
| Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail8]]combinedAttachFail8
| Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail11]]combinedAttachFail11
| Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail12]]combinedAttachFail12
| Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail13]]combinedAttachFail13
| Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail14]]combinedAttachFail14
| Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail15]]combinedAttachFail15
| Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail19]]combinedAttachFail19
| Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail25]]combinedAttachFail25
| Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".
| TAI:Value, IMSIPLMN:Value

| Tx
| [[combinedAttachFail111]]combinedAttachFail111
| Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".
| TAI:Value, IMSIPLMN:Value

| Rx
| [[emergencyAttachRequest]]emergencyAttachRequest
| Количество сообщений S1 Attach Request с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[emergencyAttachSuccess]]emergencyAttachSuccess
| Количество сообщений S1 Attach Accept с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value

| Tx
| [[emergencyAttachFail]]emergencyAttachFail
| Количество сообщений S1 Attach Reject с *_EPS Attach type = EPS emergency attach_*.
| TAI:Value, IMSIPLMN:Value
|===

NOTE: Описание причин ошибок EMM см. https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf[3GPP TS 24.301].

.Группа
[cols="1,6,2"]

|===
| Название | Описание | Тип

| TAI
| Идентификатор области отслеживания. Формат: +
*_<plmn_id><tac>_*.
| hex

| <plmn_id>
| Идентификатор сети PLMN.
| int

| <tac>
| Код области отслеживания.
| hex

| IMSIPLMN
| Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.
| int
|===

.Пример
[source,console]
----
TAI:2500103E8
IMSIPLMN:25099
----

.Пример
[source,csv]
----
rx,attachRequest,,288
rx,attachRequest,IMSIPLMN:20893,287
rx,attachRequest,TAI:208930001,288
tx,attachSuccess,,276
tx,attachSuccess,IMSIPLMN:20893,276
tx,attachSuccess,TAI:208930001,276
tx,attachFail17,,15
tx,attachFail17,IMSIPLMN:20893,14
tx,attachFail17,TAI:208930001,15
rx,combinedAttachRequest,,7
rx,combinedAttachRequest,IMSIPLMN:00101,2
rx,combinedAttachRequest,IMSIPLMN:25020,1
rx,combinedAttachRequest,IMSIPLMN:25060,2
rx,combinedAttachRequest,IMSIPLMN:99999,2
rx,combinedAttachRequest,TAI:001010001,7
tx,combinedAttachSuccess,,5
tx,combinedAttachSuccess,IMSIPLMN:00101,2
tx,combinedAttachSuccess,IMSIPLMN:99999,3
tx,combinedAttachSuccess,TAI:001010001,4
tx,combinedAttachSuccess,TAI:999990001,1
tx,combinedAttachFail12,,3
tx,combinedAttachFail12,IMSIPLMN:25020,1
tx,combinedAttachFail12,IMSIPLMN:25060,2
tx,combinedAttachFail12,TAI:001010001,3
rx,emergencyAttachRequest,,0
tx,emergencyAttachSuccess,,0
----