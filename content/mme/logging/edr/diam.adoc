---
title: DIAM EDR
description: Журнал diam_cdr
type: docs
weight: 20
---

= DIAM EDR

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

Для записей по различным процедурам применяется единообразный формат. Данные представлены в формате CSV: разделитель полей - запятая.

.Описание используемых полей
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| Timestamp
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event_diam,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| SessionId
| Идентификатор сессии, *_Session-Id_*. См. https://tools.ietf.org/html/rfc6733[RFC 6733]. Формат: +
*_<DiameterIdentity>;<high 32 bits>;<low 32 bits>_*
| string

| 4
| IMSI
| Номер IMSI абонента в формате https://www.itu.int/rec/T-REC-E.212-201609-I/en[ITU-T E.212].
| string

| 5
| MSISDN
| Международный номер мобильного абонента, https://www.itu.int/rec/T-REC-E.164-201011-I/en[ITU-T E.164].
| string

| 6
| Origin-Host
| Значение *_Origin-Host_* для протокола Diameter. См. https://tools.ietf.org/html/rfc6733[RFC 6733].
| string

| 7
| Origin-Realm
| Значение *_Origin-Realm_* для протокола Diameter. См. https://tools.ietf.org/html/rfc6733[RFC 6733].
| string

| 8
| Dest-Host
| Значение *_Destination-Host_* для протокола Diameter. См. https://tools.ietf.org/html/rfc6733[RFC 6733].
| string

| 9
| Dest-Realm
| Значение *_Destination-Realm_* для протокола Diameter. См. https://tools.ietf.org/html/rfc6733[RFC 6733].
| string

| 10
| Flags
| Флаги сообщения Diameter, специфичные для конкретной процедуры.
| int

| 11
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 12
| Result-Code
| Значение *_Result-Code_* для протокола Diameter. См. https://tools.ietf.org/html/rfc6733[RFC 6733].
| int

| 13
| ErrorCode
| Внутренний xref:logging/edr/error_code.adoc[код MME] результата.
| int
|===

[[event_diam]]
== События и процедуры, Event

* <<authentication_info_request,Authentication-Info-Request>>;
* <<cancel_location_request,Cancel-Location>>;
* <<update_location_request,Update-Location>>;
* <<insert_subscriber_data_request,Insert-Subscriber-Data-Request>>;
* <<delete_subscriber_data_request,Delete-Subscriber-Data-Request>>;
* <<notify_request,Notify-Request>>;
* <<purge_request,Purge-Request>>;
* <<me_identity_check_request,ME-Identity-Check-Request>>;
* <<reset_request,Reset-Request>>;
* <<mo_forward_short_message_answer,MO-Forward-Short-Message-Answer>>;
* <<mt_forward_short_message_answer,MT-Forward-Short-Message-Answer>>.

[[authentication_info_request]]
== Authentication-Info-Request

[source,log]
----
DateTime,AI,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[cancel_location_request]]
== Cancel-Location

[source,log]
----
DateTime,CL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Ошибка процедуры Paging.

| 2
| Ошибка процедуры Detach.
|===

[[update_location_request]]
== Update-Location

[source,log]
----
DateTime,UL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Получено неожидаемое сообщение по протоколу Diameter вместо сообщения Diameter: Update-Location-Answer.

| 2
| В сообщении Diameter: Update-Location-Answer отсутствуют информация о подписке.

| 3
| Значение *_Access-Restriction-Data_* запрещает использование сетей E-UTRAN.

| 4
| Использование значений *_QCI_* из диапазона 128-254 запрещено.

| 5
| Получено нестандартное значение {*_QCI_*}.

| 6
| В сообщении Diameter: Update-Location-Answer отсутствует номер MSISDN.

| 7
| Получено сообщение Diameter: Update-Location-Answer с ошибочным значением *_Result-Code_*.

| 8
| Текущий код TAC не входит ни в одну из зон.

| 9
| Контекст по умолчанию имеет Wildcard APN.
|===

[[insert_subscriber_data_request]]
== Insert-Subscriber-Data-Request

[source,log]
----
DateTime,ISD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Флаг *_Current Location Request_* активирован, а флаг *_EPS Location Information Request_* -- нет.

| 2
| Ошибка процедуры Paging.

| 3
| Флаг {PPF} не позволяет начать процедуру Paging.

| 4
| Данная сеть PLMN не поддерживает процедуры P-CSCF Restoration.
|===

[[delete_subscriber_data_request]]
== Delete-Subscriber-Data-Request

[source,log]
----
DateTime,DSD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Активирован флаг *_Complete APN Configuration Profile Withdrawal_*.

| 2
| Не удалось извлечь значение *_Context-Identifier_*.

| 3
| Не найдено PDN Connectivity с указанным *_Context-Identifier_*.

| 4
| Контекст по умолчанию не может быть удален.
|===

[[notify_request]]
== Notify-Request

[source,log]
----
DateTime,Notify,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[purge_request]]
== Purge-Request

[source,log]
----
DateTime,Purge,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[reset_request]]
== Reset-Request

[source,log]
----
DateTime,Reset,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[me_identity_check_request]]
== ME-Identity-Check-Request

[source,log]
----
DateTime,ECR,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[mo_forward_short_message_answer]]
== MO-Forward-Short-Message-Answer

[source,log]
----
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----

[[mt_forward_short_message_answer]]
== MT-Forward-Short-Message-Answer

[source,log]
----
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
----