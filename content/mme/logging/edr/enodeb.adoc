---
title: eNodeB EDR
description: Журнал enodeb_cdr
type: docs
weight: 20
---

= eNodeB EDR

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

.Описание используемых полей
[options="header",cols="1,4,8,2"]
|===
| N | Поле | Описание | Тип

| 1
| DateTime
| Дата и время события. Формат: +
*_YYYY-MM-DD HH:MM:SS_*.
| datetime

| 2
| <<event_enodeb,Event>>
| Событие или процедура, сформировавшая запись.
| string

| 3
| eNodeB ID
| Глобальный уникальный идентификатор базовой станции.
| int

| 4
| eNodeB Name
| Имя базовой станции.
| string

| 5
| PLMN List
| Идентификаторы обслуживаемых {PLMN} на базовой станции.
| [int]

| 6
| TAC List
| Идентификаторы обслуживаемых {TAC} на базовой станции.
| [int]

| 7
| eNodeB IP
| IP-адрес базовой станции S1-интерфейсе.
| ip

| 8
| Duration
| Длительность процедуры, в миллисекундах.
| int

| 9
| ErrorCode
| Внутренний xref:logging/edr/error_code.adoc[код MME] результата.
| int
|===

[[event_enodeb]]
== События и процедуры, Event

* <<enodeb_configuration_update,eNodeB Configuration Update>>;
* <<mme_configuration_update,MME Configuration Update>>;
* <<enodeb_configuration_transfer,eNodeB Configuration Transfer>>;
* <<disconnected,Disconnected>>;
* <<reset_enodeb,Reset>>;
* <<s1_setup,S1 Setup>>.

[[enodeb_configuration_update]]
== eNodeB Configuration Update

[source,log]
----
DateTime,eNodeB Configuration Update,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
----

[[mme_configuration_update]]
== MME Configuration Update

[source,log]
----
DateTime,MME Configuration Update,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
----

[[enodeb_configuration_transfer]]
== eNodeB Configuration Transfer

[source,log]
----
DateTime,eNodeB Configuration Transfer,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| eNodeB назначения отключена.

| 2
| Неизвестен IP-адрес другого узла MME.
|===

[[disconnected]]
== Disconnected

[source,log]
----
DateTime,Disconnected,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
----

[[reset_enodeb]]
== Reset

[source,log]
----
DateTime,Reset,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
----

[[s1_setup]]
== S1 Setup

[source,log]
----
DateTime,S1Setup,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
----

.Локальные коды ошибок
[options="header",cols="1,16"]
|===
| N | Описание

| 1
| Запрещённый идентификатор eNodeB.

| 2
| Все указанные PLMN не разрешены или не поддерживают E-UTRAN.
|===