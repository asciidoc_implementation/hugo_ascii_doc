---
title: Резервное копирование и аварийное восстановление
description: Процедура резервного копирования и восстановления ПО узла PROTEI MME
type: docs
weight: 50
---

= Резервное копирование и аварийное восстановление

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

== Резервное копирование

Процедура резервного копирования узла MME осуществляется выполнением скрипта *backup_conf.sh* по
расписанию с помощью демона планировщика *cron*/*systemd.timer*.

Резервному копированию подлежат:

* конфигурационные файлы узла MME: *_/usr/protei/Protei_MME/config/_*;
* исполняемые файлы узла MME: *_/usr/protei/Protei_MME/bin/_*;
* конфигурационные файлы сервера: *_/etc/_*.

SHA-256 хэш-сумма конфигурационных файлов записывается в файл *_.config_hash.sha256_* в директории *_/usr/protei/Protei_MME/config_*.

Необходимость обновления резервных копий определяется автоматически с помощью сравнения хэш-суммы текущих конфигурационных файлов
с хэш-суммой конфигурационных файлов в последней резервной копии, *_.config_hash.sha256_*.

Резервная копия сохраняется на дублирующий узел MME в директорию *_/usr/protei/backup/_* в виде архива *.tar.gz*.

Имя архива имеет формат: *_MME.backup_%Y%m%d-%H%M.%S.tar.gz_*, где:

* *%Y%m%d* -- год, месяц и день начала создания копии;
* *%H%M.%S* -- час, минута и секунда начала создания копии;

== Аварийное восстановление

Для выполнения аварийного восстановления узла MME осуществляются следующие действия:

. Запустить скрипт *_backup_restore_* в директории *_/usr/protei/Protei_MME/scripts/_* на резервном узле, указав имя
архива с резервной копией.

[source,bash]
----
$ /usr/protei/Protei_MME/scripts/backup_restore MME.backup_<datetime>.tar.gz
----

[start=2]
. При запуске скрипта без имени архива указать его в ответе на сообщение:

[source,console]
----
$ /usr/protei/Protei_MME/scripts/backup_restore
Please provide backup_NAME
MME.backup_<datetime>.tar.gz
----

[start=3]
. Дождаться завершения работы скрипта и распаковки файлов.

[source,console]
----
$ /usr/protei/Protei_MME/scripts/backup_restore MME.backup_<datetime>.tar.gz
doing restore..
restore is done
----

В результате будут восстановлены:

* конфигурационные файлы узла MME: *_/usr/protei/Protei_MME/config/_*;
* исполняемые файлы узла MME: *_/usr/protei/Protei_MME/bin/_*;
* конфигурационные файлы сервера: *_/etc/_*.