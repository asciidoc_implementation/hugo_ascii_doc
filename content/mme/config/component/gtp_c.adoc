---
title: gtp_c.cfg
description: Параметры компонента GTP-C
type: docs
weight: 20
---

= gtp_c.cfg

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

В файле задаются настройки компонента GTP-C.

NOTE: Наличие файла обязательно.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| ComponentAddr
| Адрес компонента.
| string
| M
| R
|

| ComponentType
| Тип компонента. +
По умолчанию: Sg.GTP_C.
| string
| O
| R
|
|===

.Пример
[source,textmate]
----
{
  ComponentAddr = Sg.GTP_C;
  ComponentType = Sg.GTP_C;
}
----