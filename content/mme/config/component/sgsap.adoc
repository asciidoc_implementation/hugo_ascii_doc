---
title: sgsap.cfg
description: Параметры компонента SGsAP
type: docs
weight: 20
---

= sgsap.cfg

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

NOTE: Наличие файла обязательно.

[[sgsap]]
.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| ComponentAddr
| Адрес компонента.
| string
| M
| R
|

| ComponentType
| Тип компонента. +
По умолчанию: Sg.SGsAP.
| string
| O
| R
|

| Params
| Параметры компонента.
| object
| M
| R
|

| *{*
|
|
|
|
|

| <<peer_table,PeerTable>>
| Таблица пиров.
| [object]
| O
| R
|

| DefaultPCSM
| Имя узла PCSM по умолчанию.
| [str]
| O
| R
|

| *}*
|
|
|
|
|
|===

[[peer_table]]
== Параметры элемента таблицы PeerTable

* Для пира с одним IP-адресом:

[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| PeerIP
| IP-адрес пира.
| ip
| M
| R
|

| GT
| Глобальный заголовок пира.
| string
| M
| R
|

| PCSM
| Компонентный адрес соответствующего узла PCSM.
| string
| M
| R
|

| PeerName
| Имя пира.
| string
| O
| R
|
|===

* Для пира с несколькими IP-адресами:

[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| GT
| Глобальный заголовок пира.
| string
| M
| R
| 1.1.0.0

| PeerName
| Имя пира.
| string
| O
| R
| 1.1.0.0

| PCSM_list
| Набор параметров отдельных PCSM.
| [object]
| M
| R
| 1.1.0.0

| *{*
|
|
|
|
|

| PeerIP
| IP-адрес пира.
| ip
| M
| R
| 1.1.0.0

| PCSM
| Компонентный адрес соответствующего узла PCSM.
| string
| M
| R
| 1.1.0.0

| Priority
| Приоритет. +
По умолчанию: 0 (высший).
| int
| O
| R
| 1.1.0.0

| Weight
| Вес. +
По умолчанию: 1.
| int
| O
| R
| 1.1.0.0

| *}*
|
|
|
|
|
|===

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| ComponentAddr
| Адрес компонента.
| string
| M
| R
|

| ComponentType
| Тип компонента. +
По умолчанию: Sg.SGsAP.PCSM.
| string
| O
| R
|

| Params
| Параметры компонента.
| object
| M
| R
|

| *{*
|
|
|
|
|

| [[peer_ip]]PeerIP
| IP-адрес подключения узла PCSM.
| ip
| M
| R
|

| [[peer_port]]PeerPort
| Порт подключения узла PCSM.
| string
| M
| R
|

| [[src_ip]]SrcIP
| Локальный IP-адрес узла PCSM. +
По умолчанию: xref:config/sgsap.adoc#local_host_sgsap[sgsap::[LocalAddress\]::LocalHost].
| string
| O
| R
|

| [[src_port]]SrcPort
| Локальный порт узла PCSM. +
По умолчанию: xref:config/sgsap.adoc#local_port_sgsap[sgsap::[LocalAddress\]::LocalPort].
| string
| O
| R
|

| RemoteInterfaces
| Удаленные адреса для Multihoming. Формат: +
*_{ "<ip>:<port>"; "<ip>:<port>"; }_*
| [ip:port]
| O
| R
|

| LocalInterfaces
| Локальные адреса для Multihoming. Формат: +
*_{ "<ip>:<port>"; "<ip>:<port>"; }_*
| [ip:port]
| O
| R
| 1.37.2.0

| *}*
|
|
|
|
|
|===

NOTE: При значениях *_PeerIP = ""_* и *_PeerPort = 0_* PCSM ожидает подключения с адреса, указанного в описании PCSM в секции <<sgsap,Sg.SGsAP>>.

.Пример
[source,console]
----
{
  ComponentAddr = Sg.SGsAP;
  ComponentType = Sg.SGsAP;
  Params = {
    PeerTable = {
      {
        PeerIP = "192.168.125.154";
        GT = "79216567568";
        PCSM = "Sg.SGsAP.PCSM.0";
      };
      {
        GT = "79216561234";
        PCSM_list = {
          {
            PeerIP = "192.168.126.155";
            PCSM = "Sg.SGsAP.PCSM.1";
            Weight = 1;
            Priority = 1;
          };
          {
            PeerIP = "192.168.126.156";
            PCSM = "Sg.SGsAP.PCSM.2";
            Weight = 2;
            Priority = 1;
          };
        };
        PeerName = "MultiIP_Peer";
      };
    };
    DefaultPCSM = {
     "Sg.SGsAP.PCSM.0";
    };
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.0;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.125.154";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.1;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.155";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.2;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.156";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}
----