---
title: rac_rules.cfg
description: Параметры RAC и LAC
type: docs
weight: 20
---

= rac_rules.cfg

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

В файле задаются правила, связывающие {RAC} и {LAC}
с адресами узлов {SGSN} и сервером MSC.
Каждому правилу соответствует одна секция.
Имя секции является именем правила и может использоваться в качестве ссылки в параметре
xref:config/served_plmn.adoc#rac_rules[served_plmn.cfg::RAC_rules].

Ключ для перегрузки -- *reload served_plmn.cfg*.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| RAC
| Код области маршрутизации для данного правила.
| int/hex
| M
| R
|

| LAC
| Код локальной области для данного правила.
| int/hex
| M
| R
|

| SGSN_IP
| IP-адрес узла SGSN. +
NOTE: Для использования GTPv1 вместо GTPv2 необходимо к адресу добавить *_(v1)_*: *_<ip>(v1)_*.
| ip
| O
| R
| 1.41.0.0

| NRI
| Идентификатор узла SGSN, *_{NRI}_*.
| int
| O
| R
| 1.44.1.0

| MSC_Server_IP
| IP-адрес сервера MSC.
| ip
| O
| R
|
|===

.Пример
[source,ini]
----
[Rule1]
RAC = 5;
LAC = 0x5;
SGSN_IP = "192.168.126.67";
MSC_Server_IP = "192.168.126.66";

[Rule2]
RAC = 0xab;
LAC = 7;
MSC_Server_IP = "192.168.126.66";

[Rule3]
RAC = 10;
LAC = 100;
SGSN_IP = "192.168.126.64(v1)";
----