---
title: pcap.cfg
description: Параметры PCAP-логгера
type: docs
weight: 20
---

= pcap.cfg

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

В файле задаются настройки PCAP-логгера.

NOTE: При отсутствии файла всем параметрам будут заданы значения по умолчанию.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[General]*
| Общие параметры логгера.
|
|
|
|

| Enable
| Флаг активации логирования. +
По умолчанию: 1.
| bool
| O
| P
|

| [[separate_by_proto]]SeparateByProto
| Флаг разделения логирования по протоколам, без разделения по абонентам. +
По умолчанию: 0.
| bool
| O
| P
|

| FileName
| Имя файла, если не указано имя абонента. +
По умолчанию: ue_pcap_log.
| string
| O
| P
|

| CustomLogPath
| Директория для хранения журнала. +
По умолчанию: /logs/pcap_trace/.
| string
| O
| P
|

| Prefix
| Префикс к имени файла. +
По умолчанию: "".
| string
| O
| P
| 1.0.1.1
|===

NOTE: Поле <<separate_by_proto,SeparateByProto>> используется только в случаях, когда не указано имя абонента.

.Пример
[source,ini]
----
[General]
Enable = 1;
SeparateByProto = 0;
FileName = "ue_pcap_log";
CustomLogPath = "/logs/pcap_trace/"
Prefix = "prefix";
----