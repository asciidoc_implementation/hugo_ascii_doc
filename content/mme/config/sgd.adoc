---
title: sgd.cfg
description: Параметры интерфейса SGd
type: docs
weight: 20
---

= sgd.cfg

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

В файле задаются настройки интерфейса SGd.

NOTE: Наличие файла обязательно.

Ключ для перегрузки -- *reload sgd.cfg*.

.Описание параметров
[options="header",cols="4,8,2,1,1,2"]
|===
| Параметр | Описание | Тип | O/M | P/R | Версия

| *[General]*
| Параметры интерфейса Diameter SGd.
| object
| M
| R
|

| SGd_addr_list
| Список адресов SC, для которых используется интерфейс SGd. Формат: +
*_<sca>,<sca>_*.
| [string]
| M
| R
|
|===

.Пример
[source,ini]
----
[General]
SGd_addr_list = 73469010001,73469876543
----