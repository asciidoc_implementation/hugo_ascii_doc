---
title: Минимальные системные требования
description: Требования к аппаратному и программному обеспечению узла PROTEI MME
type: docs
weight: 70
---

= Системные требования

ifeval::["{backend}" == "html5"]
include::partials/html5s.adoc[]
endif::[]

.Требования к аппаратному обеспечению
[options="header",cols="1,9"]
|===
| Параметр | Требование

| CPU
| не менее 8 ядер типа CPU E5--2620 v4

| NIC
| не менее 2 x 1 GbE PCIe NIC

| HDD/SSD
| не менее 300 Гб

| RAM
| не менее 16 Гб
|===

.Требования к программному обеспечению
[options="header",cols="1,9"]
|===
| Параметр | Требование

| OC
| OLE 8.6/Alt 8 SP или позднее
|===