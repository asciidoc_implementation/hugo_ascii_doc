---
title: "ss7fw.cfg"
description: "Основные параметры узла"
weight: 20
type: docs
---

В файле задаются основные настройки узла SigFW. 

Ключ для reload - **ss7fw.cfg**.

## Секции ##

* **[\[General\]](#general)** - общие параметры;
* **[\[Statistics\]](#statistics)** - параметры сбора статистики;
* **[\[ATI\]](#ati)** - параметры отправки запроса MAP-Any-Time-Interrogation;
* **[\[HPLMN_Mask\]](#hplmn_mask)** - параметры масок домашней сети;
* **[\[RulesChainConfig\]](#rules_chain_config)** - параметры цепочек правил;
* **[\[DB\]](#db)** - параметры базы данных;
* **[\[CloseBeginWithoutComponents\]](#close_begin_without_components)** - параметры прерывания транзакций;
* **[\[Prometheus\]](#prometheus)** - параметры соединения с Prometheus;
* **[\[DIAM\]](#diam)** - параметры протокола Diameter;
* **[\[TCAP\]](#tcap)** - параметры протокола TCAP.

### Описание параметров ###

| Параметр                                                                        | Описание                                                                                                                                                                                                                          | Тип             | O/M | P/R | Версия   |
|---------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-----|-----|----------|
| **<a name="general">\[General\]</a>**                                           | Общие параметры узла.                                                                                                                                                                                                             | object          | O   | P   |          |
| <a name="core_count">CoreCount</a>                                              | Количество потоков.<br>Диапазон: 1-32. По умолчанию: 2.                                                                                                                                                                           | int             | O   | P   |          |
| Handlers                                                                        | Количество логик, одновременно работающих обработчиков вызовов.<br>Max: 20&nbsp;000. По умолчанию: 1000.                                                                                                                          | int             | O   | P   |          |
| DefaultAction                                                                   | Флаг блокирования сообщения при отсутствии ответа на сообщение MAP-Any-Time-Interrogation.<br>По умолчанию: 0.                                                                                                                    | bool            | O   | P   |          |
| MAP_SL_Timeout                                                                  | Время ожидания ответа на сообщение MAP, в секундах.<br>По умолчанию: 30.                                                                                                                                                          | int             | O   | R   |          |
| CAP_SL_Timeout                                                                  | Время ожидания ответа на сообщение CAP, в секундах.<br>По умолчанию: 18&nbsp;000.                                                                                                                                                 | int             | O   | R   |          |
| OwnAddress                                                                      | IP-адрес узла.                                                                                                                                                                                                                    | string          | O   | P   |          |
| BeginGT                                                                         | Флаг восстановления GT CgPA из сообщения TCAP_BEGIN.<br>По умолчанию: 0.                                                                                                                                                          | bool            | O   | P   | 1.0.5.1  |
| ATI_DirectionID                                                                 | Индикатор HTTP-направления для запросов MAP-Any-Time-Interrogation.<br>По умолчанию: 1.                                                                                                                                           | int             | O   | P   | 1.0.9.1  |
| DatabaseType                                                                    | Индикатор используемой базы данных для хранения последней регистрации абонента.<br>`0` - MariaDB / `1` - Redis.<br>По умолчанию: 0.                                                                                               | int             | O   | P   | 1.0.13.0 |
| ActiveMode                                                                      | Флаг обработки входящих сообщений вместо прозрачной передачи.<br>По умолчанию: 0.                                                                                                                                                 | bool            | O   | P   |          |
| GTP_Mode                                                                        | Флаг обработки запросов по протоколу GTP.                                                                                                                                                                                         | bool            | O   | P   |          |
| TCM_ID                                                                          | Идентификатор сущности приложения.<br>**Примечание.** Используется для генерации уникального TransactionId, который записывается в CDR.                                                                                           | int             | M   | P   | 1.0.13.0 |
| **<a name="statictics">\[Statistics\]</a>**                                     | Параметры статистики.                                                                                                                                                                                                             | object          | O   | R   |          |
| <a name="out_interval">OutInterval</a>                                          | Период сбора и отправки статистики, в секундах.<br>По умолчанию: 30.<br>**Примечание.** При значении 0 сбор статистики не осуществляется.                                                                                         | int             | O   | R   |          |
| OnlineInterval                                                                  | Период сбора и отправки статистики на внешнее приложение, в секундах.<br>По умолчанию: 30.<br>**Примечание.** При значении 0 или [OutInterval](#out_interval) = 0 статистика не отправляется.                                     | int             | O   | R   |          |
| **<a name="ati">\[ATI\]</a>**                                                   | Параметры обработки сообщения MAP-Any-Time-Interrogation.                                                                                                                                                                         | object          | O   | R   |          |
| MaxAgeOfLocation                                                                | Максимальное допустимое значение `AgeOfLocation`, в минутах.<br>По умолчанию: 60.                                                                                                                                                 | int             | O   | R   |          |
| PrefixLen                                                                       | Количество символов в префиксе.<br>По умолчанию: 4.                                                                                                                                                                               | int             | O   | R   |          |
| SaveInterval                                                                    | Период хранения ответа на запрос, в секундах.                                                                                                                                                                                     | int             | O   | R   |          |
| **<a name="hplmn_mask">\[HPLMN_Mask\]</a>**                                     | Параметры масок номеров домашнего диапазона.                                                                                                                                                                                      | object          | O   | R   |          |
| Mask                                                                            | Маска номеров для анализа SCCP.CgPA и SCCP.CdPA на принадлежность HPLMN.<br>**Примечание.** Если ни один из номеров не относятся к домашнему диапазону, то пакет блокируется до работы правил.                                    | regex           | O   | R   |          |
| **<a name="rules_chain_config">\[RulesChainConfig\]</a>**                       | Параметры цепочек правил.                                                                                                                                                                                                         | object          | O   | R   |          |
| UseLocalConfig                                                                  | Флаг отправки запросов на удаленную сторону.<br>По умолчанию: 1.                                                                                                                                                                  | bool            | O   | R   |          |
| OutDir                                                                          | Идентификатор HTTP-направления, куда направляются запросы.<br>По умолчанию: -1.                                                                                                                                                   | int             | O   | R   |          |
| InDir                                                                           | Идентификатор HTTP-направления, откуда получены запросы.<br>По умолчанию: -1.                                                                                                                                                     | int             | O   | R   |          |
| Port                                                                            | Порт для приема конфигурации SigFW Web.<br>**Примечание.** Подставляется в запросах, отправляемых SigFW.<br>По умолчанию: -1.                                                                                                     | int             | O   | R   |          |
| IP                                                                              | IP-адрес или имя хоста для приема конфигурации SigFW Web.<br>**Примечание.** Подставляется в запросах, отправляемых SigFW.                                                                                                        | string          | O   | R   |          |
| Expire                                                                          | Время жизни подписки, в секундах.<br>Диапазон: 1-86&nbsp;400. По умолчанию: 3600.                                                                                                                                                 | int             | O   | R   |          |
| ID                                                                              | Идентификатор подписчика.                                                                                                                                                                                                         | string          | O   | R   |          |
| **<a name="db">\[DB\]</a>**                                                     | Параметры базы данных.                                                                                                                                                                                                            | object          | M   | P   |          |
| Engine                                                                          | IP-адрес базы данных.                                                                                                                                                                                                             | ip              | M   | P   |          |
| Database                                                                        | Название базы данных.                                                                                                                                                                                                             | string          | M   | P   |          |
| User                                                                            | Логин для авторизации.                                                                                                                                                                                                            | string          | M   | P   |          |
| Password                                                                        | Пароль для авторизации.                                                                                                                                                                                                           | string          | M   | P   |          |
| **<a name="close_begin_without_component">\[CloseBeginWithoutComponents\]</a>** | Параметры прерывания транзакций.                                                                                                                                                                                                  | object          | O   | R   | 1.0.5.2  |
| lClose                                                                          | Индикатор прерывания транзакции, для которой в TCAP_BEGIN не найдены компоненты.<br>`0` - не прерывать;<br>`1` - прерывать по совпадению с маской [ACN](#mask_acn);<br>2 - прерывать все подобные транзакции.<br>По умолчанию: 0. | int             | O   | R   | 1.0.5.2  |
| <a name="mask_acn">MaskACN</a>                                                  | Маска <abbr titile="Application Context Name">ACN</abbr> для фильтрации транзакций.                                                                                                                                               | regex           | O   | R   | 1.0.5.2  |
| **<a name="prometheus">\[Prometheus\]</a>**                                     | Параметры работы с Prometheus.                                                                                                                                                                                                    | object          | O   | R   | 1.0.7.0  |
| ListenAddress                                                                   | Прослушиваемый IP-адрес и порт.<br>По умолчанию: 0.0.0.0:8080.                                                                                                                                                                    | ip:port         | O   | R   | 1.0.7.0  |
| **<a name="tcap">\[DIAM\]</a>**                                                 | Параметры обработки сообщений Diameter.                                                                                                                                                                                           | object          | O   | R   |          |
| DiamEnable                                                                      | Флаг работы с протоколом Diameter.                                                                                                                                                                                                | bool            | O   | R   |          |
| DiamWaitAns                                                                     | Флаг ожидания ответа на запрос по протоколу Diameter.                                                                                                                                                                             | bool            | O   | R   |          |
| IMSI_MSISDN_Converter                                                           | Преобразование префиксов для приведения номера IMSI к формату E.214 для отправки запроса MAP-Any-Time-Interrogation.                                                                                                              | {string,string} | O   | R   | 1.0.7.2  |
| **<a name="tcap">\[TCAP\]</a>**                                                 | Параметры обработки TCAP-транзакций.                                                                                                                                                                                              | object          | O   | R   | 1.0.13.0 |
| TransactionLifetime                                                             | Длительность хранения данных об активной TCAP-транзакции, в секундах.<br>По умолчанию: 3600.                                                                                                                                      | int             | O   | R   | 1.0.13.0 |
| SwitchingTimeMAP                                                                | Длительность переходного режима пропуска хвоста MAP-транзакций, в секундах.<br>По умолчанию: 300.                                                                                                                                 | int             | O   | R   | 1.0.14.6 |
| SwitchingTimeCAP                                                                | Длительность переходного режима пропуска хвоста СAP-транзакций, в секундах.<br>По умолчанию: 1800.                                                                                                                                | int             | O   | R   | 1.0.14.6 |

#### Пример ####

```ini
[General]
CoreCount = 4;
Handlers = 20000;
MAP_SL_Timeout = 30;
DefaultAction = 1;
OwnAddress = 79990000000;
ActiveMode = 1;
ATI_DirectionID = 2;
GTP_Mode = 1;
TCM_ID = 0;

[Statistics]
OnlineInterval = 10;

[ATI]
MaxAgeOfLocation = 2;
PrefixLen = 4;
SaveInterval = 60;

[RulesChainConfig]
UseLocalConfig = 0;
OutDir = 0;
InDir = 0;
Port = 46000;
IP = "192.168.109.250";
Expire = "3333";
ID = "fw1";

[DB]
Engine = 172.49.0.3;
Database = Protei_SS7FW;
User = ss7fw;
Password = sql;

[HPLMN_Mask]
Mask = "79.(0,12)";

[DIAM]
DiamEnable = 1;
DiamWaitAns = 0;
IMSI_MSISDN_Converter = {
  { "25002"; "7921"; };
};
```