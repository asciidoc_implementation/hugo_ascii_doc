---
title: "ap.cfg & ap_dictionary"
description: "Параметры подсистемы сбора аварий"
weight: 20
type: docs
---

### ap.cfg

{{< getcontent path="/Mobile/SharedDocs/Alarm/config/ap_cfg.md" >}}

#### Пример ####

```ini
[General]
Root = PROTEI(1,3,6,1,4,1,20873)
ApplicationAddress = SS7FW
MaxConnectionCount = 10
ManagerThread = 1
CyclicTreeWalk = 0

[Dynamic]
[AtePath2ObjName]
{ SS7FW(146).General(1,1); CA(2); }
{ SS7FW(146).General(1,1); OSTATE(3); }
{ SS7FW(146).Traffic,Stat(2,1); CA(2); };
{ SS7FW(146).Traffic,Stat(2,1); PARAM(3); };
{ SS7FW(146).Abonent,Stat(3,1); CA(2); };
{ SS7FW(146).Abonent,Stat(3,1); PARAM(3); };
{ SS7FW(146).Reload(4,1); CA(2); };
{ SS7FW(146).Reload(4,1); OSTATE(3); };
{ SS7FW(146).Reload(4,1); PARAM(4); };

{ SS7FW(146).OVRLOAD,Handler,SL(5,1); CA(2); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); CA(2); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); PARAM(4); };

{ SS7(3).TCAP(1).Ovrload(2).SL(1,1); CA(100); };
{ SS7(3).TCAP(1).Ovrload(2).SL(1,1); PARAM(200); };
{ SS7(3).TCAP(1).Ovrload(2).SL(1,1); OSTATE(4096); };

#ASP
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); CA(100); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); OSTATE(4096); };

{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Alarm,Decode(2); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Alarm,Encode(3); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Alarm,CDI,Num(4); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Alarm,ASP,CDI(5); };

{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Warn,ErrCodeInfo,ASP,Connect(6); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Warn,ASPUP(7); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Warn,ASPUP,Num(8); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Warn,ASPDN(9); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Warn,ASPDN,Num(10); };

{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,DAVA(11); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,DUNA(12); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,SCON(13); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,DUPU(14); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,DUPU,UC(15); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,DRST(16); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,ASP,Connect(17); };
{ Sg(2).SIGTRAN(1).M3UA(1).ASP(1,1); Info,ASP,UP(18); };

#AS
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); CA(100); };

{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Alarm,DPC,Invalid(2); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Alarm,DPC,Num(3); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Alarm,UP,Invalid(4); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Alarm,UP,Num(5); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Alarm,ChCfg,Invalid(6); };

{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Act,Failed(7); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Act,Num(8); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Deact,Failed(9); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Deact,Num(10); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Reg,Failed(11); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Reg,Num(12); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Dereg,Failed(13); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,Dereg,Num(14); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Warn,ASP,Failure(15); };

{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Info,LinkUP,Num(16); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Info,ASInit(17); };
{ Sg(2).SIGTRAN(1).M3UA(1).AS(2,1); Info,AS,ACT(18); };

#Diameter
{ Sg(2).DIAM(30).PCSM(1,1); CA(100); }
{ Sg(2).DIAM(30).PCSM(1,1); OSTATE(4096); }
#Ad
{ Ad(6).OMI(1).ACCL(1,1); CA(100) }
{ Ad(6).OMI(1).ACCL(1,1); OSTATE(4096) }
{ Ad(6).OMI(1).ASCL(2,1); CA(100) }
{ Ad(6).OMI(1).ASCL(2,1); OSTATE(4096) }
{ Ad(6).OMI(1).ASCL(2,1); SESS(3) }
{ Ad(6).OMI(1).Traffic(3).Stat(1,1); CA(100) }
{ Ad(6).OMI(1).Traffic(3).Stat(1,1); PARAM(200) }

[SNMP]
ListenIP  =  0.0.0.0;
ListenPort  =  3128

[StandardMib]
#sysDescr
{1.3.6.1.2.1.1.1.0;STRING;"CallMe"; };
#sysObjectID
{1.3.6.1.2.1.1.2.0;OBJECT_ID;1.3.6.1.4.1.20873; };

[AtePath2Oid]
[SNMPTrap]
FirstVarOwn  =  0;
IndexID  =  1;

[Filter]
CA_Object = ".*"
CT_Object = ".*"
CA_Var = ".*"
TrapIndicator = -1
DynamicIndicator = -1

[SpecificTrapCT_Object]
{ SS7FW.General; 1; }
{ SS7FW.Traffic.Stat; 2; }
{ SS7FW.Abonent.Stat; 3; }
{ SS7FW.Reload; 4; }
{ SS7FW.OVRLOAD.*; 5; }

{ Sg.SIGTRAN.M3UA.ASP; 8; }
{ Sg.SIGTRAN.M3UA.AS; 9; }

[SpecificTrapCA_Var]
{ Alarm.Decode.*; 111; }
{ Alarm.Encode.*; 121; }
{ Alarm.CDI.*; 131; }
{ Warn.ErrCodeInfo.ASP.Connect.*; 141; }
{ Warn.ASPUP.*; 151; }
{ Warn.ASPDN.*; 161; }
{ Info.DAVA.*; 171; }
{ Info.DUNA.*; 181; }
{ Info.SCON.*; 191; }
{ Info.DUPU.*; 211; }
{ Info.DRST.*; 221; }
{ Info.ASP.UP.*; 231; }
{ Info.ASP.Connect.*; 232; }
{ Alarm.DPC.*; 241; }
{ Alarm.UP.*; 251; }
{ Alarm.ChCfg.Invalid.*; 261; }
{ Warn.Act.*; 271; }
{ Warn.Deact.*; 281; }
{ Warn.Reg.*; 291; }
{ Warn.Dereg.*; 311; }
{ Warn.ASP.Failure.*; 321; }
{ Info.LinkUP.Num.*; 331; }
{ Info.ASInit.*; 341; }
{ Info.AS.ACT.*; 342; }
{ Sg.DIAM.PCSM; 14};
```

### ap_dictionary

{{< getcontent path="/Mobile/SharedDocs/Alarm/config/ap_dictionary.md" >}}

```ini
# Dictionary

OSTATE = { { 1; ACTIVATE }; { 0; FAIL }; };
ASTATE = { { 1; UNBLOCKED }; { 0; BLOCKED }; };
HSTATE = { { 1; ON }; { 0; OFF }; };
```