---
title: "om_interface.cfg"
description: "Параметры OM-интерфейса"
weight: 20
type: docs
---

В файле задаются настройки соединений по OM-интерфейсу.

### Секции ###

* **[\[General\]](#general)** - основные параметры интерфейса;
* **[\[Client\]](#client)** - параметры автоматического соединения с абонентом;
* **[\[Server\]](#server)** - параметры автоматического соединения с сервером.

### Описание параметров ###

| Параметр                                        | Описание                                                                                                                                                                  | Тип        | O/M | P/R | Версия |
|-------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| **<a name="general">\[General\]</a>**           | Основные параметры интерфейса.                                                                                                                                            | object     | M   | P   |        |
| ServerIP                                        | Адрес динамического OM-сервера.                                                                                                                                           | string     | O   | R   |        |
| ServerPort                                      | Порт динамического OM-сервера.                                                                                                                                            | int        | O   | R   |        |
| [Timers](#timers)                               | Параметры таймеров.                                                                                                                                                       | object     | M   | R   |        | 
| **<a name="client">\[Client\]</a>**             | Параметры автоматического соединения с абонентом.                                                                                                                         | object     | O   | P   |        |
| **<a name="server">\[Server\]</a>**             | Параметры автоматического соединения с сервером.                                                                                                                          | object     | O   | P   |        |
| [Timers](#timers)                               | Параметры таймеров.                                                                                                                                                       | object     | O   | P   |        |
| Sockets                                         | Параметры сокетов.                                                                                                                                                        | object     | O   | P   |        |
| **{**                                           |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Address                             | Разрешенный IP-адрес.<br>**Примечание.** Значение `0.0.0.0` открывает доступ всем IP-адресам.                                                                             | ip         | O   | P   |        |
| &nbsp;&nbsp;Port                                | Порт для соединения.                                                                                                                                                      | int        | O   | P   |        |
| **}**                                           |                                                                                                                                                                           |            |     |     |        |
| ConnectionLogics                                | Параметры сетевых логик.                                                                                                                                                  | \[object\] | M   | P   |        |
| **{**                                           |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;CL.<id>                             | Параметры логики CL.                                                                                                                                                      | object     | M   | P   |        |
| &nbsp;&nbsp;**{**                               |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;id                      | Идентификатор логики CL.                                                                                                                                                  | int        | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Priority                | Флаг оповещения абонента об изменении статуса подключения.<br>По умолчанию: false.                                                                                        | bool       | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;MaxTransactionCount     | Время ожидания переподключения после разрыва, после которого входящие запросы передаются на резервные подключения, в миллисекундах.<br>По умолчанию: 0.                   | int        | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;IP                      | IР–адрес для подключения.                                                                                                                                                 | ip         | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Port                    | Порт для соединения.                                                                                                                                                      | int        | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Login                   | Логин подключения.                                                                                                                                                        | string     | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Password                | Пароль подключения.                                                                                                                                                       | string     | O   | P   |        |
| &nbsp;&nbsp;**}**                               |                                                                                                                                                                           |            |     |     |        |
| **}**                                           |                                                                                                                                                                           |            |     |     |        |
| Directions                                      | Параметры направлений автоподключений.                                                                                                                                    | \[object\] | M   | P   |        |
| **{**                                           |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Dir.<id>                            | Параметры направления Dir.                                                                                                                                                | object     | M   | P   |        |
| &nbsp;&nbsp;**{**                               |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;id                      | Идентификатор направления Dir.                                                                                                                                            | int        | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;CL_Monitoring           | Флаг оповещения абонента об изменении статуса подключения.<br>По умолчанию: 0.                                                                                            | bool       | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;ChangeOverTimeOut       | Время ожидания восстановления подключений после разрыва, по истечении которого входящие запросы передаются на резервные подключения, в миллисекундах.<br>По умолчанию: 0. | int        | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Primary                 | Параметры основных подключений.                                                                                                                                           | object     | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;Secondary               | Параметры резервных подключений.                                                                                                                                          | object     | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**                   |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MaxTraffic  | Максимальное количество транзакций, единовременно обрабатываемых на направлении.                                                                                          | int        | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Connections | Список сетевых логик.                                                                                                                                                     | \[int\]    | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**                   |                                                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;**}**                               |                                                                                                                                                                           |            |     |     |        |
| **}**                                           |                                                                                                                                                                           |            |     |     |        |

#### Параметры таймеров, Timers {#timers}

| Параметр                   | Описание                                                                                                          | Тип | O/M | P/R | Версия |
|----------------------------|-------------------------------------------------------------------------------------------------------------------|-----|-----|-----|--------|
| SessionResponseTimeOut     | Максимальное время существования сессии, в миллисекундах.<br>По умолчанию: 0.                                     | int | O   | R   |        |
| TransactionResponseTimeOut | Максимальное время существования транзакции, в миллисекундах.<br>По умолчанию: 0.                                 | int | O   | R   |        |
| SegmentResponseTimeOut     | Время ожидания ответа на запрос, в миллисекундах.<br>По умолчанию: 0.                                             | int | O   | R   |        |
| MaxSegmentErrorCount       | Максимальное количество ошибок SegmentError, при превышении которого текущая сессия закрывается, в миллисекундах. | int | M   | R   |        |
| LoginReqTimeOut            | Время ожидания ответа LoginReq, по истечении которого соединение разрывается, в миллисекундах.                    | int | M   | R   |        |
| ReconnectTimeOut           | Время ожидания клиентской сетевой логики до очередной попытки соединения, в миллисекундах.                        | int | M   | R   |        |
| KeepAliveTimeOut           | Интервал посылки сообщения KeepAlive, в миллисекундах.                                                            | int | M   | R   |        |
| KeepAliveResponseTimeOut   | Время ожидания подтверждения KeepAlive_ACK на запрос KeepAlive, в миллисекундах.                                  | int | M   | R   |        |

#### Пример ####

```
[General]
ServerIP = <string>;
ServerPort = <int>;
Timers = {
  SessionResponseTimeOut = <ms>;
  TransactionResponseTimeOut = <ms>;
  SegmentResponseTimeOut = <ms>;
  MaxSegmentErrorCount = <int>;
  LoginReqTimeOut = <ms>;
  ReconnectTimeOut = <ms>;
  KeepAliveTimeOut = <ms>;
  KeepAliveResponseTimeOut = <ms>;
}

[Server]
Timers = {
  TransactionResponseTimeOut = <ms>;
  SegmentResponseTimeOut = <ms>;
  LoginReqTimeOut = <ms>;
  KeepAliveTimeOut = <ms>;
  KeepAliveResponseTimeOut = <ms>;
}
Sockets = {
  {
    Address = <string>;
    Port = <int>;
  }
}
ConnectionLogics = {
  CL.<int> = {
    Priority = <int>;
    MaxTransactionCount = <int>;
    Login = <string>;
    Password = <string>;
  }
}
Directions = {
  Dir.<string> = {
    CL_Monitoring = <bool>;
    ChangeOverTimeOut = <mss>;
    Primary = {
      MaxTraffic = <int>;
      Connections = {<int>,...,<int>};
    }
    Secondary = {
      MaxTraffic = <int>;
      Connections = {<int>;...;<int>};
    }
  }
}

[Client]
Timers = {
  TransactionResponseTimeOut = <ms>;
  SegmentResponseTimeOut = <ms>;
  ReconnectTimeOut = <ms>;
  KeepAliveTimeOut = <ms>;
  KeepAliveResponseTimeOut = <ms>;
}
ConnectionLogics = {
  CL.<int> = {
    Priority = <int>;
    MaxTransactionCount = <int>;
    IP = <string>;
    Port = <int>;
    Login = <string>;
    Password = <string>;
    Info = <string>;
  }
}
Directions = {
  Dir.<string> = {
    CL_Monitoring = <bool>;
    ChangeOverTimeOut = <ms>;
    Primary = {
      MaxTraffic = <int>;
      Connections = {<int>,...,<int>};
    }
    Secondary = {
      MaxTraffic = <int>;
      Connections = {<int>,...,<int>};
    }
  }
}
```