---
title: Конфигурирование
description: Описание конфигурационных файлов
type: docs
weight: 40
---
= Конфигурирование
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
:eir: pass:[<abbr title="Equipment Identity Register">EIR</abbr>]
:hss: pass:[<abbr title="Home Subscriber Server">HSS</abbr>]
:pgw: pass:[<abbr title="Packet Data Network Gateway">PGW</abbr>]
:sgw: pass:[<abbr title="Serving Gateway">SGW</abbr>]
:sgsn: pass:[<abbr title="Serving GPRS Support Node">SGSN</abbr>]
:nsa: pass:[<abbr title="Non Standalone">NSA</abbr>]
:nb-iot: pass:[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:srvcc: pass:[<abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>]
:volte: pass:[<abbr title="Voice over LTE">VoLTE</abbr>]
:csfb: pass:[<abbr title="Circuit Switch Fallback">CSFB</abbr>]
:ldn: pass:[<abbr title="Local Distinguished Name">LDN</abbr>]
:edrx: pass:[<abbr title="Extended Discontinuous Reception">eDRX</abbr>]
:qci: pass:[<abbr title="QoS Class Identifier">QCI</abbr>]
:rac: pass:[<abbr title="Routing Area Code">RAC</abbr>]
:lac: pass:[<abbr title="Location Area Code">LAC</abbr>]
:ambr: pass:[<abbr title="Aggregate Maximum Bit Rate">AMBR</abbr>]
:arp: pass:[<abbr title="Allocation and Retention Policy">ARP</abbr>]
:fqdn: pass:[<abbr title="Fully Qualified Domain Name">FQDN</abbr>]
:pdn: pass:[<abbr title="Packet Data Network">PDN</abbr>]
:tac: pass:[<abbr title="Tracking Area Code">TAC</abbr>]
:nas: pass:[<abbr title="Non-Access Stratum">NAS</abbr>]
:imei: pass:[<abbr title="International Mobile Equipment Identifier">IMEI</abbr>]
:apn: pass:[<abbr title="Access Point Name">APN</abbr>]
:plmn: pass:[<abbr title="Public Landing Mobile Network">PLMN</abbr>]
:kasme: pass:[<abbr title="Key Access Security Management Entity">KASME</abbr>]
:scef: pass:[<abbr title="Service Capability Exposure Function">SCEF</abbr>]
:vops: pass:[<abbr title="Voice over PS Session">VoPS</abbr>]
:psm: pass:[<abbr title="Power Saving Mode">PSM</abbr>]
:ppf: pass:[<abbr title="Page Proceed Flag">PPF</abbr>]
:guti: pass:[<abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>]
:alpn: pass:[<abbr title="Application-Layer Protocol Negotiation">ALPN</abbr>]
:tai: pass:[<abbr title="Tracking Area Identity">TAI</abbr>]
:rnc: pass:[<abbr title="Radio Network Controller">RNC</abbr>]
:nhi: pass:[<abbr title="Next Hop Indicator">NHI</abbr>]
:pti: pass:[<abbr title="Procedure Transaction ID">PTI</abbr>]
:csg: pass:[<abbr title="Closed Subscriber Group">CSG</abbr>]
:rai: pass:[<abbr title="Routing Area Identity">RAI</abbr>]
:tmsi: pass:[<abbr title="Temporary Mobile Subscriber Identifier">TMSI</abbr>]
:dcn: pass:[<abbr title="Dedicated Core Network">DCN</abbr>]
:sni: pass:[<abbr title="Server Name Indication">SNI</abbr>]
:mme: pass:[<abbr title="Mobile Management Entity">MME</abbr>]
:rab: pass:[<abbr title="Radio Access Bearer">RAB</abbr>]
:mcc: pass:[<abbr title="Mobile Country Code">MCC</abbr>]
:mnc: pass:[<abbr title="Mobile Network Code">MNC</abbr>]
:tce: pass:[<abbr title="Trace Collection Entity">TCE</abbr>]
:dpdk: pass:[<abbr title="Data Plane Development Kit">DPDK</abbr>]
:eal: pass:[<abbr title="Environment Abstraction Layer">EAL</abbr>]
:pcrf: pass:[<abbr title="Policy and Charging Rules Function">PCRF</abbr>]
:utran: pass:[<abbr title="UMTS Terrestrial Radio Access Network">UTRAN</abbr>]
:geran: pass:[<abbr title="GSM/EDGE Radio Access Network">GERAN</abbr>]
:gan: pass:[<abbr title="Generic Access Network">GAN</abbr>]
:hspa: pass:[<abbr title="High-Speed Packet Data Access">HSPA</abbr>]
:e-utran: pass:[<abbr title="Evolved Universal Terrestrial Radio Access Network">E-UTRAN</abbr>]
:imsi: pass:[<abbr title="International Mobile Subscriber Identifier">IMSI</abbr>]
:nidd: pass:[<abbr title="Non-IP Data Delivery">NIDD</abbr>]
:qos: pass:[<abbr title="Quality of Service">QoS</abbr>]
:sqn: pass:[<abbr title="Sequence Number">SQN</abbr>]
:pfcp: pass:[<abbr title="Packet Forwarding Control Protocol">PFCP</abbr>]
:udsf: pass:[<abbr title="Unstructured Data Storage Function">UDSF</abbr>]
:cups: pass:[<abbr title="Control and User Plane Separation">CUPS</abbr>]
:icmp: pass:[<abbr title="Internet Control Message Protocol">ICMP</abbr>]
:drx: pass:[<abbr title="Discontinuous Reception">DRX</abbr>]
endif::[]


ifeval::["{backend}" == "pdf"]
:eir: EIR
:hss: HSS
:nas: NAS
:pgw: PGW
:sgw: SGW
:sgsn: SGSN
:nsa: NSA
:nb-iot: NB-IoT
:srvcc: SRVCC
:volte: VoLTE
:csfb: CSFB
:ldn: LDN
:edrx: Extended DRX
:qci: QCI
:rac: RAC
:lac: LAC
:ambr: AMBR
:arp: ARP
:fqdn: FQDN
:pdn: PDN
:tac: TAC
:imei: IMEI
:apn: APN
:plmn: PLMN
:kasme: KASME
:scef: SCEF
:vops: VoPS
:psm: PSM
:ppf: PPF
:guti: GUTI
:alpn: ALPN
:tai: TAI
:rnc: RNC
:nhi: NHI
:pti: PTI
:csg: CSG
:rai: RAI
:tmsi: TMSI
:imeisv: IMEISV
:dcn: DCN
:sni: SNI
:mme: MME
:rab: RAB
:mcc: MCC
:mnc: MNC
:tce: TCE
:dpdk: DPDK
:eal: EAL
:pcrf: PCRF
:utran: UTRAN
:geran: GERAN
:gan: GAN
:hspa: HSPA
:e-utran: E-UTRAN
:imsi: IMSI
:nidd: NIDD
:qos: QoS
:sqn: SQN
:pfcp: PFCP
:udsf: UDSF
:cups: CUPS
:icmp: ICMP
:drx: DRX
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

Настройка узла SigFW осуществляется в файлах конфигурации, расположенных в директории *_/usr/protei/Protei_SigFW/config_*.

Параметры конфигурации задаются в следующих файлах:

* link:ap[ap.cfg] -- конфигурация настроек подсистемы аварийной индикации;
* link:dbrm[dbrm.cfg] -- конфигурация настроек;
* link:diameter[diameter.cfg] -- конфигурация настроек подключений по протоколу Diameter;
* link:gtp_forwarder[gtp_forwarder.cfg] -- конфигурация настроек компонента GTP-C;
* link:http[http.cfg] -- конфигурация настроек HTTP-соединений;
* link:mariadb[mariadb.cfg] -- конфигурация настроек подключений к базе данных MariaDB;
* link:om_interface[om_interface.cfg] -- конфигурация настроек интерфейса OM;
* link:protei[protei.cfg] -- конфигурация настроек программного обеспечения;
* link:redis[redis.cfg] -- конфигурация настроек подключений к базе данных Redis;
* link:router_json[router.json] -- конфигурация правил обработки сообщений;
* link:ss7fw[ss7fw.cfg] -- конфигурация основных настроек узла;
* link:threshold[threshold.cfg] -- конфигурация порогов для активации аварий;
* link:trace[trace.cfg] -- конфигурация настроек подсистемы журналирования.

В описании каждого файла конфигурации приведены следующие сведения:

* Описание всех настраиваемых в файле параметров, включая тип данных, возможный диапазон значений, значение по умолчанию и единицы измерения (при наличии);
* Информация об обязательности задания значения каждого параметра -- столбец O/M;
* Информация о необходимости перезапуска ПО узла SigFW для применения нового значения параметра в случае изменения -- столбец P/R;
* Информация о версиях приложения, в которых используется используется параметр -- столбец "Версия" (отсутствие информации означает, что параметр используется во всех версиях приложения);
* Работоспособный пример файла конфигурации.

=== Условные обозначения в таблицах описания параметров конфигурации

Поле *O/M* -- необходимость указания значения параметра:

* O (Optional) -- опциональный параметр, может отсутствовать в конфигурации, компонентом используются значения по умолчанию;
* M (Mandatory) -- обязательный параметр, при отсутствии в конфигурации не гарантируется работоспособность компонента;
* C (Conditional) -- требуется указание параметра при определенном условии (см. примечания в описании конфигурации).

Поле *P/R* -- необходимость перезагрузки компонента для применения изменений параметра:

* P (Permanent) -- изменения параметра не применяются динамически, требуется перезагрузка компонента;
* R (Reloadable) -- изменения параметра применяются динамически, перезагрузка компонента не требуется.

=== Типы данных параметров конфигурации

|===
| Тип | Описание

| bool
| Логический тип. Задает флаг. Принимает два значения, *_true_* и *_false_*.

| flag
| Числовой тип. Задает флаг. Принимает два значения, *_0_* и *_1_*.

| string
| Строковый тип. Задает строку. Использует буквенные, цифровые и специальные символы.

| int
| Числовой тип. Задает целое 32--битное число, записанное цифрами 0--9 и знаком минуса "--". Диапазон: от --2^31^ до 2^31^ -- 1.

| choice
| Коллекция. Задает объект с набором полей, для каждого экземпляра должно быть определено только одно любое поле.

| object
| Кортеж. Содержит фиксированное количество параметров различных типов.

| list
| Список. Содержит несколько значений одного типа или структуры.

| map
| Ассоциативный массив, словарь. Задает неупорядоченный набор пар ключ-значение.

| float
| Числовой тип. Задает число с плавающей точкой.

| double
| Числовой тип. Задает число с плавающей точкой двойной точности.

| datetime
| Тип для задания даты и времени. Формат по умолчанию: +
*YYYY-MM-DD hh:mm:ss.mss* +
YYYY -- год; MM -- месяц; DD -- день; +
hh -- час; mm -- минута; ss -- секунда; mss -- миллисекунда. +
Время задается в формате 24--часового дня.

| hex
| Числовой тип. Задает целое число в формате шестнадцатеричного числа, записанного цифрами 0--9 и буквами A--F. Числу может предшествовать обозначение 0x. При отсутствии обозначения определяется как строка.

| ip
| Строковый тип. Задает IP-адрес версии 4: *xxx.xxx.xxx.xxx*

| None
| Нулевой тип. Не задает значение, необходима лишь инициализация объекта.

| units
| Объект. Задает величину и единицы измерения. +
*_d_* -- день;/*_h_* -- час;/*_m_* -- минута;/*_s_* -- секунда;/*_ms_* -- миллисекунда;/*_us_* -- микросекунда; +
*_b_* -- биты;/*_Kb_* -- килобиты;/*_Mb_* -- мегабиты;/*_Gb_* -- гигабиты;/*_Tb_* -- терабиты; +
*_B_* -- байты;/*_KB_* -- килобайты;/*_MB_* -- мегабайты;/*_GB_* -- гигабайты;/*_TB_* -- терабайты.
|===