---
title: "diameter.cfg"
description: "Файл настройки Diameter-соединений"
weight: 20
type: docs
---

{{< getcontent path="/Mobile/SharedDocs/Diameter/config.md" >}}

#### Пример ####

```
[General]
ReceivingFromAnyHost = 1;

[LocalAddress]
LocalHost = "0.0.0.0";
LocalPort = 3868;
Transport = "sctp";
assoc_change_event = 1;
peer_addr_change_event = 1;

[LocalPeerCapabilities]
Origin-Host = "hss.autotest.protei.ru";
Origin-Realm = "autotest.protei.ru";
Vendor-ID = 12345;
Product-Name = "diameter";
Firmware-Revision = 12345;
Origin-State-Id = 1111;
Host-IP-Address = { "192.168.126.223"; };
Auth-Application-Id = { 4; 16777216; 16777217; 16777251; 16777252; };
Inband-Security-Id = { 0; };
Vendor-Specific-Application-Id = {
  { Vendor-Id = 10415; Auth-Application-Id = 16777251;}
  { Vendor-Id = 10415; Auth-Application-Id = 16777216;}
  { Vendor-Id = 10415; Auth-Application-Id = 16777217;}
}
Supported-Vendor-Id = { 10415; };

[SpecificLocalPeerCapabilities]
Sg.DIAM.PCSM.5 = {
  Origin-State-Id = 1111;
  Host-IP-Address = {
    { "192.168.100.183"; 0 }
  };
  Vendor-Specific-Application-Id = {
    {
      Vendor-Id = 193;
      Acct-Application-Id = 19302;
    };
  }
  Supported-Vendor-Id = { 193; };
  Inband-Security-Id = { 0; };
};

[Timers]
```