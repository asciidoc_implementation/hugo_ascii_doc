---
title: "RouterDIAM"
description: "Параметры маршрутизатора RouterDIAM"
weight: 30
type: docs
---

**RouterDIAM** маршрутизирует сообщения по коду операции и идентификатору приложения.

### Описание параметров ###

| Параметр                                        | Описание                                                                                                                                  | Тип        | O/M | P/R | Версия |
|-------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| ID                                              | Уникальный идентификатор узла маршрутизации.                                                                                              | int        | O   | R   |        |
| Name                                            | Уникальное имя узла маршрутизации.                                                                                                        | string     | M   | R   |        |
| Type                                            | Тип узла маршрутизации. Должно быть `RouterDIAM`.                                                                                         | string     | M   | R   |        |
| Routing                                         | Параметры переадресации.                                                                                                                  | \[object\] | M   | R   |        |
| *{*                                             |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Key                                 | Главный ключ для задания адресации. Формат:<br>`"Key": { "OpCode": <op_code>, "AID": <app_id> }`.                                         | \[object\] | M   | R   |        |
| &nbsp;&nbsp;*{*                                 |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[OpCode](#command_code) | Код операции, `Command-Code`.                                                                                                             | int        | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[AID](#app_id)          | Идентификатор приложения, `Application–Id`.                                                                                               | int        | O   | R   |        |
| &nbsp;&nbsp;*}*                                 |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Route                               | Параметры последующей маршрутизации при совпадении ключа. Формат:<br>`"Route": { "GOTO": "<goto>" }`.                                     | object     | O   | R   |        |
| &nbsp;&nbsp;*{*                                 |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;GOTO                    | Название узла, куда перенаправляется запрос.<br>**Примечание.** Должно совпадать с именем существующего правила / маршрутизатора / "OUT". | string     | M   | R   |        |
| &nbsp;&nbsp;*}*                                 |                                                                                                                                           |            |     |     |        |
| *}*                                             |                                                                                                                                           |            |     |     |        |

#### Пример ####

```json
{
  "ID": 10,
  "Name": "MainDIAM",
  "Type": "RouterDIAM",
  "Routing": [
    {
      "Key": {
        "OpCode": 318,
        "AID": 16777251
      },
      "Route": { "GOTO": "RuleDiamAI" }
    }
  ]
}
```

#### Коды операций, OpCode {#command_code}

Подробную информацию см. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).

* 316 — Diameter: Update-Location-Request/Answer;
* 317 — Diameter: Cancel-Location-Request/Answer;
* 318 — Diameter: Authentication-Information-Request/Answer;
* 319 — Diameter: Insert-Subscriber-Data-Request/Answer;
* 320 — Diameter: Delete-Subscriber-Data-Request/Answer;
* 321 — Diameter: Purge-UE-Request/Answer;
* 322 — Diameter: Reset-Request/Answer;
* 323 — Diameter: Notify-Request/Answer.

#### Идентификаторы приложений, Application-ID {#app_id}

Подробную информацию см. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).

* 16777251 — интерфейс S6a/S6d;
* 16777252 — интерфейс S13/S13';