---
title: xDR
description: Описание журналов xDR
type: docs
weight: 10
---
= xDR
:caution-caption: Внимание
:experimental:
:image-caption: Рисунок
:important-caption: Внимание
:note-caption: Примечание
:table-caption: Таблица


ifeval::["{backend}" == "html5s"]
:nofooter:
:notitle:
endif::[]


ifeval::["{backend}" == "pdf"]
:toc-title: Содержание
:toclevels: 5
:sectnumlevels: 5
:sectnums:
:showtitle:
:pagenums:
:outlinelevels: 5
:toc: auto
endif::[]

В данном разделе приведено описание журналов xDR.
По умолчанию журналы EDR хранятся в директории *_/usr/protei/cdr/Protei_SigFW_*.