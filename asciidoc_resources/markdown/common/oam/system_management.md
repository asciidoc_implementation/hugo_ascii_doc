---
title: "Управление"
description: "Основные команды управления узлом PROTEI MME"
weight: 20
type: docs
---

### Директории ###

В PROTEI MME используются следующие директории:

- `/usr/protei/Protei_MME` — рабочая директория;
- `/usr/protei/Protei_MME/bin` — директория для исполняемых файлов;
- `/usr/protei/Protei_MME/cdr` — директория для CDR–журналов;
- `/usr/protei/Protei_MME/config` — директория для конфигурационных файлов;
- `/usr/protei/Protei_MME/log` — директория для лог-файлов;
- `/usr/protei/Protei_MME/metrics` — директория для файлов метрик;
- `/usr/protei/Protei_MME/scripts` — директория для скриптов, реализующих запросы API;

### Управляющие команды ###

* Чтобы запустить PROTEI MME, следует выполнить одну из команд:

  * команду `systemctl start` от лица суперпользователя:

        $ sudo systemctl start mme

  * скрипт `start` в рабочей папке:
  
        $ /usr/protei/Protei_MME/start

* Чтобы остановить PROTEI MME, следует выполнить одну из команд:

  * команду `systemctl stop` от лица суперпользователя:

        $ sudo systemctl stop mme

  * скрипт `stop` в рабочей папке:

        $ /usr/protei/Protei_MME/stop

* Чтобы проверить текущее состояние PROTEI MME, следует выполнить команду `systemctl status` от лица суперпользователя:

        $ sudo systemctl status mme
        ● mme.service - MME
           Loaded: loaded (/usr/lib/systemd/system/mme.service; enabled; vendor preset: disable>
          Drop-In: /etc/systemd/system/mme.service.d
                   └─override.conf
           Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
          Process: 998814 ExecStopPost=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
          Process: 998729 ExecStopPost=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
          Process: 998683 ExecStop=/usr/protei/Protei_MME/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
          Process: 999135 ExecStart=/usr/protei/Protei_MME/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
          Process: 999108 ExecStartPre=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
          Process: 999082 ExecStartPre=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
         Main PID: 999151 (Protei_MME)
            Tasks: 11 (limit: 35612)
           Memory: 489.7M
           CGroup: /system.slice/mme.service
                   └─999151 ./bin/Protei_MME

* Чтобы проверить текущую версию PROTEI MME, следует запустить скрипт `version` в рабочей папке:

        $ ./version 
        Start: Protei_MME
        MME: Company: Protei
        Copyright: 
        Product: Protei_MME
        ProductCode: 1.46.2.0
        BuildNumber: 997
        Label: 
        AdditionalInfo: Supported license
         Release
        CVS_Version: 
        SVN_Version: 
        GIT_Version: https://git.protei.ru/MobileDevelop/Protei_MME.git HEAD 648c530dab58568bebc5468c2acb270aee654e40
        VersionDate: Sep 20 2023 19:40:10

* Чтобы перезагрузить PROTEI MME, следует выполнить одну из команд:

  * команду `systemctl restart` от лица суперпользователя:

        $ sudo systemctl restart mme
        ● mme.service - MME
           Loaded: loaded (/usr/lib/systemd/system/mme.service; enabled; vendor preset: disabled)
           Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
          Process: 99426 ExecStopPost=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
          Process: 99344 ExecStopPost=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
          Process: 99300 ExecStop=/usr/protei/Protei_MME/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
          Process: 99508 ExecStart=/usr/protei/Protei_MME/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
          Process: 99481 ExecStartPre=/usr/protei/Protei_MME/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
          Process: 99453 ExecStartPre=/usr/protei/Protei_MME/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
         Main PID: 99524 (Protei_MME)
            Tasks: 1 (limit: 35612)
           Memory: 2.5M
           CGroup: /system.slice/mme.service
                   └─99524 ./bin/Protei_MME --mme-standalone

  * скрипт `restart` в рабочей папке:

        $ sh /usr/protei/Protei_MME/restart

* Чтобы перезагрузить конфигурационный файл `file.cfg`, следует запустить скрипт `reload` в рабочей папке:

        $ /usr/protei/Protei_MME/reload <file.cfg>
        reload <file> config Ok

* Чтобы записать дамп ядра, следует запустить скрипт `core_dump` в рабочей папке:

        $ /usr/protei/Protei_MME/core_dump
        Are you sure you want to continue? y
        Core dump generated!

**Примечание.** Файл дампа хранится в директории `/var/lib/systemd/coredump`.