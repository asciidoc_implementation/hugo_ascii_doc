---
title: "HTTP API"
description: "Описание методов HTTP API"
weight: 30
type: docs
---

## Общая информация HTTP API ##

HTTP API используется для получения информации:

- состояние GTP-/SGs-соединений;
- состояние базовых станций;
- текущий статус абонентов;
- состояние баз данных.

Все запросы обрабатываются в синхронном режиме. Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Запросы передаются по протоколу HTTP.

Приложение должно использовать один адрес URL для отправки запросов:

```curl
http://host:port/mme/v1/command?arg=value&...&arg=value
```

| Поле    | Описание                                 | Тип       | O/M |
|---------|------------------------------------------|-----------|-----|
| host    | IP-адрес или DNS-имя PROTEI MME API.     | ip/string | M   |
| port    | Номер порта для установления соединения. | int       | M   |
| command | Имя выполняемой команды.                 | string    | M   |
| arg     | Параметр команды.                        | string    | O   |
| value   | Значение параметра.                      | Any       | O   |

Все запросы осуществляются с помощью метода **HTTP GET**.

## Ответ от PROTEI MME ##

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON и **Content-Type: application/json**.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```text
200 OK
```

**Примечание.** Именно такой ответ передает система, если не указано иное.

## Команды ##

* [clear_dns_cache](#очистить-cache-адресов-DNS-clear_dns_cache) - очистить cache адресов DNS;
* [deact_bearer](#деактивировать-bearer-службу-deact_bearer) - деактивировать bearer-службу;
* [detach](#удалить-регистрацию-абонента-из-сети-detach) - удалить регистрацию абонента из сети;
* [detach_by_vlr](#удалить-регистрации-абонентов-на-определенном-VLR-и-запретить-новые-detach_by_vlr) - удалить регистрации абонентов на определенном VLR и запретить новые;
* [disconnect_enb](#разорвать-соединение-с-eNodeB-disconnect_enb) - разорвать соединение с eNodeB;
* [enable_vlr](#разрешить-регистрации-на-определенном-VLR-enable_vlr) - разрешить регистрации на определенном VLR;
* [get_db_status](#получить-информацию-о-состоянии-базы-данных-get_db_status) - получить информацию о состоянии базы данных;
* [get_gtp_peers](#получить-информацию-о-состоянии-GTP-соединений-get_gtp_peers) - получить информацию о состоянии GTP-соединений;
* [get_location](#получить-местоположение-абонента-get_location) - получить местоположение абонента;
* [get_metrics](#получить-текущие-значения-метрик-get_metrics) - получить текущие значения метрик;
* [get_profile](#получить-информацию-об-абоненте-get_profile) - получить информацию об абоненте;
* [get_s1_peers](#получить-информацию-о-состоянии-базовых-станций-LTE-из-хранилища-MME-get_s1_peers) - получить информацию о состоянии базовых станций LTE из хранилища MME;
* [get_sgs_peers](#получить-информацию-о-состоянии-SGs-соединений-get_sgs_peers) - получить информацию о состоянии SGs-соединений;
* [reset_metrics](#сбросить-текущие-значения-метрик-reset_metrics) - сбросить текущие значения метрик;
* [ue_disable_trace](#деактивировать-трассировку-на-eNodeB-для-абонента-ue_disable_trace) - деактивировать трассировку на eNodeB для абонента;
* [ue_enable_trace](#активировать-трассировку-на-eNodeB-для-абонента-ue_enable_trace) - активировать трассировку на eNodeB для абонента.

### Очистить cache адресов DNS, clear_dns_cache ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/clear_dns_cache
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/clear_dns_cache
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата. 

### Деактивировать bearer-службу, deact_bearer ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/deact_bearer&imsi=<imsi>&bearer_id=<id>
```

#### Поля запроса ####

| Поле                              | Описание                                    | Тип    | O/M |
|-----------------------------------|---------------------------------------------|--------|-----|
| imsi                              | Номер IMSI абонента.                        | string | M   |
| <a name="bearer_id">bearer_id</a> | Идентификатор деактивируемой bearer-службы. | int    | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/deact_bearer&imsi=25048123456789&bearer_id=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No bearer_id` - некорректный запрос: не задано значение **[bearer_id](#bearer_id)**;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to deact bearer` - услуга недоступна ввиду невозможности деактивировать bearer-службу.

### Удалить регистрацию абонента из сети, detach ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach?imsi=<imsi>[&reattach=<bool>][&purge=<bool>]
```

#### Поля запроса ####

| Поле     | Описание                                                | Тип    | O/M |
|----------|---------------------------------------------------------|--------|-----|
| imsi     | Номер IMSI абонента.                                    | string | M   |
| reattach | Флаг переподключения к сети. По умолчанию: 0.           | bool   | O   |
| purge    | Флаг полного удаления профиля из сети. По умолчанию: 1. | bool   | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach?imsi=25048123456789&reattach=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to detach UE` - услуга недоступна ввиду невозможности удаления регистрации.

### Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach_by_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле                              | Описание           | Тип | O/M |
|-----------------------------------|--------------------|-----|-----|
| <a name="ip_detach_by_vlr">ip</a> | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_detach_by_vlr)**;

### Разорвать соединение с eNodeB, disconnect_enb ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/disconnect_enb&ip=<ip>
```

#### Поля запроса ####

| Поле                            | Описание         | Тип | O/M |
|---------------------------------|------------------|-----|-----|
| <a name="ip_disable_vlr">ip</a> | IP-адрес eNodeB. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/disconnect_enb&ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_disable_vlr)**;
* `404 Not Found` - указанная eNodeB не найдена;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Разрешить регистрации на определенном VLR, enable_vlr ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/enable_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле                           | Описание           | Тип | O/M |
|--------------------------------|--------------------|-----|-----|
| <a name="ip_enable_vlr">ip</a> | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_enable_vlr)**.

### Получить информацию о состоянии базы данных, get_db_status ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_db_status
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_db_status
```

#### Поля ответа ####

| Поле                               | Описание                                                              | Тип            | O/M |
|------------------------------------|-----------------------------------------------------------------------|----------------|-----|
| Writer                             | Параметры соединения, добавляющего записи в базу данных.              | object         | M   |
| **{**                              |                                                                       |                |     |
| &nbsp;&nbsp;State                  | Состояние соединения.<br>`connected`/`disconnected`/`busy`/`unknown`. | string         | M   |
| &nbsp;&nbsp;Last job               | Параметры последнего изменения.                                       | object         | M   |
| &nbsp;&nbsp;**{**                  |                                                                       |                |     |
| &nbsp;&nbsp;&nbsp;&nbsp;Done       | Флаг завершения операции.                                             | bool           | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;Rows saved | Количество записанных строк.                                          | int            | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;Timestamp  | Временная метка завершения операции. Формат:<br>`YYYY-MM-DD hh:mm:ss` | datetime       | M   |
| &nbsp;&nbsp;**}**                  |                                                                       |                |     |
| **}**                              |                                                                       |                |     |
| Readers                            | Параметры соединений, считывающих записи из базы данных.              | list\<object\> | M   |
| **{**                              |                                                                       |                |     |
| &nbsp;&nbsp;State                  | Состояние соединения.<br>`connected`/`disconnected`/`busy`/`unknown`. | string         | M   |
| **}**                              |                                                                       |                |     |

#### Пример ответа ####

```json5
{
  "Writer": {
    "State": "connected",
    "Last job": {
      "Done": true,
      "Rows saved": 0,
      "Timestamp": "2023-01-01 01:23:45"
    }
  },
  "Readers": [
    {
      "State": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить местоположение абонента, get_location ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_location?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса ####

```
http://localhost:65535/mme/v1/get_location&imsi=25048123456789
```

#### Поля ответа ####

| Поле                             | Описание                    | Тип    | O/M |
|----------------------------------|-----------------------------|--------|-----|
| [EPS_State](#значения-eps-state) | Код состояния абонента EPS. | int    | M   |
| PLMN                             | Идентификатор сети PLMN.    | string | M   |
| TAC                              | Код области отслеживания.   | string | M   |
| CellID                           | Идентификатор соты.         | hex    | M   |
| ENB-ID                           | Идентификатор eNodeB.       | int    | M   |


#### Значения EPS State ####

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* `0` - Detached;
* `1` - AttachedNotReachableForPaging;
* `2` - AttachedReachableForPaging;
* `3` - ConnectedNotReachableForPaging;
* `4` - ConnectedReachableForPaging;
* `5` - NotProvidedFromSGSN.

#### Пример ответа ####

```json5
{
  "EPS_State": 0,
  "PLMN": "25002",
  "TAC": "0",
  "CellID": 0x10,
  "ENB-ID": 15000
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить текущие значения метрик, get_metrics ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_metrics
```

#### Пример ответа ####

```console
Total records count: 5
Number of attached subscribers: 2
Number of default bearers: 0
Number of dedicated bearers: 0

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 10847  100 10847    0     0   271k      0 --:--:-- --:--:-- --:--:--  278k
{
  "Diameter": {
    "capabilitiesExchangeAnswer": 0,
    "capabilitiesExchangeAnswer2001": 0,
    "capabilitiesExchangeRequest": 0,
    "deviceWatchdogAnswer": 155,
    "deviceWatchdogAnswer2001": 155,
    "deviceWatchdogRequest": 26
  },
  "Handover": {
    "interNodeHandoverRequestFromLteToUmts": 0,
    "interNodeHandoverRequestFromUmtsToLte": 0,
    "interNodeHandoverSuccessFromLteToUmts": 0,
    "interNodeHandoverSuccessFromUmtsToLte": 0,
    "interS1BasedHandoverRequestSgwChange": 0,
    "interS1BasedHandoverRequestSgwNotChange": 0,
    "interS1BasedHandoverSuccessSgwChange": 0,
    "interS1BasedHandoverSuccessSgwNotChange": 0,
    "intraS1BasedHandoverRequestSgwChange": 0,
    "intraS1BasedHandoverRequestSgwNotChange": 0,
    "intraS1BasedHandoverSuccessSgwChange": 0,
    "intraS1BasedHandoverSuccessSgwNotChange": 0,
    "intraX2BasedHandoverRequestSgwChange": 0,
    "intraX2BasedHandoverRequestSgwNotChange": 0,
    "intraX2BasedHandoverSuccessSgwChange": 0,
    "intraX2BasedHandoverSuccessSgwNotChange": 0
  },
  "Paging": {
    "s1PagingRequest": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfbLastEnodeb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestCsfbLastTa": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestLastEnodeb": {
      "": {
        "Value": 0
      }
    },
    "s1PagingRequestLastTa": {
      "": {
        "Value": 0
      }
    },
    "s1PagingSuccess": {
      "": {
        "Value": 0
      }
    },
    "s1PagingSuccessCsfb": {
      "": {
        "Value": 0
      }
    }
  },
  "Resource": {
    "averageCpuUtilization": 1.0854070276353633,
    "maxCpuUtilization": 1.402805611222445
  },
  "S1-Security": {
    "authenticationRequest": 4,
    "authenticationSuccess": 2,
    "securityModeCommand": 4,
    "securityModeComplete": 4
  },
  "S1-Service": {
    "ExtendedServiceRequest": {
      "": {
        "Value": 0
      }
    },
    "ExtendedServiceSuccess": {
      "": {
        "Value": 0
      }
    },
    "ServiceRequest": {
      "": {
        "Value": 0
      }
    },
    "ServiceSuccess": {
      "": {
        "Value": 0
      }
    },
    "csfbMoInitialContextSetupResponse": {
      "": {
        "Value": 0
      }
    },
    "csfbMoUeContextModificationResponse": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-attach": {
    "attachRequest": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "attachSuccess": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "combinedAttachRequest": {
      "": {
        "Value": 0
      }
    },
    "combinedAttachSuccess": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-bearer-activation": {
    "dedicatedBearerActiveRequest": {
      "": {
        "Value": 0
      }
    },
    "dedicatedBearerActiveSuccess": {
      "": {
        "Value": 0
      }
    },
    "pdnConnectivityRequest": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    }
  },
  "S1-bearer-deactivation": {
    "deactivateEpsBearerContextAccept": {
      "": {
        "Value": 0
      }
    },
    "deactivateEpsBearerContextRequest": {
      "": {
        "Value": 0
      }
    },
    "dedicatedBearerDeactivationRequest": {
      "": {
        "Value": 0
      }
    },
    "defaultBearerDeactivationRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-bearer-modification": {
    "hssInitBearerModRequest": {
      "": {
        "Value": 0
      }
    },
    "hssInitBearerModSuccess": {
      "": {
        "Value": 0
      }
    },
    "pgwInitBearerModRequest": {
      "": {
        "Value": 0
      }
    },
    "pgwInitBearerModSuccess": {
      "": {
        "Value": 0
      }
    },
    "ueInitBearerResModRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-detach": {
    "detachRequest": {
      "": {
        "Value": 3
      },
      "IMSIPLMN:20893": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "detachRequestMmeInit": {
      "": {
        "Value": 0
      }
    },
    "detachSuccess": {
      "": {
        "Value": 0
      }
    },
    "detachSuccessMmeInit": {
      "": {
        "Value": 0
      }
    }
  },
  "S1-interface": {
    "eNbConfigurationUpdateRequest": {
      "": {
        "Value": 0
      }
    },
    "eNbConfigurationUpdateSuccess": {
      "": {
        "Value": 0
      }
    },
    "eNodeBInitS1ResetRequest": {
      "": {
        "Value": 0
      }
    },
    "eNodeBInitS1ResetSuccess": {
      "": {
        "Value": 0
      }
    },
    "eRabModificationConfirm": {
      "": {
        "Value": 0
      }
    },
    "eRabModificationIndication": {
      "": {
        "Value": 0
      }
    },
    "eRabModifyRequest": {
      "": {
        "Value": 0
      }
    },
    "eRabModifyResponse": {
      "": {
        "Value": 0
      }
    },
    "eRabReleaseCommand": {
      "": {
        "Value": 0
      }
    },
    "eRabReleaseResponse": {
      "": {
        "Value": 0
      }
    },
    "eRabSetupRequest": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupFailure": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupRequest": {
      "": {
        "Value": 0
      }
    },
    "initialContextSetupResponse": {
      "": {
        "Value": 0
      }
    },
    "mmeConfigurationUpdateRequest": {
      "": {
        "Value": 0
      }
    },
    "mmeConfigurationUpdateSuccess": {
      "": {
        "Value": 0
      }
    },
    "s1SetupRequest": {
      "": {
        "Value": 4
      },
      "TAI:001010001": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "s1SetupSuccess": {
      "": {
        "Value": 4
      },
      "TAI:001010001": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextModificationFailure": {
      "": {
        "Value": 0
      }
    },
    "ueContextModificationRequest": {
      "": {
        "Value": 0
      }
    },
    "ueContextModificationResponse": {
      "": {
        "Value": 0
      }
    },
    "ueContextReleaseCommand": {
      "": {
        "Value": 8
      },
      "TAI:208930001": {
        "Value": 8
      }
    },
    "ueContextReleaseCommand1_20": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseCommand3_0": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    },
    "ueContextReleaseCommand3_2": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseComplete": {
      "": {
        "Value": 8
      },
      "TAI:208930001": {
        "Value": 8
      }
    },
    "ueContextReleaseRequest": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    },
    "ueContextReleaseRequest1_20": {
      "": {
        "Value": 3
      },
      "TAI:208930001": {
        "Value": 3
      }
    }
  },
  "S11-interface": {
    "bearerResourceCommand": 0,
    "bearerResourceFailureIndication": 0,
    "createBearerRequest": 0,
    "createBearerResponse": 0,
    "createIndirectDataForwardingTunnelRequest": 0,
    "createIndirectDataForwardingTunnelResponse": 0,
    "createSessionRequest": 4,
    "createSessionResponse": 4,
    "deleteBearerCommand": 0,
    "deleteBearerFailureIndication": 0,
    "deleteBearerRequest": 0,
    "deleteBearerResponse": 0,
    "deleteIndirectDataForwardingTunnelRequest": 0,
    "deleteIndirectDataForwardingTunnelResponse": 0,
    "deleteSessionRequest": 5,
    "deleteSessionResponse": 5,
    "downlinkDataNotification": 0,
    "downlinkDataNotificationAck": 0,
    "downlinkDataNotificationFailureInd": 0,
    "modifyBearerCommand": 0,
    "modifyBearerFailureIndication": 0,
    "modifyBearerRequest": 0,
    "modifyBearerResponse": 0,
    "releaseAccessBearersRequest": 3,
    "releaseAccessBearersResponse": 3,
    "suspendAcknowledge": 0,
    "suspendNotification": 0,
    "updateBearerRequest": 0,
    "updateBearerResponse": 0
  },
  "S6a-interface": {
    "authenticationInformationAnswer": 4,
    "authenticationInformationAnswer2001": 4,
    "authenticationInformationAnswer3004": 0,
    "authenticationInformationAnswer5001": 0,
    "authenticationInformationAnswer5420": 0,
    "authenticationInformationRequest": 4,
    "cancelLocationAnswer": 0,
    "cancelLocationAnswer2001": 0,
    "cancelLocationRequest": 0,
    "deleteSubscriberDataAnswer": 0,
    "deleteSubscriberDataRequest": 0,
    "insertSubscriberDataAnswer": 0,
    "insertSubscriberDataRequest": 0,
    "notifyAnswer": 4,
    "notifyAnswer2001": 4,
    "notifyRequest": 4,
    "purgeUeAnswer": 0,
    "purgeUeAnswer2001": 0,
    "purgeUeRequest": 0,
    "resetAnswer": 0,
    "resetRequest": 0,
    "updateLocationAnswer": 2,
    "updateLocationAnswer2001": 2,
    "updateLocationRequest": 2
  },
  "SGs-interface": {
    "sGsApLocationUpdateAccept": {
      "": {
        "Value": 0
      }
    },
    "sGsApLocationUpdateRequest": {
      "": {
        "Value": 0
      }
    }
  },
  "Sv-interface": {
    "srvccPsToCsCompleteAcknowledge": 0,
    "srvccPsToCsCompleteNotification": 0,
    "srvccPsToCsRequest": 0,
    "srvccPsToCsResponse": 0
  },
  "TAU": {
    "interCombinedTauRequest": {
      "": {
        "Value": 0
      }
    },
    "interCombinedTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "interTauRequest": {
      "": {
        "Value": 0
      }
    },
    "interTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "intraCombinedTauRequest": {
      "": {
        "Value": 0
      }
    },
    "intraCombinedTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "intraTauRequest": {
      "": {
        "Value": 0
      }
    },
    "intraTauSuccess": {
      "": {
        "Value": 0
      }
    },
    "periodTauRequest": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    },
    "periodTauSuccess": {
      "": {
        "Value": 2
      },
      "TAI:208930001": {
        "Value": 2
      }
    }
  },
  "Users": {
    "realTimeAttachedUsersAtEcmConnectedStatus": {
      "": {
        "Value": 1
      },
      "IMSIPLMN:20893": {
        "Value": 1
      },
      "TAI:208930001": {
        "Value": 1
      }
    },
    "realTimeAttachedUsersAtEcmIdleStatus": {
      "": {
        "Value": 4
      },
      "IMSIPLMN:20893": {
        "Value": 4
      },
      "TAI:208930001": {
        "Value": 4
      }
    },
    "realTimeDedicatedBearerNumber": {
      "": {
        "Value": 0
      }
    },
    "realTimePdnConnectionNumber": {
      "": {
        "Value": 0
      }
    },
    "realTimeUsersAtEmmDeregisteredStatus": {
      "": {
        "Value": 5
      },
      "IMSIPLMN:20893": {
        "Value": 5
      },
      "TAI:208930001": {
        "Value": 5
      }
    }
  }
}
```

### Получить информацию об абоненте, get_profile ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_profile?[imsi=<imsi>&][guti=<guti>&][imeisv=<imeisv>&][select=<fields>]
```

#### Поля запроса ####

| Поле                        | Описание                                                                                                   | Тип    | O/M |
|-----------------------------|------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi                        | Номер IMSI абонента.                                                                                       | string | C   |
| guti                        | Глобальный уникальный временный идентификатор абонента.                                                    | string | C   |
| imeisv                      | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | C   |
| <a name="select">select</a> | Запрашиваемые поля, разделенные запятой.                                                                   | string | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_profile?imsi=25048123456789&select=guti,imeisv
```

#### Поля ответа ####

| Поле   | Описание                                                                                                   | Тип    | O/M |
|--------|------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi   | Номер IMSI абонента.                                                                                       | string | C   |
| guti   | Глобальный уникальный временный идентификатор абонента.                                                    | string | C   |
| imeisv | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | C   |

**Примечание.** Используемые параметры соответствуют значениям, указанным в запросе в поле [select](#select).

#### Пример ответа ####

```json5
{
  "imsi": "250020123456789",
  "guti": "2500200020111111111",
  "imeisv": "3520990012345612"
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено.

### Получить информацию о состоянии GTP-соединений, get_gtp_peers ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_gtp_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_gtp_peers
```

#### Поля ответа ####

| Поле           | Описание                                                                                                                                           | Тип            | O/M |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------|----------------|-----|
| gtpPeers       | Перечень соединений.                                                                                                                               | list\<object\> | M   |
| ip             | IP-адрес соединения.                                                                                                                               | ip             | M   |
| version        | Версия протокола GTP.<br>`V1`/`V2`.                                                                                                                | string         | M   |
| restartCounter | Значение счетчика перезапусков. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf). | int            | M   |
| state          | Состояние соединения.<br>`connected`/`disconnected`/`busy`/`unknown`.                                                                              | string         | M   |

#### Пример ответа ####

```json5
{
  "gtpPeers": [
    {
      "ip": "127.0.0.1",
      "version":"V2",
      "restartCounter": 1,
      "state": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить информацию о состоянии базовых станций LTE из хранилища MME, get_s1_peers ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_s1_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_s1_peers
```

#### Поля ответа ####

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | ip     | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |


#### Пример ответа ####

```json5
[
  {
    "PLMN": "99999",
    "TAC": 1,
    "id": "00101-176849",
    "ip": "172.30.153.1",
    "name": "enb2b2d1",
    "state": true
  }
]
```

### Получить информацию о состоянии SGs-соединений, get_sgs_peers ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_sgs_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_sgs_peers
```

#### Поля ответа ####

| Поле     | Описание                                                              | Тип            | O/M |
|----------|-----------------------------------------------------------------------|----------------|-----|
| sgsPeers | Перечень соединений.                                                  | list\<object\> | M   |
| ip       | IP-адрес соединения.                                                  | ip             | M   |
| gt       | Глобальный заголовок соединения.                                      | string         | M   |
| state    | Состояние соединения.<br>`connected`/`disconnected`/`busy`/`unknown`. | string         | M   |
| blocked  | Флаг блокировки соединения.                                           | bool           | M   |

#### Пример ответа ####

```json5
{
  "sgsPeers": [
    {
      "ip": "127.0.0.1",
      "gt": "250021234567890",
      "state": "connected",
      "blocked": false
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Сбросить текущие значения метрик, reset_metrics ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/reset_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/reset_metrics
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `412 Precondition Failed, Metrics disabled` - запрос не выполнен, поскольку сбор метрик не активирован;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Деактивировать трассировку на eNodeB для абонента, ue_disable_trace ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_disable_trace?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_disable_trace?imsi=25048123456789
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `406 Not Acceptable, No trace id data` - запрос не может быть выполнен ввиду отсутствия соответствующей логики;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging.

### Активировать трассировку на eNodeB для абонента, ue_enable_trace ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_enable_trace?imsi=<imsi>&TCE_IP=<ip>
```

#### Поля запроса ####

| Поле                        | Описание                                                                                             | Тип    | O/M |
|-----------------------------|------------------------------------------------------------------------------------------------------|--------|-----|
| imsi                        | Номер IMSI абонента.                                                                                 | string | M   |
| <a name="tce_ip">TCE_IP</a> | IP-адрес узла сбора и хранения файлов трассировки, <abbr title="Trace Collection Entity">TCE</abbr>. | ip     | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_enable_trace?imsi=25048123456789&TCE_IP=127.0.0.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, UE already traced` - некорректный запрос: указанное устройство уже отслеживается;
* `400 Bad Request, Trace Collection Entity IP Address empty` - некорректный запрос: не задано значение **[TCE_IP](#tce_ip)**;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, No free trace ID` - услуга недоступна ввиду отсутствия свободных логик;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging.