---
title: "KPI"
description: "Описание показателей производительности"
weight: 60
type: docs
---

Для узла Protei_MME отслеживаются следующие ключевые показатели:

* Количество зарегистрированных абонентов:

SAU = [realTimeAttachedUsersAtEcmIdleStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmIdleStatus/) + 
[realTimeAttachedUsersAtEcmConnectedStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmConnectedStatus/)

* Количество bearer-служб

Number of bearers = [realTimeAttachedUsersAtEcmIdleStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmIdleStatus/) + 
[realTimeAttachedUsersAtEcmConnectedStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmConnectedStatus/) + 
[realTimePdnConnectionNumber](../../logging/stat/MME_users/#realTimePdnConnectionNumber/)

* Количество выделенных bearer-служб

Number of dedicated bearers = [realTimeDedicatedBearerNumber](../../logging/stat/MME_users/#realTimeDedicatedBearerNumber/)

* Доля успешных процедур Attach

Attach SR = ([attachSuccess](../../logging/stat/MME_s1_Attach/#attachSuccess/) * 100%) / 
[attachRequest](../../logging/stat/MME_s1_Attach/#attachRequest/)

* Доля успешных процедур аутентификации и шифрования

Authentication SR = ([securityModeCommandSuccess](../../logging/stat/MME_s1_Security/#securityModeCommandSuccess/) * 100%) / 
[authenticationRequest](../../logging/stat/MME_s1_Security/#authenticationRequest/)

* Доля успешных запросов TRACKING AREA UPDATE

TAU SR = ([intraTauSuccess](../../logging/stat/MME_tau/#intraTauSuccess/) + 
[interTauSuccess](../../logging/stat/MME_tau/#interTauSuccess/)) * 100% / 
([intraTauRequest](../../logging/stat/MME_tau/#intraTauRequest/) + [intraTauSuccess](../../logging/stat/MME_tau/#intraTauSuccess/))

* Доля успешных процедур Paging

Paging SR = ([pagingSuccess](../../logging/stat/MME_s1_Paging/#pagingSuccess/) * 100%) / 
[pagingRequest](../../logging/stat/MME_s1_Paging/#pagingRequest/)

* Доля успешных запросов SGsAP-LOCATION-UPDATE

SGsAP Location Update SR = ([sGsApLocationUpdateAccept](../../logging/stat/MME_sgs_Interface/#sGsApLocationUpdateAccept/) * 100%) / 
[sGsApLocationUpdateRequest](../../logging/stat/MME_sgs_Interface/#sGsApLocationUpdateRequest/)

* Доля успешных процедур SRVCC

SRVCC SR = ([srvccPsToCsCompleteAcknowledge](../../logging/stat/MME_sv_Interface/#srvccPsToCsCompleteAcknowledge/) * 100%) / 
[srvccPsToCsRequest](../../logging/stat/MME_sv_Interface/#srvccPsToCsRequest/)

* Доля успешных процедур MO CS Fallback

CSFB MO SR = ([csfbMoUeContextModificationResponse](../../logging/stat/MME_s1_Service/#csfbMoUeContextModificationResponse/) * 100%) / 
[ExtendedServiceRequest](../../logging/stat/MME_s1_Service/#ExtendedServiceRequest/)

* Доля успешных процедур MO CS Fallback

CSFB MT SR = ([csfbMoInitialContextSetupResponse](../../logging/stat/MME_s1_Service/#csfbMoInitialContextSetupResponse/) * 100%) / 
[ExtendedServiceRequest](../../logging/stat/MME_s1_Service/#ExtendedServiceRequest/)

* Доля успешных межсетевых хэндоверов

Intra-Node Handover from LTE to UMTS SR = ([intraNodeHandoverSuccessFromLteToUmts](../../logging/stat/MME_handover/#intraNodeHandoverSuccessFromLteToUmts/) * 100%) / 
[intraNodeHandoverRequestFromLteToUmts](../../logging/stat/MME_handover/#intraNodeHandoverRequestFromLteToUmts/)

Inter-Node Handover from LTE to UMTS SR = ([interNodeHandoverSuccessFromLteToUmts](../../logging/stat/MME_handover/#interNodeHandoverSuccessFromLteToUmts/) * 100%) / 
[interNodeHandoverRequestFromLteToUmts](../../logging/stat/MME_handover/#interNodeHandoverRequestFromLteToUmts/)

Intra-Node Handover from UMTS to LTE SR = ([intraNodeHandoverSuccessFromUmtsToLte](../../logging/stat/MME_handover/#intraNodeHandoverSuccessFromUmtsToLte/) * 100%) / 
[intraNodeHandoverRequestFromUmtsToLte](../../logging/stat/MME_handover/#intraNodeHandoverRequestFromUmtsToLte/)

Inter-Node Handover from UMTS to LTE SR = ([interNodeHandoverSuccessFromUmtsToLte](../../logging/stat/MME_handover/#interNodeHandoverSuccessFromUmtsToLte/) * 100%) / 
[interNodeHandoverRequestFromUmtsToLte](../../logging/stat/MME_handover/#interNodeHandoverRequestFromUmtsToLte/)

* Доля успешных хэндоверов S1

Intra-Node S1 Handover SR = ([intraS1BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#intraS1BasedHandoverSuccessSgwNotChange/) + 
[intraS1BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#intraS1BasedHandoverSuccessSgwChange/)) / 
([intraS1BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#intraS1BasedHandoverRequestSgwNotChange/) + 
[intraS1BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#intraS1BasedHandoverRequestSgwChange/)) * 100%

Intra-Node X2 Handover SR = ([intraX2BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#intraX2BasedHandoverSuccessSgwNotChange/) + 
[intraX2BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#intraX2BasedHandoverSuccessSgwChange/)) / 
([intraX2BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#intraX2BasedHandoverRequestSgwNotChange/) + 
[intraX2BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#intraX2BasedHandoverRequestSgwChange/)) * 100%

Inter-Node S1 Handover SR = ([interS1BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#interS1BasedHandoverSuccessSgwNotChange/) + 
[interS1BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#interS1BasedHandoverSuccessSgwChange/)) / 
([interS1BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#interS1BasedHandoverRequestSgwNotChange/) + 
[interS1BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#interS1BasedHandoverRequestSgwChange/)) * 100%

* Доля успешных запросов GTP: Create Bearer и GTP: Create Session

Сreate Bearer SR = ([createBearerResponse](../../logging/stat/MME_s11_Interface/#createBearerResponse/) * 100%) / 
[createBearerRequest](../../logging/stat/MME_s11_Interface/#createBearerRequest/)

Сreate Session SR = ([createSessionResponse](../../logging/stat/MME_s11_Interface/#createSessionResponse/) * 100%) / 
[createSessionRequest](../../logging/stat/MME_s11_Interface/#createSessionRequest/)

* Доля успешных запросов GTP: Bearer Modification

Bearer Modification SR = ([modifyBearerResponse](../../logging/stat/MME_s11_Interface/#modifyBearerResponse/) * 100%) / 
[modifyBearerRequest](../../logging/stat/MME_s11_Interface/#modifyBearerRequest/)

* Доля успешных запросов GTP: Dedicated Bearer Creation

Dedicated Bearer Creation SR = ([dedicatedBearerActiveSuccess](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveSuccess/) * 100%) / 
[dedicatedBearerActiveRequest](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveRequest/)

* Доля успешных запросов GTP: Dedicated Bearer Modification

Dedicated Bearer Modification SR = ([ueInitBearerResModRequest](../../logging/stat/MME_s1_Bearer_Activation/#ueInitBearerResModRequest/) * 100%) / 
[dedicatedBearerActiveRequest](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveRequest/)

* Доля использованных мощностей CPU

* [averageCpuUtilization](../../logging/stat/MME_resource/#averageCpuUtilization/)
* [maxCpuUtilization](../../logging/stat/MME_resource/#maxCpuUtilization/)


* Доля использованной памяти

Memory utilization = ([maxMemoryUsed](../../logging/stat/MME_resource/#maxMemoryUsed/) * 100%) / 
[memoryAllocated](../../logging/stat/MME_resource/#memoryAllocated/)

* Доля занятого дискового пространства

Disk utilization = ([maxDiskUsed](../../logging/stat/MME_resource/#maxDiskUsed/) * 100%) / 
[diskAllocated](../../logging/stat/MME_resource/#diskAllocated/)
