---
title: "dns.cfg"
description: "Параметры DNS"
weight: 20
type: docs
---

В файле задаются настройки DNS.

**Примечание.** Наличие файла обязательно.

### Используемые секции ###

* [\[Client\]](#client) - параметры клиентской части;
* [\[Server\]](#server) - параметры серверной части;
* [\[ClientPool\]](#client-pool) - параметры пула клиентов.

### Описание параметров ###

| Name                                         | Description                                                                                                                       | Type        | Default | O/M | Version |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|-------------|---------|-----|---------|
| **<a name="client">\[Client\]</a>**          | Параметры клиентской части.                                                                                                       | object      | -       | M   |         |
| ID                                           | Идентификатор направления.<br>Должно быть 0.                                                                                      | int         | -       | M   |         |
| Transport                                    | Транспортный протокол.<br>UDP/TCP.                                                                                                | string      | UDP     | O   |         |
| DestAddress                                  | IP-адрес сервера, куда направляются запросы.                                                                                      | ip          | -       | M   |         |
| DestPort                                     | Прослушиваемый порт сервера.                                                                                                      | int         | 53      | O   |         |
| SrcAddress                                   | Локальный IP-адрес.                                                                                                               | ip          | 0.0.0.0 | O   |         |
| SrcPort                                      | Локальный порт.                                                                                                                   | int         | 0       | O   |         |
| ResponseTimeout                              | Время ожидания ответа.                                                                                                            | int<br>с    | 60      | O   |         |
| ResendOverTcp                                | Флаг перепосылки запроса по TCP для получения полного ответа.                                                                     | bool        | 0       | O   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>. | int         | 0       | O   | 1.0.1.1 |
| **<a name="server">\[Server\]</a>**          | Параметры серверной части.                                                                                                        | object      | -       | M   |         |
| ID                                           | Идентификатор направления.                                                                                                        | int         | -       | M   |         |
| Transport                                    | Транспортный протокол.<br>UDP/TCP.                                                                                                | string      | UDP     | O   |         |
| Address                                      | Прослушиваемый IP-адрес.                                                                                                          | ip          | 0.0.0.0 | O   |         |
| Port                                         | Прослушиваемый порт.                                                                                                              | int         | 53      | O   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>. | int         | 0       | O   | 1.0.1.1 |
| **<a name="client-pool">\[ClientPool\]</a>** | Параметры пула клиентов.                                                                                                          | object      | -       | O   | 1.0.2.0 |
| ID                                           | Идентификатор пула клиентских направлений.                                                                                        | int         | -       | M   | 1.0.2.0 |
| Directions                                   | Клиентские направления, задействованные в балансировке.                                                                           | list\<int\> | -       | M   | 1.0.2.0 |

**Примечание.** Если секция [ClientPool](#client-pool) пуста, по умолчанию создается пул с `ID = 0`, содержащий все сконфигурированные клиентские направления.

#### Пример ####

```
[Client]
{
  ID = 1;
  Transport = TCP;
  DestAddress = 8.8.8.8;
  ResendOverTcp = 1;
  ResponseTimeout = 1000;
}
{
  ID = 2;
  DestAddress = 127.0.0.1;
  DestPort = 1234;
  ResponseTimeout = 10;
}
{
  ID = 3;
  DestAddress = 127.0.0.1;
  DestPort = 53;
  dscp = 56;
}

[Server]
{
  ID = 1;
  Address = 127.0.0.1;
  Port = 1234;
}
{
  ID = 2;
}

[ClientPool]
{
  ID = 1;
  Directions = { 2; 3; };
}
```