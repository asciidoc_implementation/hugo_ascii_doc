---
title: "config.cfg"
description: "Файл настройки компонент"
weight: 20
type: docs
---

В файле задаются настройки компонент системы.

**Примечание.** Наличие файла обязательно.

### Описание параметров ###

| Параметр                    | Описание                                                                                             | Тип    | По умолчанию | O/M | P/R | Версия |
|:----------------------------|:-----------------------------------------------------------------------------------------------------|:-------|:-------------|:----|:----|:-------|
| **\[Options\]**             | Параметры COM при старте приложения.                                                                 |        |              |     |     |        |
| DisableBackup               | Флаг отключения записи в конфигурационные файлы.                                                     | bool   | 0            | O   | R   |        |
| **\[Conformity\]**          | Параметры привязки маски компонентного адреса к имени файла. Формат:<br>`{ "<filename>";"<path>"; }` |        |              |     |     |        |
| filename                    | Имя конфигурационного файла.                                                                         | string | -            | M   | R   |        |
| path                        | Относительный путь до конфигурационного файла, считая от рабочей папки.                              | string | -            | M   | R   |        |
| **\[InitialLoadSequence\]** | Последовательность загрузки файлов.                                                                  |        |              |     |     |        |
| path                        | Относительный путь до конфигурационного файла, считая от рабочей папки.                              | string | -            | M   | R   |        |

#### Пример

```
[Conformity]
{"Sg.DIAM$";config/component/diameter.cfg;}
{"Sg.GTP_C$";config/component/gtp_c.cfg;}
{"Sg.S1AP$";config/component/s1ap.cfg;}
{"Sg.SGsAP$";config/component/sgsap.cfg;}

[InitialLoadSequence]
config/component/diameter.cfg;
config/component/gtp_c.cfg;
config/component/s1ap.cfg;
config/component/sgsap.cfg;
```