---
title: "sgsap.cfg"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

**Примечание.** Наличие файла обязательно.

### <a name="sgsap">Описание параметров Sg.SGsAP</a> ###

| Параметр                 | Описание                                                                                   | Тип            | По умолчанию | O/M | P/R | Версия |
|--------------------------|--------------------------------------------------------------------------------------------|----------------|--------------|-----|-----|--------|
| ComponentAddr            | Адрес компонента.                                                                          | string         | -            | M   | R   |        |
| ComponentType            | Тип компонента.                                                                            | string         | Sg.SGsAP     | O   | R   |        |
| Params                   | Параметры компонента.                                                                      | object         | -            | M   | R   |        |
| **{**                    |                                                                                            |                |              |     |     |        |
| Origin-State-Id          | Значение `Origin-State-Id`. См. [RFC 6733](https://datatracker.ietf.org/doc/html/rfc6733). | int            | 0            | O   | R   |        |
| [PeerTable](#peer-table) | Таблица пиров.                                                                             | list\<object\> | -            | O   | R   |        |
| DefaultPCSM              | Имя узла PCSM по умолчанию.                                                                | list\<str\>    | -            | O   | R   |        |
| **}**                    |                                                                                            |                |              |     |     |        |

#### <a name="peer-table">Таблица пиров, PeerTable</a> ####

| Параметр | Описание                                       | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|------------------------------------------------|--------|--------------|-----|-----|--------|
| PeerIP   | IP-адрес пира.                                 | ip     | -            | M   | R   |        |
| GT       | Глобальный заголовок пира.                     | string | -            | O   | R   |        |
| PCSM     | Компонентный адрес соответствующего узла PCSM. | string | -            | M   | R   |        |
| PeerName | Имя пира.                                      | string | -            | O   | R   |        |

### Описание параметров Sg.SGsAP.PCSM ###

| Параметр                         | Описание                                                                         | Тип             | По умолчанию                                                       | O/M | P/R | Версия   |
|----------------------------------|----------------------------------------------------------------------------------|-----------------|--------------------------------------------------------------------|-----|-----|----------|
| ComponentAddr                    | Адрес компонента.                                                                | string          | -                                                                  | M   | R   |          |
| ComponentType                    | Тип компонента.                                                                  | string          | Sg.SGsAP.PCSM                                                      | O   | R   |          |
| Params                           | Параметры компонента.                                                            | object          | -                                                                  | M   | R   |          |
| **{**                            |                                                                                  |                 |                                                                    |     |     |          |
| <a name="peer-ip">PeerIP</a>     | IP-адрес подключения узла PCSM.                                                  | ip              | -                                                                  | M   | R   |          |
| <a name="peer-port">PeerPort</a> | Порт подключения узла PCSM.                                                      | string          | -                                                                  | M   | R   |          |
| <a name="src-ip">SrcIP</a>       | Локальный IP-адрес узла PCSM.                                                    | string          | [sgsap :: \[LocalAddress\] :: LocalHost](../../sgsap/#local-host/) | O   | R   |          |
| <a name="src-port">SrcPort</a>   | Локальный порт узла PCSM.                                                        | string          | [sgsap :: \[LocalAddress\] :: LocalPort](../../sgsap/#local-port/) | O   | R   |          |
| RemoteInterfaces                 | Удаленные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }` | list\<ip:port\> | -                                                                  | O   | R   |          |
| LocalInterfaces                  | Локальные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }` | list\<ip:port\> | -                                                                  | O   | R   | 1.37.2.0 |
| **}**                            |                                                                                  |                 |                                                                    |     |     |          |

**Примечание.** При значениях `PeerIP = ""` и `PeerPort = 0` PCSM ожидает подключения с адреса, указанного в описании PCSM в секции [Sg.SGsAP](#sgsap).

#### Пример ####

```
{
  ComponentAddr = Sg.SGsAP;
  ComponentType = Sg.SGsAP;
  Params = {
    Origin-State-Id = "1";
    PeerTable = {
      {
        PeerIP = "192.168.125.154";
        GT = "79216567568";
        PCSM = "Sg.SGsAP.PCSM.0";
      };
    };
    DefaultPCSM = {
     "Sg.SGsAP.PCSM.0";
    };
  };
}
{
  ComponentAddr = Sg.SGsAP.PCSM.0;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.125.154";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}
```
