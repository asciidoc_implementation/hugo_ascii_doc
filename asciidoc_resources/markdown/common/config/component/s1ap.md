---
title: "s1ap.cfg"
description: "Параметры компонента S1AP"
weight: 20
type: docs
---

В файле задаются настройки компонентов S1AP и S1AP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

**Примечание.** Наличие файла обязательно.

### Описание параметров Sg.S1AP ###

| Параметр                 | Описание                                                                                   | Тип            | По умолчанию | O/M | P/R | Версия |
|--------------------------|--------------------------------------------------------------------------------------------|----------------|--------------|-----|-----|--------|
| ComponentAddr            | Адрес компонента.                                                                          | string         | -            | M   | R   |        |
| ComponentType            | Тип компонента.                                                                            | string         | Sg.S1AP      | O   | R   |        |
| Params                   | Параметры компонента.                                                                      | object         | -            | M   | R   |        |
| **{**                    |                                                                                            |                |              |     |     |        |
| Origin-State-Id          | Значение `Origin-State-Id`. См. [RFC 6733](https://datatracker.ietf.org/doc/html/rfc6733). | int            | 0            | O   | R   |        |
| [PeerTable](#peer-table) | Таблица пиров.                                                                             | list\<object\> | -            | O   | R   |        |
| DefaultPCSM              | Имя узла PCSM по умолчанию.                                                                | list\<str\>    | -            | O   | R   |        |
| **}**                    |                                                                                            |                |              |     |     |        |

#### <a name="peer-table">Таблица пиров, PeerTable</a> ####

| Параметр | Описание                                       | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|------------------------------------------------|--------|--------------|-----|-----|--------|
| PeerIP   | IP-адрес пира.                                 | ip     | -            | M   | R   |        |
| PCSM     | Компонентный адрес соответствующего узла PCSM. | string | -            | M   | R   |        |

### Описание параметров Sg.S1AP.PCSM ###

| Параметр                         | Описание                            | Тип    | По умолчанию | O/M | P/R | Версия |
|----------------------------------|-------------------------------------|--------|--------------|-----|-----|--------|
| ComponentAddr                    | Адрес компонента.                   | string | -            | M   | R   |        |
| ComponentType                    | Тип компонента.                     | string | Sg.S1AP.PCSM | O   | R   |        |
| Params                           | Параметры компонента.               | object | -            | M   | R   |        |
| **{**                            |                                     |        |              |     |     |        |
| <a name="peer-ip">PeerIP</a>     | IP-адрес подключения узла PCSM.     | ip     | -            | M   | R   |        |
| <a name="peer-port">PeerPort</a> | Порт подключения узла PCSM.         | string | -            | M   | R   |        |
| Transport                        | Транспортный протокол.<br>sctp/tcp. | string | sctp         | O   | R   |        |
| **}**                            |                                     |        |              |     |     |        |

**Примечание.** При значениях `PeerIP = ""` и `PeerPort = 0` PCSM ожидает подключения с адреса, указанного в описании PCSM в секции [Sg.S1AP](#описание-параметров-sg-s1ap).

#### Пример

```
{
  ComponentAddr = Sg.S1AP;
  ComponentType = Sg.S1AP;
  Params = { 
    Origin-State-Id = "1";
    PeerTable = {
      {
        PeerIP = 192.168.126.245;
        PCSM = "Sg.S1AP.PCSM.0";
      };
    };
    DefaultPCSM = { "Sg.S1AP.PCSM.3"; };
  };
}
{
  ComponentAddr = Sg.SGsAP.PCSM.0;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "";
    PeerPort = 0; 
    Transport = "sctp"; 
  };
}
```