---
title: "ap.cfg"
description: "Параметры подсистемы сбора аварий"
weight: 20
type: docs
---

В файле настраиваются параметры подсистемы аварийной индикации, Alarm Processor, SNMP-соединений и правил преобразования компонентных адресов в SNMP-адреса.

**Внимание!** Крайне не рекомендуется менять параметры в этом файле.

### Разделы ###

* **[General](#general)** — основные параметры;
* **[Dynamic](#dynamic)** — переменные и их значения, при которых удаляются динамические объекты;
* **[SNMP](#snmp)** — параметры SNMP;
* **[StandardMib](#standardmib)** — параметры объектов из MIB-файла;
* **[AtePath2ObjName](#atepath2objname)** — правила преобразования АТЕ-пути в SNMP-путь;
* **[SNMPTrap](#snmptrap)** — правила отправки траповых переменных;
* **[Filter](#filter)** — параметры фильтрации аварий;
* **[SpecificTrapCA_Object](#st_ca_object)** — параметры базы SpecificTrap для компонентных адресов;
* **[SpecificTrapCT_Object](#st_ct_object)** — параметры базы SpecificTrap для компонентных типов;
* **[SpecificTrapCA_Var](#st_ca_var)** — параметры смещения SpecificTrap;
* **[Logs](#logs)** — параметры ведения лог-файла;
* **[FilterLevel](#filterlevel)** — правила фильтрации аварий по журналам;
* **[SNMPv3](#snmpv3)** — настройки SNMPv3;

### Описание параметров ###

| Параметр                                                 | Описание                                                                                                                      | Тип            | По умолчанию                              | O/M | P/R | Версия |
|:---------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------|:---------------|:------------------------------------------|:----|:----|:-------|
| **<a name="general">\[General\]</a>**                    | Основные параметры.                                                                                                           |                |                                           |     |     |        |
| Root                                                     | Корень дерева.                                                                                                                | string         | PROTEI(1.3.6.1.4.1.20873)                 | O   | P   |        |
| <a name="application-address">ApplicationAddress</a>     | Адрес приложения.                                                                                                             | string         | MME                                       | M   | R   |        |
| <a name="max-connection-count">MaxConnectionCount</a>    | Максимальное количество одновременных подключений.                                                                            | int            | 10                                        | O   | R   |        |
| ManagerThread                                            | Флаг запуска встроенного менеджера в отдельном потоке.                                                                        | bool           | 0                                         | O   | P   |        |
| <a name="cyclic-walk-tree">CyclicWalkTree</a>            | Флаг циклического обхода деревьев.                                                                                            | bool           | 0                                         | O   | R   |        |
| **<a name="dynamic">\[Dynamic\]</a>**                    | Переменные и их значения, при которых динамические объекты следует удалять.                                                   |                |                                           |     |     |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                | string         | -                                         | O   | P   |        |
| value                                                    | Значение переменной.                                                                                                          | string         | -                                         | O   | P   |        |
| **<a name="snmp">\[SNMP\]</a>**                          | Настройки SNMP.                                                                                                               | object         | -                                         | O   | P   |        |
| ListenIP                                                 | IP-адрес, с которым будет устанавливать соединение система обработки сообщений AlarmProcessor.                                | ip             | 0.0.0.0                                   | O   | P   |        |
| ListenPort                                               | Прослушиваемый порт.                                                                                                          | int<br>0-65535 | 161                                       | O   | P   |        |
| OwnEnterprise                                            | SNMP-адрес приложения.                                                                                                        | string         | 1.3.6.1.4.1.20873                         | O   | P   |        |
| <a name="check-community">CheckCommunity</a>             | Флаг проверки сообщества.                                                                                                     | bool           | 0                                         | O   | P   |        |
| **<a name="standardmib">\[StandardMib\]</a>**            | Параметры объектов из стандартного MIB-файла.                                                                                 | object         | -                                         | O   | R   |        |
| addrSNMP                                                 | Адрес SNMP переменной.                                                                                                        | string         | -                                         | O   | R   |        |
| typeVar                                                  | Тип переменной.                                                                                                               | string         | -                                         | O   | R   |        |
| value                                                    | Значение переменной.                                                                                                          | string         | -                                         | O   | R   |        |
| **<a name="atepath2objname">\[AtePath2ObjName\]</a>**    | Правила преобразования АТЕ-пути в SNMP-путь.                                                                                  | object         | -                                         | O   | P   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                     | regex          | -                                         | O   | P   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                | string         | -                                         | O   | P   |        |
| **<a name="snmptrap">\[SNMPTrap\]</a>**                  | Правила посылки трапов.                                                                                                       | object         | -                                         | O   | R   |        |
| ipManagerSNMP                                            | IP-адрес SNMP-менеджера.                                                                                                      | ip             | -                                         | O   | R   |        |
| portManagerSNMP                                          | Порт SNMP-менеджера.                                                                                                          | int<br>0-65535 | -                                         | O   | R   |        |
| caObjectFilter                                           | Фильтр по адресу объекта.                                                                                                     | regex          | -                                         | O   | R   |        |
| ctObjectFilter                                           | Фильтр по типу объекта.                                                                                                       | regex          | -                                         | O   | R   |        |
| caVarFilter                                              | Фильтр по адресу переменной.                                                                                                  | regex          | -                                         | O   | R   |        |
| **<a name="filter">\[Filter\]</a>**                      | Параметры фильтрации аварий.                                                                                                  | object         | -                                         | O   | P   |        |
| caObject                                                 | Фильтр по адресу объекта.                                                                                                     | regex          | `.*`                                      | O   | P   |        |
| ctObject                                                 | Фильтр по типу объекта.                                                                                                       | regex          | `.*`                                      | O   | P   |        |
| caVar                                                    | Фильтр по адресу переменной.                                                                                                  | regex          | `.*`                                      | O   | P   |        |
| TrapIndicator                                            | Фильтр по индикатору трапа.                                                                                                   | string         | 1                                         | O   | P   |        |
| DynamicIndicator                                         | Фильтр по индикатору динамического объекта.                                                                                   | string         | 0                                         | O   | P   |        |
| **<a name="st_ca_object">\[SpecificTrapCA_Object\]</a>** | Параметры базы SpecificTrap для компонентных адресов.                                                                         | object         | -                                         | O   | R   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                | regex          | -                                         | O   | R   |        |
| specificTrapBase                                         | Значение для задания базы SP.                                                                                                 | int            | -                                         | O   | R   |        |
| **<a name="st_ct_object">\[SpecificTrapCT_Object\]</a>** | Параметры базы SpecificTrap для компонентных типов.                                                                           | object         | -                                         | O   | R   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                     | string         | -                                         | O   | R   |        |
| specificTrapBase                                         | Значение для задания базы SP.                                                                                                 | int            | -                                         | O   | R   |        |
| **<a name="st_ca_var">\[SpecificTrapCA_Var\]</a>**       | Параметры смещения SpecificTrap.                                                                                              | object         | -                                         | O   | R   |        |
| caObject                                                 | Компонентный адрес объекта.                                                                                                   | regex          | -                                         | O   | R   |        |
| specificTrapOffset                                       | Смещение для задания SP.                                                                                                      | int            | -                                         | O   | R   |        |
| **<a name="logs">\[Logs\]</a>**                          | Вывод текущего состояния объектов в файл                                                                                      | object         | -                                         | O   | R   |        |
| TreeTimerPeriod                                          | Период сохранения текущего состояния объектов в лог-файлах.                                                                   | int<br>мс      | 60&nbsp;000                               | O   | R   |        |
| **<a name="filterlevel">\[FilterLevel\]</a>**            | Правила фильтрации аварий по журналам.                                                                                        | list\<object\> | -                                         | O   | P   |        |
| caObject                                                 | Компонентный адрес объекта.                                                                                                   | regex          | -                                         | O   | P   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                     | regex          | -                                         | O   | P   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                | regex          | -                                         | O   | P   |        |
| nLevel                                                   | Уровень журнала.                                                                                                              | int            | -                                         | O   | P   |        |
| **<a name="snmpv3">\[SNMPv3\]</a>**                      | Настройки SNMPv3 <br> Корректное формирование EngineID/ContextEngineID <br> (rfc 1910, rfc 3411)                              | object         | -                                         | O   | P   |        |
| <a name="engine-id-conformance">EngineIdConformance</a>  | Флаг поддержки SNMPv3.                                                                                                        | bool           | 1                                         | O   | P   |        |
| <a name="engine-id-enterprise">EngineIdEnterprise</a>    | Идентификатор организации.                                                                                                    | string         | 20873                                     | O   | P   |        |
| [EngineIdFormat](#engine-id-format)                      | Индикатор форматирования 6 октета и далее, является 5 октетом. См. [RFC 3411](https://datatracker.ietf.org/doc/html/rfc3411). | int            | 4                                         | O   | P   |        |
| MaxMessageSize                                           | Максимальный размер обрабатываемого сообщения в октетах.<br>**Примечание.** Используется только для SNMPv3.                   | int            | 484                                       | O   | P   |        |
| EngineBoots                                              | Количество загрузок модуля с момента последнего конфигурирования.<br>**Примечание.** Используется только для SNMPv3.          | int            | 0                                         | O   | P   |        |
| TimeWindow                                               | Интервал обновления                                                                                                           | int            | 0                                         | O   | P   |        |
| Community                                                | Имя сообщества.<br>**Примечание.** Используется для SNMPv1 и SNMPv2, если [CheckCommunity](#check-community) = 1.             | string         | public                                    | O   | P   |        |
| <a name="snmp-engine-id">SNMP_EngineID</a>               | Идентификатор движка SNMP.<br>Правила генерации см. ниже.                                                                     | string         | protei                                    | O   | P   |        |
| ContextEngineID                                          | Идентификатор реализации контекста SNMP.                                                                                      | string         | значение [SNMP_EngineID](#snmp-engine-id) | O   | P   |        |
| ContextName                                              | Имя контекста SNMP.                                                                                                           | string         | protei                                    | O   | P   |        |
| **<a name="agents_v3">Agents_v3</a>**                    | Адреса для отправки трапов по протоколу SNMPv3.                                                                               |                |                                           | O   | P   |        |
| addr                                                     | IP-адрес назначения. Формат:<br>`{ "<addr>";"<port>" }`.                                                                      | ip             | 0.0.0.0                                   | O   | P   |        |
| Port                                                     | Порт назначения.                                                                                                              | int<br>0-65535 | -                                         | O   | P   |        |
| User                                                     | Имя пользователя.                                                                                                             | string         | protei                                    | O   | P   |        |
| Protocol                                                 | Используемый транспортный протокол.<br>`TCP`/`UDP`.                                                                           | string         | TCP                                       | O   | P   |        |
| **<a name="agents_v2c">Agents_v2c</a>**                  | Адреса для отправки трапов по протоколу SNMPv2c.                                                                              | object         | -                                         | O   | P   |        |
| Address                                                  | IP-адрес назначения.                                                                                                          | ip             | 0.0.0.0                                   | O   | P   |        |
| Port                                                     | Порт назначения.                                                                                                              | int<br>0-65535 | -                                         | O   | P   |        |
| User                                                     | Имя пользователя.<br>**Примечание.** Поле не используется, значение может быть любым.                                         | string         |                                           | O   | P   |        |
| Protocol                                                 | Используемый транспортный протокол.<br>`TCP`/`UDP`.                                                                           | string         | TCP                                       | O   | P   |        |
| **<a name="users">Users</a>**                            | Параметры пользователей для протокола SNMPv3.                                                                                 |                |                                           | O   | P   |        |
| Name                                                     | Имя пользователя.<br>**Примечание.** Имя должно совпадать с одним из значений в из [Agents_v3](#agents_v3).                   | string         | protei                                    | O   | P   |        |
| AuthProtocol                                             | Протокол авторизации.<br>**Примечание.** Допускается только значение none, авторизация не используется.                       | string         | none                                      | O   | P   |        |
| PrivProtocol                                             | Протокол шифрования.<br>**Примечание.** Допускается только значение none, шифрование не используется.                         | string         | none                                      | O   | P   |        |

#### <a name="engine-id-format">Значения EngineIdFormat ####

* 0 — reserved, unused; зарезервированы;
* 1 — IPv4 address (4 octets), lowest non-special IP address; IP-адрес формата IPv4, 4 октета, наименьший не особый IP-адрес;
* 2 — IPv6 address (16 octets), lowest non-special IP address; IP-адрес формата IPv6, 16 октетов, наименьший не особый IP-адрес;
* 3 — MAC address (6 octets), lowest IEEE MAC address, canonical order; MAC-адрес, 6 октетов, наименьший IEEE MAC-адрес;
* 4 — Text, administratively assigned, maximum remaining length 27; текст, максимальный размер оставшейся части — 27;
* 5 — Octets, administratively assigned, maximum remaining length 27; октеты, максимальный размер оставшейся части — 27;
* 6-127 — reserved, unused; зарезервированы;
* 128-255 — as defined by the enterprise, maximum remaining length 27; определяется организацией, максимальный размер оставшейся части — 27;

#### <a name="snmp-engine-id">Правила генерации snmpEngineID</a>

Значение идентификатора snmpEngineID зависит от значения [EngineIdConformance](#engine-id-conformance).

* при `EngineIdConformance = 0`: 

`SNMP_EngineID = 80 00 + 51 89 + <EngineID>`

80 00 — значение `EngineIdConformance = 1`;
51 89 — значение `EngineIdEnterprise` (20873 в формате hex);

**Примечание.** Длина SNMP_EngineID корректируется строго до 12 байт.

* при `EngineIdConformance = 1`:

`SNMP_EngineID = 00 00 + 51 89 + 04 + <EngineID>`

(EngineIdEnterprise=20873)
 (EngineIdEnterprise=20873)
(EngineIdFormat)

### Форматы ###

* формат [\[Dynamic\]](#dynamic):

`{ <caVar>;<value>; };`

* формат [\[StandardMib\]](#standardmib):

`{ <addrSNMP>;<typeVar>;<value>; };`

* формат [\[AtePath2ObjName\]](#atepath2objname):

`{ <ctObject>;<caVar>; }`;

Для каждого типа необходимо прописать переменную CA(1) -- адрес объекта, иначе объекты в SNMP-дерево добавляться не будут.

* формат [\[SNMPTrap\]](#snmptrap):

`{ <ipManagerSNMP>;<portManagerSNMP>;<caObjectFilter>;<ctObjectFilter>;<caVarFilter>; };`

* формат [\[SpecificTrapCA_Object\]](#st_ca_object):

`{ <caVar>;<specificTrapOffset>; }`

* формат [\[SpecificTrapCT_Object\]](#st_ct_object):

`{ <ctObject>;<specificTrapBase>; }`

* формат [\[SpecificTrapCA_Var\]](#st_ca_var):

`{ <caObject>;<specificTrapOffset>; }`

* формат [\[FilterLevel\]](#filterlevel):

`{ <caObject>;<ctObject>;<caVar>;<nLevel> }`

* формат [Agents_v3](#agents_v3):

`{ <address>;<port>;<user>;<protocol> };`
 
* формат [Agents_v2c](#agents_v2c):

`{ <address>;<port>;<user>;<protocol> };`

### Правила вычисления идентификатора трапа (SpecificTrap) ###

1. Значение, заданное для `caObject` или `ctObject`, является базой (`base`) для формирования идентификатора.<br>
Если указан `caObject`, `ctObject` игнорируется. Если подходящий `caObject` не найден, за основу берется `ctObject`.<br>
Если не найден ни `caObject`, ни `ctObject`, то `base = 0`.
2. Полученное значение умножается на 1000.
3. Значение, заданное для `caVar` и являющееся смещением (`offset`), прибавляется к базе.

Идентификаторы для `caObject` и `caObject > 0`.

Идентификаторы для `caVar > 100`.

Идентификаторы для `caVar` в диапазоне 1-100 зарезервированы.

| Переменная | Значение  | SP_Offset |
|:-----------|:----------|:----------|
| OSTATE     | ACTIVATE  | 1         |
| OSTATE     | FAIL      | 2         |
| ASTATE     | UNBLOCKED | 3         |
| ASTATE     | BLOCKED   | 4         |
| HSTATE     | ON        | 5         |
| HSTATE     | OFF       | 6         |

### Примеры ###

```
[Dynamic]
{ "OSTATE";"0"; };

[StandardMib]
#sysDescr
{ 1.3.6.1.2.1.1.1.0;STRING;"Protei_CPE"; };
#sysObjectID
{ 1.3.6.1.2.1.1.2.0;OBJECT_ID;1.3.6.1.4.1.501; };
#get-request SNMPv3 for EngineID
{ "1.3.6.1.6.3.15.1.1.4.0";"COUNTER";"2335"; };

[AtePath2ObjName]
{ Ph(2).Trunk(1,1);CA(1); }; 
{ Ph(2).Trunk(1,1);ASTATE(2); };
{ Ph(2).Trunk(1,1);OSTATE(3); };
{ Ph(2).Trunk(1,1);Alrm(4).LOS(1).EPH(1); };
{ Ph(2).Trunk(1,1);Alrm(4).LOS(1).LASTDT(2); };
```

После АТЕ-названия в скобках через запятую указывается последовательность цифр, которой соответствует это название в SNMP-пути.

```
[SNMPTrap]
{ "192.168.100.151";162;"Ph.*"; };
{ "192.168.100.154";162;"Sg.TPI.CDIR"; };

[SpecificTrapCA_Object]
{ Sg.SMPP.Dir.1; 1; }
{ Sg.SMPP.Dir.*; 2; }

[SpecificTrapCT_Object]
{ Sg.SMPP.Dir; 1; }
{ SMSC.GENERAL; 3; }

[SpecificTrapCA_Var]
{ STAT.*; 101; }
{ RESULT.*; 102; }

[FilterLevel]
{ Ph.Card.1; Ph.Card; OSTATE; 4 };
{ .*; .*; OSTATE; 5 };
```

Для переменной `OSTATE` в объекте `Ph.Card.1` типа `Ph.Card` задать уровень 4. Для переменной `OSTATE` во всех объектах задать уровень 5.

```
[SNMPv3]
EngineIdConformance = 1;
EngineIdEnterprise = 20873;
EngineIdFormat = 4;
MaxMessageSize = 484;
EngineBoots = 21;
SNMP_EngineID = "protei";
ContextEngineID = "protei";
ContextName = "protei";
Community = "bookmark";

Agents_v3 = {
  { "0.0.0.0";3162;"protei";"UDP" };
}

Users = {
  {
    Name = "protei";
    AuthProtocol = "none";
    PrivProtocol = "none";
  }
}
```

[//]: # (SNMP_EngineID = "protei"; #protei default # 515170726f7465693131 - что за магическое число?)

### Перегрузка параметров ###

Параметры конфигурации делятся на две группы:

1. Параметры AP_Agent. Формат перегрузки:

```bash
./reload ap_agent.di
./reload ap_agent.di "config/alarm/ap.cfg"
```

Если параметры не указаны, путь к ap.cfg по умолчанию: `config/ap.cfg`.

2. Параметры AP_Manager. Формат перегрузки:

```bash
./reload ap_manager.di
./reload ap_manager.di "config/alarm/ap.cfg"
```

Перегрузить можно только приведенный ниже список параметров/секций:

1. AP_Agent:

* [\[General\]](#general):
  * [ApplicationAddress](#application-address);
  * [MaxConnectionCount](#max-connection-count);
* [\[Logs\]](#logs);

2. AP_Manager:

* [\[General\]](#general):
  * CyclicWalkTree = 0<br>
* \[StandardMib\]<br>
* \[SNMPTrap\]<br>
* \[SpecificTrapCA_Object\]<br>
* \[SpecificTrapCT_Object\]<br>
* \[SpecificTrapCA_Var\]<br>
