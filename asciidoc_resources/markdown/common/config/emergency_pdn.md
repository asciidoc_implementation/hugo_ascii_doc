---
title: "emergency_pdn.cfg"
description: "Параметры PDN-подключений для экстренных служб"
weight: 20
type: docs
---

В файле задаются настройки PDN-подключений для служб передачи данных, применяемые в случае процедуры Emergency Attach без запроса профиля у узла HSS. 
Каждому PDN-соединению соответствует одна секция. Имя секции может использоваться в качестве ссылки 
в параметре [served_plmn.cfg :: EmergencyPDN](../served_plmn/#emergency-pdn/).

Ключ для перегрузки — **reload served_plmn.cfg**.

### Описание параметров ###

| Параметр      | Описание                                                                                                                                                | Тип                    | По умолчанию | O/M | P/R | Версия |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|--------------|-----|-----|--------|
| APN_NI        | Индикатор сети APN.                                                                                                                                     | string                 | -            | M   | R   |        |
| ServedPartyIP | Обслуживаемый статический IP-адрес.<br>**Примечание.** Может быть указан один IPv4, один IPv6 или оба одновременно.                                     | ip/string<br>ip,string | -            | O   | R   |        |
| AMBR          | Показатели скоростей передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для DL- и UL-направлений. Формат:<br>`<dl>,<ul>`.                    | list\<int\>            | 0, 0         | O   | R   |        |
| PDN_Type      | Тип подключения к сети передачи данных.<br>`IPv4`/`IPv6`/`IPv4v6`/`IPv4_or_IPv6`.                                                                       | string                 | IPv4         | O   | R   |        |
| QCI           | Идентификатор класса QoS.                                                                                                                               | int                    | 0            | O   | R   |        |
| PriorityLvl   | Уровень приоритета.                                                                                                                                     | int                    | 0            | O   | R   |        |
| PreemptCap    | Флаг `Pre-emption Capability`.<br>См. [3GPP TS 23.107](https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf).    | bool                   | 0            | O   | R   |        |
| PreemptVuln   | Флаг `Pre-emption Vulnerability`.<br>См. [3GPP TS 23.107](https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf). | bool                   | 0            | O   | R   |        |

#### Пример ####

```ini
[Rule_for_IMS]
APN_NI = ims;
ServedPartyIP = 127.0.0.1,::1;
AMBR = 100500,100500;
PDN_Type = IPv4_or_IPv6;
PriorityLvl = 1;

[internet_rule]
APN_NI = internet;
PDN_Type = IPv4v6;
PreemptCap = 1;
PreemptVuln = 1;
```