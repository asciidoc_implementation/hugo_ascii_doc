---
title: "trace.cfg"
description: "Параметры ведения xDR и log"
weight: 20
type: docs
---

В файле задаются настройки подсистемы журналирования.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload trace.cfg**.

### Описание параметров ###

| Параметр                             | Описание                                                                                                                                                                                                                                                                                                                                                                  | Тип         | По умолчанию                     | O/M | P/R | Версия |
|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|----------------------------------|-----|-----|--------|
| **\[Trace\]**                        |                                                                                                                                                                                                                                                                                                                                                                           |             |                                  |     |     |        |
| <a name="common">common</a>          | Общие настройки системы журналирования.                                                                                                                                                                                                                                                                                                                                   | object      | -                                | O   | R   |        |
| tracing                              | Флаг активности системы журналирования.                                                                                                                                                                                                                                                                                                                                   | bool        | 1                                | O   | R   |        |
| dir                                  | Путь к директории, где находятся журналы.<br>**Примечание.** Путь может содержать ".." и маску формата времени.                                                                                                                                                                                                                                                           | string      | от каталога по умолчанию         | O   | R   |        |
| no_signal                            | Коды сигналов, не перехватываемых системой журналирования.<br>Значение all — не перехватывать никакие сигналы.<br>**Примечание.** Разделитель — запятая.                                                                                                                                                                                                                  | list\<int\> | перехватывать все сигналы        | O   | R   |        |
| <a name="separator">separator</a>    | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.                                                                                                                                                                                                                                                 | string      | " "                              | O   | R   |        |
| logs                                 | Конфигурация журналов.                                                                                                                                                                                                                                                                                                                                                    | object      | -                                | O   | R   |        |
| log_name                             | Наименование журнала.                                                                                                                                                                                                                                                                                                                                                     | string      | -                                | O   | R   |        |
| [mask](#модификаторы-mask)           | Маска формата вывода автоматических полей в журнале.                                                                                                                                                                                                                                                                                                                      | string      | -                                | O   | R   |        |
| file                                 | Путь к файлу лога.<br>**Примечание.** При указании не существующих директорий система создает все необходимые каталоги. Допускается задание пустого имени файла, если `level = 0`. В этом случае запись производится согласно параметру [tee](#tee). При отсутствии этого параметра запись на диск не производится.<br>Путь может содержать ".." и маску формата времени. | string      | от каталога по умолчанию         | O   | R   |        |
| level                                | Уровень журнала.<br>**Примечание.** Сообщения с большим уровнем игнорируются.                                                                                                                                                                                                                                                                                             | int         | -                                | O   | R   |        |
| [period](#модификаторы-period)       | Период обновления файла лога. Формат:<br>`<interval>+<shift>`<br>interval — интервал между соседними обновлениями;<br>shift — первоначальный сдвиг.<br>**Примечание.** Сдвиг не может превышать длину периода, и в случае некорректного значения игнорируется.                                                                                                            | string      | -                                | O   | R   |        |
| \<interval\>                         | Период между соседними обновлениями.                                                                                                                                                                                                                                                                                                                                      | string      | -                                | O   | R   |        |
| \<shift\>                            | Первоначальный сдвиг.                                                                                                                                                                                                                                                                                                                                                     | units       | -                                | O   | R   |        |
| [buffering](#модификаторы-buffering) | Настройки буферизированной записи.                                                                                                                                                                                                                                                                                                                                        | object      | -                                | O   | R   |        |
| separator                            | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.                                                                                                                                                                                                                                                 | string      | значение [separator](#separator) | O   | R   |        |
| [type](#модификаторы-type)           | Тип журнала и дополнительные настройки.                                                                                                                                                                                                                                                                                                                                   | string      |                                  | O   | R   |        |
| <a name="tee">tee</a>                | Дублирование потока вывода.<br>stdout/cout/info/\<log_file_name\>.<br>**Примечание.** При знаке минуса "–" не пишется имя исходного лога при дублировании.                                                                                                                                                                                                                | string      | -                                | O   | R   |        |
| limit                                | Максимальное количество строк в файле.<br>**Примечание.** По достижении предела строк файл автоматически открывается заново. Действительное количество строк в файле не исследуется. Если имя файла зависит от времени, то открывается новый файл, иначе файл очищается.                                                                                                  | int         | -                                | O   | R   |        |
| force_recreate                       | Флаг создания пустых журналы по прошествии периода [period](#period).                                                                                                                                                                                                                                                                                                     | bool        | 0                                | O   | R   |        |

#### Модификаторы mask ####

Маска формата вывода автоматических полей в журнале. Возможные модификаторы: `date & time & tick & state & pid & tid & level & file`.

| Параметр                | Описание                                                                                               | Тип        | По умолчанию |
|-------------------------|--------------------------------------------------------------------------------------------------------|------------|--------------|
| date                    | Дата создания. Формат: `DD/MM/YY`                                                                      | date       | -            |
| <a name="time">time</a> | Время создания. Формат: `hh:mm:ss`                                                                     | time       | -            |
| tick                    | Миллисекунды. Формат:<br>если задано [time](#time): `.mss`;<br>если не задано [time](#time): `.mssmss` | string     | -            |
| state                   | Состояние системы.                                                                                     | int/string | -            |
| pid                     | Идентификатор процесса. Формат: `xxxxxx`                                                               | int        | -            |
| tid                     | Идентификатор потока. Формат: `xxxxxx`                                                                 | int        | -            |
| level                   | Уровень журнала для записи.                                                                            | int        | -            | 
| file                    | Файл и строка в файле с исходным кодом, откуда производится вывод.                                     | string     | -            |

#### Модификаторы period ####

| Параметр | Описание                                                                                                               | Тип    | По умолчанию |
|----------|------------------------------------------------------------------------------------------------------------------------|--------|--------------|
| count    | Количество стандартных периодов.                                                                                       | int    | -            |
| type     | Единицы измерения периода.<br>sec - секунда/min - минута/hour - час/day - день/week - неделя/month - месяц/year - год. | string | -            |

**Пример:** `day+3hour` - файл обновляется каждый день в 3 часа ночи.

#### Модификаторы type ####

Три пары взаимоисключающих значений: log/cdr, truncate/append, name_now/name_period.

| Параметр                        | Описание                                                                                            | Тип      | По умолчанию |
|---------------------------------|-----------------------------------------------------------------------------------------------------|----------|--------------|
| <a name="name-now">name_now</a> | Текущее время для имени файла.                                                                      | datetime | -            |
| name_period                     | Начало периода записи.                                                                              | datetime | -            |
| <a name="truncate">truncate</a> | Флаг очистки файла при открытии.                                                                    | bool     | -            |
| <a name="append">append</a>     | Файл добавления информации в конец файла.                                                           | bool     | -            |
| <a name="log">log</a>           | Состоит из [truncate](#truncate) и [name_now](#name-now), при падении пишется информация о сигнале. | string   | -            |
| <a name="cdr">cdr</a>           | Состоит из [append](#append) и [name_now](#name-now), при падении не пишется информация о сигнале   | string   | -            |

#### Модификаторы buffering ####

| Параметр                                | Описание                                                                                          | Тип       | По умолчанию |
|-----------------------------------------|---------------------------------------------------------------------------------------------------|-----------|--------------|
| <a name="cluster-size">cluster_size</a> | Размер кластера.                                                                                  | int<br>Кб | 128          |
| clusters_in_buffer                      | Количество кластеров [cluster_size](#cluster-size) в буфере.                                      | int       | 0            |
| overflow_action                         | Действие, выполняемое при переполнении буфера.<br>`erase` — удаление;<br>`dump` — запись на диск. | string    | dump         |

#### Зарезервированные имена журналов ####

* **stdout** — стандартный вывод;
* **stderr** — стандартный вывод ошибок;
* **trace** — журнал по умолчанию;
* **warning** — журнал предупреждений;
* **error** — журнал ошибок;
* **config** — журнал чтения конфигурации;
* **info** — журнал информации о событиях, адаптирован для стороннего пользователя.

#### Пример ####

```
[Trace]
common = {
  tracing = 1;
  dir = ".";
  no_signal = all;
}

logs = {
  connect_cdr = {
    file = "cdr/s1ap/connect-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 2;
    tee = s1ap_cdr;
    separator = ",";
  };

  dedicated_bearer_cdr = {
    file = "cdr/s1ap/dedicated_bearer-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  diam_cdr = {
    file = "cdr/diam/diam-%Y%m%d-%H%M.cdr";
    mask = date&time;
    level = 2;
    separator = ",";
  };

  enodeb_cdr = {
    file = "cdr/s1ap/enodeB-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    separator = ",";
  };

  gtp_c_cdr = {
    file = "cdr/gtp_c/gtp_c-%Y%m%d-%H%M.cdr";
    mask = date&time;
    period = 1day;
    level = 2;
    separator = ",";
  };

  http_cdr = {
    file = "cdr/http/http-%Y%m%d-%H%M.cdr";
    mask = date&time;
    period = 1day;
    level = 1;
    separator = ",";
  };

  irat_handover_cdr = {
    file = "cdr/s1ap/irat_handover-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  lte_handover_cdr = {
    file = "cdr/s1ap/lte_handover-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  paging_cdr = {
    file = "cdr/s1ap/paging_cdr-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  reject_cdr = {
    file = "cdr/reject/reject-%Y%m%d-%H%M.cdr";
    mask = date&time;
    period = 1day;
    level = 2;
    separator = ",";
  };

  s1ap_cdr = {
    file = "cdr/s1ap/s1ap-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 2;
    separator = ",";
  };

  s1ap_context_cdr = {
    file = "cdr/s1ap/s1ap_context-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  s1ap_overload_cdr = {
    file = "cdr/s1ap_overload/s1ap_overload-%Y%m%d-%H%M.cdr";
    mask = date&time;
    period = 1hour;
    level = 2;
    separator = ",";
  };

  sgsap_cdr = {
    file = "cdr/sgsap/sgsap-%Y%m%d-%H%M.cdr";
    mask = date&time;
    period = 1day;
    level = 2;
    separator = ",";
  };

  tau_cdr = {
    file = "cdr/s1ap/tau-%Y%m%d-%H%M.cdr";
    mask = date & time & tick;
    period = 1day;
    level = 1;
    tee = s1ap_cdr;
    separator = ",";
  };

  alarm_cdr = {
    file = "logs/alarm/alarm-cdr.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 6;
  };

  alarm_trace = {
    file = "logs/alarm/alarm_trace.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 6;
  };

  bc_trace = {
    file = "logs/bc_trace.log";
    mask = file & date & time & tick;
    level = 10;
    separator = ";";
  };

  bc_warning = {
    file = "logs/bc_warning.log";
    mask = file & date & time & tick;
    level = 10;
    separator = ";";
  };

  COM_warning = {
    file = "logs/com_warning.log";
    mask = file & date & time & tick;
    level = 10;
    separator = ";";
  };

  config = {
    file = "logs/config.log";
    mask = file & date & time & tick;
    level = 10;
  };
  
  db_trace = {
    file = "logs/db_trace.log";
    mask = file & date & time & tick;
    period = 1hour;
    level = 10;
  };
  
  diam_info = {
    file = "logs/diam_info.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };
  
  diam_trace = {
    file = "logs/diam/diam_trace-%Y%m%d-%H%M.log";
    mask = date & time & tick & pid & file;
    period = hour;
    level = 10;
  };
  
  diam_warning = {
    file = "logs/diam_warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };
  
  dns_trace = {
    file = "logs/dns_trace.log";
    mask = file & date & time & tick;
    level = 10;
    period = day;
  };

  dns_warning = {
    file = "logs/dns_warning.log";
    mask = file & date & time & tick;
    level = 10;
    period = day;
  };

  GTP_C_trace = {
    file = "logs/gtp_c/gtp_c_trace-%Y%m%d-%H%M.log";
    mask = date & time & tick & pid & file;
    period = hour;
    level = 11;
  };

  GTP_C_warning = {
    file = "logs/gtp_c_warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };

  http_trace = {
    file = "logs/http_trace.log";
    mask = file & date & time & tick;
    level = 10;
    separator = ";";
  };

  mme_config = {
    file = "logs/mme_config.log";
    mask = file & date & time & tick;
    level = 10;
  };

  profilers = {
    file = "logs/profilers/profilers_%Y%m%d-%H%M.log";
    mask = file & date & time & tick;
    level = 10;
    period = hour;
    separator = ";";
  };

  S1AP_trace = {
    file = "logs/s1ap/s1ap_trace-%Y%m%d-%H%M.log";
    mask = date & time & tick & pid & file;
    period = hour;
    level = 10;
  };

  S1AP_warning = {
    file = "logs/s1ap_warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };

  sctp_binary = {
    file = "logs/sctp_binary.log";
    mask = file & date & time & tick;
    level = 0;
    separator = ";";
  };

  Sg_info = {
    file = "logs/sg/sg_info.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };

  Sg_trace = {
    file = "logs/sg/sg_trace.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };
  
  Sg_warning = {
    file = "logs/sg/sg_warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };
  
  SGsAP_trace = {
    file = "logs/sgsap/sgsap_trace-%Y%m%d-%H%M.log";
    mask = date & time & tick & pid & file;
    period = hour;
    level = 10;
  };
  
  SGsAP_warning = {
    file = "logs/sgsap_warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
  };
  
  si = {
    file = "logs/si/trace.log";
    mask = date & time & tick & pid & file;
    level = 10;
    tee = trace | fsm;
  };
  
  si_info = {
    file = "logs/si/info.log";
    mask = date & time & tick & pid & file;
    level = 10;
    tee = trace;
  };
  
  si_warning = {
    file = "logs/si/warning.log";
    mask = date & time & tick & pid & file;
    level = 10;
    tee = trace;
  };
  
  trace = {
    file = "logs/trace/trace_%Y%m%d-%H%M.log";
    mask = file & date & time & tick;
    level = 11;
    period = hour;
    separator = ";";
  };

  ue_trace = {
    file = "logs/trace/ue_trace_%Y%m%d-%H%M.log";
    mask = file & date & time & tick;
    level = 10;
    period = hour;
    separator = ";";
  };
}
```
