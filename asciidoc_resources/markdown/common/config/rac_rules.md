---
title: "rac_rules.cfg"
description: "Параметры RAC и LAC"
weight: 20
type: docs
---

В файле задаются правила, связывающие <abbr title="Routing Area Code">RAC</abbr> и <abbr title="Location Area Code">LAC</abbr> с адресами 
узлов <abbr title="Serving GPRS Support Node">SGSN</abbr> и сервером MSC. Каждому правилу соответствует одна секция. Имя секции является именем 
правила и может использоваться в качестве ссылки в параметре [served_plmn.cfg::RAC_rules](../served_plmn/#rac-rules/).

Ключ для перегрузки — **reload served_plmn.cfg**.

### Описание параметров ###

| Параметр      | Описание                                                                                                                     | Тип     | По умолчанию | O/M | P/R | Версия   |
|---------------|------------------------------------------------------------------------------------------------------------------------------|---------|--------------|-----|-----|----------|
| RAC           | Код области маршрутизации для данного правила.                                                                               | int/hex | -            | M   | R   |          |
| LAC           | Код локальной области для данного правила.                                                                                   | int/hex | -            | M   | R   |          |
| SGSN_IP       | IP-адрес узла SGSN.<br>**Примечание.** Для использования GTPv1 вместо GTPv2 необходимо к адресу добавить `(v1)`: `<ip>(v1)`. | ip      | -            | O   | R   | 1.41.0.0 |
| NRI           | Идентификатор узла SGSN, <abbr title="Network Resource Identifier">`NRI`</abbr>.                                             | int     | -            | O   | R   | 1.44.1.0 |
| MSC_Server_IP | IP-адрес сервера MSC.                                                                                                        | ip      | -            | O   | R   |          |

#### Пример ####

```ini
[Rule1]
RAC = 5;
LAC = 0x5;
SGSN_IP = "192.168.126.67";
MSC_Server_IP = "192.168.126.66";

[Rule2]
RAC = 0xab;
LAC = 7;
MSC_Server_IP = "192.168.126.66";

[Rule3]
RAC = 10;
LAC = 100;
SGSN_IP = "192.168.126.64(v1)";
```