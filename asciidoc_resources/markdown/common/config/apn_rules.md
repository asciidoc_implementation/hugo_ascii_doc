---
title: "apn_rules.cfg"
description: "Параметры APN NI и адресов PGW, SGW"
weight: 20
type: docs
---

В файле задаются правила, связывающие APN NI и адреса PGW и SGW. Каждому правилу соответствует одна секция. 
Имя секции является именем правила и может использоваться в качестве ссылки в параметре [served_plmn.cfg::APN_rules](../served_plmn/#apn-rules).

Ключ для перегрузки — **reload served_plmn.cfg**.

### Описание параметров ###

| Параметр                 | Описание                                                                                                                                                                                   | Тип                  | По умолчанию | O/M | P/R | Версия |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------|--------------|-----|-----|--------|
| APN_NI                   | Регулярное выражение APN NI для правила.                                                                                                                                                   | string               | -            | M   | R   |        |
| <a name="pgw">PGW_IP</a> | Набор IP-адресов, <abbr title="Fully Qualified Domain Name">FQDN</abbr> и весов узла PGW. Формат:<br>`"(<fqdn>)-<ip>-<weight>,(<fqdn>)-<ip>-<weight>"`.                                    | string               | -            | O   | R   |        |
| <a name="sgw">SGW_IP</a> | Набор IP-адресов, <abbr title="Fully Qualified Domain Name">FQDN</abbr>, весов и приоритетов узла SGW. Формат:<br>`"(<fqdn>)-<ip>-<weight>-<priority>,(<fqdn>)-<ip>-<weight>-<priority>"`. | string               | -            | O   | R   |        |
| \<fqdn\>                 | Полностью определенное доменное имя.                                                                                                                                                       | string               | -            | O   | R   |        |
| \<ip\>                   | IP-адрес.                                                                                                                                                                                  | ip                   | -            | M   | R   |        |
| \<weight\>               | Вес адреса.<br>**Примечание.** Если 0 или отсутствует, то адрес игнорируется.<br>Если указан единственный адрес без указания веса, то присваивается значение&nbsp;1.                       | int<br>0-65&nbsp;535 | -            | O   | R   |        |
| \<priority\>             | Приоритет адреса.<br>Чем меньше значение, тем выше приоритет.                                                                                                                              | int<br>0-65&nbsp;535 | 0            | O   | R   |        |

**Примечание.** Адреса SGW и PGW ([SGW_IP](#sgw)/[PGW_IP](#pgw)) определяются в нескольких конфигурационных файлах.

Приоритеты значений в порядке убывания:

- apn_rules.cfg;
- [dns.cfg](../dns);
- [served_plmn.cfg](../served_plmn);
- [gtp_c.cfg](../gtp_c).

#### Пример ####

```
[FullRule]
APN_NI = ims;
SGW_IP = 192.168.0.19;
PGW_IP = "(pgw1.s5s8.site.node.epc.mnc001.mcc001.3gppnetwork.org)-192.168.126.244-1-1";
SGW_IP = "(sgw1.s11.site.node.epc.mnc001.mcc001.3gppnetwork.org)-192.168.125.95-1-1";
PGW_IP = 192.168.126.244;

[Rule_1]
APN_NI = internet;
SGW_IP = 192.168.126.241;

[Rule_2]
APN_NI = internet;
SGW_IP = 192.168.0.19;
PGW_IP = 192.168.126.244;
```