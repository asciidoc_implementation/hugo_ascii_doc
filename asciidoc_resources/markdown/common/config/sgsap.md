---
title: "sgsap.cfg"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
--- 

В файле задаются настройки компонента SGsAP.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload sgsap.cfg**.

### Используемые секции ###

* [\[LocalAddress\]](#local-address) - параметры локального хоста;
* [\[LocalInterfaces\]](#local-interfaces) - параметры адресов для multihoming;
* [\[Timers\]](#timers) - параметры таймеров;
* [\[RetryCounters\]](#retry-counters) - параметры счетчиков повторных попыток;
* [\[LAC_GT\]](#lac-gt) - параметры соответствий LAC и GT;
* [\[MSC_Pool\]](#msc-pool) - параметры соответствий LAC и наборов GT с параметрами.

### Описание параметров ###

| Параметр                                               | Описание                                                                                                                                                                                                   | Тип                                               | По умолчанию | O/M | P/R | Версия |
|--------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------|--------------|-----|-----|--------|
| **<a name="local-address">\[LocalAddress\]</a>**       | Параметры локального хоста.                                                                                                                                                                                | object                                            | -            | M   | P   |        |
| <a name="local-host">LocalHost</a>                     | IP-адрес локального хоста.                                                                                                                                                                                 | string                                            | -            | M   | P   |        |
| <a name="local-port">LocalPort</a>                     | Прослушиваемый порт.                                                                                                                                                                                       | int                                               | 29118        | O   | P   |        |
| **<a name="local-interfaces">\[LocalInterfaces\]</a>** | Параметры адресов для multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>" }`                                                                                                                           | list\<ip/ip:port\>                                | -            | O   | P   |        |
| **<a name="timers">\[Timers\]</a>**                    | Параметры таймеров.                                                                                                                                                                                        | object                                            | -            | O   | P   |        |
| Reconnect_Timeout                                      | Время ожидания попытки переподключения после разрыва соединения.                                                                                                                                           | int<br>мс                                         | 30&nbsp;000  | O   | P   |        |
| Response_Timeout                                       | Время ожидания ответного сообщения.                                                                                                                                                                        | int<br>мс                                         | 30&nbsp;000  | O   | R   |        |
| Ts8                                                    | Таймер Ts8.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).                                                                          | int<br>мс                                         | 4&nbsp;000   | O   | R   |        |
| Ts9                                                    | Таймер Ts9.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).                                                                          | int<br>мс                                         | 4&nbsp;000   | O   | R   |        |
| **<a name="retry-counters">\[RetryCounters\]</a>**     | Параметры счетчиков повторных попыток.                                                                                                                                                                     | object                                            | -            | O   | R   |        |
| Ns8                                                    | Таймер Ns8.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).                                                                          | int                                               | 2            | O   | R   |        |
| Ns9                                                    | Таймер Ns9.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).                                                                          | int                                               | 2            | O   | R   |        |
| **<a name="lac-gt">\[LAC_GT\]</a>**                    | Параметры соответствий LAC и GT. Формат:<br>`[<plmn>-]<lac> = <gt>`<br>Для LAC может быть указан PLMN.<br>**Примечание.** Первым задается GT по умолчанию для не упомянутых LAC в формате:<br>`DefaultGT`. | list\<mapping\<\<int\>,\<int,string\>\>\>         | -            | O   | R   |        |
| \<plmn\>                                               | Код сети PLMN.                                                                                                                                                                                             | int                                               | -            | O   | R   |        |
| \<lac\>                                                | Код локальной области LAC.                                                                                                                                                                                 | int                                               | -            | M   | R   |        |
| \<gt\>                                                 | Глобальный заголовок.                                                                                                                                                                                      | string                                            | -            | O   | R   |        |
| **<a name="msc-pool">\[MSC_Pool\]</a>**                | Параметры соответствий LAC и наборов GT с весами и приоритетами. Формат:<br>`[<plmn>-]<lac> = <gt>-<weight>-<priority>,<gt>-<weight>-<priority>`<br>Для LAC может быть указан PLMN.                        | list\<mapping\<\<int,int\>,\<string,int,int\>\>\> | -            | O   | R   |        |
| \<plmn\>                                               | Код сети PLMN.                                                                                                                                                                                             | int                                               | -            | O   | R   |        |
| \<lac\>                                                | Код локальной области LAC.                                                                                                                                                                                 | int                                               | -            | M   | R   |        |
| \<gt\>                                                 | Глобальный заголовок.                                                                                                                                                                                      | string                                            | -            | O   | R   |        |
| \<weight\>                                             | Вес глобального заголовка.                                                                                                                                                                                 | int                                               | -            | M   | R   |        |
| \<priority\>                                           | Приоритет глобального заголовка.                                                                                                                                                                           | int                                               | -            | M   | R   |        |

#### Пример ####

```
[LocalAddress]
LocalHost = 192.168.100.1;
LocalPort = 29118;

[LocalInterfaces]
{ "192.168.100.10:29118"; "192.168.101.10:29118"; "192.168.102.10:29118" }

[Timers]
Reconnect_Timeout = 30000;
Response_Timeout = 30000;
Ts8 = 4000;
Ts9 = 4000;

[RetryCounters]
Ns8 = 2;
Ns9 = 2;

[LAC_GT]
79211230000
5 = 79231234567
25048-7 = 79111234567

[MSC_Pool]
5 = 79231234567-1-1,79237654321-10-1
25048-7 = 79111234567-1-1,79117654321-10-1
```
