---
title: "code_mapping_rules.cfg"
description: "Параметры Diameter: Result Code и EMM Cause"
weight: 20
type: docs
---

В файле задаются правила, связывающие Diameter: Result-Code и EMM Cause. Каждому правилу соответствует одна секция. 
Имя секции является именем правила и может использоваться в качестве ссылки в параметре [served_plmn.cfg 
:: CodeMapping_rules](../served_plmn/#code-mapping-rules/).

Ключ для перегрузки — **reload served_plmn.cfg**.

### Описание параметров ###

| Параметр   | Описание                                                                                                                                                               | Тип         | По умолчанию | O/M | P/R | Версия   |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|--------------|-----|-----|----------|
| ResultCode | Значение Diameter: Result-Code. См. [RFC 6733](https://tools.ietf.org/html/rfc6733). Формат:<br>`"<code>,<code>"`.                                                     | list\<int\> | -            | M   | R   | 1.37.2.0 |
| EMM_Cause  | Причина завершения вызова в LTE-сетях, `EMM Cause`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | int         | -            | M   | R   | 1.37.2.0 |

#### Пример ####

```ini
[Rule1]
ResultCode = 5421;
EMM_Cause = 123;

[Rule2]
ResultCode = "1,2,3";
EMM_Cause = 1;
```