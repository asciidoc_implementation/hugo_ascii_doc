---
title: "mme.cfg"
description: "Основные параметры MME"
weight: 20
type: docs
---

В файле задаются основные настройки PROTEI MME.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload mme.cfg**.

### Используемые секции ###

* **[\[General\]](#general)** - общие параметры узла;  
* **[\[Timeout\]](#timeout)** - параметры времени ожидания;  
* **[\[HO_Timeout\]](#ho_timeout)** - параметры времени ожидания при хэндовере;  
* **[\[Database\]](#database)** - параметры баз данных;  
* **[\[GUTI_Reallocation\]](#guti)** - параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>;  
* **[\[DCN\]](#dcn)** - параметры <abbr title="Dedicated Core Network">DCN</abbr>;
* **[\[Ranges\]](#ranges**) - параметры допустимых диапазонов идентификаторов;  
* **[\[VendorSpecific\]](#vendor_specific)** - параметры, задающие ограничения, вызванные оборудованием других производителей;  
* **[\[ENB_Id\]](#enodeb)** - параметры допустимых eNodeB.

### Описание параметров ###

| Параметр                                             | Описание                                                                                                                                                                                           | Тип                           | По умолчанию | O/M | P/R | Версия   |
|------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|--------------|-----|-----|----------|
| **<a name="general">\[General\]</a>**                |                                                                                                                                                                                                    |                               |              |     |     |          |
| Handlers                                             | Количество задействованных логик.                                                                                                                                                                  | int                           | 10           | O   | P   |          |
| CoreCount                                            | Количество ядер.                                                                                                                                                                                   | int<br>1-7                    | 2            | O   | P   |          |
| EchoSendPeriod                                       | Периодичность отправки сообщения ECHO-Request по интерфейсу GTP-C.                                                                                                                                 | int, с<br>10-600              | 30           | O   | R   |          |
| ISR                                                  | Флаг `Operation Indication` в сообщении Delete Session Request.                                                                                                                                    | bool                          | 0            | O   | R   |          |
| DNS_IPv6                                             | Флаг запроса IPv6-адреса от DNS-сервера.                                                                                                                                                           | bool                          | 0            | O   | R   |          |
| ReleaseOnICS_Fail                                    | Индикатор способа очистки контекста после получения сообщения ICS Failure.<br>`E` — explicit, явно;/`I` — implicit, неявно.                                                                        | string                        | E            | O   | R   | 1.33.0.0 |
| PagingRepeatCount                                    | Количество повторов попыток процедуры Paging.                                                                                                                                                      | int                           | 3            | O   | R   |          | 
| Ignore_ENB_UE_ID_Reset                               | Флаг игнорирования значения `UE-associated logical S1-connection Item` при проведении процедуры S1AP-RESET для устройств, имеющих указанный идентификатор `eNB UE S1AP ID`.                        | bool                          | 0            | O   | R   |          |
| **<a name="timeout">\[Timeout\]</a>**                | Параметры времени ожидания.                                                                                                                                                                        | object                        | -            | O   | R   |          |
| ContextReq_Release                                   | Время ожидания удаления сессии на предыдущем узле SGW после приёма Context-Request от нового узла ММЕ.                                                                                             | int<br>мс                     | 5&nbsp;000   | O   | R   |          |
| **<a name="ho-timeout">\[HO_Timeout\]</a>**          | Параметры времени ожидания при хэндовере.                                                                                                                                                          | object                        | -            | O   | R   |          |
| S1_HO_ContextRelease                                 | Время ожидания очистки контекста на предыдущей базовой станции при хэндовере S1.                                                                                                                   | int<br>мс                     | 5&nbsp;000   | O   | R   | 1.10.0.0 |
| HO_from_UTRAN_TAU                                    | Время ожидания сообщения TAU-Request при хэндовере из сети UTRAN.                                                                                                                                  | int<br>мс                     | 10&nbsp;000  | O   | R   |          |
| HO_from_GERAN_TAU                                    | Время ожидания сообщения TAU-Request при хэндовере из сети GERAN.                                                                                                                                  | int<br>мс                     | 10&nbsp;000  | O   | R   |          |
| X2_HO_DelSession                                     | Время ожидания отложенного удаления сессий на исходном SGW при X2-хэндовере.                                                                                                                       | int<br>мс                     | 2&nbsp;000   | O   | R   |          |
| EtoUTRAN_HO_Release                                  | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в UTRAN.                                                                                                       | int<br>мс                     | 5&nbsp;000   | O   | R   |          |
| EtoGERAN_HO_Release                                  | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в GERAN.                                                                                                       | int<br>мс                     | 5&nbsp;000   | O   | R   |          |
| **<a name="database">\[Database\]</a>**              | Параметры базы данных.                                                                                                                                                                             | object                        | -            | O   | P   |          |
| Enable                                               | Флаг взаимодействия с базой данных.                                                                                                                                                                | bool                          | 0            | O   | R   |          |
| <a name="hostname">Hostname</a>                      | DNS-имя или IP-адрес хоста базы данных.                                                                                                                                                            | string                        | localhost    | O   | P   |          |
| <a name="port">Port</a>                              | Порт подключения к базе данных.                                                                                                                                                                    | int                           | 3306         | O   | P   |          |
| Username                                             | Логин для авторизации в базе данных.                                                                                                                                                               | string                        | -            | O   | P   |          |
| Password                                             | Пароль для авторизации в базе данных.                                                                                                                                                              | string                        | -            | O   | P   |          |
| DB_Name                                              | Название базы данных.                                                                                                                                                                              | string                        | -            | O   | P   |          |
| UpdatePeriod                                         | Периодичность обновления базы данных.                                                                                                                                                              | int<br>мс                     | 10&nbsp;000  | O   | R   |          |
| ReaderCount                                          | Количество соединений базы данных с правами на чтение.                                                                                                                                             | int<br>1-100                  | 3            | O   | P   |          |
| UnixSock                                             | Путь до сокета базы данных.<br>**Примечание.** Если задан, то параметры [Hostname](#hostname) и [Port](#port) игнорируются.                                                                        | string                        | -            | O   | P   |          |
| **<a name="guti">\[GUTI_Reallocation\]</a>**         | Параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>.                                                                                                             | object                        | -            | O   | R   |          |
| ReqCount                                             | Максимальное количество обработанных запросов Attach/TAU/Service Request до смены GUTI.                                                                                                            | int                           | 0            | O   | R   |          |
| ReallocPeriod                                        | Периодичность смены GUTI.                                                                                                                                                                          | int<br>мс                     | 0            | O   | R   |          |
| **<a name="dcn">\[DCN\]</a>**                        | Параметры <abbr title="Dedicated Core Network">DCN</abbr>.                                                                                                                                         | object                        | -            | O   | R   |          |
| List                                                 | Сети DCN. Формат:<br>`<dcn_id>-<relative_capacity>,<dcn_id>-<relative_capacity>`.                                                                                                                  | list\<mapping\<int,int\>\>    | -            | O   | R   |          |
| \<dcn_id\>                                           | Идентификатор сети.                                                                                                                                                                                | int                           | -            | M   | R   |          |
| \<relative_capacity\>                                | Относительная емкость.                                                                                                                                                                             | int                           | -            | M   | R   |          |
| **<a name="ranges">\[Ranges\]</a>**                  | Диапазоны допустимых значений идентификаторов.                                                                                                                                                     | object                        | -            | O   | P   | 1.44.2.0 |
| MME_UE_ID                                            | Границы диапазона допустимых MME UE ID. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\].                                              | string                        | -            | O   | P   | 1.44.2.0 |
| M_TMSI                                               | Диапазон допустимых M-TMSI для GUTI. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\].                                                 | string                        | -            | O   | P   | 1.44.2.0 |
| TEID                                                 | Диапазон допустимых TEID, используемых для идентификации абонента на интерфейсе S11. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | string                        | -            | O   | P   | 1.44.2.0 |
| **<a name="vendor_specific">\[VendorSpecific\]</a>** | Параметры, задающие ограничения, вызванные оборудованием других производителей.                                                                                                                    | object                        | -            | O   | P   | 1.46.2.0 |
| LongUTRAN_TransparentContainer                       | Флаг разрешения использования длинного (более 1024 байт) UTRAN Transparent Container при хэндовере от 4G к 3G.                                                                                     | bool                          | 1            | O   | R   | 1.46.2.0 |
| Suspend                                              | Флаг поддержки процедуры Suspend для bearer-служб.                                                                                                                                                 | bool                          | 0            | O   | R   | 1.46.2.0 |
| **<a name="enodeb">\[ENB_Id\]</a>**                  | Маски белого списка идентификаторов eNodeB. Формат:<br>`{ <active_flag>; "<enodeb_id>" }`<br>**Примечание.** Если нет записей, то любой считается допустимым.                                      | list\<mapping\<bool,regex\>\> | -            | O   | R   |          |
| \<active_flag\>                                      | Флаг активности маски идентификатора eNodeB.                                                                                                                                                       | bool                          | -            | M   | R   |          |
| \<enodeb_id\>                                        | Маска идентификатора eNodeB.                                                                                                                                                                       | regex                         | -            | M   | R   |          |

#### Пример ####

```ini
[General]
Handlers = 100;
CoreCount = 3;
PagingRepeatCount = 3;
ReleaseOnICS_Fail = I;
EchoSendPeriod = 22;

[ENB_Id]
{1; "107217"};
{1; "172754"};
{1; "3584"};
{1; "411"};

[Database]
Enable = 1;
Username = "mme";
Password = "777";
DB_Name = "MME";
UnixSock = "/var/lib/mysql/mysql.sock";
```