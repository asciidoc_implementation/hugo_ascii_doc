---
title: "Журналы"
description: "Описание журналов и статистики"
weight: 50
type: docs
---

Узел Protei_MME ведет следующие журналы:

* EDR:
  * [connect_cdr](./edr/connect/) — журнал событий с PDN-соединениями;
  * [dedicated_bearer_cdr](./edr/dedicated_bearer/) — журнал событий с выделенной bearer-службой;
  * [diam_cdr](./edr/diam/) — журнал событий Diameter;
  * [enodeb_cdr](./edr/enodeb/) — журнал событий eNodeB;
  * [gtp_c_cdr](./edr/gtp_c/) — журнал событий GTP-C;
  * [gtp_c_overload](./edr/gtp_c_overload/) — журнал событий перегрузки интерфейса GTP-C;
  * [http_cdr](./edr/http/) — журнал событий HTTP;
  * [irat_handover_cdr](./edr/irat_handover/) — журнал межсетевого хэндовера;
  * [lte_handover_cdr](./edr/lte_handover/) — журнал хэндовера в сети LTE;
  * [paging_cdr](./edr/paging/) — журнал событий Paging;
  * [reject_cdr](./edr/reject/) — журнал событий отбоя сообщений;
  * [s1ap_cdr](./edr/s1ap/) — журнал событий S1AP;
  * [s1ap_context_cdr](./edr/s1ap_context/) — журнал событий с контекстом S1AP;
  * [s1ap_overload_cdr](./edr/s1ap_overload/) — журнал событий перегрузки интерфейса S1AP;
  * [sgsap_cdr](./edr/sgsap/) — журнал событий SGsAP;
  * [tau_cdr](./edr/tau/) — журнал событий Tracking-Area-Update;
* log:
  * alarm — общий журнал аварий системы;
  * alarm_cdr — журнал CDR подсистемы сбора аварий;
  * alarm_trace — журнал действий подсистемы сбора аварий;
  * bc_trace — журнал действий базовой компоненты;
  * bc_warning — журнал предупреждений базовой компоненты;
  * COM_trace — журнал действий подсистемы конфигурирования компонент;
  * COM_warning — журнал предупреждений подсистемы конфигурирования компонент;
  * config — журнал загрузок конфигурационных файлов
  * db_trace — журнал действий базы данных MME;
  * diam_info — журнал событий протокола Diameter;
  * diam_trace — журнал действий протокола Diameter;
  * diam_warning — журнал предупреждений протокола Diameter;
  * dns_trace — журнал действий протокола DNS;
  * dns_warning — журнал предупреждений протокола DNS;
  * GTP_C_trace — журнал действий протокола GTP Control Plane: GTPv1-C и GTPv2;
  * GTP_C_warning — журнал предупреждений протокола GTP Control Plane: GTPv1-C и GTPv2;
  * http_trace — журнал действий HTTP-интерфейса;
  * mme_config — журнал чтения конфигурационных файлов MME: **mme.cfg**, **served_plmn.cfg** и связанных с ним **\*_rules**;
  * profilers — журнал использования физических ресурсов и логик приложения;
  * S1AP_trace — журнал действий протокола S1AP;
  * S1AP_warning — журнал предупреждений протокола S1AP;
  * sctp_binary — дамп SCTP-соединений;
  * Sg_info — журнал событий системы сигнализации;
  * Sg_trace — журнал действий системы сигнализации;
  * Sg_warning — журнал предупреждений системы сигнализации;
  * SGsAP_trace — журнал действий протокола SGsAP;
  * SGsAP_warning — журнал предупреждений протокола SGsAP;
  * si — журнал действий сокет-интерфейса;
  * si_info — журнал событий сокет-интерфейса;
  * si_warning — журнал предупреждений сокет-интерфейса;
  * trace — общий журнал действий;
  * ue_trace — журнал отслеживания устройств.

Узел Protei_MME ведет следующие файлы со статистиками по метрикам:

* [MME_Diameter.csv](./stat/MME_Diameter/) — статистическая информация по метрикам MME для процедур протокола Diameter: Base;
* [MME_handover.csv](./stat/MME_handover/) — статистическая информация по метрикам MME для процедур хэндовера;
* [MME_paging.csv](./stat/MME_paging/) — статистическая информация по метрикам MME для процедур Paging;
* [MME_resource.csv](./stat/MME_resource/) — статистическая информация по метрикам MME для использования ресурсов сервера;
* [MME_s11_Interface.csv](./stat/MME_s11_Interface/) — статистическая информация по метрикам MME для процедур интерфейса S11;
* [MME_s1_Attach.csv](./stat/MME_s1_Attach/) — статистическая информация по метрикам MME для интерфейса S1 по процедурам Attach;
* [MME_s1_Bearer_Activation.csv](./stat/MME_s1_Bearer_Activation/) — статистическая информация по метрикам MME для процедур S1 Bearer Activation;
* [MME_s1_Bearer_Deactivation.csv](./stat/MME_s1_Bearer_Deactivation/) — статистическая информация по метрикам MME для процедур S1 Bearer Deactivation;
* [MME_s1_Bearer_Modification.csv](./stat/MME_s1_Bearer_Modification/) — статистическая информация по метрикам MME для процедур S1 Bearer Modification;
* [MME_s1_Detach.csv](./stat/MME_s1_Detach/) — статистическая информация по метрикам MME для интерфейса S1 по процедурам Detach;
* [MME_s1_Interface.csv](./stat/MME_s1_Interface/) — статистическая информация по метрикам MME для процедур протокола S1AP;
* [MME_s1_Security.csv](./stat/MME_s1_Security/) — статистическая информация по метрикам MME для процедур протокола NAS;
* [MME_s1_Service.csv](./stat/MME_s1_Service/) — статистическая информация по метрикам MME для процедур S1/S1AP Service;
* [MME_S6a_interface.csv](./stat/MME_S6a_interface/) — статистическая информация по метрикам MME для процедур интерфейса S6a;
* [MME_sgs_Interface.csv](./stat/MME_sgs_Interface/) — статистическая информация по метрикам MME для процедур интерфейса SGs;
* [MME_sv_Interface.csv](./stat/MME_sv_Interface/) — статистическая информация по метрикам MME для процедур интерфейса Sv;
* [MME_tau.csv](./stat/MME_tau/) — статистическая информация по метрикам MME для процедур TRACKING AREA UPDATE;
* [MME_users.csv](./stat/MME_users/) — статистическая информация по метрикам MME для количества абонентов и сессий.
