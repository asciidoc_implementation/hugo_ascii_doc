---
title: "Users"
description: "Статистика по абонентам и сессиям"
weight: 20
type: docs
---

Файл **<node_name>\_MME-users\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для количества абонентов и сессий.

### Описание параметров ###

| Tx/Rx | Метрика                                                                                           | Описание                                                              | Группа                               |
|-------|---------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|--------------------------------------|
|       | <a name="realTimePdnConnectionNumber">realTimePdnConnectionNumber</a>                             | Количество активных PDN-соединений.                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeDedicatedBearerNumber">realTimeDedicatedBearerNumber</a>                         | Количество активных выделенных bearer-служб.                          | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtEcmIdleStatus">realTimeAttachedUsersAtEcmIdleStatus</a>           | Количество зарегистрированных абонентов в состоянии ECM-IDLE.         | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtEcmConnectedStatus">realTimeAttachedUsersAtEcmConnectedStatus</a> | Количество зарегистрированных абонентов в состоянии ECM-CONNECTED.    | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtDeregStatus">realTimeAttachedUsersAtDeregStatus</a>               | Количество зарегистрированных абонентов в состоянии EMM-DEREGISTERED. | TAI:Value, IMSIPLMN:Value, APN:Value |

### Группа ###

| Название    | Описание                                                                             | Тип    |
|-------------|--------------------------------------------------------------------------------------|--------|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`.                     | hex    |
| \<plmn_id\> | Идентификатор сети PLMN.                                                             | int    |
| \<tac\>     | Код области отслеживания.                                                            | hex    |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.                      | int    |
| APN         | Идентификатор области отслеживания. Формат:<br>`<name>.mnc<mnc>.mcc<mcc>.<network>`. | string |
| \<name\>    | Имя точки доступа.                                                                   | string |
| \<mnc\>     | Код мобильной сети, <abbr title="Mobile Network Code">MNC</abbr>.                    | int    |
| \<mcc\>     | Мобильный код страны, <abbr title="Mobile Country Code">MCC</abbr>.                  | int    |
| \<network\> | Тип сети.                                                                            | string |

#### Пример ####

```
TAI:2500103E8
IMSIPLMN:25099
APN:internet.mnc01.mcc250.gprs
```

#### Пример файла ####

```csv
,realTimeAttachedUsersAtEcmIdleStatus,,1
,realTimeAttachedUsersAtEcmIdleStatus,IMSIPLMN:00101,1
,realTimeAttachedUsersAtEcmIdleStatus,TAI:001010001,1
,realTimeAttachedUsersAtEcmConnectedStatus,,2
,realTimeAttachedUsersAtEcmConnectedStatus,IMSIPLMN:00101,1
,realTimeAttachedUsersAtEcmConnectedStatus,IMSIPLMN:20893,1
,realTimeAttachedUsersAtEcmConnectedStatus,TAI:001010010,1
,realTimeAttachedUsersAtEcmConnectedStatus,TAI:208930001,1
,realTimeUsersAtEmmDeregisteredStatus,,0
,realTimePdnConnectionNumber,,3
,realTimePdnConnectionNumber,IMSIPLMN:00101,2
,realTimePdnConnectionNumber,IMSIPLMN:20893,1
,realTimePdnConnectionNumber,TAI:001010001,1
,realTimePdnConnectionNumber,TAI:001010010,1
,realTimePdnConnectionNumber,TAI:208930001,1
,realTimeDedicatedBearerNumber,,0
```