---
title: "S1 Security"
description: "Статистика процедур NAS"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Security\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для протокола NAS.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf) и
[3GPP TS 24.501](https://www.etsi.org/deliver/etsi_ts/124500_124599/124501/17.10.01_60/ts_124501v171001p.pdf).

| Tx/Rx | Метрика                                                               | Описание                                                                  | Группа |
|-------|-----------------------------------------------------------------------|---------------------------------------------------------------------------|--------|
| Tx    | <a name="securityModeCommand">securityModeCommand</a>                 | Количество сообщений NAS SECURITY MODE COMMAND.                           |        |
| Rx    | <a name="securityModeCommandComplete">securityModeCommandComplete</a> | Количество сообщений NAS SECURITY MODE COMPLETE.                          |        |
| Tx    | <a name="authenticationRequest">authenticationRequest </a>            | Количество сообщений NAS AUTHENTICATION REQUEST.                          |        |
| Rx    | <a name="authenticationSuccess">authenticationSuccess </a>            | Количество сообщений NAS AUTHENTICATION RESULT с `EAP message = SUCCESS`. |        |

#### Пример файла ####

```csv
tx,securityModeCommand,,101
rx,securityModeCommandComplete,,100
tx,authenticationRequest,,59
rx,authenticationSuccess,,53
```
