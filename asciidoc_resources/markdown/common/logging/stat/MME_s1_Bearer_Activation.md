---
title: "S1 Bearer Activation"
description: "Статистика процедур S1 Bearer Activation"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1BearerActivation\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур S1 Bearer Activation.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Метрика                                                                                                                                             | Описание                                                                                                                             | Группа                               |
|-------|-----------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| Tx    | <a name="dedicatedBearerActiveRequest">dedicatedBearerActiveRequest</a>                                                                             | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST.                                                               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveSuccess">dedicatedBearerActiveSuccess</a>                                                                             | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT ACCEPT.                                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject">dedicatedBearerActiveReject</a>                                                                               | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REJECT                                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject26">dedicatedBearerActiveReject26</a>                                                                           | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REJECT c причиной "#26 Insufficient Resources".                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject31">dedicatedBearerActiveReject31</a>                                                                           | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REJECT с причиной "#31 Request rejected, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject43">dedicatedBearerActiveReject43</a>                                                                           | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REJECT с причиной "#43 Invalid EPS bearer identity".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject111">dedicatedBearerActiveReject111</a>                                                                         | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REJECT с причиной "#95-111 Protocol error, unspecified".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci1">dedicatedBearerActiveRequestQci1</a>                                                                     | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 1`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci2">dedicatedBearerActiveRequestQci2</a>                                                                     | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 2`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci3">dedicatedBearerActiveRequestQci3</a>                                                                     | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 3`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci4">dedicatedBearerActiveRequestQci4</a>                                                                     | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 4`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci65">dedicatedBearerActiveRequestQci65</a>                                                                   | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 65`.                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci66">dedicatedBearerActiveRequestQci66</a>                                                                   | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 66`.                                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci67">dedicatedBearerActiveRequestQci67</a>                                                                   | Количество сообщений S1 ACTIVATE DEDICATED EPS BEARER CONTEXT REQUEST для `QCI = 3`.                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest">activateDefaultEpsBearerContextRequest</a>                                                         | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REQUEST.                                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequestIpv4Only">activateDefaultEpsBearerContextRequestIpv4Only</a>                                         | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REQUEST с причиной "#50 PDN type IPv4 only allowed".                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequestIpv6Only">activateDefaultEpsBearerContextRequestIpv6Only</a>                                         | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REQUEST с причиной "#51 PDN type IPv6 only allowed".                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequestSingleAddrBearersOnlyAllowed">activateDefaultEpsBearerContextRequestSingleAddrBearersOnlyAllowed</a> | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REQUEST с причиной "#52 Single address bearers only allowed".            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="activateDefaultEpsBearerContextAccept">activateDefaultEpsBearerContextAccept</a>                                                           | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT ACCEPT.                                                                  | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject26">activateDefaultEpsBearerContextReject26</a>                                                       | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REJECT с причиной "#26 Insufficient resources".                          | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject31">activateDefaultEpsBearerContextReject31</a>                                                       | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REJECT с причиной "#31 Request rejected, unspecified".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject111">activateDefaultEpsBearerContextReject111</a>                                                     | Количество сообщений S1 ACTIVATE DEFAULT EPS BEARER CONTEXT REJECT с причиной "#95-111 Protocol error, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="pdnConnectivityRequest">pdnConnectivityRequest</a>                                                                                         | Количество сообщений S1 PDN CONNECTIVITY REQUEST.                                                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject">pdnConnectivityReject</a>                                                                                           | Количество сообщений S1 PDN CONNECTIVITY REJECT                                                                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject8">pdnConnectivityReject8</a>                                                                                         | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#8 Operator determined barring".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject26">pdnConnectivityReject26</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#26 Insufficient resources".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject27">pdnConnectivityReject27</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#27 Missing or unknown APN".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject28">pdnConnectivityReject28</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#28 Unknown PDN type".                                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject29">pdnConnectivityReject29</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#29 User authentication failed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject30">pdnConnectivityReject30</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#30 Request rejected by Serving GW or PDN GW".                           | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject31">pdnConnectivityReject31</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#31 Request rejected, unspecified".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject32">pdnConnectivityReject32</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#32 Service option not supported".                                       | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject33">pdnConnectivityReject33</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#33 Requested service option not subscribed".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject34">pdnConnectivityReject34</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#34 Service option temporarily out of order".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject35">pdnConnectivityReject35</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#35 PTI already in use".                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject38">pdnConnectivityReject38</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#38 Network failure".                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject50">pdnConnectivityReject50</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#50 PDN type IPv4 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject51">pdnConnectivityReject51</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#51 PDN type IPv6 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject54">pdnConnectivityReject54</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#54 PDN connection does not exist".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject55">pdnConnectivityReject55</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#55 Multiple PDN connections for a given APN not allowed".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject96">pdnConnectivityReject96</a>                                                                                       | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#96 Invalid mandatory information".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject111">pdnConnectivityReject111</a>                                                                                     | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#95-111 Protocol error, unspecified".                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject112">pdnConnectivityReject112</a>                                                                                     | Количество сообщений S1 PDN CONNECTIVITY REJECT с причиной "#112 APN restriction value incompatible with active EPS bearer context". | TAI:Value, IMSIPLMN:Value, APN:Value |

### Группа ###

| Название    | Описание                                                                             | Тип    |
|-------------|--------------------------------------------------------------------------------------|--------|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`.                     | hex    |
| \<plmn_id\> | Идентификатор сети PLMN.                                                             | int    |
| \<tac\>     | Код области отслеживания.                                                            | hex    |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.                      | int    |
| APN         | Идентификатор области отслеживания. Формат:<br>`<name>.mnc<mnc>.mcc<mcc>.<network>`. | string |
| \<name\>    | Имя точки доступа.                                                                   | string |
| \<mnc\>     | Код мобильной сети, <abbr title="Mobile Network Code">MNC</abbr>.                    | int    |
| \<mcc\>     | Мобильный код страны, <abbr title="Mobile Country Code">MCC</abbr>.                  | int    |
| \<network\> | Тип сети.                                                                            | string |

**Примечание.** Описание причин ошибок ESM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

#### Пример ####

```
TAI,2500103E8
IMSIPLMN,25099
APN:internet.mnc01.mcc250.gprs
```

#### Пример файла ####

```csv
tx,dedicatedBearerActiveRequest,,0
rx,dedicatedBearerActiveSuccess,,0
rx,dedicatedBearerActiveReject,,0
tx,activateDefaultEpsBearerContextRequest,,24
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:00101,1
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:20893,21
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:46692,2
tx,activateDefaultEpsBearerContextRequest,TAI:001010010,1
tx,activateDefaultEpsBearerContextRequest,TAI:208930001,21
tx,activateDefaultEpsBearerContextRequest,TAI:466920051,2
tx,activateDefaultEpsBearerContextRequest50,,23
tx,activateDefaultEpsBearerContextRequest50,IMSIPLMN:00101,1
tx,activateDefaultEpsBearerContextRequest50,IMSIPLMN:20893,21
tx,activateDefaultEpsBearerContextRequest50,IMSIPLMN:46692,1
tx,activateDefaultEpsBearerContextRequest50,TAI:001010010,1
tx,activateDefaultEpsBearerContextRequest50,TAI:208930001,21
tx,activateDefaultEpsBearerContextRequest50,TAI:466920051,1
rx,activateDefaultEpsBearerContextAccept,,1
rx,activateDefaultEpsBearerContextAccept,IMSIPLMN:46692,1
rx,activateDefaultEpsBearerContextAccept,TAI:466920051,1
rx,pdnConnectivityRequest,,30
rx,pdnConnectivityRequest,IMSIPLMN:00101,1
rx,pdnConnectivityRequest,IMSIPLMN:20893,27
rx,pdnConnectivityRequest,IMSIPLMN:46692,2
rx,pdnConnectivityRequest,TAI:001010010,1
rx,pdnConnectivityRequest,TAI:208930001,27
rx,pdnConnectivityRequest,TAI:466920051,2
tx,pdnConnectivityReject,,0
```
