---
title: "S6a Interface"
description: "Статистика процедур S6a"
weight: 20
type: docs
---

Файл **<node_name>\_MME-S6a-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S6a.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf) 
и [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                                         | Описание                                                           | Группа         | 
|-------|---------------------------------------------------------------------------------|--------------------------------------------------------------------|----------------|
| Tx    | <a name="authenticationInformationRequest">authenticationInformationRequest</a> | Количество сообщений Diameter: Authentication-Information-Request. | IMSIPLMN:Value | 
| Rx    | <a name="authenticationInformationAnswer">authenticationInformationAnswer</a>   | Количество сообщений Diameter: Authentication-Information-Answer.  | IMSIPLMN:Value | 
| Tx    | <a name="updateLocationRequest">updateLocationRequest</a>                       | Количество сообщений Diameter: Update-Location-Request.            | IMSIPLMN:Value | 
| Rx    | <a name="updateLocationAnswer">updateLocationAnswer</a>                         | Количество сообщений Diameter: Update-Location-Answer.             | IMSIPLMN:Value | 
| Rx    | <a name="resetRequest">resetRequest</a>                                         | Количество сообщений Diameter: Reset-Request.                      | IMSIPLMN:Value | 
| Tx    | <a name="resetAnswer">resetAnswer</a>                                           | Количество сообщений Diameter: Reset-Answer.                       | IMSIPLMN:Value | 
| Rx    | <a name="cancelLocationRequest">cancelLocationRequest</a>                       | Количество сообщений Diameter: Cancel-Location-Request.            | IMSIPLMN:Value | 
| Rx    | <a name="cancelLocationAnswer">cancelLocationAnswer</a>                         | Количество сообщений Diameter: Cancel-Location-Answer.             | IMSIPLMN:Value | 
| Rx    | <a name="insertSubscriberDataRequest">insertSubscriberDataRequest</a>           | Количество сообщений Diameter: Insert-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Tx    | <a name="insertSubscriberDataAnswer">insertSubscriberDataAnswer</a>             | Количество сообщений Diameter: Insert-Subscriber-Data-Answer.      | IMSIPLMN:Value | 
| Tx    | <a name="purgeUeRequest">purgeUeRequest</a>                                     | Количество сообщений Diameter: Purge-UE-Request.                   | IMSIPLMN:Value | 
| Rx    | <a name="purgeUeAnswer">purgeUeAnswer</a>                                       | Количество сообщений Diameter: Purge-UE-Answer.                    | IMSIPLMN:Value | 
| Tx    | <a name="notifyRequest">notifyRequest</a>                                       | Количество сообщений Diameter: Notify-Request.                     | IMSIPLMN:Value | 
| Rx    | <a name="notifyAnswer">notifyAnswer</a>                                         | Количество сообщений Diameter: Notify-Answer.                      | IMSIPLMN:Value | 
| Rx    | <a name="deleteSubscriberDataRequest">deleteSubscriberDataRequest</a>           | Количество сообщений Diameter: Delete-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Tx    | <a name="deleteSubscriberDataAnswer">deleteSubscriberDataAnswer</a>             | Количество сообщений Diameter: Delete-Subscriber-Data-Answer.      | IMSIPLMN:Value | 

**Примечание.** В таблице ниже приведены возможные значения AVP `Result-Code` из перечисленных в 
[3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).

| Значение | Описание                                 |
|----------|------------------------------------------|
| 2001     | DIAMETER_SUCCESS                         |
| E4181    | DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE |
| E5001    | DIAMETER_ERROR_USER_UNKNOWN              |
| E5003    | DIAMETER_AUTHORIZATION_REJECTED          |
| E5004    | DIAMETER_ERROR_ROAMING_NOT_ALLOWED       |
| E5012    | DIAMETER_UNABLE_TO_COMPLY                |
| E5420    | DIAMETER_ERROR_UNKNOWN_EPS_SUBSCRIPTION  |
| E5421    | DIAMETER_ERROR_RAT_NOT_ALLOWED           |
| E5423    | DIAMETER_ERROR_UNKOWN_SERVING_NODE       |

При наступлении события формируется метрика со значением AVP в конце. 

Пример: `updateLocationAnswerE5420`.

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
IMSIPLMN:25099
```

#### Пример файла ####

```csv
rx,updateLocationAnswer,,0
rx,updateLocationAnswer2001,,0
rx,cancelLocationRequest,,0
rx,authenticationInformationAnswer,,2
rx,authenticationInformationAnswer2001,,2
rx,insertSubscriberDataRequest,,0
rx,deleteSubscriberDataRequest,,0
rx,purgeUeAnswer,,0
rx,purgeUeAnswer2001,,0
rx,resetRequest,,0
rx,notifyAnswer,,0
rx,notifyAnswer2001,,0
rx,notifyAnswer3004,,0
tx,updateLocationRequest,,0
tx,cancelLocationAnswer,,0
tx,cancelLocationAnswer2001,,0
tx,authenticationInformationRequest,,2
tx,insertSubscriberDataAnswer,,0
tx,deleteSubscriberDataAnswer,,0
tx,purgeUeRequest,,0
tx,resetAnswer,,0
tx,notifyRequest,,0
rx,updateLocationAnswer2001,IMSIPLMN:00101,0
rx,updateLocationAnswer,IMSIPLMN:00101,0
rx,cancelLocationRequest,IMSIPLMN:00101,0
rx,authenticationInformationAnswer2001,IMSIPLMN:00101,2
rx,authenticationInformationAnswer,IMSIPLMN:00101,2
rx,purgeUeAnswer2001,IMSIPLMN:00101,0
rx,purgeUeAnswer,IMSIPLMN:00101,0
rx,notifyAnswer2001,IMSIPLMN:00101,0
rx,notifyAnswer3004,IMSIPLMN:00101,0
rx,notifyAnswer,IMSIPLMN:00101,0
rx,updateLocationAnswer2001,IMSIPLMN:20893,0
rx,updateLocationAnswer,IMSIPLMN:20893,0
rx,authenticationInformationAnswer2001,IMSIPLMN:20893,0
rx,authenticationInformationAnswer,IMSIPLMN:20893,0
rx,notifyAnswer2001,IMSIPLMN:20893,0
rx,notifyAnswer,IMSIPLMN:20893,0
tx,updateLocationRequest,IMSIPLMN:00101,0
tx,cancelLocationAnswer2001,IMSIPLMN:00101,0
tx,cancelLocationAnswer,IMSIPLMN:00101,0
tx,authenticationInformationRequest,IMSIPLMN:00101,2
tx,purgeUeRequest,IMSIPLMN:00101,0
tx,notifyRequest,IMSIPLMN:00101,0
tx,updateLocationRequest,IMSIPLMN:20893,0
tx,authenticationInformationRequest,IMSIPLMN:20893,0
tx,notifyRequest,IMSIPLMN:20893,0
```