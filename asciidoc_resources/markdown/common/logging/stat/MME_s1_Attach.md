---
title: "S1 Attach"
description: "Статистика процедур S1 Attach"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Attach\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S1 по процедурам Attach.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                                                                           | Описание                                                                                          | Группа                    |
|-------|-------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|---------------------------|
| Rx    | <a name="attachRequest">attachRequest</a>                                                                         | Количество сообщений S1 Attach Request с `EPS Attach Type = EPS attach`.                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachSuccess">attachSuccess</a>                                                                         | Количество сообщений S1 Attach Accept с `EPS Attach Type = EPS attach`.                           | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail3IllegalUe">attachFail3IllegalUe</a>                                                           | Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail6IllegalMe">attachFail6IllegalMe</a>                                                           | Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail7ServNotAllowed">attachFail7ServNotAllowed</a>                                                 | Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail8EpsAndNonEpsServicesNotAllowed">attachFail8EpsAndNonEpsServicesNotAllowed</a>                 | Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".       | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail11PlmnNotAllowed">attachFail11PlmnNotAllowed</a>                                               | Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail12TaNotAllowed">attachFail12TaNotAllowed</a>                                                   | Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail13RoamingNotAllowedInTa">attachFail13RoamingNotAllowedInTa</a>                                 | Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area". | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail14ServNotAllowedPlmn">attachFail14ServNotAllowedPlmn</a>                                       | Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail15NoSuitCellsInTa">attachFail15NoSuitCellsInTa</a>                                             | Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".        | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail19EsmFailure">attachFail19EsmFailure</a>                                                       | Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail25NotAuthForThisCsg">attachFail25NotAuthForThisCsg</a>                                         | Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail111ProtocolErr">attachFail111ProtocolErr</a>                                                   | Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".              | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="combinedAttachRequest">combinedAttachRequest</a>                                                         | Количество сообщений S1 Attach Request с `EPS Attach type = combined EPS/IMSI attach`.            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachSuccess">combinedAttachSuccess</a>                                                         | Количество сообщений S1 Attach Accept с `EPS Attach type = combined EPS/IMSI attach`.             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail3IllegalUe">combinedAttachFail3IllegalUe</a>                                           | Количество сообщений S1 Attach Reject с причиной "#3 Illegal UE".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail6IllegalMe">combinedAttachFail6IllegalMe</a>                                           | Количество сообщений S1 Attach Reject с причиной "#6 Illegal ME".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail7ServNotAllowed">combinedAttachFail7ServNotAllowed</a>                                 | Количество сообщений S1 Attach Reject с причиной "#7 EPS services not allowed".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail8EpsAndNonEpsServicesNotAllowed">combinedAttachFail8EpsAndNonEpsServicesNotAllowed</a> | Количество сообщений S1 Attach Reject с причиной "#8 EPS and non-EPS services not allowed".       | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail11PlmnNotAllowed">combinedAttachFail11PlmnNotAllowed</a>                               | Количество сообщений S1 Attach Reject с причиной "#11 PLMN not allowed".                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail12TaNotAllowed">combinedAttachFail12TaNotAllowed</a>                                   | Количество сообщений S1 Attach Reject с причиной "#12 Tracking Area not allowed".                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail13RoamingNotAllowed">combinedAttachFail13RoamingNotAllowed</a>                         | Количество сообщений S1 Attach Reject с причиной "#13 Roaming not allowed in this tracking area". | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail14ServNotAllowedPlmn">combinedAttachFail14ServNotAllowedPlmn</a>                       | Количество сообщений S1 Attach Reject с причиной "#14 EPS services not allowed in this PLMN".     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail15NoSuitCellsInTa">combinedAttachFail15NoSuitCellsInTa</a>                             | Количество сообщений S1 Attach Reject с причиной "#15 No Suitable Cells In tracking area".        | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail19EsmFailure">combinedAttachFail19EsmFailure</a>                                       | Количество сообщений S1 Attach Reject с причиной "#19 ESM failure".                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail25NotAuthForThisCsg">combinedAttachFail25NotAuthForThisCsg</a>                         | Количество сообщений S1 Attach Reject с причиной "#25 Not authorized for this CSG".               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail111ProtocolErr">combinedAttachFail111ProtocolErr</a>                                   | Количество сообщений S1 Attach Reject с причиной "#111 Protocol error, unspecified".              | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="emergencyAttachRequest">emergencyAttachRequest</a>                                                       | Количество сообщений S1 Attach Request с `EPS Attach type = EPS emergency attach`.                | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="emergencyAttachSuccess">emergencyAttachSuccess</a>                                                       | Количество сообщений S1 Attach Accept с `EPS Attach type = EPS emergency attach`.                 | TAI:Value, IMSIPLMN:Value |

**Примечание.** Описание причин ошибок EMM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
TAI:2500103E8
IMSIPLMN:25099
```

#### Пример файла ####

```csv
rx,attachRequest,,0
tx,attachSuccess,,0
tx,attachFail,,0
rx,combinedAttachRequest,,1
rx,combinedAttachRequest,IMSIPLMN:00101,1
rx,combinedAttachRequest,TAI:001010010,1
tx,combinedAttachSuccess,,1
tx,combinedAttachSuccess,IMSIPLMN:00101,1
tx,combinedAttachSuccess,TAI:001010010,1
tx,combinedAttachFail,,0
rx,emergencyAttachRequest,,0
tx,emergencyAttachSuccess,,0
```