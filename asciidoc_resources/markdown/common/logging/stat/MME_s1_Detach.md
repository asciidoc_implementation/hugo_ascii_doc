---
title: "S1 Detach"
description: "Статистика процедур S1 Detach"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Detach\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S1 по процедурам Detach.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                   | Описание                                                                                                                                      | Группа                    |
|-------|-----------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|---------------------------|
| Rx    | <a name="detachRequest">detachRequest</a>                 | Количество сообщений S1 Detach Request с `Detach type = EPS detach`, инициированых UE.                                                        | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccess">detachSuccess</a>                 | Количество сообщений S1 Detach Accept в процедуре EPS Detach, в ответ на сообщения Detach Request с `Detach type = EPS detach`.               | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachRequestImsi">detachRequestImsi</a>         | Количество сообщений S1 Detach Request с `Detach type = IMSI detach`, инициированых UE.                                                       | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccessImsi">detachSuccessImsi</a>         | Количество сообщений S1 Detach Accept в процедуре EPS Detach, в ответ на сообщения Detach Request с `Detach type = IMSI detach`.              | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachRequestCombined">detachRequestCombined</a> | Количество сообщений S1 Detach Request с `Detach type = combined EPS/IMSI detach`, инициированых UE.                                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccessCombined">detachSuccessCombined</a> | Количество сообщений S1 Detach Accept в процедуре EPS Detach, в ответ на сообщения Detach Request с `Detach type = combined EPS/IMSI detach`. | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachRequestMmeInit">detachRequestMmeInit</a>   | Количество сообщений S1 Detach Request, инициированых со стороны сети.                                                                        | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachSuccessMmeInit">detachSuccessMmeInit</a>   | Количество сообщений S1 Detach Accept в процедуре EPS Detach, в ответ на сообщение Detach Request, инициированого со стороны сети.            | TAI:Value, IMSIPLMN:Value |

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
TAI:2500103E8
IMSIPLMN:25099
```

#### Пример файла ####

```csv
rx,detachRequest,,0
rx,detachSuccess,,1
rx,detachSuccess,IMSIPLMN:00101,1
rx,detachSuccess,TAI:001010001,1
tx,detachRequestMmeInit,,1
tx,detachRequestMmeInit,IMSIPLMN:00101,1
tx,detachRequestMmeInit,TAI:001010001,1
tx,detachSuccessMmeInit,,0
```