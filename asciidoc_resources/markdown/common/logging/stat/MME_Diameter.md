---
title: "Diameter"
description: "Статистика процедур Diameter"
weight: 20
type: docs
---

Файл **<node_name>\_MME-Diameter\_<datetime>\_<granularity>.csv** содержит статистическую информацию по метрикам MME для процедур протокола Diameter: Base.

### Описание параметров ###

Подробную информацию см. [RFC 6733](https://www.ietf.org/rfc/rfc6733.txt.pdf).

| Tx/Rx | Метрика                                                               | Описание                                                           | Группа      |
|-------|-----------------------------------------------------------------------|--------------------------------------------------------------------|-------------|
| Tx/Rx | <a name="deviceWatchdogRequest">deviceWatchdogRequest</a>             | Количество сообщений Diameter: Device-Watchdog-Request, DWR.       | REALM:Value |
| Tx/Rx | <a name="deviceWatchdogAnswer">deviceWatchdogAnswer</a>               | Количество сообщений Diameter: Device-Watchdog-Answer, DWA.        | REALM:Value |
| Tx/Rx | <a name="capabilitiesExchangeRequest">capabilitiesExchangeRequest</a> | Количество сообщений Diameter: Capabilities-Exchange-Request, CER. | REALM:Value |
| Tx/Rx | <a name="capabilitiesExchangeAnswer">capabilitiesExchangeAnswer</a>   | Количество сообщений Diameter: Capabilities-Exchange-Answer, CEA.  | REALM:Value |
| Tx/Rx | <a name="disconnectPeerRequest">disconnectPeerRequest</a>             | Количество сообщений Diameter: Disconnect-Peer-Request, DPR.       | REALM:Value |
| Tx/Rx | <a name="disconnectPeerAnswer">disconnectPeerAnswer</a>               | Количество сообщений Diameter: Disconnect-Peer-Answer, DPA.        | REALM:Value |

### Группа

| Название | Описание                                                                                                                                                                        | Тип    |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| REALM    | Значение `Destination-Realm`, заданное параметром [diam_dest.cfg :: DestRealm](../../../config/diam_dest/#dest-realm/). Формат:<br>`<domain>.mnc<MNC>.mcc<MCC>.3gppnetwork.org` | string |

#### Пример ####

```
REALM:epc.mnc001.mcc001.3gppnetwork.org
```

#### Пример файла ####

```csv
rx,capabilitiesExchangeRequest,,0
rx,capabilitiesExchangeAnswer,,0
rx,capabilitiesExchangeAnswer2001,,0
rx,deviceWatchdogRequest,,211
rx,deviceWatchdogAnswer,,87
rx,deviceWatchdogAnswer2001,,87
tx,capabilitiesExchangeRequest,,0
tx,capabilitiesExchangeAnswer,,0
tx,deviceWatchdogRequest,,87
tx,deviceWatchdogAnswer,,211
tx,deviceWatchdogAnswer2001,,211
```