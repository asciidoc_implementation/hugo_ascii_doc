---
title: "S11 Interface"
description: "Статистика процедур S11"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s11Interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S11.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf).

| Tx/Rx | Метрика                                                                                                 | Описание                                                                                                                                                                                                    | Группа |
|-------|---------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| Rx    | <a name="createBearerRequest">createBearerRequest</a>                                                   | Количество сообщений GTP: Create Bearer Request в процедуре активации выделенной bearer-службы за гранулярный период.                                                                                       |        |
| Tx    | <a name="createBearerResponse">createBearerResponse</a>                                                 | Количество сообщений GTP: Create Bearer Response за гранулярный период.                                                                                                                                     |        |
| Tx    | <a name="createBearerResponse16">createBearerResponse16</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#16 Request accepted за гранулярный период"                                                                                                    |        |
| Tx    | <a name="createBearerResponse17">createBearerResponse17</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#17 Request accepted partially за гранулярный период"                                                                                          |        |
| Tx    | <a name="createBearerResponse64">createBearerResponse64</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#64 Context Not Found" за гранулярный период.                                                                                                  |        |
| Tx    | <a name="createBearerResponse70">createBearerResponse70</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#70 Mandatory IE missing" за гранулярный период.                                                                                               |        |
| Tx    | <a name="createBearerResponse72">createBearerResponse72</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#72 System failure" за гранулярный период.                                                                                                     |        |
| Tx    | <a name="createBearerResponse73">createBearerResponse73</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#73 No resources available" за гранулярный период.                                                                                             |        |
| Tx    | <a name="createBearerResponse87">createBearerResponse87</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#87 UE not responding" за гранулярный период.                                                                                                  |        |
| Tx    | <a name="createBearerResponse88">createBearerResponse88</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#88 UE refuses" за гранулярный период.                                                                                                         |        |
| Tx    | <a name="createBearerResponse90">createBearerResponse90</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#90 Unable to page UE" за гранулярный период.                                                                                                  |        |
| Tx    | <a name="createBearerResponse94">createBearerResponse94</a>                                             | Количество сообщений GTP: Create Bearer Response с причиной "#94 Request rejected (reason not specified)" за гранулярный период.                                                                            |        |
| Tx    | <a name="createBearerResponse102">createBearerResponse102</a>                                           | Количество сообщений GTP: Create Bearer Response с причиной "#102 Unable to page UE due to Suspension" за гранулярный период.                                                                               |        |
| Tx    | <a name="modifyBearerRequest">modifyBearerRequest</a>                                                   | Количество сообщений GTP: Modify Bearer Request за гранулярный период.                                                                                                                                      |        |
| Rx    | <a name="modifyBearerResponse">modifyBearerResponse</a>                                                 | Количество сообщений GTP: Modify Bearer Response за гранулярный период.                                                                                                                                     |        |
| Rx    | <a name="modifyBearerResponse64>modifyBearerResponse64</a>                                              | Количество сообщений GTP: Modify Bearer Response с причиной "#64 Context Not Found" за гранулярный период.                                                                                                  |        |
| Rx    | <a name="modifyBearerResponse68>modifyBearerResponse68</a>                                              | Количество сообщений GTP: Modify Bearer Response с причиной "#68 Service not supported" за гранулярный период.                                                                                              |        |
| Rx    | <a name="modifyBearerResponse70">modifyBearerResponse70</a>                                             | Количество сообщений GTP: Modify Bearer Response с причиной "#70 Mandatory IE missing" за гранулярный период.                                                                                               |        |
| Rx    | <a name="modifyBearerResponse72">modifyBearerResponse72</a>                                             | Количество сообщений GTP: Modify Bearer Response с причиной "#72 System failure" за гранулярный период.                                                                                                     |        |
| Rx    | <a name="deleteBearerRequest">deleteBearerRequest</a>                                                   | Количество сообщений GTP: Delete Bearer Request за гранулярный период.                                                                                                                                      |        |
| Tx    | <a name="deleteBearerResponse">deleteBearerResponse</a>                                                 | Количество сообщений GTP: Delete Bearer Response за гранулярный период.                                                                                                                                     |        |
| Tx    | <a name="createSessionRequest">createSessionRequest</a>                                                 | Количество сообщений GTP: Create Session Request за гранулярный период.                                                                                                                                     |        |
| Rx    | <a name="createSessionResponse">createSessionResponse</a>                                               | Количество сообщений GTP: Create Session Response за гранулярный период.                                                                                                                                    |        |
| Tx    | <a name="deleteSessionRequest">deleteSessionRequest</a>                                                 | Количество сообщений GTP: Delete Session Request за гранулярный период.                                                                                                                                     |        |
| Rx    | <a name="deleteSessionResponse">deleteSessionResponse</a>                                               | Количество сообщений GTP: Delete Session Response за гранулярный период.                                                                                                                                    |        |
| Tx    | <a name="bearerResourceCommand">bearerResourceCommand</a>                                               | Количество сообщений GTP: Bearer Resource Command за гранулярный период.                                                                                                                                    |        |
| Rx    | <a name="bearerResourceFailureIndication">bearerResourceFailureIndication</a>                           | Количество сообщений GTP: Bearer Resource Failure Indication за гранулярный период.                                                                                                                         |        |
| Rx    | <a name="downlinkDataNotification">downlinkDataNotification</a>                                         | Количество сообщений GTP: Downlink Data Notification за гранулярный период.                                                                                                                                 |        |
| Tx    | <a name="downlinkDataNotificationAck">downlinkDataNotificationAck</a>                                   | Количество сообщений GTP: Downlink Data Notification ACK за гранулярный период.                                                                                                                             |        |
| Tx    | <a name="downlinkDataNotificationFailureInd">downlinkDataNotificationFailureInd</a>                     | Количество сообщений GTP: Downlink Data Notification Failure Indication за гранулярный период.                                                                                                              |        |
| Rx    | <a name="updateBearerRequest">updateBearerRequest</a>                                                   | Количество сообщений GTP: Update Bearer Request за гранулярный период.                                                                                                                                      |        |
| Tx    | <a name="updateBearerResponse">updateBearerResponse</a>                                                 | Количество сообщений GTP: Update Bearer Response за гранулярный период.                                                                                                                                     |        |
| Tx    | <a name="deleteBearerCommand">deleteBearerCommand</a>                                                   | Количество сообщений GTP: Delete Bearer Command за гранулярный период.                                                                                                                                      |        |
| Rx    | <a name="deleteBearerFailureIndication">deleteBearerFailureIndication</a>                               | Количество сообщений GTP: Delete Bearer Failure Indication за гранулярный период.                                                                                                                           |        |
| Tx    | <a name="createIndirectDataForwardingTunnelRequest">createIndirectDataForwardingTunnelRequest</a>       | Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Request за гранулярный период.                                                                                                             |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse">createIndirectDataForwardingTunnelResponse</a>     | Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Response за гранулярный период.                                                                                                            |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse16">createIndirectDataForwardingTunnelResponse16</a> | Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Response с причиной "#16 Request accepted" за гранулярный период.                                                                          |        |
| Rx    | <a name="createIndirectDataForwardingTunnelResponse17">createIndirectDataForwardingTunnelResponse17</a> | Количество сообщений GTP: Create Indirect Data Forwarding Tunnel Response с причиной "#17 Request accepted partially" за гранулярный период.                                                                |        |
| Tx    | <a name="deleteIndirectDataForwardingTunnelRequest">deleteIndirectDataForwardingTunnelRequest</a>       | Количество сообщений GTP: Delete Indirect Data Forwarding Tunnel Request за гранулярный период.                                                                                                             |        |
| Rx    | <a name="deleteIndirectDataForwardingTunnelResponse">deleteIndirectDataForwardingTunnelResponse</a>     | Количество сообщений GTP: Delete Indirect Data Forwarding Tunnel Response за гранулярный период.                                                                                                            |        |
| Rx    | <a name="deleteIndirectDataForwardingTunnelResponse16">deleteIndirectDataForwardingTunnelResponse16</a> | Количество сообщений GTP: Delete Indirect Data Forwarding Tunnel Response с причиной "#16 Request accepted" за гранулярный период.                                                                          |        |
| Tx    | <a name="modifyAccessBearersRequest">modifyAccessBearersRequest</a>                                     | Количество сообщений GTP: Modify Access Bearers Request за гранулярный период.                                                                                                                              |        |
| Rx    | <a name="modifyAccessBearersResponse">modifyAccessBearersResponse</a>                                   | Количество сообщений GTP: Modify Access Bearers Response за гранулярный период.                                                                                                                             |        |
| Tx    | <a name="releaseAccessBearersRequest">releaseAccessBearersRequest</a>                                   | Количество сообщений GTP: Release Access Bearers Request за гранулярный период.                                                                                                                             |        |
| Rx    | <a name="releaseAccessBearersResponse">releaseAccessBearersResponse</a>                                 | Количество сообщений GTP: Release Access Bearers Response в процедурах S1 Release за гранулярный период.                                                                                                    |        |
| Tx    | <a name="modifyBearerCommand">modifyBearerCommand</a>                                                   | Количество сообщений GTP: Modify Bearer Command в процедурах HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) или при `SQCI = 1` в сообщении Context Response за гранулярный период. |        |
| Rx    | <a name="modifyBearerFailureIndication">modifyBearerFailureIndication</a>                               | Количество сообщений GTP: Modify Bearer Failure Indication, отправленных при ошибке в процедуре HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) за гранулярный период.              |        |
| Tx    | <a name="suspendNotification">suspendNotification</a>                                                   | Количество сообщений GTP: Suspend Notification в процедурах CSFB за гранулярный период.                                                                                                                     |        |
| Rx    | <a name="suspendAcknowledge">suspendAcknowledge</a>                                                     | Количество сообщений GTP: Suspend Notification ACK в процедурах CSFB за гранулярный период.                                                                                                                 |        |

**Примечание.** Описание причин ошибок GTP см. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf).

#### Пример файла ####

```csv
rx,createBearerRequest,,0
tx,createBearerResponse,,0
tx,bearerResourceCommand,,0
rx,bearerResourceFailureIndication,,0
tx,modifyBearerRequest,,6
rx,modifyBearerResponse,,6
tx,modifyBearerResponse16,,6
rx,deleteBearerRequest,,1
tx,deleteBearerResponse,,1
tx,modifyBearerCommand,,0
rx,modifyBearerFailureIndication,,0
rx,updateBearerRequest,,0
tx,updateBearerResponse,,0
tx,deleteBearerCommand,,0
rx,deleteBearerFailureIndication,,0
tx,releaseAccessBearersRequest,,7
rx,releaseAccessBearersResponse,,7
tx,createSessionRequest,,1
rx,createSessionResponse,,1
tx,deleteSessionRequest,,1
rx,deleteSessionResponse,,1
rx,downlinkDataNotification,,5
tx,downlinkDataNotificationAck,,4
rx,downlinkDataNotificationFailureInd,,1
tx,createIndirectDataForwardingTunnelRequest,,0
rx,createIndirectDataForwardingTunnelResponse,,0
tx,deleteIndirectDataForwardingTunnelRequest,,0
rx,deleteIndirectDataForwardingTunnelResponse,,0
tx,suspendNotification,,0
rx,suspendAcknowledge,,0
```