---
title: "S1AP Interface"
description: "Статистика процедур S1AP"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур S1AP.

### Описание параметров ###

Подробную информацию см. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf). 

| Tx/Rx | Метрика                                                                                                                 | Описание                                                                                           | Группа                 |
|-------|-------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|------------------------|
| Rx    | <a name="s1SetupRequest">s1SetupRequest </a>                                                                            | Количество сообщений S1 Setup Request.                                                             | TAI:Value              |
| Tx    | <a name="s1SetupSuccess">s1SetupSuccess </a>                                                                            | Количество сообщений S1 Setup Response.                                                            | TAI:Value              |
| Rx    | <a name="eNodeBInitS1ResetRequest">eNodeBInitS1ResetRequest</a>                                                         | Количество сообщений S1 Reset Request (eNB-initiated).                                             | TAI:Value              |
| Tx    | <a name="eNodeBInitS1ResetSuccess">eNodeBInitS1ResetSuccess </a>                                                        | Количество сообщений S1 Reset Acknowledge (eNB-initiated).                                         | TAI:Value              |
| Rx    | <a name="eNbConfigurationUpdateRequest">eNbConfigurationUpdateRequest</a>                                               | Количество сообщений S1AP: ENB CONFIGURATION UPDATE REQUEST.                                       | TAI:Value              |
| Tx    | <a name="eNbConfigurationUpdateSuccess">eNbConfigurationUpdateSuccess</a>                                               | Количество сообщений S1AP: ENB CONFIGURATION UPDATE ACKNOWLEDGE.                                   | TAI:Value              |
| Tx    | <a name="mmeConfigurationUpdateRequest">mmeConfigurationUpdateRequest</a>                                               | Количество сообщений S1AP: MME CONFIGURATION UPDATE REQUEST.                                       | TAI:Value              |
| Rx    | <a name="mmeConfigurationUpdateSuccess">mmeConfigurationUpdateSuccess</a>                                               | Количество сообщений S1AP: MME CONFIGURATION UPDATE ACKNOWLEDGE.                                   | TAI:Value              |
| Tx    | <a name="eRabSetupRequest">eRabSetupRequest</a>                                                                         | Количество сообщений S1AP: E-RAB SETUP REQUEST.                                                    | TAI:Value              |
| Rx    | <a name="eRabSetupResponse">eRabSetupResponse</a>                                                                       | Количество сообщений S1AP: E-RAB SETUP RESPONSE.                                                   | TAI:Value              |
| Rx    | <a name="eRabSetupResponseRadioResourcesNotAvailable">eRabSetupResponseRadioResourcesNotAvailable</a>                   | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Radio resources not available".        | TAI:Value              |
| Rx    | <a name="eRabSetupResponseFailureInTheRadioInterfaceProcedure">eRabSetupResponseFailureInTheRadioInterfaceProcedure</a> | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Failure in Radio Interface Procedure". | TAI:Value              |
| Rx    | <a name="eRabSetupResponseTransportResourceUnavailable">eRabSetupResponseTransportResourceUnavailable</a>               | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Transport Resource Unavailable".       | TAI:Value              |
| Rx    | <a name="eRabSetupResponseInvalidQosCombination">eRabSetupResponseInvalidQosCombination</a>                             | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Invalid QoS combination".              | TAI:Value              |
| Rx    | <a name="eRabSetupResponseNotSupportedQciValue">eRabSetupResponseNotSupportedQciValue</a>                               | Количество сообщений S1AP: E-RAB SETUP RESPONSE с причиной "Not supported QCI Value".              | TAI:Value              |
| Tx    | <a name="eRabModifyRequest">eRabModifyRequest</a>                                                                       | Количество сообщений S1AP: E-RAB MODIFY REQUEST.                                                   | TAI:Value              |
| Rx    | <a name="eRabModifyResponse">eRabModifyResponse</a>                                                                     | Количество сообщений S1AP: E-RAB MODIFY RESPONSE.                                                  | TAI:Value              |
| Tx    | <a name="eRabReleaseCommand">eRabReleaseCommand</a>                                                                     | Количество сообщений S1AP: E-RAB RELEASE COMMAND.                                                  | TAI:Value              |
| Rx    | <a name="eRabReleaseResponse">eRabReleaseResponse</a>                                                                   | Количество сообщений S1AP: E-RAB RELEASE RESPONSE.                                                 | TAI:Value              |
| Rx    | <a name="eRabReleaseIndication">eRabReleaseIndication</a>                                                               | Количество сообщений S1AP: E-RAB RELEASE INDICATION.                                               | TAI:Value              |
| Rx    | <a name="eRabModificationIndication">eRabModificationIndication</a>                                                     | Количество сообщений S1AP: E-RAB MODIFICATION INDICATION.                                          | TAI:Value              |
| Tx    | <a name="eRabModificationConfirm">eRabModificationConfirm</a>                                                           | Количество сообщений S1AP: E-RAB MODIFICATION CONFIRM.                                             | TAI:Value              |
| Tx    | <a name="initialContextSetupRequest">initialContextSetupRequest</a>                                                     | Количество сообщений S1AP: INITIAL CONTEXT SETUP REQUEST.                                          | TAI:Value              |
| Rx    | <a name="initialContextSetupResponse">initialContextSetupResponse</a>                                                   | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE.                                         | TAI:Value              |
| Rx    | <a name="initialContextSetupFailure">initialContextSetupFailure</a>                                                     | Количество сообщений S1AP: INITIAL CONTEXT SETUP FAILURE.                                          | TAI:Value, Cause:Value |
| Rx    | <a name="ueContextReleaseRequest">ueContextReleaseRequest</a>                                                           | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST.                                             | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequestUeInactive">ueContextReleaseRequestUeInactive</a>                                       | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "User Inactivity".                | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequestCsfbTriggered">ueContextReleaseRequestTimesCsfbTriggered</a>                            | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "CS Fallback Triggered".          | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequestNormalRelease">ueContextReleaseRequestNormalRelease</a>                                 | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "Normal Release".                 | TAI:Value              |
| Rx    | <a name="ueContextReleaseRequestDetach">ueContextReleaseRequestDetach</a>                                               | Количество сообщений S1AP: UE CONTEXT RELEASE REQUEST с причиной "Detach".                         | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommand">ueContextReleaseCommand</a>                                                           | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND.                                             | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommandSuccessfulHandover">ueContextReleaseCommandSuccessfulHandover</a>                       | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с причиной "Successful Handover".            | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommandDetach">ueContextReleaseCommandDetach</a>                                               | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с причиной "Detach".                         | TAI:Value              |
| Tx    | <a name="ueContextReleaseCommandNormalRelease">ueContextReleaseCommandNormalRelease</a>                                 | Количество сообщений S1AP: UE CONTEXT RELEASE COMMAND с причиной "Normal Release".                 | TAI:Value              |
| Rx    | <a name="ueContextReleaseComplete">ueContextReleaseComplete</a>                                                         | Количество сообщений S1AP: UE CONTEXT RELEASE COMPLETE.                                            | TAI:Value              |
| Tx    | <a name="ueContextModificationRequest">ueContextModificationRequest</a>                                                 | Количество сообщений S1AP: UE CONTEXT MODIFICATION REQUEST.                                        | TAI:Value              |
| Rx    | <a name="ueContextModificationResponse">ueContextModificationResponse</a>                                               | Количество сообщений S1AP: UE CONTEXT MODIFICATION RESPONSE.                                       | TAI:Value              |
| Rx    | <a name="ueContextModificationFailure">ueContextModificationFailure</a>                                                 | Количество сообщений S1AP: UE CONTEXT MODIFICATION FAILURE.                                        | TAI:Value              |
|       | <a name="numberOfEnodeb">numberOfEnodeb</a>                                                                             | Количество eNodeB.                                                                                 | TAI:Value              |
| Tx    | <a name="overloadStart">overloadStart</a>                                                                               | Количество сообщений S1AP: OVERLOAD START.                                                         | TAI:Value              |
| Tx    | <a name="overloadStop">overloadStop</a>                                                                                 | Количество сообщений S1AP: OVERLOAD STOP.                                                          | TAI:Value              |

**Примечание.** Описание причин ошибок S1AP см. [3GPP TS 38.413](https://www.etsi.org/deliver/etsi_ts/138400_138499/138413/17.05.00_60/ts_138413v170500p.pdf).

### Группа ###

| Название    | Описание                                                         | Тип    |
|-------------|------------------------------------------------------------------|--------|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex    |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int    |
| \<tac\>     | Код области отслеживания.                                        | hex    |
| Cause       | Имя ошибки.                                                      | string |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
rx,s1SetupRequest,,12
rx,s1SetupRequest,TAI:001010001,3
rx,s1SetupRequest,TAI:001010010,4
rx,s1SetupRequest,TAI:208930001,4
rx,s1SetupRequest,TAI:460002009,1
tx,s1SetupSuccess,,12
tx,s1SetupSuccess,TAI:001010001,3
tx,s1SetupSuccess,TAI:001010010,4
tx,s1SetupSuccess,TAI:208930001,4
tx,s1SetupSuccess,TAI:460002009,1
rx,eNodeBInitS1ResetRequest,,0
tx,eNodeBInitS1ResetSuccess,,0
rx,eNbConfigurationUpdateRequest,,0
tx,eNbConfigurationUpdateSuccess,,0
tx,mmeConfigurationUpdateRequest,,0
rx,mmeConfigurationUpdateSuccess,,0
tx,eRabSetupRequest,,0
tx,eRabModifyRequest,,0
rx,eRabModifyResponse,,0
tx,eRabReleaseCommand,,0
rx,eRabReleaseResponse,,0
rx,eRabModificationIndication,,0
tx,eRabModificationConfirm,,0
tx,initialContextSetupRequest,,83
tx,initialContextSetupRequest,TAI:001010001,15
tx,initialContextSetupRequest,TAI:001010010,33
tx,initialContextSetupRequest,TAI:466920051,29
tx,initialContextSetupRequest,TAI:999990001,6
rx,initialContextSetupResponse,,83
rx,initialContextSetupResponse,TAI:001010001,15
rx,initialContextSetupResponse,TAI:001010010,33
rx,initialContextSetupResponse,TAI:466920051,29
rx,initialContextSetupResponse,TAI:999990001,6
rx,initialContextSetupFailure,,0
rx,ueContextReleaseRequest,,92
rx,ueContextReleaseRequest,TAI:001010001,14
rx,ueContextReleaseRequest,TAI:001010010,34
rx,ueContextReleaseRequest,TAI:208930001,9
rx,ueContextReleaseRequest,TAI:466920051,29
rx,ueContextReleaseRequest,TAI:999990001,6
rx,ueContextReleaseRequest1_20,,90
rx,ueContextReleaseRequest1_20,TAI:001010001,14
rx,ueContextReleaseRequest1_20,TAI:001010010,33
rx,ueContextReleaseRequest1_20,TAI:208930001,8
rx,ueContextReleaseRequest1_20,TAI:466920051,29
rx,ueContextReleaseRequest1_20,TAI:999990001,6
rx,ueContextReleaseRequest1_21,,2
rx,ueContextReleaseRequest1_21,TAI:001010010,1
rx,ueContextReleaseRequest1_21,TAI:208930001,1
tx,ueContextReleaseCommand,,99
tx,ueContextReleaseCommand,TAI:001010001,16
tx,ueContextReleaseCommand,TAI:001010010,35
tx,ueContextReleaseCommand,TAI:208930001,13
tx,ueContextReleaseCommand,TAI:466920051,29
tx,ueContextReleaseCommand,TAI:999990001,6
tx,ueContextReleaseCommand1_20,,90
tx,ueContextReleaseCommand1_20,TAI:001010001,14
tx,ueContextReleaseCommand1_20,TAI:001010010,33
tx,ueContextReleaseCommand1_20,TAI:208930001,8
tx,ueContextReleaseCommand1_20,TAI:466920051,29
tx,ueContextReleaseCommand1_20,TAI:999990001,6
tx,ueContextReleaseCommand1_21,,2
tx,ueContextReleaseCommand1_21,TAI:001010010,1
tx,ueContextReleaseCommand1_21,TAI:208930001,1
tx,ueContextReleaseCommand3_0,,6
tx,ueContextReleaseCommand3_0,TAI:001010001,2
tx,ueContextReleaseCommand3_0,TAI:001010010,1
tx,ueContextReleaseCommand3_0,TAI:208930001,3
tx,ueContextReleaseCommand3_1,,1
tx,ueContextReleaseCommand3_1,TAI:208930001,1
rx,ueContextReleaseComplete,,99
rx,ueContextReleaseComplete,TAI:001010001,16
rx,ueContextReleaseComplete,TAI:001010010,35
rx,ueContextReleaseComplete,TAI:208930001,13
rx,ueContextReleaseComplete,TAI:466920051,29
rx,ueContextReleaseComplete,TAI:999990001,6
tx,ueContextModificationRequest,,0
rx,ueContextModificationResponse,,0
rx,ueContextModificationFailure,,0
```
