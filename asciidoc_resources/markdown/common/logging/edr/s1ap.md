---
title: "S1AP EDR"
description: "Журнал s1ap_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                                | Описание                                                | Тип       |
|-----|-------------------------------------|---------------------------------------------------------|-----------|
| 1   | DateTime                            | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime  |
| 2   | [Event](#cобытия-и-процедуры-event) | Событие или процедура, сформировавшая запись.           | string    |
| 3   | eNodeB UE ID                        | Идентификатор S1-контекста на стороне eNodeB.           | int       |
| 4   | MME UE ID                           | Идентификатор S1-контекста на узле MME.                 | int       |
| 5   | IMSI                                | Номер IMSI абонента.                                    | string    |
| 6   | MSISDN                              | Номер MSISDN абонента.                                  | string    |
| 7   | GUTI                                | Глобальный уникальный временный идентификатор абонента. | string    |
| 8   | PLMN                                | Идентификатор PLMN.                                     | string    |
| 9   | CellID                              | Идентификатор соты.                                     | int/hex   |
| 10  | IMEI                                | Номер IMEI устройства.                                  | string    |
| 11  | Duration                            | Длительность процедуры.                                 | int<br>мс |
| 12  | ErrorCode                           | Внутренний [код MME](../error_code/) результата.        | int       |

#### События и процедуры, Event ####

- [Authentication](#authentication);
- [Bearer Resource Modification](#bearer-resource-modification);
- [Deactivate Bearer](#deactivate-bearer);
- [E-RAB Modification](#e-rab-modification);
- [GUTI Reallocation](#guti-reallocation);
- [Initial Context Setup](#initial-context-setup);
- [MT Data Transport](#mt-data-transport);
- [MO Data Transport](#mo-data-transport);
- [Modify EPS bearer context](#modify-eps-bearer-context);
- [PDN Disconnection](#pdn-disconnection);
- [Secondary RAT Data Usage Report](#secondary-rat-data-usage-report);
- [UE Context Modification](#ue-context-modification);
- [Trace Failure Indication](#trace-failure-indication).

#### Authentication ####

```log
DateTime,Authentication,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                       |
|-----|------------------------------------------------------------------------------------------------|
| 1   | Неуспешное сообщение Diameter: Authentication-Information-Answer.                              |
| 2   | В сообщении Diameter: Authentication-Information-Answer отсутствуют данные для аутентификации. |
| 3   | Полученный ключ <abbr title="Key Access Security Management Entries">KASME</abbr> пуст.        |
| 4   | Получено сообщение S1 Security Mode Reject.                                                    |
| 5   | Невозможно извлечь номер IMSI из сообщения Identity Response.                                  |
| 6   | Несовпадение полученного RES с ожидаемым XRES.                                                 |
| 7   | Ошибка <abbr title="Message Authentication Code">MAC</abbr>.                                   |
| 8   | Не удалось определить алгоритм шифрования.                                                     |
| 9   | Не удалось определить алгоритм контроля целостности.                                           |
| 10  | Недопустимое значение UE-Usage-Type.                                                           |

#### Bearer Resource Modification ####

```log
DateTime,Bearer Resource Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                             |
|-----|--------------------------------------------------------------------------------------|
| 1   | Абонент находится в состоянии <abbr title="EPC Mobility Management">EMM</abbr>-IDLE. |
| 2   | Не указана ни одна bearer-служба.                                                    |
| 3   | Запрошена неизвестная bearer-служба.                                                 |
| 4   | Получен отказ от узла SGW.                                                           |
| 5   | Ошибка вспомогательной логики.                                                       |

#### Deactivate Bearer ####

```log
DateTime,Deactivate Bearer,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                            |
|-----|-------------------------------------|
| 7   | Ошибка процедуры Paging.            |
| 8   | Получен отказ от станции eNodeB.    |
| 9   | Указанная bearer-служба не найдена. |

#### E-RAB Modification ####

```log
DateTime,E-RAB Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                             |
|-----|------------------------------------------------------|
| 1   | Не найдены подходящие для модификации bearer-службы. |

#### GUTI Reallocation ####

```log
DateTime,GUTI Reallocation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Initial Context Setup ####

```log
DateTime,Initial Context Setup,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,CauseType,CauseReason,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                 |
|-----|------------------------------------------|
| 1   | Отсутствуют bearer-службы для активации. |
| 2   | Получено сообщение ICS Failure.          |

#### MT Data Transport ####

```log
DateTime,MT Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### MO Data Transport ####

```log
DateTime,MO Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                  |
|-----|-------------------------------------------|
| 1   | Запрошена неизвестная bearer-служба.      |
| 2   | Ошибка модификации bearer-службы.         |
| 3   | Отсутствует значение User Data Container. |

#### Modify EPS Bearer Context ####

```log
DateTime,Modify EPS Bearer Context,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Bearer ID,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                         |
|-----|----------------------------------|
| 11  | Bearer-служба отвергнута eNodeB. |
| 12  | Bearer-служба отвергнута UE.     |

#### PDN Disconnection ####

```log
DateTime,PDN Disconnection,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,APN,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                        |
|-----|---------------------------------------------------------------------------------|
| 1   | Не удалось декодировать сообщение PDN Disconnect Request.                       |
| 2   | Не удалось извлечь связанную bearer-службу из сообщения PDN Disconnect Request. |

#### Secondary RAT Data Usage Report ####

```log
DateTime,Secondary RAT Data Usage Report,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### UE Context Modification ####

```log
DateTime,UE Context Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Trace Failure Indication ####

```log
DateTime,Trace Failure Indication,ENB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```
