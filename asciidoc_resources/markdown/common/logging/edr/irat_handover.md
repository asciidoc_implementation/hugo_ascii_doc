---
title: "Inter-RAT Handover EDR"
description: "Журнал irat_handover_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                                               | Описание                                                                                                                                                                                                                                                  | Тип       |
|-----|----------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| 1   | DateTime                                           | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                                                                                                                                   | datetime  |
| 2   | [Event](#cобытия-и-процедуры-event)                | Событие или процедура, сформировавшая запись.                                                                                                                                                                                                             | string    |
| 3   | eNodeB UE ID                                       | Идентификатор S1-контекста на стороне исходного eNodeB.                                                                                                                                                                                                   | int       |
| 4   | MME UE ID                                          | Идентификатор S1-контекста на узле MME.                                                                                                                                                                                                                   | int       |
| 6   | IMSI                                               | Номер IMSI абонента.                                                                                                                                                                                                                                      | string    |
| 7   | MSISDN                                             | Номер MSISDN абонента.                                                                                                                                                                                                                                    | string    |
| 8   | GUTI                                               | Глобальный уникальный временный идентификатор абонента.                                                                                                                                                                                                   | string    |
| 9   | IMEI                                               | Номер IMEI устройства.                                                                                                                                                                                                                                    | string    |
| 10  | Source PLMN                                        | Исходная PLMN.                                                                                                                                                                                                                                            | string    |
| 11  | Source TAC                                         | Исходный код области отслеживания.                                                                                                                                                                                                                        | string    |
| 12  | Source CellID                                      | Идентификатор исходной соты.                                                                                                                                                                                                                              | string    |
| 13  | eNodeB IP                                          | IP-адрес исходного eNodeB.                                                                                                                                                                                                                                | ip        |
| 14  | Source SGW IP                                      | IP-адрес исходного узла SGW.                                                                                                                                                                                                                              | ip        |
| 15  | Target PLMN                                        | Идентификатор PLMN назначения.                                                                                                                                                                                                                            | string    |
| 16  | Target TAC                                         | Код области отслеживания назначения.                                                                                                                                                                                                                      | int       |
| 17  | Target LAC                                         | Код локальной области назначения.                                                                                                                                                                                                                         | int       |
| 18  | Target CellID                                      | Идентификатор соты назначения.                                                                                                                                                                                                                            | int/hex   |
| 19  | Target RAC                                         | Код зоны маршрутизации назначения.                                                                                                                                                                                                                        | int       |
| 20  | Target RNC Id                                      | Идентификатор <abbr title="Radio Network Controller">RNC</abbr> назначения.                                                                                                                                                                               | int       |
| 21  | PS supported                                       | Флаг поддержки хэндовера для PS-домена при исходящем <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>.<br>**Примечание.** См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.04.00_60/ts_136413v170400p.pdf). | bool      |
| 24  | Duration                                           | Длительность процедуры.                                                                                                                                                                                                                                   | int<br>мс |
| 25  | [Event Code](#cобытия-и-процедуры-event):ErrorCode | Код ошибки + Внутренний [код MME](../error_code/) результата.                                                                                                                                                                                             | object    |

#### События и процедуры, Event #####

* 1 - [HO to GERAN](#ho-to-geran);
* 2 - [HO to UTRAN](#ho-to-utran);
* 3 - [SRVCC HO to GERAN](#srvcc-ho-to-geran);
* 4 - [SRVCC HO to UTRAN](#srvcc-ho-from-e-utran-to-utran);
* 5 - [HO from UTRAN](#ho-from-utran);
* 6 - [HO from GERAN](#ho-from-geran);
* 7 - [SRVCC HO from UTRAN/GERAN](#srvcc-ho-from-utran-geran).

#### HO to GERAN ####

```log
DateTime,HO to GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,1:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                                                  |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Не удалось определить IP-адрес узла SGSN.                                                                                                 |
| 2   | Узел SGSN отказал в подготовке хэндовера.                                                                                                 |
| 3   | Узел SGSN отказал в переносе данных абонента.                                                                                             |
| 4   | Получен отказ в хэндовере.                                                                                                                |
| 5   | Параметр Access Restriction Data запрещает использование сетей GERAN.                                                                     |
| 6   | Попытка хэндовера из области <abbr title="Tracking Area Code">TAC</abbr> сети <abbr title="Narrow Band Internet of Things">NB-IoT</abbr>. |

#### HO to UTRAN</a> ####

```log
DateTime,HO to UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,2:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                                                  |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Не удалось определить IP-адрес узла SGSN.                                                                                                 |
| 2   | Узел SGSN отказал в подготовке хэндовера.                                                                                                 |
| 3   | Узел SGSN отказал в переносе данных абонента.                                                                                             |
| 4   | Получен отказ в хэндовере.                                                                                                                |
| 5   | Параметр Access Restriction Data запрещает использование сетей GERAN.                                                                     |
| 6   | Попытка хэндовера из области <abbr title="Tracking Area Code">TAC</abbr> сети <abbr title="Narrow Band Internet of Things">NB-IoT</abbr>. |
| 7   | Слишком длинный UTRAN Transparent Container.                                                                                              |

#### SRVCC HO to GERAN ####

```log
DateTime,SRVCC HO to GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,3:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                               |
|-----|--------------------------------------------------------------------------------------------------------|
| 1   | Не найдена голосовая bearer-служба.                                                                    |
| 2   | Не удалось определить IP-адрес сервера MSC.                                                            |
| 3   | Не удалось определить IP-адрес узла SGSN.                                                              |
| 4   | Параметр Access Restriction Data запрещает использование сетей GERAN.                                  |
| 5   | Получен отказ от сервера MSC.                                                                          |
| 6   | Отсутствует сообщение Suspend Notification.                                                            |
| 7   | Отсутствует сообщение <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr> PS to CS Complete. |
| 8   | Получен отказ в хэндовере.                                                                             |
| 9   | Узел SGSN отказал в подготовке хэндовера.                                                              |

#### SRVCC HO to UTRAN ####

```log
DateTime,SRVCC HO to UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC, Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,4:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                           |
|-----|--------------------------------------------------------------------------------------------------------------------|
| 1   | Не найдена голосовая bearer-служба.                                                                                |
| 2   | Не удалось определить IP-адрес сервера MSC.                                                                        |
| 3   | Не удалось определить IP-адрес узла SGSN.                                                                          |
| 4   | Параметр Access Restriction Data запрещает использование сетей UTRAN.                                              |
| 5   | Получен отказ от сервера MSC.                                                                                      |
| 7   | Не найдено сообщение <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr> PS to CS Complete Notification. |
| 8   | Получен отказ в хэндовере.                                                                                         |
| 9   | Узел SGSN отказал в подготовке хэндовера.                                                                          |

#### HO from UTRAN ####

```log
DateTime,HO from UTRAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,5:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                                                  |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Получен отказ в хэндовере.                                                                                                                |
| 2   | SGW назначения не принял создание сессии.                                                                                                 |
| 3   | Нет данных о eNodeB назначения.                                                                                                           |
| 4   | Получено сообщение HANDOVER FAILURE.                                                                                                      |
| 5   | eNodeB не приняла ни одной bearer-службы по умолчанию.                                                                                    |
| 6   | Не найдено сообщение HANDOVER NOTIFY.                                                                                                     |
| 7   | Не найдено сообщение Forward Relocation Complete Acknowledge.                                                                             |
| 8   | Ошибка процедуры Modify-Bearer.                                                                                                           |
| 9   | Не найдено сообщение TAU-Request.                                                                                                         |
| 10  | Не удалось извлечь идентификаторы IMSI, <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr>.       |
| 11  | Попытка хэндовера из области <abbr title="Tracking Area Code">TAC</abbr> сети <abbr title="Narrow Band Internet of Things">NB-IoT</abbr>. |

#### HO from GERAN ####

```log
DateTime,HO from GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,6:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                                            |
|-----|-------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Получен отказ в хэндовере.                                                                                                          |
| 2   | SGW назначения не принял создание сессии.                                                                                           |
| 3   | Нет данных о eNodeB назначения.                                                                                                     |
| 4   | Получено сообщение HANDOVER FAILURE.                                                                                                |
| 5   | eNodeB не приняла ни одной bearer-службы по умолчанию.                                                                              |
| 6   | Не найдено сообщение HANDOVER NOTIFY.                                                                                               |
| 7   | Не найдено сообщение Forward Relocation Complete Acknowledge.                                                                       |
| 8   | Ошибка процедуры Modify-Bearer.                                                                                                     |
| 9   | Не найдено сообщение TAU-Request.                                                                                                   |
| 10  | Не удалось извлечь идентификаторы IMSI, <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr>. |

#### SRVCC HO from UTRAN/GERAN ####

```log
DateTime,SRVCC HO from UTRAN/GERAN,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,Source PLMN,Source TAC,Source CellID,eNodeB IP,Target PLMN,Target TAC,Target LAC,Target CellID,Target RAC,Target RNC Id,PS supported,Duration,7:ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                                            |
|-----|-------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Не удалось определить адрес предыдущего узла MME/SGSN.                                                                              |
| 2   | Нет данных о eNodeB назначения.                                                                                                     |
| 3   | Ошибка получения предыдущего контекста.                                                                                             |
| 4   | Ошибка на новом узле SGW.                                                                                                           |
| 5   | eNodeB назначения не приняла ни одной bearer-службы.                                                                                |
| 6   | Не получено сообщение HANDOVER NOTIFY.                                                                                              |
| 7   | Не получено сообщение <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr> CS to PS Complete Acknowledgement.              |
| 8   | Ошибка модификации bearer-служб.                                                                                                    |
| 9   | В сообщении <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr> CS to PS Request отсутствуют данные об eNodeB назначения. |
| 10  | Получено сообщение HANDOVER FAILURE.                                                                                                |
| 11  | Не получено сообщение HANDOVER REQUEST ACKNOWLEDGEMENT.                                                                             |
| 12  | Получено сообщение <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr> CS to PS Cancel Notification.                      |
