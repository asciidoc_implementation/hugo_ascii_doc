---
title: "eNodeB EDR"
description: "Журнал enodeb_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                                | Описание                                                                                                 | Тип         |
|-----|-------------------------------------|----------------------------------------------------------------------------------------------------------|-------------|
| 1   | DateTime                            | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                  | datetime    |
| 2   | [Event](#cобытия-и-процедуры-event) | Событие или процедура, сформировавшая запись.                                                            | string      |
| 3   | eNodeB ID                           | Глобальный уникальный идентификатор базовой станции.                                                     | int         |
| 4   | eNodeB Name                         | Имя базовой станции.                                                                                     | string      |
| 5   | PLMN List                           | Идентификаторы обслуживаемых <abbr title="Public Landing Mobile Network">PLMN</abbr> на базовой станции. | list\<int\> |
| 6   | TAC List                            | Идентификаторы обслуживаемых <abbr title="Tracking Area Code">TAC</abbr> на базовой станции.             | list\<int\> |
| 7   | eNodeB IP                           | IP-адрес базовой станции S1-интерфейсе.                                                                  | ip          |
| 8   | Duration                            | Длительность процедуры.                                                                                  | int<br>мс   |
| 9   | ErrorCode                           | Внутренний [код MME](../error_code/) результата.                                                         | int         |

#### События и процедуры, Event ####

- [eNodeB Configuration Update](#enodeb-configuration-update);
- [MME Configuration Update](#mme-configuration-update);
- [eNodeB Configuration Transfer](#enodeb-configuration-transfer);
- [Disconnected](#disconnected);
- [Reset](#reset);
- [S1 Setup](#s1-setup).

#### eNodeB Configuration Update ####

```log
DateTime,eNodeB Configuration Update,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
```

#### MME Configuration Update ####

```log
DateTime,MME Configuration Update,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
```

#### eNodeB Configuration Transfer ####

```log
DateTime,eNodeB Configuration Transfer,eNodeB ID,eNodeB Name,PLMN List,TAC List,eNodeB IP,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                              |
|-----|---------------------------------------|
| 1   | eNodeB назначения отключена.          |
| 2   | Неизвестен IP-адрес другого узла MME. |

#### Disconnected ####

```log
DateTime,Disconnected,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
```

#### Reset ####

```log
DateTime,Reset,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
```

#### S1 Setup ####

```log
DateTime,S1Setup,eNodeB ID,eNodeB Name,[PLMN List],[TAC List],eNodeB IP,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                     |
|-----|--------------------------------------------------------------|
| 1   | Запрещённый идентификатор eNodeB.                            |
| 2   | Все указанные PLMN не разрешены или не поддерживают E-UTRAN. |
