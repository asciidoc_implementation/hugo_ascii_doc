---
title: "HTTP EDR"
description: "Журнал http_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                                | Описание                                                | Тип       |
|-----|-------------------------------------|---------------------------------------------------------|-----------|
| 1   | DateTime                            | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime  |
| 2   | [Event](#cобытия-и-процедуры-event) | Событие или процедура, сформировавшая запись.           | string    |
| 3   | MME UE ID                           | Идентификатор S1-контекста на узле MME.                 | int       |
| 4   | IMSI                                | Номер IMSI абонента.                                    | string    |
| 5   | MSISDN                              | Номер MSISDN абонента.                                  | string    |
| 6   | GUTI                                | Глобальный уникальный временный идентификатор абонента. | string    |
| 7   | PLMN                                | Идентификатор PLMN.                                     | string    |
| 8   | CellID                              | Идентификатор соты.                                     | int/hex   |
| 9   | IMEI                                | Номер IMEI устройства.                                  | string    |
| 10  | Duration                            | Длительность процедуры.                                 | int<br>мс |
| 11  | ErrorCode                           | Внутренний [код MME](../error_code/) результата.        | object    |

#### События и процедуры, Event ####

- [Deact bearer](#deact-bearer);
- [Detach](#detach);
- [Disconnect eNodeB](#disconnect-eNodeB);
- [Get Profile](#get-profile);
- [Get DB Status](#get-db-status);
- [Get GTP Peers](#get-gtp-peers);
- [Get S1 Peers](#get-s1-peers);
- [Get SGs Peers](#get-sgs-peers);
- [Clear DNS Cache](#clear-dns-cache);
- [UE enable trace](#ue-enable-trace);
- [UE disable trace](#ue-disable-trace);
- [Get Metrics](#get-metrics);
- [Reset Metrics](#reset-metrics).

#### Deact Bearer ####

```log
DateTime,Deact bearer,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                         |
|-----|--------------------------------------------------|
| 1   | Не удалось получить идентификатор bearer-службы. |
| 2   | Неизвестный идентификатор bearer-службы.         |
| 3   | Ошибка процедуры Paging.                         |
| 4   | Ошибка деактивации bearer-службы.                |

#### Detach ####

```log
DateTime,Detach,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                 |
|-----|--------------------------|
| 1   | Ошибка процедуры Paging. |
| 2   | Ошибка процедуры Detach. |

#### Disconnect eNodeB ####

```log
DateTime,Disconnect eNodeB,IP,Duration,ErrorCode
```

#### Get Profile ####

```log
DateTime,GetProfile,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Get DB Status ####

```log
DateTime,Get DB Status,Duration,ErrorCode
```

#### Get GTP Peers ####

```log
DateTime,Get GTP Peers,Duration,ErrorCode
```

#### Get S1 Peers ####

```log
DateTime,Get S1 Peers,Duration,ErrorCode
```

#### Get SGs Peers ####

```log
DateTime,Get SGS Peers,Duration,ErrorCode
```

#### Clear DNS Cache ####

```log
DateTime,Clear DNS Cache,Duration,ErrorCode
```

#### UE enable trace ####

```log
DateTime,UE enable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                           |
|-----|--------------------------------------------------------------------|
| 1   | Абонент уже находится в списке отслеживания.                       |
| 2   | Не указан IP-адрес узла <a name="Trace Collection Entity">TCE</a>. |
| 3   | Ошибка процедуры Paging.                                           |
| 4   | Не найдены свободные идентификаторы Trace ID.                      |

#### UE disable trace ####

```log
DateTime,UE disable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                               |
|-----|--------------------------------------------------------|
| 1   | Ошибка процедуры Paging.                               |
| 2   | Идентификатор E-UTRAN Trace ID не найден для абонента. |

#### Get Metrics ####

```log
DateTime,Get Metrics,Duration,ErrorCode
```

#### Reset Metrics ####

```log
DateTime,Reset Metrics,Duration,ErrorCode
```