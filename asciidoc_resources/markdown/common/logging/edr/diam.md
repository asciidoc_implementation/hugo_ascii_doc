---
title: "DIAM EDR"
description: "Журнал diam_cdr"
weight: 20
type: docs
---

Для записей по различным процедурам применяется единообразный формат. Данные представлены в формате CSV: разделитель полей - запятая.

### Описание используемых полей ###

| N   | Поле                                | Описание                                                                                                                                              | Тип       |
|-----|-------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| 1   | Timestamp                           | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                               | datetime  |
| 2   | [Event](#cобытия-и-процедуры-event) | Событие или процедура, сформировавшая запись.                                                                                                         | string    |
| 3   | SessionId                           | Идентификатор сессии, `Session-Id`. См. [RFC 6733](https://tools.ietf.org/html/rfc6733). Формат:<br>`<DiameterIdentity>;<high 32 bits>;<low 32 bits>` | string    |
| 4   | IMSI                                | Номер IMSI абонента в формате [ITU-T E.212](https://www.itu.int/rec/T-REC-E.212-201609-I/en).                                                         | string    |
| 5   | MSISDN                              | Международный номер мобильного абонента, [ITU-T E.164](https://www.itu.int/rec/T-REC-E.164-201011-I/en).                                              | string    |
| 6   | Origin-Host                         | Значение `Origin-Host` для протокола Diameter. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                                   | string    |
| 7   | Origin-Realm                        | Значение `Origin-Realm` для протокола Diameter. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                                  | string    |
| 8   | Dest-Host                           | Значение `Destination-Host` для протокола Diameter. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                              | string    |
| 9   | Dest-Realm                          | Значение `Destination-Realm` для протокола Diameter. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                             | string    |
| 10  | Flags                               | Флаги сообщения Diameter, специфичные для конкретной процедуры.                                                                                       | int       |
| 11  | Duration                            | Длительность процедуры.                                                                                                                               | int<br>мс |
| 12  | Result-Code                         | Значение `Result-Code` для протокола Diameter. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                                   | int       |
| 13  | ErrorCode                           | Внутренний [код MME](../error_code/) результата.                                                                                                      | int       |

#### События и процедуры, Event ####

* [Authentication-Info-Request](#authentication-info-request);
* [Cancel-Location](#cancel-location-request);
* [Update-Location](#update-location-request);
* [Insert-Subscriber-Data-Request](#insert-subscriber-data-request);
* [Delete-Subscriber-Data-Request](#delete-subscriber-data-request);
* [Notify-Request](#notify-request);
* [Purge-Request](#purge-request);
* [ME-Identity-Check-Request](#me-identity-check-request);
* [Reset-Request](#reset-request);
* [MO-Forward-Short-Message-Answer](#mo-forward-short-message-answer);
* [MT-Forward-Short-Message-Answer](#mt-forward-short-message-answer).

#### Authentication-Info-Request ####

```log
DateTime,AI,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### Cancel-Location ####

```log
DateTime,CL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                 |
|-----|--------------------------|
| 1   | Ошибка процедуры Paging. |
| 2   | Ошибка процедуры Detach. |

#### Update-Location ####

```log
DateTime,UL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                                |
|-----|---------------------------------------------------------------------------------------------------------|
| 1   | Получено неожидаемое сообщение по протоколу Diameter вместо сообщения Diameter: Update-Location-Answer. |
| 2   | В сообщении Diameter: Update-Location-Answer отсутствуют информация о подписке.                         |
| 3   | Значение `Access-Restriction-Data` запрещает использование сетей E-UTRAN.                               |
| 4   | Использование значений `QCI` из диапазона 128-254 запрещено.                                            |
| 5   | Получено нестандартное значение <abbr title="QoS Class Identifier">`QCI`</abbr>.                        |
| 6   | В сообщении Diameter: Update-Location-Answer отсутствует номер MSISDN.                                  |
| 7   | Получено сообщение Diameter: Update-Location-Answer с ошибочным значением `Result-Code`.                |
| 8   | Текущий код TAC не входит ни в одну из зон.                                                             |
| 9   | Контекст по умолчанию имеет Wildcard APN.                                                               |

#### Insert-Subscriber-Data-Request ####

```log
DateTime,ISD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                                      |
|-----|-----------------------------------------------------------------------------------------------|
| 1   | Флаг `Current Location Request` активирован, а флаг `EPS Location Information Request` — нет. |
| 2   | Ошибка процедуры Paging.                                                                      |
| 3   | Флаг <abbr title="Paging Proceed Flag">PPF</abbr> не позволяет начать процедуру Paging.       |
| 4   | Данная сеть PLMN не поддерживает процедуры P-CSCF Restoration.                                |

#### Delete-Subscriber-Data-Request ####

```log
DateTime,DSD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                          |
|-----|-------------------------------------------------------------------|
| 1   | Активирован флаг `Complete APN Configuration Profile Withdrawal`. |
| 2   | Не удалось извлечь значение `Context-Identifier`.                 |
| 3   | Не найдено PDN Connectivity с указанным `Context-Identifier`.     |
| 4   | Контекст по умолчанию не может быть удален.                       |

#### Notify-Request ####

```log
DateTime,Notify,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### Purge-Request ####

```log
DateTime,Purge,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### Reset-Request ####

```log
DateTime,Reset,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### ME-Identity-Check-Request ####

```log
DateTime,ECR,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### MO-Forward-Short-Message-Answer ####

```log
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```

#### MT-Forward-Short-Message-Answer ####

```log
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,Flags,Duration,Result-Code,ErrorCode
```
