---
title: "SGsAP EDR"
description: "Журнал sgsap_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                                | Описание                                                | Тип       |
|-----|-------------------------------------|---------------------------------------------------------|-----------|
| 1   | DateTime                            | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime  |
| 2   | [Event](#cобытия-и-процедуры-event) | Событие или процедура, сформировавшая запись.           | string    |
| 3   | MME UE ID                           | Идентификатор S1-контекста на узле MME.                 | string    |
| 4   | IMSI                                | Номер IMSI абонента.                                    | string    |
| 5   | MSISDN                              | Номер MSISDN абонента.                                  | string    |
| 6   | GUTI                                | Глобальный уникальный временный идентификатор абонента. | string    |
| 7   | PLMN                                | Идентификатор PLMN.                                     | string    |
| 8   | LAC                                 | Код локальной области LAC.                              | int       |
| 9   | CellID                              | Идентификатор соты.                                     | int/hex   |
| 10  | IMEI                                | Номер IMEI устройства.                                  | string    |
| 11  | Duration                            | Длительность процедуры.                                 | int<br>мс |
| 12  | ErrorCode                           | Внутренний [код MME](../error_code/) результата.        | object    |

#### События и процедуры, Event ####

- [MT Call](#mt-call);
- [MT SMS](#mt-sms);
- [MO Call](#mo-call);
- [MO SMS](#mo-sms);
- [Detach Indication](#detach-indication);
- [VLR Reset Indication](#vlr-reset-indication);
- [VLR Disconnection](#vlr-disconnection);
- [UE Activity Indication](#ue-activity-indication);
- [Peer connected indication](#peer-connected-indication);
- [Peer disconnected indication](#peer-disconnected-indication).

#### MT Call ####

```log
DateTime,MT Call,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                     |
|-----|--------------------------------------------------------------|
| 1   | Отсутствует сообщение ICS Response.                          |
| 2   | Отсутствует сообщение S1: Extended Service Request.          |
| 3   | Отсутствует сообщение S1AP: UE CONTEXT MODIFICATION REQUEST. |
| 4   | Отсутствует сообщение S1AP: UE CONTEXT RELEASE REQUEST.      |
| 5   | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен. |
| 6   | Абонент недоступен для PS-сетей.                             |
| 7   | Процедура Paging завершилась с ошибкой.                      |

#### MT SMS ####

```log
DateTime,MT SMS,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                            |
|-----|-------------------------------------------------------------------------------------|
| 1   | Отсутствует VLR GT.                                                                 |
| 2   | Процедура Paging завершилась с ошибкой.                                             |
| 3   | Вместо ожидаемого сообщения SGsAP-DOWNLINK-UNITDATA получено SGsAP-RELEASE-REQUEST. |
| 4   | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен.                        |
| 5   | Абонент недоступен для PS-сетей.                                                    |

#### MO Call ####

```log
DateTime,MO Call,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                      |
|-----|-----------------------------------------------|
| 8   | Для данного абонента неизвестен VLR GT.       |
| 9   | Ошибка процедуры S1AP: INITIAL CONTEXT SETUP. |

#### MO SMS ####

```log
DateTime,MO SMS,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,LAC,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание            |
|-----|---------------------|
| 1   | Отсутствует VLR GT. |

#### Detach Indication ####

```log
DateTime,Detach Ind,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### VLR Reset Indication ####

```log
DateTime,VLR Reset Indication,MME name,Duration,ErrorCode
```

#### VLR Disconnection ####

```log
DateTime,VLR Disconnection,IP-адрес узла VLR.,Duration,ErrorCode
```

#### UE Activity Indication ####

```log
DateTime,UE Activity Indication,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Peer Connected Indication ####

```log
DateTime,Connected,IP:port
```

#### Peer Disconnected Indication ####

```log
DateTime,Disconnected,IP:port
```