---
title : "bfd.conf"
description : "Конфигурация обнаружения двунаправленной передачи"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [timers](#timers) - параметры таймеров;
* [listen](#listen) - параметры сокетов для приема;
* [send](#send) - параметры сокетов для передачи;
* [peers](#peers) - параметры пиров.

### Описание параметров ###

| Наименование                    | Описание                                                                                                              | Тип            | По&nbsp;умолчанию | O/M | P/R | Версия |
|---------------------------------|-----------------------------------------------------------------------------------------------------------------------|----------------|-------------------|-----|-----|--------|
| bfd                             | Корень файла.                                                                                                         | object         | -                 | M   | P   |        |
| **{**                           |                                                                                                                       |                |                   |     |     |        |
| **<a name="timers">timers</a>** | Параметры таймеров.                                                                                                   | object         | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**               |                                                                                                                       |                |                   |     |     |        |
| recreate                        | Время повторного создания сокета.                                                                                     | units          | -                 | O   | P   |        |
| reinit                          | Время ожидания до отправки инициализирующих сообщений: init, down.                                                    | units          | -                 | O   | P   |        |
| sleep_time                      | Период сна потока при простое.                                                                                        | units          | -                 | O   | P   |        |
| &nbsp;&nbsp;**}**               |                                                                                                                       |                |                   |     |     |        |
| message_queue_size              | Размер очереди команд.                                                                                                | int            | -                 | O   | P   |        |
| core_number                     | Номер ядра для запуска потока.                                                                                        | int            | -                 | O   | P   |        |
| **<a name="listen">listen</a>** | Параметры сокетов, работающих на прием.                                                                               | list\<object\> | -                 | M   | P   |        |
| &nbsp;&nbsp;**{**               |                                                                                                                       |                |                   |     |     |        |
| addr                            | IPv4-/IPv6-адрес для получения пакетов.                                                                               | ip/string      | -                 | M   | P   |        |
| port                            | Порт для получения пакетов.                                                                                           | int            | -                 | M   | P   |        |
| type                            | Тип сокета.<br>socket/dpdk;                                                                                           | string         | -                 | M   | P   |        |
| packet_queue_size               | Размер буфера очереди входящих пакетов от получателя.<br>**Примечание.** Только для типа `dpdk`.                      | int            | -                 | C   | P   |        |
| &nbsp;&nbsp;**}**               |                                                                                                                       |                |                   |     |     |        |
| **<a name="send">send</a>**     | Параметры сокетов, работающих на отправку.                                                                            | list\<object\> | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**               |                                                                                                                       |                |                   |     |     |        |
| addr                            | IPv4-/IPv6-адрес для отправки пакетов.                                                                                | ip/string      | -                 | M   | P   |        |
| port                            | Порт для отправки пакетов.                                                                                            | int            | -                 | M   | P   |        |
| type                            | Тип сокета.<br>socket/dpdk;                                                                                           | string         | -                 | M   | P   |        |
| packet_queue_size               | Размер буфера очереди исходящих пакетов от отправителя.<br>**Примечание.** Только для типа `dpdk`.                    | int            | -                 | C   | P   |        |
| &nbsp;&nbsp;**}**               |                                                                                                                       |                |                   |     |     |        |
| **<a name="peers">peers</a>**   | Параметры пиров.                                                                                                      | list\<object\> | -                 | M   | P   |        |
| &nbsp;&nbsp;**{**               |                                                                                                                       |                |                   |     |     |        |
| name                            | Уникальное имя пира.                                                                                                  | string         | -                 | M   | P   |        |
| addr                            | IP-адрес пира.                                                                                                        | ip             | -                 | M   | P   |        |
| port                            | Прослушиваемый порт пира.                                                                                             | int            | -                 | M   | P   |        |
| detect_multi                    | Значение `Detect Mult`.<br>**Примечание.** См. [RFC 5880](https://www.rfc-editor.org/info/rfc5880).                   | int            | -                 | M   | P   |        |
| timers                          | Параметры таймеров.                                                                                                   | object         | -                 | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**   |                                                                                                                       |                |                   |     |     |        |
| desired_min_tx                  | Значение `Desired Min TX Interval`.<br>**Примечание.** См. [RFC 5880](https://www.rfc-editor.org/info/rfc5880).       | units          | -                 | M   | P   |        |
| required_min_tx                 | Значение `Required Min RX Interval`.<br>**Примечание.** См. [RFC 5880](https://www.rfc-editor.org/info/rfc5880).      | units          | -                 | M   | P   |        |
| required_min_echo               | Значение `Required Min Echo RX Interval`.<br>**Примечание.** См. [RFC 5880](https://www.rfc-editor.org/info/rfc5880). | units          | -                 | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**   |                                                                                                                       |                |                   |     |     |        |
| &nbsp;&nbsp;**}**               |                                                                                                                       |                |                   |     |     |        |
| **}**                           |                                                                                                                       |                |                   |     |     |        |

#### Пример ####

```json
{
  "bfd": {
    "timers": {
      "recreate": "1s",
      "reinit": "1s",
      "sleep_time": "1ms"
    },
    "message_queue_size": 100,
    "core_number": 0,
    "listen": [
      {
        "addr": "127.0.0.2",
        "port": 3784,
        "type": "socket"
      },
      {
        "addr": "127.0.0.2",
        "port": 3785,
        "type": "socket"
      },
      {
        "addr": "11.0.0.62",
        "port": 3784,
        "type": "dpdk",
        "packet_queue_size": 8192
      }
    ],
    "send": [
      {
        "addr": "127.0.0.2",
        "port": 49152,
        "type": "socket"
      },
      {
        "addr": "11.0.0.62",
        "port": 49152,
        "type": "dpdk",
        "packet_queue_size": 8192
      }
    ],
    "peers": [
      {
        "name": "name",
        "addr": "127.0.0.3",
        "port": 3784,
        "detect_multi": 5,
        "timers": {
          "desired_min_tx": "100ms",
          "required_min_rx": "200ms",
          "required_min_echo": "200ms"
        }
      },
      {
        "name": "name2",
        "addr": "11.0.0.59",
        "port": 3784,
        "detect_multi": 5,
        "timers": {
          "desired_min_tx": "100ms",
          "required_min_rx": "200ms",
          "required_min_echo": "200ms"
        }
      }
    ]
  }
}
```