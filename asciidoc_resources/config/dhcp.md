---
title : "dhcp.conf"
description : "Конфигурация протокола DHCP"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [common](#common) - общие параметры;
* [endpoints](#endpoints) - параметры оконечных устройств;

### Описание параметров ###

| Наименование                          | Описание                                                                                                                      | Тип            | По<br>умолчанию | O/M | P/R | Версия |
|---------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|----------------|-----------------|-----|-----|--------|
| dhcp_client                           | Корень файла.                                                                                                                 | object         | -               | M   | P   |        |
| **{**                                 |                                                                                                                               |                |                 |     |     |        |
| **<a name="common">common</a>**       | Общие параметры.                                                                                                              | object         | -               | M   | P   |        |
| &nbsp;&nbsp;**{**                     |                                                                                                                               |                |                 |     |     |        |
| message_queue_size                    | Размер внутренней очереди соообщений.                                                                                         | int            | -               | O   | P   |        |
| data_queue_size                       | Размер очереди входящих соообщений.<br>**Примечание.** Только для типа `dpdk`.                                                | int            | -               | C   | P   |        |
| socket_buffer_size                    | Размер буфера сокета для I/O операций.<br>**Примечание.** Только для типа `socket`.                                           | int<br>Б       | -               | C   | P   |        |
| max_accounts                          | Максимальное количество активных абонентов.                                                                                   | int            | -               | O   | P   |        |
| max_count_to_send_discover            | Максимальное количество отправленных запросов `DHCPDISCOVER` до активации ошибки.                                             | int            | -               | O   | P   |        |
| max_count_send_request                | Максимальное количество отправленных DHCP-запросов до активации ошибки.                                                       | int            | -               | O   | P   |        |
| send_release_while_stop               | Флаг освобождения всех адресов при остановке узла или активации команды `reload_net` для перегрузки файла [net.conf](net.md). | bool           | -               | O   | P   |        |
| core_number                           | Номер ядра CPU для запуска потока.                                                                                            | int            | -               | O   | P   |        |
| timers                                | Параметры таймеров DHCP.                                                                                                      | object         | -               | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**         |                                                                                                                               |                |                 |     |     |        |
| prefered_order_time                   | Время аренды IP-адреса DHCP-клиентом.                                                                                         | units          | -               | O   | P   |        |
| retransmit                            | Время ожидания для повторной отправки сообщения.<br>**Примечание.** Не включая сообщения `DHCPDISCOVER`.                      | units          | -               | O   | P   |        |
| wait_offer                            | Время ожидания ответа от DHCP-сервера.                                                                                        | units          | -               | O   | P   |        |
| sleep_time                            | Период сна потока при простое.                                                                                                | units          | -               | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**         |                                                                                                                               |                |                 |     |     |        |
| &nbsp;&nbsp;**}**                     |                                                                                                                               |                |                 |     |     |        |
| **<a name="endpoints">endpoints</a>** | Параметры оконечных устройств.                                                                                                | list\<object\> | -               | M   | P   |        |
| &nbsp;&nbsp;**{**                     |                                                                                                                               |                |                 |     |     |        |
| name                                  | Имя устройства.                                                                                                               | string         | -               | M   | P   |        |
| iface_id                              | Идентификатор интерфейса.                                                                                                     | int            | -               | M   | P   |        |
| transport                             | Тип устройства.<br>socket/dpdk.                                                                                               | string         | -               | M   | P   |        |
| relay                                 | IP-адрес агента ретрансляции (Relay).                                                                                         | ip             | -               | O   | P   |        |
| server                                | IP-адрес DHCP-сервера.                                                                                                        | ip             | -               | M   | P   |        |
| &nbsp;&nbsp;**}**                     |                                                                                                                               |                |                 |     |     |        |
| **}**                                 |                                                                                                                               |                |                 |     |     |        |

#### Пример ####

```json
{
  "dhcp_client": {
    "common": {
      "message_queue_size": 40000,
      "max_accounts": 100000,
      "max_count_to_send_discover": 3,
      "max_count_send_request": 4,
      "send_release_while_stop": false,
      "timers": {
        "prefered_order_time": "2m",
        "retransmit": "1s",
        "wait_offer": "1s",
        "sleep_time": "1ms"
      }
    },
    "endpoints": [
      {
        "name": "prim",
        "iface_id": 3,
        "transport": "dpdk",
        "relay": "192.168.100.10",
        "server": "192.168.100.1"
      }
    ]
  }
}
```