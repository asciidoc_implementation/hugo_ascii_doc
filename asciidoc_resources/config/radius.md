---
title : "radius.conf"
description : "Конфигурация протокола RADIUS"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [common](#common) - общие параметры;
* [groups](#groups) - параметры групп;

### Описание параметров ###

| Наименование                      | Описание                                                                                                                    | Тип            | По<br>умолчанию         | O/M | P/R | Версия |
|-----------------------------------|-----------------------------------------------------------------------------------------------------------------------------|----------------|-------------------------|-----|-----|--------|
| **<a name="common">common</a>**   | Общие параметры протокола RADIUS.                                                                                           | object         | -                       | O   | P   |        |
| **{**                             |                                                                                                                             |                |                         |     |     |        |
| max_retransmit                    | Максимальное количество перенаправлений для пира.<br>**Примечание.** При превышении сообщения отправляются следующему пиру. | int            | 10                      | O   | P   |        |
| max_try_send                      | Максимальное количество попыток отправки.<br>**Примечание.** При превышении сообщения отправляются следующему пиру.         | int            | 1&nbsp;000              | O   | P   |        |
| max_not_replied_message           | Максимальное количество сообщений без ответа.<br>**Примечание.** При превышении сообщения отправляются следующему пиру.     | int            | -                       | O   | P   |        |
| message_queue_size                | Размер внутренней очереди сообщений.                                                                                        | int<br>Б       | -                       | O   | P   |        |
| socket_buffer_size                | Размер буфера сокета.                                                                                                       | int<br>Б       | 65&nbsp;536             | O   | P   |        |
| timers                            | Параметры таймеров.                                                                                                         | object         | -                       | O   | P   |        |
| &nbsp;&nbsp;**{**                 |                                                                                                                             |                |                         |     |     |        |
| transaction                       | Время жизни сообщения с ответом.                                                                                            | int<br>мс      | 1                       | O   | P   |        |
| retransmit                        | Время ожидания перепосылки сообщения.                                                                                       | int<br>мс      | 100                     | O   | P   |        |
| check_inactive                    | Период проверки появления активности узла.                                                                                  | int<br>мс      | 1&nbsp;000              | O   | P   |        |
| &nbsp;&nbsp;**}**                 |                                                                                                                             |                |                         |     |     |        |
| **}**                             |                                                                                                                             |                |                         |     |     |        |
| **<a name="groups">groups</a>**   | Параметры групп протокола RADIUS.                                                                                           | list\<object\> | -                       | M   | P   |        |
| **{**                             |                                                                                                                             |                |                         |     |     |        |
| group_name                        | Уникальное имя группы.                                                                                                      | string         | -                       | M   | P   |        |
| <a name="bind-addr">bind_addr</a> | Привязанный IP-адрес.                                                                                                       | ip             | -                       | M   | P   |        |
| <a name="bind-port">bind_port</a> | Привязанные порты.                                                                                                          | list\<int\>    | -                       | M   | P   |        |
| port_policy                       | Политика выбора порта сервера.<br>seq --- последовательный выбор;<br>fan --- одновременный выбор нескольких.                | string         | seq                     | O   | P   |        |
| peer_policy                       | Политика выбора пира.<br>seq --- последовательный выбор;<br>fan --- одновременный выбор нескольких.                         | string         | seq                     | O   | P   |        |
| listen_port                       | Порт для ожидания запроса RADIUS Disconnect от AAA-сервера.                                                                 | int            | 1812                    | O   | P   |        |
| peers                             | Параметры пиров.                                                                                                            | list\<object\> | -                       | M   | P   |        |
| &nbsp;&nbsp;**{**                 |                                                                                                                             |                |                         |     |     |        |
| name                              | Уникальное имя RADIUS-сервера.                                                                                              | string         | -                       | M   | P   |        |
| addr                              | Удаленный IP-адрес RADIUS-сервера.                                                                                          | ip             | -                       | M   | P   |        |
| secret                            | Секретный код RADIUS-сервера.<br>**Примечание.** Должен совпадать с таковым на сервере.                                     | string         | -                       | M   | P   |        |
| weight                            | Приоритет пира.<br>**Примечание.** Чем меньше значение, тем выше приоритет.                                                 | int            |                         | M   | P   |        |
| auth_port                         | Порт для процедуры авторизации.                                                                                             | int            | 1812                    | O   | P   |        |
| acct_port                         | Порт для процедуры тарификации.                                                                                             | int            | 1813                    | O   | P   |        |
| mirroring                         | Параметры зеркалирования.                                                                                                   | object         | -                       | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**     |                                                                                                                             |                |                         |     |     |        |
| addr                              | IP-адрес зеркала.                                                                                                           | ip             | -                       | M   | P   |        |
| port                              | Порт зеркала.                                                                                                               | ip             | [bind_port](#bind-port) | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**     |                                                                                                                             |                |                         |     |     |        |
| &nbsp;&nbsp;**}**                 |                                                                                                                             |                |                         |     |     |        |
| **}**                             |                                                                                                                             |                |                         |     |     |        |

#### Пример ####

```json
{
  "radius": {
    "common": {
      "message_queue_size": 1000,
      "socket_buffer_size": 65536,
      "max_retransmit": 10,
      "max_try_send": 1000,
      "max_not_replied_message": 1,
      "timers": {
        "transaction": 1,
        "retransmit": 100,
        "check_inactive": 1000
      }
    },
    "groups": [
      {
        "group_name": "name",
        "bind_addr": "192.168.102.10",
        "bind_port": "1-4, 5",
        "port_policy": "seq",
        "peer_policy": "seq",
        "listen_port": 1812,
        "peers": [
          {
            "name": "auth",
            "addr": "192.168.108.45",
            "secret": "secret",
            "weight": 1,
            "auth_port": 1812,
            "acct_port": 1813,
            "mirroring": [
              {
                "addr": "192.168.100.231",
                "port": 1834
              }
            ]
          },
          {
            "name": "acct",
            "addr": "192.168.102.22",
            "secret": "testing123",
            "weight": 1,
            "auth_port": 1812,
            "acct_port": 1813
          }
        ]
      }
    ]
  }
}
```