---
title : "http_auth.conf"
description : "Конфигурация HTTP-авторизации"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Описание параметров ###

| Наименование             | Описание                                                                                                                                                                            | Тип       | По<br>умолчанию | O/M | P/R | Версия |
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|-----------------|-----|-----|--------|
| http_auth                | Корень файла.                                                                                                                                                                       | object    | -               | M   | P   |        |
| **{**                    |                                                                                                                                                                                     |           |                 |     |     |        |
| name                     | Имя клиента.                                                                                                                                                                        | string    | -               | M   | P   |        |
| bind_addr                | Привязываемый IP-адрес.                                                                                                                                                             | ip        | -               | M   | P   |        |
| bind_port                | Прослушиваемый порт для входящих подключений.                                                                                                                                       | int       | -               | M   | P   |        |
| master_server_addr       | IP-адрес мастер-сервера.                                                                                                                                                            | ip        | -               | M   | P   |        |
| master_server_port       | Порт мастер-сервера.                                                                                                                                                                | int       | -               | M   | P   |        |
| fo_server_addr           | IP-адрес сервера для аварийного подключения.                                                                                                                                        | ip        | -               | O   | P   |        |
| fo_server_port           | Порт сервера для аварийного подключения.                                                                                                                                            | int       | -               | O   | P   |        |
| active_connection_num    | Количество активных подключений к мастер-серверу.                                                                                                                                   | int       | -               | O   | P   |        |
| message_queue_size       | Размер внутренней очереди соообщений.                                                                                                                                               | int       | -               | O   | P   |        |
| not_answered_count       | Максимальное количество запросов без ответа.                                                                                                                                        | int       | -               | O   | P   |        |
| active_fo_connection_num | Количество активных подключений к аварийному серверу.                                                                                                                               | int       | -               | O   | P   |        |
| fail_send_policy         | Политика при ошибке доставки сообщения.<br>wait_free --- ожидание освободившихся подключений;<br>use_fo_server --- подключение к аварийному серверу;<br>reverse --- отбой отправки. | string    | wait_free       | O   | P   |        |
| core_number              | Номер ядра для запуска потока.                                                                                                                                                      | int       | -               | O   | P   |        |
| timers                   | Параметры таймеров.                                                                                                                                                                 | object    | -               | O   | P   |        |
| &nbsp;&nbsp;**{**        |                                                                                                                                                                                     |           |                 |     |     |        |
| wait_answer              | Время ожидания ответа.                                                                                                                                                              | int<br>мс | -               | O   | P   |        |
| total_wait_answer        | Время хранения сообщения.                                                                                                                                                           | int<br>мс | -               | O   | P   |        |
| wait_established         | Время ожидания подключения.                                                                                                                                                         | int<br>мс | -               | O   | P   |        |
| cleanup_fo_connections   | Время ожидания перед освобождением подключений к аварийному серверу.                                                                                                                | int<br>мс | -               | O   | P   |        |
| &nbsp;&nbsp;**}**        |                                                                                                                                                                                     |           |                 |     |     |        |
| **}**                    |                                                                                                                                                                                     |           |                 |     |     |        |

#### Пример ####

```json
{
  "http_auth": {
    "name": "client",
    "bind_addr": "192.168.100.231",
    "bind_port": 18080,
    "master_server_addr": "192.168.102.10",
    "master_server_port": 19018,
    "fo_server_addr": "192.168.102.10",
    "fo_server_port": 19019,
    "active_connection_num": 10,
    "message_queue_size": 1000,
    "not_answered_count": 10,
    "active_fo_connection_num": 4,
    "fail_send_policy": "wait_free",
    "timers": {
      "wait_answer": 1000,
      "total_wait_answer": 3000,
      "wait_established": 2000,
      "cleanup_fo_connections": 5000
    }
  }
}
```