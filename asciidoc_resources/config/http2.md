---
title : "http2.conf"
description : "Конфигурация взаимодействия по протоколу HTTP2"
weight : 3
type: docs
---
### Назначение ###
В конфигурационном файле **http2.conf** определяются параметры взаимодействия с узлом PGW по протоколу HTTP2 как в качестве сервера, так и в качестве клиента.

Файл имеет формат JSON.

### Секции ###

* [server](#server) — параметры PGW-сервера:
  * [common](#common) — общие параметры;
  * [listen](#listen) — параметры подключения к серверу.
* [client](#client) — параметры PGW-клиента.


### Описание параметров ###

| Наименование                              | Описание                                                                                                                                                                      | Тип            | По умолчанию | O/M | P/R | Версия |
|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|--------------|-----|-----|--------|
| **<a name="server">server</a>**           | Параметры, определяющие взаимодействие узла по протоколу HTTP2 в качестве сервера.                                                                                            | object         | \-           | O   | P   |        |
| **{**                                     |                                                                                                                                                                               |                |              |     |     |        |
| <a name="common">common</a>               | Общие параметры взаимодействия по протоколу HTTP2.                                                                                                                            | object         | \-           | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                               |                |              |     |     |        |
| core_number                               | Номер ядра CPU для запуска потока.                                                                                                                                            | int            | \-           | O   | P   |        |
| message_queue_size                        | Размер очереди сообщений команд в байтах.                                                                                                                                     | int            | \-           | O   | P   |        |
| max_connections                           | Максимальное количество активных подключений.                                                                                                                                 | int            | \-           | O   | P   |        |
| max_empty_cycles_wo_sleep                 | Максимальное количество "пустых" циклов без событий до вызова функции *sleep*.                                                                                                | int            | \-           | O   | P   |        |
| max_empty_cycles_wo_proceed_timer         | Максимальное количество "пустых" циклов без событий до вызова функции *timer_manage*.                                                                                         | int            | \-           | O   | P   |        |
| max_empty_cycles_wo_proceed_event         | Максимальное количество "пустых" циклов без событий до вызова функции *proceed_event*.                                                                                        | int            | \-           | O   | P   |        |
| max_hstreams_per_connection               | Максимальное количество HTTP2-потоков  в подключении.                                                                                                                         | int            | \-           | O   | P   |        |
| timers                                    | Параметры таймеров.                                                                                                                                                           | object         | \-           | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                               |                |              |     |     |        |
| thread_sleep_time                         | Период сна потока при простое.                                                                                                                                                | units          | \-           | O   | P   |        |
| recreate_socket                           | Время для пересоздания слушающего сокета.                                                                                                                                     | units          | \-           | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                               |                |              |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                               |                |              |     |     |        |
| **<a name="listen">listen</a>**           | Параметры подключения к серверу.                                                                                                                                              | object         | \-           | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                               |                |              |     |     |        |
| addr                                      | IPv4-/IPv6-адрес для приема API-запросов.                                                                                                                                     | ip/string      | \-           | M   | P   |        |
| port                                      | Порт для приема API-запросов.                                                                                                                                                 | string         | \-           | M   | P   |        |
| transport                                 | Используемый транспортный протокол.<br>Возможные значения:<br>`tcp` — подключение по TCP-протоколу;<br>`ssl_tcp` — подключение по защищенному соединению с применением SSL.   | string         | \-           | M   | P   |        |
| buffer_size                               | Размер буфера сокета в байтах. <br>**Примечание** Параметр не используется для SSL-соединений.                                                                                | int            | \-           | C   | P   |        |                                                                                                                                                                            |                |                             |     |     |        
| ssl_options                               | Параметры SSL-соединений. <br> **Примечание** Секция указывается только при использовании `ssl_tcp`.                                                                          | object         | \-           | C   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{** |                                                                                                                                                                               |                |              |     |     |        |
| certificate_path                          | Путь к директории хранения SSL-сертификата \*.pem.                                                                                                                            | string         | \-           | M   | P   |        |
| key_path                                  | Путь к директории хранения SSL-ключа.                                                                                                                                         | string         | \-           | M   |     |        |
| disable_sslv2                             | Флаг исключения SSLv2 из списка поддерживаемых протоколов. <br>Возможные значения:<br>`true` — не поддерживается работа с SSLv2; <br>`false` — работа с SSLv2 поддерживается. | bool           | false        | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}** |                                                                                                                                                                               |                |              |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                               |                |              |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                               |                |              |     |     |        |
| **<a name="client">client</a>**           | Параметры, определяющие взаимодействие узла по протоколу HTTP2 в качестве клиента.                                                                                            | list\<object\> | \-           | O   | P   |        |
| **{**                                     |                                                                                                                                                                               |                |              |     |     |        |
| common                                    | Общие параметры взаимодействия по протоколу HTTP2.                                                                                                                            | object         | \-           | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                               |                |              |     |     |        |               
| core_number                               | Номер ядра CPU для запуска потока.                                                                                                                                            | int            | \-           | O   | P   |        |
| message_queue_size                        | Размер очереди сообщений команд в байтах.                                                                                                                                     | int            | \-           | O   | P   |        |
| max_out_connections                       | Максимальное количество активных подключений.                                                                                                                                 | int            | \-           | O   | P   |        |
| max_empty_cycles_wo_sleep                 | Максимальное количество "пустых" циклов (без событий) до вызова функции *sleep*.                                                                                              | int            | \-           |     |     |        |
| max_empty_cycles_wo_proceed_timer         | Максимальное количество "пустых" циклов (без событий) до вызова функции *timer_manage*.                                                                                       | int            | \-           | O   | P   |        |
| max_empty_cycles_wo_proceed_event         | Максимальное количество "пустых" циклов (без событий) до вызова функции *proceed_event*.                                                                                      | int            | \-           | O   | P   |        |
| max_hstreams_per_connection               | Максимальное количество HTTP2-потоков в подключении.                                                                                                                          | int            | \-           | O   | P   |        |
| max_reset_counter                         | Максимальное количество попыток переподключения при разрыве соединения. <br>При превышении значения очередь исходящих запросов очищается.                                     | int            | \-           |     |     |        |
| use_ssl                                   | Флаг использования SSL при соединении с сервером. <br>Возможные значения:<br>`true` — SSL используется; <br>`false` — SSL не используется.                                    | bool           | \-           |     |     |        |
| dscp                                      | Десятичное значение DSCP.                                                                                                                                                     |                |              |     |     |        |
| timers                                    | Параметры таймеров.                                                                                                                                                           | object         | \-           | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                               |                |              |     |     |        |
| wait_answer                               | Время ожидания ответа на запрос.                                                                                                                                              | units          | \-           | O   | P   |        |
| thread_sleep_time                         | Период сна потока при простое.                                                                                                                                                | units          | \-           | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                               |                |              |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                               |                |              |     |     |        |
| **}**                                     |                                                                                                                                                                               |                |              |     |     |        |


### Пример ###

```json
{
  "server": {
    "common": {
      "core_number": 0,
      "message_queue_size": 40000,
      "max_connections": 10,
      "max_empty_cycles_wo_sleep": 10,
      "max_empty_cycles_wo_proceed_timer": 11,
      "max_empty_cycles_wo_proceed_event": 10,
      "max_hstreams_per_connection": 10,
      "timers": {
        "recreate_socket": "5s",
        "thread_sleep_time": "1ms"
      }
    },
    "listen": [
      {
        "addr": "192.168.126.236",
        "port": 80,
        "transport": "tcp",
        "buffer_size": 65000
      },
      {
        "addr": "127.0.0.1",
        "port": 443,
        "transport": "ssl_tcp",
        "ssl_options": {
          "certificate_path": "",
          "key_path": "",
          "disable_sslv2": true
        }
      }
    ]
  },

  "client":  {
    "common": {
      "core_number": 0,
      "message_queue_size": 40000,
      "max_out_connections": 10,
      "max_empty_cycles_wo_sleep": 10,
      "max_empty_cycles_wo_proceed_timer": 11,
      "max_empty_cycles_wo_proceed_event": 1,
      "max_hstreams_per_connection": 10,
      "max_reset_counter": 2,
      "use_ssl": true,
      "dscp": 18,
      "timers": {
        "wait_answer": "5s",
        "thread_sleep_time": "1ms"
      }
    }
  }
}
```