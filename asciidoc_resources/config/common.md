---
title : "common.conf"
description : "Конфигурация HTTP API, дампов трафика и обеспечения высокой доступности"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [dump_writer](#dump-writer) - параметры записи дампа;
* [oam](#oam) - параметры OAM:
  * [common](#common) - общие параметры;
  * [listen](#listen) - параметры удаленного адреса;
* [high_availability](#ha) - параметры обеспечения высокой доступности:
  * [timers](#timers) - параметры таймеров;
  * [announcing](#announcing) - параметры объявления адресов;
  * [peers_availability](#pa) - параметры доступности пиров.

[//]: # (https://docs.protei.ru/test/Mobile/Protei_PGW/common/config/common/#mode)
https://docs.protei.ru/test/Mobile/Protei_PGW/common/oam/config/common/#mode
### Описание параметров ###

| Наименование                              | Описание                                                                                                                                                                                                    | Тип                      | По&nbsp;умолчанию | O/M | P/R | Версия |
|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------|-------------------|-----|-----|--------|
| **<a name="dump-writer">dump_writer</a>** | Параметры записи дампов.                                                                                                                                                                                    | object                   | -                 | O   | P   |        |
| **{**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |
| <a name="mode">mode</a>                   | Режим записи дампов.<br>Возможные значения:<br>`all` — запись пользовательского и сигнального трафика; <br>`cp` — запись только сигнального трафика; <br>`none` — запись дампов сетевого трафика отключена. | string                   | all               | O   | P   |        |
| <a name="dir">dir</a>                     | Директория для хранения дампов трафика.                                                                                                                                                                     | string                   | -                 | O   | P   |        |
| core_number                               | Номер ядра для запуска потока.                                                                                                                                                                              | int                      | -                 | O   | P   |        |
| arrays_count                              | Количество выделенных массивов.                                                                                                                                                                             | int                      | -                 | O   | P   |        |
| maximum_dumps_on_disk                     | Максимальное количество дампов до ротации.                                                                                                                                                                  | int                      | -                 | O   | P   |        |
| size_limit                                | Максимальный размер файла.                                                                                                                                                                                  | int<br>Кб                | -                 | O   | P   |        |
| nodeID                                    | Имя узла.                                                                                                                                                                                                   | string                   | -                 | O   | P   |        |
| timers                                    | Параметры таймеров.                                                                                                                                                                                         | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| thread_sleep_time                         | Период сна потока при простое.                                                                                                                                                                              | units                    | -                 | O   | P   |        |
| period_limit                              | Максимальный период записи.                                                                                                                                                                                 | units                    | 1h                | O   | P   |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| trace_dump_writer                         | Параметры записи trace-дампов.                                                                                                                                                                              | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| root_directory                            | Корневая директория для хранения ротированных дампов.                                                                                                                                                       | string                   | 1h                | O   | P   |        |
| rotation_hour                             | Час времени суток, в который происходит ротация дампов.<br>Возможные значения: 0-23                                                                                                                         | int                      | 0                 | O   | P   |        |
| max_size                                  | Максимальный размер дампа в килобайтах до ротации. Если значение параметра не указано, то ротация по размеру отключена.                                                                                     | int                      | -                 | O   | P   |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **}**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **<a name="oam">oam</a>**                 | Параметры OAM.                                                                                                                                                                                              | object                   | -                 | O   | P   |        |
| **{**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |
| <a name="common">common</a>               | Общие параметры OAM.                                                                                                                                                                                        | object                   | -                 | O   | R   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| message_queue_size                        | Размер внутренней очереди сообщений.                                                                                                                                                                        | int<br>Б                 | -                 | O   | P   |        |
| max_online_connections                    | Максимальное количество активных подключений.                                                                                                                                                               | int                      | -                 | O   | P   |        |
| core_number                               | Номер ядра для запуска потока.                                                                                                                                                                              | int                      | -                 | O   | P   |        |
| dscp                                      | DSCP-метка для трафика.                                                                                                                                                                                     | int                      | -                 | O   | P   |        |
| timers                                    | Параметры таймеров.                                                                                                                                                                                         | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| recreate_socket                           | Период пересоздания сокета.                                                                                                                                                                                 | units                    | -                 | O   | P   |        |
| thread_sleep_time                         | Период сна потока при простое.                                                                                                                                                                              | units                    | -                 | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| listen                                    | Параметры удаленного пользователя.                                                                                                                                                                          | list\<object\>           | -                 | M   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| addr                                      | IPv4-/IPv6-адрес для приема API-запросов.                                                                                                                                                                   | ip/string                | -                 | M   | P   |        |
| port                                      | Порт для приема API-запросов.                                                                                                                                                                               | ip/string                | -                 | M   | P   |        |
| transport                                 | Используемый транспортный протокол.<br>tcp/ssl_tcp.                                                                                                                                                         | string                   | -                 | M   | P   |        |
| buffer_size                               | Размер буфера сокета.<br>**Примечание.** Только для протокола `tcp`.                                                                                                                                        | int                      | -                 | C   | P   |        |
| ssl_options                               | Параметры SSL.<br>**Примечание.** Только для протокола `ssl_tcp`.                                                                                                                                           | object                   | -                 | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| key_path                                  | Путь до ключа сертификата.                                                                                                                                                                                  | object                   | -                 | C   | P   |        |
| certificate_path                          | Путь до файла сертификата \*.pem.                                                                                                                                                                           | string                   | -                 | M   | P   |        |
| disable_sslv2                             | Флаг исключения SSLv2 из списка поддерживаемых.                                                                                                                                                             | bool                     | true              | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **}**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **<a name="ha">high_availability</a>**    | Параметры доступности.                                                                                                                                                                                      | object                   | -                 | O   | P   |        |
| **{**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |
| synchro_peer                              | Имя `Destination-Host`.<br>**Примечание.** Должно быть задано в [diameter_client::groups::peers](diameter_client.md).                                                                                       | string                   | -                 | M   | P   |        |
| max_reconnect_count                       | Максимальное количество попыток переподключения.                                                                                                                                                            | int                      | -                 | O   | P   |        |
| timers                                    | Параметры таймеров.                                                                                                                                                                                         | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| reconnect_time                            | Период переподключения.                                                                                                                                                                                     | units                    | -                 | O   | P   |        |
| send_message_interval                     | Период отправки сообщения.                                                                                                                                                                                  | units                    | -                 | O   | P   |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| failover_strategy                         | Политика аварийного подключения.<br>fastest --- проверка подключений к частным сетям;<br>reliable --- проверка подключений ко внешним и частным сетям;                                                      | string                   | -                 | O   | P   |        |
| **<a name="announcing">announcing</a>**   | Параметры объявления адресов.                                                                                                                                                                               | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| iface_id                                  | Идентификатор интерфейса, связанный с объявлением адресов.<br>**Примечание.** Только для алгоритма `garp`.                                                                                                  | int                      | -                 | O   | P   |        |
| algorithm                                 | Алгоритм объявления адреса.<br><abbr title="Generic Attribute Registration Protocol">garp</abbr>/<abbr title="Border Gateway Protocol">bgp</abbr>.                                                          | string                   | -                 | M   | P   |        |
| address                                   | IP-адрес, используемый для объявления.                                                                                                                                                                      | ip                       | -                 | M   | P   |        |
| ???bgp_name                               | Клиенты, которым объявляются адреса.<br>**Примечание.** Для выбора всех узлов используется значение `all`.                                                                                                  | list\<string\><br>string | -                 | O   | P   |        |
| ???bgp_neighbor                           | Соседние узлы, которым объявляются адреса.<br>**Примечание.** Для выбора всех узлов используется значение `all`.                                                                                            | list\<string\><br>string | -                 | O   | P   |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **<a name="pa">peers_availability</a>**   | Параметры доступности пиров.                                                                                                                                                                                | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| change_state_policy                       | Алгоритм изменения состояния пира.<br>reset_on_update --- изменение состояния при обновлении;<br>save_state_on_update --- сохранение состояния при обновлении.                                              | string                   | -                 | O   | P   |        |
| timers                                    | Параметры таймеров.                                                                                                                                                                                         | object                   | -                 | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| send_new_state                            | Период задания состояния пира.                                                                                                                                                                              | units                    | -                 | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| actions                                   | Параметры проверки пиров.                                                                                                                                                                                   | list\<object\>           | -                 | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| peers                                     | Проверяемые пиры.                                                                                                                                                                                           | list\<string\>           | -                 | M   | P   |        |
| active_in_role                            | Тип пира.<br>master/slave.                                                                                                                                                                                  | string                   | -                 | M   | P   |        |
| max_fail_count                            | Максимальное количество неуспешных проверок до аварийного подключения.                                                                                                                                      | int                      | -                 | O   | P   |        |
| wait_reply                                | Время ожидания ответа на запрос.                                                                                                                                                                            | int<br>мс                | -                 | O   | P   |        |
| check_type                                | Способ проверки.<br>icmp_ping;                                                                                                                                                                              | string                   | -                 | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                             |                          |                   |     |     |        |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                                             |                          |                   |     |     |        |
| **}**                                     |                                                                                                                                                                                                             |                          |                   |     |     |        |

### Пример ###

```json
{
  "dump_writer": {
    "mode": "cp",
    "dir": "./dumps/",
    "core_number": 1,
    "arrays_count": 2,
    "maximum_dumps_on_disk": 3,
    "nodeID" : "Protei_GGSN1",
    "timers": {
      "thread_sleep_time": "1s",
      "period_limit": "1m"
    },
    "trace_dump_writer" : {
            "root_directory" : "./dumps/",    
            "rotation_hour" : 0,			
            "max_size" : 102400  		
    },
    "size_limit": 102400
  },
  "oam": {
    "common": {
      "message_queue_size": 40000,
      "max_online_connections": 10,
      "core_number": 0,
      "timers": {
        "recreate_socket": "5s",
        "thread_sleep_time": "1ms"
      }
    },
    "listen": [
      {
        "addr": "127.0.0.1",
        "port": 9000,
        "transport": "tcp",
        "buffer_size": 65000
      },
      {
        "addr": "127.0.0.1",
        "port": 443,
        "transport": "ssl_tcp",
        "ssl_options": {
          "certificate_path": "",
          "key_path": "",
          "disable_sslv2": true
        }
      }
    ]
  },
  "high_availability": {
    "synchro_peer": "ha",
    "max_reconnect_count": 4,
    "timers": {
      "reconnect_time": "5s",
      "send_message_interval": "1s"
    },
    "failover_strategy": "fastest",
    "announcing": [
      {
        "address": "192.168.108.209",
        "algorithm": "garp",
        "iface_id": 1,
        "bgp_name": "all",
        "bgp_neighbor": "all"
      },
      {
        "address": "1.1.1.1",
        "algorithm": "garp",
        "iface_id": 0,
        "bgp_name": "all",
        "bgp_neighbor": "all"
      },
      {
        "address": "2.2.2.2",
        "algorithm": "garp",
        "iface_id": 1
      }
    ],
    "peers_availability": {
      "change_state_policy": "reset_on_update",
      "timers": {
        "send_new_state": "1ms"
      },
      "actions": [
        {
          "peers": [ "192.168.108.200" ],
          "active_in_role": "slave",
          "max_fail_count": 3,
          "wait_reply": 1000,
          "check_type": "icmp_ping"
        }
      ]
    }
  }
}
```