---
title : "diameter_client.conf"
description : "Конфигурация клиента протокола Diameter"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [common](#common) - общие параметры;
* [groups](#groups) - параметры групп;
  * [common](#common-groups) - общие параметры групп;
  * [vendor_spec_app_id](#vendor) - параметры Vendor-Specific-App;
  * [listen](#listen) - параметры удаленного адреса;
  * [peers](#peers) - параметры пиров;
  * [routing_table](#routing-table) - параметры таблицы маршрутизации;

### Описание параметров ###

| Наименование                                          | Описание                                                                                                                                                                                       | Тип                            | По<br>умолчанию | O/M | P/R | Версия |
|-------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-----------------|-----|-----|--------|
| diameter_client                                       | Корень файла.                                                                                                                                                                                  | object                         | -               | M   | R   |        |
| **{**                                                 |                                                                                                                                                                                                |                                |                 |     |     |        |
| <a name="common">common</a>                           | Общие параметры.                                                                                                                                                                               | object                         | -               | O   | R   |        |
| &nbsp;&nbsp;**{**                                     |                                                                                                                                                                                                |                                |                 |     |     |        |
| message_queue_size                                    | Размер очереди команд.                                                                                                                                                                         | int                            | -               | O   | R   |        |
| core_number                                           | Номер ядра для запуска потока.                                                                                                                                                                 | int                            | -               | O   | R   |        |
| timers                                                | Параметры таймеров.                                                                                                                                                                            | object                         | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**                         |                                                                                                                                                                                                |                                |                 |     |     |        |
| thread_sleep_time                                     | Период сна потока при простое.                                                                                                                                                                 | units                          | -               | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**                         |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;**}**                                     |                                                                                                                                                                                                |                                |                 |     |     |        |
| **<a name="groups">groups</a>**                       | Параметры групп.                                                                                                                                                                               | list\<object\>                 | -               | O   | R   |        |
| &nbsp;&nbsp;**{**                                     |                                                                                                                                                                                                |                                |                 |     |     |        |
| **<a name="common-groups">common</a>**                | Общие параметры групп.                                                                                                                                                                         | object                         | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**{**                         |                                                                                                                                                                                                |                                |                 |     |     |        |
| max_not_replied_message                               | Максимальное количество отправленных запросов без ответа до того, как узел будет считаться отключенным.                                                                                        | int                            | -               | O   | R   |        |
| max_wd_not_replied                                    | Максимальное количество отправленных запросов Diameter: Device-Watchdog-Request без ответа до того, как узел будет считаться отключенным.                                                      | int                            | -               | O   | R   |        |
| spurious_connections                                  | Количество зарезервированных соединений для подключений других пиров.                                                                                                                          | int                            | -               | O   | R   |        |
| transactions_per_peer                                 | Количество транзакций на пира.<br>**Примечание.** Пул транзакций ограничен.                                                                                                                    | int                            |                 |     |     |        |
| timers                                                | Параметры таймеров.                                                                                                                                                                            | object                         | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| wait_answer                                           | Время ожидания ответа на запрос.                                                                                                                                                               | units                          | -               | O   | R   |        |
| watchdog                                              | Время ожидания для отправки запроса Diameter: Device-Watchdog-Request.                                                                                                                         | units                          | -               | O   | R   |        |
| recreate_socket                                       | Период пересоздания сокета.                                                                                                                                                                    | units                          | -               | O   | R   |        |
| reconnect                                             | Период переподключения.                                                                                                                                                                        | units                          | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**                         |                                                                                                                                                                                                |                                |                 |     |     |        |
| name                                                  | Уникальное имя группы.                                                                                                                                                                         | string                         | -               | M   | R   |        |
| send_policy                                           | Политика выбора портов сервера для отправки сообщений.<br>seq --- последовательный выбор;<br>fan --- одновременный выбор нескольких.                                                           | string                         | -               | O   | R   |        |
| acc_app_id                                            | Значение `Acct-Application-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 259.                  | list\<int\>                    | -               | O   | R   |        |
| auth_app_id                                           | Значение `Auth-Application-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 258.                  | list\<int\>                    | -               | O   | R   |        |
| vendor_id                                             | Значение `Vendor-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 266.                            | int                            | -               | O   | R   |        |
| product_name                                          | Значение `Product-Name` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 269.                         | string                         | -               | O   | R   |        |
| supported_vendor_id                                   | Значение `Supported-Vendor-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 265.                  | list\<int\>                    | -               | O   | R   |        |
| firmware_rev                                          | Значение `Firmware-Revision-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 267.                 | int                            | -               | O   | R   |        |
| sharing_contexts                                      | Флаг поддержки аварийного переключения внутри группы для запросов Diameter: Credit-Control-Request Initial.                                                                                    | bool                           | true            | O   | R   |        |
| process_cer_from_unknown_peer                         | Флаг отправки ответа Diameter: Capabilities-Exchange-Answer c `Result-Code = DIAMETER_UNKNOWN_PEER` на запросы от неизвестных пиров.                                                           | bool                           | false           | O   | R   |        |
| **<a name="vendor">vendor_spec_app_id</a>**           | Значения `Vendor-Specific-Application-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 260.       | list\<object\>                 | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| acc_app_id                                            | Значение `Acct-Application-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 259.                  | int                            | -               | O   | R   |        |
| auth_app_id                                           | Значение `Auth-Application-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 258.                  | int                            | -               | O   | R   |        |
| vendor_id                                             | Значение `Vendor-Id` для запросов Diameter: Capabilities-Exchange-Request/Answer.<br>**Примечание.** См. [RFC 6733](https://tools.ietf.org/html/rfc6733). AVP: 266.                            | list\<int\>                    | -               | M   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| **<a name="listen">listen</a>**                       | Параметры удаленного пользователя.                                                                                                                                                             | list\<object\>                 | -               | M   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| addr<br>addrs                                         | IPv4-/IPv6-адрес удаленного пользователя.<br>Список IPv4-/IPv6-адресов.<br>**Примечание.** Длина списка не должна превышать 2.                                                                 | ip/string<br>list\<ip/string\> | -               | M   | P   |        |
| port<br>ports                                         | Порт удаленного пользователя.<br>Список портов.                                                                                                                                                | int<br>list\<int\>             | -               | M   | P   |        |
| transport                                             | Используемый транспортный протокол.<br>tcp/sctp/ssl_tcp/ssl_sctp.                                                                                                                              | string                         | -               | M   | P   |        |
| buffer_size                                           | Размер буфера сокета.<br>**Примечание.** Только для транспортных протоколов `tcp` и `sctp`.                                                                                                    | int                            | -               | C   | P   |        |
| ssl_options                                           | Параметры SSL.<br>**Примечание.** Только для транспортных протоколов `ssl_tcp` и `ssl_sctp`.                                                                                                   | object                         | -               | C   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{** |                                                                                                                                                                                                |                                |                 |     |     |        |
| certificate_path                                      | Путь до файла сертификата \*.pem.                                                                                                                                                              | string                         | -               | M   | P   |        |
| key_path                                              | Путь до ключа сертификата.                                                                                                                                                                     | string                         | -               | M   | P   |        |
| disable_sslv2                                         | Флаг исключения SSLv2 из списка поддерживаемых версий.                                                                                                                                         | bool                           | true            | O   | P   |        |
| read_ahead                                            | Флаг включения опции `read_ahead` для SSL.                                                                                                                                                     | bool                           | true            | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}** |                                                                                                                                                                                                |                                |                 |     |     |        |
| sctp_options                                          | Параметры SCTP.<br>**Примечание.** Только для транспортных протоколов `sctp` и `ssl_sctp`.<br>См. [RFC 4895](https://datatracker.ietf.org/doc/html/rfc4895).                                   | object                         | -               | C   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{** |                                                                                                                                                                                                |                                |                 |     |     |        |
| srto_max                                              | Максимальное время передачи сообщения.                                                                                                                                                         | int<br>мс                      | -               | O   | R   |        |
| srto_min                                              | Минимальный порог времени передачи сообщения.                                                                                                                                                  | int<br>мс                      | -               | O   | R   |        |
| sasoc_asocmaxrxt                                      | Максимальное количество попыток передачи для ассоциации.                                                                                                                                       | int                            | -               | O   | R   |        |
| sinit_num_ostreams                                    | Максимальное количество исходящих потоков.                                                                                                                                                     | int                            | -               | O   | R   |        |
| sinit_num_istreams                                    | Максимальное количество входящих потоков.                                                                                                                                                      | int                            | -               | O   | R   |        |
| sinit_max_attempts                                    | Максимальное количество попыток передачи запросов `INIT` конечному узлу.                                                                                                                       | int                            | -               | O   | R   |        |
| sinit_max_init_timeo                                  | Время ожидания между попытками передачи запроса `INIT`.                                                                                                                                        | int                            | -               | O   | R   |        |
| reset_linger                                          | Флаг использования `SO_LINGER`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                     | bool                           | true            | O   | R   |        |
| use_mapped_v4_address                                 | Флаг `SCTP_I_WANT_MAPPED_V4_ADDR`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                  | bool                           | true            | O   | R   |        |
| sctp_data_io_event                                    | Флаг `sctp_data_io_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                          | bool                           | 0               | O   | R   |        |
| sctp_association_event                                | Флаг `sctp_association_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                      | bool                           | 0               | O   | R   |        |
| sctp_address_event                                    | Флаг `sctp_address_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                          | bool                           | 0               | O   | R   |        |
| sctp_send_failure_event                               | Флаг `sctp_send_failure_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                     | bool                           | 0               | O   | R   |        |
| sctp_peer_error_event                                 | Флаг `sctp_peer_error_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                       | bool                           | 0               | O   | R   |        |
| sctp_shutdown_event                                   | Флаг `sctp_shutdown_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                         | bool                           | 0               | O   | R   |        |
| sctp_partial_delivery_event                           | Флаг `sctp_partial_delivery_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                 | bool                           | 0               | O   | R   |        |
| sctp_authentication_event                             | Флаг `sctp_authentication_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                   | bool                           | 0               | O   | R   |        |
| sctp_adaptation_layer_event                           | Флаг `sctp_adaptation_layer_event`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                 | bool                           | 0               | O   | R   |        |
| disable_frag                                          | Флаг `SCTP_DISABLE_FRAGMENTS`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                      | bool                           | false           | O   | R   |        |
| enable_heart_beats                                    | Флаг `SCTP_PEER_ADDR_PARAMS`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                                       | bool                           | false           | O   | R   |        |
| spp_hbinterval                                        | Период посылки сообщения `heartbeat`.<br>**Примечание.** См. [RFC 6458](https://www.rfc-editor.org/rfc/rfc6458).                                                                               | int<br>мс                      | 6000            | O   | R   |        |
| ppid                                                  | Значение `PPID` для фрагментов открытого текста SCTP DATA.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733).                                                          | int                            | 46              | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}** |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| **<a name="peers">peers</a>**                         | Параметры пиров.                                                                                                                                                                               | object                         | -               | C   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| application_id                                        | Значение `Application-Id`.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733).                                                                                          | string                         | -               | M   | P   |        |
| origin_host                                           | Значение `Origin-Host`.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733). AVP: 264.                                                                                   | string                         | -               | M   | P   |        |
| origin_realm                                          | Значение `Origin-Realm`.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733). AVP: 296.                                                                                  | string                         | -               | M   | P   |        |
| destination_host                                      | Значение `Destination-Host`.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733). AVP: 293.                                                                              | string                         | -               | M   | P   |        |
| destination_realm                                     | Значение `Destination-Host`.<br>**Примечание.** См. [RFC 6733](https://www.rfc-editor.org/rfc/rfc6733). AVP: 283.                                                                              | string                         | -               | M   | P   |        |
| weight                                                | Вес, приоритет пира.                                                                                                                                                                           | int                            | -               | M   | R   |        |
| addr<br>addrs                                         | IPv4-/IPv6-адрес пира.<br>Список IPv4-/IPv6-адресов пира.<br>**Примечание.** Обязательный, если доступен без DRA.<br>**Примечание.** Длина списка не должна превышать 2.                       | ip/string<br>list\<ip/string\> | -               | C   | P   |        |
| port<br>ports                                         | Прослушиваемый порт пира.<br>Список прослушиваемых портов пира.<br>**Примечание.** Обязательный, если доступен без DRA.<br>**Примечание.** Длина списка не должна превышать 2.                 | int<br>list\<int\>             | -               | C   | P   |        |
| transport                                             | Используемый транспортный протокол.<br>tcp/sctp/ssl_tcp/ssl_sctp.<br>**Примечание.** Обязательный, если доступен без DRA.                                                                      | string                         | -               | C   | P   |        |
| buffer_size                                           | Размер буфера сокета.<br>**Примечание.** Только для транспортных протоколов `tcp` и `sctp`.                                                                                                    | int                            | -               | C   | P   |        |
| limiters                                              | Параметры нагрузки.                                                                                                                                                                            | object                         | -               | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{** |                                                                                                                                                                                                |                                |                 |     |     |        |
| ccr_i                                                 | Максимальное количество запросов Diameter: Credit-Control-Request Initial.<br>**Примечание.** При превышении осуществляется попытка аварийного переключения. При неудаче возвращение в логику. | object                         | -               | O   | P   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}** |                                                                                                                                                                                                |                                |                 |     |     |        |
| bind                                                  | Параметры узла для отправки.                                                                                                                                                                   | object                         | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{** |                                                                                                                                                                                                |                                |                 |     |     |        |
| addr<br>addrs                                         | IPv4-/IPv6-адрес узла отправления.<br>Список IPv4-/IPv6-адресов узла отправления.<br>**Примечание.** Длина списка не должна превышать 2.                                                       | ip/string<br>list\<ip/string\> | -               | O   | R   |        |
| port<br>ports                                         | Порт узла отправления.<br>Список портов узла отправления.<br>**Примечание.** Длина списка не должна превышать 2.                                                                               | int<br>list\<int\>             | -               | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}** |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| **<a name="routing-table">routing_table</a>**         | Параметры таблицы маршрутизации.                                                                                                                                                               | list\<object\>                 | -               | C   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| <a name="host">host</a>                               | Хост узла.<br>**Примечание.** Должен быть задан этот параметр или [realm](#realm).                                                                                                             | string                         | -               | C   | R   |        |
| <a name="realm">realm</a>                             | Realm узла.<br>**Примечание.** Должен быть задан этот параметр или [host](#host).                                                                                                              | string                         | -               | C   | R   |        |
| route_peers_weight                                    | Вес/номер группы пира для получения host/realm.                                                                                                                                                | int                            | -               | M   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**}**             |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;**}**                         |                                                                                                                                                                                                |                                |                 |     |     |        |
| &nbsp;&nbsp;**}**                                     |                                                                                                                                                                                                |                                |                 |     |     |        |
| **}**                                                 |                                                                                                                                                                                                |                                |                 |     |     |        |

#### Пример ####

```json
{
  "diameter_client": {
    "common": {
      "message_queue_size": 40000,
      "core_number": 0,
      "timers": { "thread_sleep_time": "1ms" }
    },
    "groups": [
      {
        "common": {
          "max_not_replied_message": 1,
          "max_wd_not_replied": 3,
          "spurious_connections": 3,
          "transactions_per_peer": 65536,
          "timers": {
            "wait_answer": "3s",
            "watchdog": "30s",
            "recreate_socket": "5s",
            "reconnect": "30s"
          }
        },
        "name": "gx",
        "send_policy": "seq",
        "acc_app_id": [],
        "auth_app_id": [],
        "vendor_id": 0,
        "product_name": "",
        "supported_vendor_id": [],
        "firmware_rev": 0,
        "sharing_contexts": true,
        "process_cer_from_unknown_peer": false,
        "vendor_spec_app_id": [
          {
            "vendor_id": [], 
            "acc_app_id": 1,
            "auth_app_id": 1
          }
        ],
        "listen": [
          {
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "tcp",
            "buffer_size": 65000
          },
          {
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "ssl_tcp",
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "disable_sslv2": true
            }
          },
          {
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "sctp",
            "buffer_size": 65000,
            "sctp_options": {
              "srto_max": 5000,
              "srto_min": 1000,
              "sasoc_asocmaxrxt": 4,
              "sinit_num_ostreams": 30,
              "sinit_num_istreams": 0,
              "sinit_max_attempts": 2,
              "sinit_max_init_timeo": 10000,
              "reset_linger": true,
              "use_mapped_v4_address": true,
              "sctp_data_io_event": 0,
              "sctp_association_event": 0,
              "sctp_address_event": 0,
              "sctp_send_failure_event": 1,
              "sctp_peer_error_event": 0,
              "sctp_shutdown_event": 1,
              "sctp_partial_delivery_event": 1,
              "sctp_authentication_event": 0,
              "sctp_adaptation_layer_event": 0,
              "disable_frag": false,
              "enable_heart_beats": true,
              "spp_hbinterval": 6000,
              "ppid": 46
            }
          },
          {
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "ssl_sctp",
            "sctp_options": {},
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "read_ahead": true,
              "disable_sslv2": true
            }
          }
        ],
        "peers": [
          {
            "application_id": 1,
            "origin_host": "P-GW1.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi1.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 10,
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "tcp",
            "buffer_size": 65000,
            "bind": {
              "addr": "192.168.108.31",
              "port": 3869
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW2.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi2.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 10,
            "addr": "192.168.100.231",
            "port": 3869,
            "transport": "ssl_tcp",
            "ssl_options": {
              "disable_sslv2": true
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW3.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi3.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 11,
            "addrs": [ "192.168.108.31", "192.168.108.31" ],
            "ports": [ 3869, 3890 ],
            "transport": "sctp",
            "buffer_size": 65000,
            "bind": {
              "addrs": [ "192.168.108.31", "192.168.108.31" ],
              "ports": [ 3869, 3890 ]
            },
            "sctp_options": {}
          },
          {
            "application_id": 1,
            "origin_host": "P-GW4.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi4.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 11,
            "addrs": ["192.168.108.31", "192.168.108.31"],
            "ports": [3869, 3890],
            "transport": "ssl_sctp",
            "buffer_size": 65000,
            "bind": {
              "addrs": ["192.168.108.33", "192.168.108.34"],
              "ports": [3869, 3890]
            },
            "sctp_options": {},
            "ssl_options": {
              "certificate_path": "",
              "key_path": "",
              "read_ahead": true,
              "disable_sslv2": true
            }
          },
          {
            "application_id": 1,
            "origin_host": "P-GW1.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi1.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 1
          },
          {
            "application_id": 1,
            "origin_host": "P-GW3.protei.ru",
            "origin_realm": "protei.ru",
            "destination_host": "dpi3.dpi.ru",
            "destination_realm": "dpi.ru",
            "weight": 2,
            "limiters": {
              "ccr_i": 1
            }
          }
        ],
        "routing_table": [
          {
            "host": "dpi1.dpi.ru",
            "route_peers_weight": 10
          },
          {
            "realm": "dpi",
            "route_peers_weight": 11
          }
        ]
      }
    ]
  }
}
```