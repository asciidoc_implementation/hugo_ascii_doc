---
title : "trace.conf"
description : "Конфигурация подсистемы журналирования"
weight : 3
type: docs
---
### Назначение ###
В конфигурационном файле **trace.conf** задаются параметры логирования узла PGW.

>Ключ для применения изменений в конфигурационном файле без перезапуска узла — **reload_trace** (см. раздел [Управление узлом](../../oam/system_management.md)).

### Описание параметров ###

| Наименование            | Описание                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | Тип    | O/M | P/R | Версия |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|--------|
| trace_dir               | Директория для хранения log-файлов по умолчанию.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | string | M   | R   |        |
| core_number             | Номер ядра для запуска потока.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | int    | O   | R   |        |
| trace_thread_queue_size | Размер очереди потока трассировки в байтах.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | int    | O   | R   |        |
| **[<a name="journal_name">journal_name</a>]**      | Секция параметров ведения журнала конкретного модуля.<br>Наименование секции соответствует модулю PGW, для которого ведется журнал:<br> - **init** — журнал инициализации PGW;<br> - **radius** — журнал протокола RADIUS;<br> - **gtpc** — журнал интерфейса GTP-C;<br> - **oam** — журнал операций OAM;<br> - **bfd** — журнал протокола BFD;<br> - **dhcp** — журнал протокола DHCP;<br> - **pe** — журнал обработчика пакетов;<br> - **network_stack** — журнал протоколов ARP и ICMP;<br> - **interface_stat** — журнал статистики интерфейсов;<br> - **http_auth** — журнал авторизации по протоколу HTTP;<br> - **common** — журнал трафика;<br> - **gtp** — журнал протокола GTP;<br> - **diameter** — журнал протокола Diameter;<br> - **bgp** — журнал протокола BGP;<br> - **cdr** — журнал CDR;<br> - **cdr_cg_med** — журнал CDR для офлайн-тарификации. | object | M   | R   |        |
| [logtype]               | Секция параметров ведения конкретного типа журнала для модуля.<br>Наименование секции соответствует типу журнала:<br>`warning`/`info`/`trace`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | object | O   | R   |        |
| name                    | Маска имени файла журнала. <br/> Возможные маркеры подстановки:<br>`%Y` — год; <br>`%m` — месяц; <br>`%d` — день;<br>`%H` — час;<br>`%M` — минута.<br/> **Примечание**. Если требуется хранить файл в директории, отличной от указанной в `trace_dir`, в значении параметра следует указать абсолютный путь хранения файла.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | string | O   | R   |        |
| level                   | Уровень записываемых сообщений.<br>Возможные значения:<br>`none`/`important`/`info`/`debug`/`verbose`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | string | O   | R   |        |
| tee                     | Поток вывода для дублирования.<br>Возможные значения:<br>`stdout`/`cout`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | string | O   | R   |        |
| mask                    | Маска формата первого поля в записи. Маркеры подстановки задаются последовательно через символ `&`.<br>Возможные маркеры подстановки:<br> - **date** — дата, формат: `DD/MM/YY`;<br> - **time** — время, формат: `hh:mm:ss`;<br> - **tick** — миллисекунды:<br> - - если задан маркер *time* — `.mss`; <br>- - если не задан маркер *time* — `.mssmss`;<br> - **usec** — микросекунды;<br> - **state** — внутреннее состояние системы;<br> - **pid** — идентификатор процесса, формат: 6 цифр;<br> - **tid** — идентификатор транзакции, формат: 6 цифр;<br> - **level** — уровень записываемых сообщений;<br> - **file** — название файл и номер строки в исходном коде.                                                                                                                                                                                             | string | O   | R   |        |
| type                    | Параметры записи файла.<br>- **name_now** — в названии указывается текущее время;<br>- **name_period** — в названии указывается начало периода сбора;<br>- **truncate** — перезапись файла;<br>- **append** — дописывание файла;<br>- **log** — включение параметров `truncate` и `name_now`, информация сигнализации при падении пишется;<br>- **cdr** — включение параметров `append` и `name_now`, информация сигнализации при падении не пишется;<br>- **binary** — файл в бинарном виде.                                                                                                                                                                                                                                                                                                                                                                         | string | O   | R   |        |
| period                  | Период обновления файла.<br>Возможные значения:<br>`1year`/`month`/`week`/`day`/`hour`/`min`/`32sec`/...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | units  | O   | R   |        |
| separator               | Разделитель полей.<br>По умолчанию — `;`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | string | O   | R   |        |

### Пример ###

```json
{
  "trace_dir": "logs",
  "core_number": 1,
  "trace_thread_queue_size": 16000,
  "init" : {
        "separator" : " ",
        "level" : "verbose",
        "name" : "init_%Y%m%d.log",
        "type" : "cdr",
        "mask" : "date & time & tick",
        "tee" : "stdout"
    },
  "radius": {
    "info": {
      "name": "radius_info_%Y_%m_%d_%H_%M.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "radius_warning_%Y_%m_%d.log"
    }
  },
  "gtpc": {
    "info": {
      "name": "gtpc_info_%Y_%m_%d.log",
      "level": "verbose",
      "mask": "date & time & tick & file",
      "type": "append"
    },
    "warning": {
      "name": "gtpc_warning.log",
      "type": "append"
    }
  },
  "oam": {
    "info": {
      "name": "oam_info_%Y_%m_%d.log",
      "level": "verbose",
      "mask": "date & time & tick & file",
      "type": "append"
    },
    "warning": {
      "name": "oam_warning.log",
      "type": "append"
    }
  },
  "dhcp": {
    "info": {
      "name": "dhcp_info_%Y_%m_%d_%H_%M.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "dhcp_warning_%Y_%m_%d.log"
    }
  },
  "pe": {
    "info": {
      "name": "packet_engine_info_%Y_%m_%d_%H_%M.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "packet_engine_warning_%Y_%m_%d.log"
    }
  },
  "network_stack": {
    "info": {
      "name": "network_stack_info_%Y_%m_%d_%H_%M.log",
      "level": "important",
      "mask": "date & time & tick & file",
      "period": "1hour"
    },
    "warning": {
      "name": "network_stack_warning_%Y_%m_%d.log"
    }
  },
  "diameter": {
    "info": {
      "name": "diameter_info_%Y_%m_%d.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "diameter_warning_%Y_%m_%d.log"
    }
  },
  "bgp": {
    "info": {
      "name": "bgp_info_%Y_%m_%d.log",
      "level": "debug",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "bgp_warning_%Y_%m_%d.log"
    }
  },
  "interface_stat": {
    "info": {
      "tee"   : "stdout",
      "name": "interface_stat_info_%Y_%m_%d.log",
      "level": "verbose",
      "mask": "date & time & tick & file",
      "type": "append"
    }
  },
  "common": {
    "info": {
      "name": "info_%Y_%m_%d.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "warning_%Y_%m_%d.log"
    }
  },
  "gtp": {
    "info": {
      "name": "gtp_info_%Y_%m_%d_%H_%M.log",
      "level": "verbose",
      "mask": "date & time & tick & file"
    },
    "warning": {
      "name": "gtp_warning_%Y_%m_%d.log"
    }
  },
  "cdr": {
    "type": "cdr",
    "tee"   : "stdout",
    "level": "verbose",
    "name": "/usr/protei/Protei_PGW/logs/cdr_%Y_%m_%d_%H_%M.cdr",
    "period": "1sec",
    "separator": ";",
    "mask": "date & time & tick"
  },
  "cdr_cg_med": {
    "type": "cdr",
    "level": "verbose",
    "name": "/usr/protei/Protei_PGW/logs/cdr_cg_med_%Y%m%d_%H%M.cdr",
    "period": "1sec",
    "separator": ";",
    "mask": "date & time & tick"
  }
}
```
