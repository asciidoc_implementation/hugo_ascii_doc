---
title : "dscp_qos.conf"
description : "Конфигурация DSCP-меток для пакетов сигнального трафика"
weight : 3
type: docs
---
### Назначение ###
В конфигурационном файле **dscp_qos.conf** задаются параметры назначения DSCP-меток для приоритезации сигнального трафика.

>Ключ для применения изменений в конфигурационном файле без перезапуска узла — **reload_dscp_qos** (см. раздел [Управление узлом](../oam/system_management.md)).

### Описание параметров ###

| Наименование | Описание                                                                                                                                                                                                                                                                                                         | Тип    | O/M | P/R | Версия |
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|--------|
| dscp_qos     | Корневая секция параметров назначения DSCP-меток.                                                                                                                                                                                                                                                                | object | O   | R   |        |
| default      | DSCP-метка по умолчанию.                                                                                                                                                                                                                                                                                         | int    | O   | R   |        |
| [v1]         | Массив значений параметров, по которым определяется DSCP-метка для пакетов GTPv1:<br/> - **arp** — приоритет назначения и удержания каналов;<br/> - **traffic_class** — класс трафика;<br/> - **thp** — приоритет обработки трафика;<br/> - **dscp** — назначаемая DSCP-метка для указанных значений параметров. | list   | O   | R   |        |
| [v2]         | Массив значений параметров, по которым определяется DSCP-метка для пакетов GTPv2:<br/> - **arp** — приоритет назначения и удержания каналов;<br/> - **qci** — идентификатор класса качества обслуживания;<br/> - **dscp** — назначаемая DSCP-метка для указанных значений параметров.                            | list   | O   | R   |        |

### Пример ###

```json
{
  "dscp_qos" : {
    "default" : { "dscp" : 8 },
    "v1" : [
  { "arp" : 1, "traffic_class" : 1, "thp" : 0, "dscp" : 10 },
  { "arp" : 2, "traffic_class" : 1, "thp" : 0, "dscp" : 10 },
  { "arp" : 3, "traffic_class" : 1, "thp" : 0, "dscp" : 10 },

  { "arp" : 1, "traffic_class" : 2, "thp" : 0, "dscp" : 10 },
  { "arp" : 2, "traffic_class" : 2, "thp" : 0, "dscp" : 10 },
  { "arp" : 3, "traffic_class" : 2, "thp" : 0, "dscp" : 10 },
  
  { "arp" : 1, "traffic_class" : 3, "thp" : 1, "dscp" : 10 },  
  { "arp" : 2, "traffic_class" : 3, "thp" : 1, "dscp" : 10 },  
  { "arp" : 3, "traffic_class" : 3, "thp" : 1, "dscp" : 10 },  
  { "arp" : 1, "traffic_class" : 3, "thp" : 2, "dscp" : 12 },  
  { "arp" : 2, "traffic_class" : 3, "thp" : 2, "dscp" : 12 },  
  { "arp" : 3, "traffic_class" : 3, "thp" : 2, "dscp" : 12 },  
  { "arp" : 1, "traffic_class" : 3, "thp" : 3, "dscp" : 14 },  
  { "arp" : 2, "traffic_class" : 3, "thp" : 3, "dscp" : 14 },  
  { "arp" : 3, "traffic_class" : 3, "thp" : 3, "dscp" : 14 },

  { "arp" : 1, "traffic_class" : 4, "thp" : 0, "dscp" : 10 },
  { "arp" : 2, "traffic_class" : 4, "thp" : 0, "dscp" : 10 },
  { "arp" : 3, "traffic_class" : 4, "thp" : 0, "dscp" : 10 }


    ],

    "v2" : [

  { "arp" : 1, "qci" : 1, "dscp" : 46 },
  { "arp" : 2, "qci" : 1, "dscp" : 46 },
  { "arp" : 3, "qci" : 1, "dscp" : 46 },
  { "arp" : 4, "qci" : 1, "dscp" : 46 },
  { "arp" : 5, "qci" : 1, "dscp" : 46 },
  { "arp" : 6, "qci" : 1, "dscp" : 46 },
  { "arp" : 7, "qci" : 1, "dscp" : 46 },
  { "arp" : 8, "qci" : 1, "dscp" : 46 },
  { "arp" : 9, "qci" : 1, "dscp" : 46 },
  { "arp" : 10, "qci" : 1, "dscp" : 46 },
  { "arp" : 11, "qci" : 1, "dscp" : 46 },
  { "arp" : 12, "qci" : 1, "dscp" : 46 },
  { "arp" : 13, "qci" : 1, "dscp" : 46 },
  { "arp" : 14, "qci" : 1, "dscp" : 46 },
  { "arp" : 15, "qci" : 1, "dscp" : 46 },

  { "arp" : 1, "qci" : 5, "dscp" : 34 },
  { "arp" : 2, "qci" : 5, "dscp" : 34 },
  { "arp" : 3, "qci" : 5, "dscp" : 34 },
  { "arp" : 4, "qci" : 5, "dscp" : 34 },
  { "arp" : 5, "qci" : 5, "dscp" : 34 },
  { "arp" : 6, "qci" : 5, "dscp" : 34 },
  { "arp" : 7, "qci" : 5, "dscp" : 34 },
  { "arp" : 8, "qci" : 5, "dscp" : 34 },
  { "arp" : 9, "qci" : 5, "dscp" : 34 },
  { "arp" : 10, "qci" : 5, "dscp" : 34 },
  { "arp" : 11, "qci" : 5, "dscp" : 34 },
  { "arp" : 12, "qci" : 5, "dscp" : 34 },
  { "arp" : 13, "qci" : 5, "dscp" : 34 },
  { "arp" : 14, "qci" : 5, "dscp" : 34 },
  { "arp" : 15, "qci" : 5, "dscp" : 34 },

  { "arp" : 1, "qci" : 69, "dscp" : 34 },
  { "arp" : 2, "qci" : 69, "dscp" : 34 },
  { "arp" : 3, "qci" : 69, "dscp" : 34 },
  { "arp" : 4, "qci" : 69, "dscp" : 34 },
  { "arp" : 5, "qci" : 69, "dscp" : 34 },
  { "arp" : 6, "qci" : 69, "dscp" : 34 },
  { "arp" : 7, "qci" : 69, "dscp" : 34 },
  { "arp" : 8, "qci" : 69, "dscp" : 34 },
  { "arp" : 9, "qci" : 69, "dscp" : 34 },
  { "arp" : 10, "qci" : 69, "dscp" : 34 },
  { "arp" : 11, "qci" : 69, "dscp" : 34 },
  { "arp" : 12, "qci" : 69, "dscp" : 34 },
  { "arp" : 13, "qci" : 69, "dscp" : 34 },
  { "arp" : 14, "qci" : 69, "dscp" : 34 },
  { "arp" : 15, "qci" : 69, "dscp" : 34 },

  { "arp" : 1, "qci" : 65, "dscp" : 46 },
  { "arp" : 2, "qci" : 65, "dscp" : 46 },
  { "arp" : 3, "qci" : 65, "dscp" : 46 },
  { "arp" : 4, "qci" : 65, "dscp" : 46 },
  { "arp" : 5, "qci" : 65, "dscp" : 46 },
  { "arp" : 6, "qci" : 65, "dscp" : 46 },
  { "arp" : 7, "qci" : 65, "dscp" : 46 },
  { "arp" : 8, "qci" : 65, "dscp" : 46 },
  { "arp" : 9, "qci" : 65, "dscp" : 46 },
  { "arp" : 10, "qci" : 65, "dscp" : 46 },
  { "arp" : 11, "qci" : 65, "dscp" : 46 },
  { "arp" : 12, "qci" : 65, "dscp" : 46 },
  { "arp" : 13, "qci" : 65, "dscp" : 46 },
  { "arp" : 14, "qci" : 65, "dscp" : 46 },
  { "arp" : 15, "qci" : 65, "dscp" : 46 },

  { "arp" : 1, "qci" : 6, "dscp" : 26 },
  { "arp" : 2, "qci" : 6, "dscp" : 26 },
  { "arp" : 3, "qci" : 6, "dscp" : 26 },
  { "arp" : 4, "qci" : 6, "dscp" : 26 },
  { "arp" : 5, "qci" : 6, "dscp" : 26 },
  { "arp" : 6, "qci" : 6, "dscp" : 26 },
  { "arp" : 7, "qci" : 6, "dscp" : 26 },
  { "arp" : 8, "qci" : 6, "dscp" : 26 },
  { "arp" : 9, "qci" : 6, "dscp" : 26 },
  { "arp" : 10, "qci" : 6, "dscp" : 26 },
  { "arp" : 11, "qci" : 6, "dscp" : 26 },
  { "arp" : 12, "qci" : 6, "dscp" : 26 },
  { "arp" : 13, "qci" : 6, "dscp" : 26 },
  { "arp" : 14, "qci" : 6, "dscp" : 26 },
  { "arp" : 15, "qci" : 6, "dscp" : 26 },

  { "arp" : 1, "qci" : 7, "dscp" : 24 },
  { "arp" : 2, "qci" : 7, "dscp" : 24 },
  { "arp" : 3, "qci" : 7, "dscp" : 24 },
  { "arp" : 4, "qci" : 7, "dscp" : 24 },
  { "arp" : 5, "qci" : 7, "dscp" : 24 },
  { "arp" : 6, "qci" : 7, "dscp" : 24 },
  { "arp" : 7, "qci" : 7, "dscp" : 24 },
  { "arp" : 8, "qci" : 7, "dscp" : 24 },
  { "arp" : 9, "qci" : 7, "dscp" : 24 },
  { "arp" : 10, "qci" : 7, "dscp" : 24 },
  { "arp" : 11, "qci" : 7, "dscp" : 24 },
  { "arp" : 12, "qci" : 7, "dscp" : 24 },
  { "arp" : 13, "qci" : 7, "dscp" : 24 },
  { "arp" : 14, "qci" : 7, "dscp" : 24 },
  { "arp" : 15, "qci" : 7, "dscp" : 24 },

  { "arp" : 1, "qci" : 8, "dscp" : 12 },
  { "arp" : 2, "qci" : 8, "dscp" : 12 },
  { "arp" : 3, "qci" : 8, "dscp" : 12 },
  { "arp" : 4, "qci" : 8, "dscp" : 12 },
  { "arp" : 5, "qci" : 8, "dscp" : 12 },
  { "arp" : 6, "qci" : 8, "dscp" : 12 },
  { "arp" : 7, "qci" : 8, "dscp" : 12 },
  { "arp" : 8, "qci" : 8, "dscp" : 12 },
  { "arp" : 9, "qci" : 8, "dscp" : 12 },
  { "arp" : 10, "qci" : 8, "dscp" : 12 },
  { "arp" : 11, "qci" : 8, "dscp" : 12 },
  { "arp" : 12, "qci" : 8, "dscp" : 12 },
  { "arp" : 13, "qci" : 8, "dscp" : 12 },
  { "arp" : 14, "qci" : 8, "dscp" : 12 },
  { "arp" : 15, "qci" : 8, "dscp" : 12 },

  { "arp" : 1, "qci" : 9, "dscp" : 10 },
  { "arp" : 2, "qci" : 9, "dscp" : 10 },
  { "arp" : 3, "qci" : 9, "dscp" : 10 },
  { "arp" : 4, "qci" : 9, "dscp" : 10 },
  { "arp" : 5, "qci" : 9, "dscp" : 10 },
  { "arp" : 6, "qci" : 9, "dscp" : 10 },
  { "arp" : 7, "qci" : 9, "dscp" : 10 },
  { "arp" : 8, "qci" : 9, "dscp" : 10 },
  { "arp" : 9, "qci" : 9, "dscp" : 10 },
  { "arp" : 10, "qci" : 9, "dscp" : 10 },
  { "arp" : 11, "qci" : 9, "dscp" : 10 },
  { "arp" : 12, "qci" : 9, "dscp" : 10 },
  { "arp" : 13, "qci" : 9, "dscp" : 10 },
  { "arp" : 14, "qci" : 9, "dscp" : 10 },
  { "arp" : 15, "qci" : 9, "dscp" : 10 }


    ]
  }
}
```