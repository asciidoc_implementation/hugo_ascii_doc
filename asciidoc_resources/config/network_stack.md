---
title : "network_stack.conf"
description : "Конфигурация протоколов ARP и ICMP"
weight : 3
type: docs
---

Файл имеет формат JSON.

### Разделы ###

* [icmp_arp_common](#common) - общие параметры протоколов <abbr title="Internet Control Message Protocol">ICMP</abbr> и <abbr title="Address Resolution Protocol">ARP</abbr>;
* [icmp](#icmp) - параметры протокола ICMP;

### Описание параметров ###

| Наименование                             | Описание                                                                                                                                   | Тип      | По<br>умолчанию | O/M | P/R | Версия |
|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|----------|-----------------|-----|-----|--------|
| **<a name="common">icmp_arp_common</a>** | Общие параметры протоколов ICMP и ARP.                                                                                                     | object   | -               | O   | R   |        |
| **{**                                    |                                                                                                                                            |          |                 |     |     |        |
| max_packets_queue_size                   | Размер очереди пакетов от `iface`.                                                                                                         | int<br>Б | -               | O   | R   |        |
| max_messages_queue_size                  | Размер очереди сообщений/команд.                                                                                                           | int<br>Б | -               | O   | R   |        |
| max_retransmit_count                     | Максимальное количество пересылок для определения MAC-адреса.<br>**Примечание.** При превышении отправляется ответ `Couldn't resolve MAC`. | int      | 20              | O   | R   |        |
| retransmite_resolve_mac_to               | Время ожидания повторной передачи для определения MAC-адреса (IPv4 - ARP, IPv6 - ICMPv6).                                                  | units    | 300ms           | O   | R   |        |
| update_arp_table_to                      | Время ожидания обновления записей ARP (IPv4 - ARP, IPv6 - ICMPv6).                                                                         | units    | 1m              | O   | R   |        |
| core_number                              | Номер ядра для запуска потока.                                                                                                             | int      | -               | O   | R   |        |
| garp_to                                  | Время ожидания объявления собственного адреса (IPv4 - ARP, IPv6 - ICMPv6).                                                                 | units    | 5s              | O   | R   |        |
| **}**                                    |                                                                                                                                            |          |                 |     |     |        |
| **<a name="icmp">icmp</a>**              | Параметры протокола ICMP.                                                                                                                  | object   | -               | O   | R   |        |
| **{**                                    |                                                                                                                                            |          |                 |     |     |        |
| long_ping_retransmite_to                 | Время ожидания между отправками продолжительного `ping`.                                                                                   | units    | 1s              | O   | R   |        |
| ping_to                                  | Время ожидания между отправками `ping`.                                                                                                    | units    | 5s              | O   | R   |        |
| **}**                                    |                                                                                                                                            |          |                 |     |     |        |

#### Пример ####

```json
{
  "icmp_arp_common": {
    "max_packets_queue_size": 8192,
    "max_messages_queue_size": 8192,
    "max_retransmit_count": 20,
    "retransmite_resolve_mac_to": "300ms",
    "update_arp_table_to": "1m",
    "core_number": 2,
    "garp_to": "5s"
  },
  "icmp": {
    "long_ping_retransmite_to": "1s",
    "ping_to": "5s"
  }
}
```