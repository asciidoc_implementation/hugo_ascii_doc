import faulthandler
import logging
import time
from enum import IntEnum
from functools import cached_property, wraps
from logging import Handler, LogRecord
from pathlib import Path
from sys import stdout as sysout
from types import FrameType
from typing import Any, Callable, Literal, Type

from loguru import logger

__all__ = ["LoggerConfiguration", "HandlerType", "LoggingLevel", "InterceptHandler", "custom_logging",
           "configure_custom_logging", "logging_faulthandler", "timer"]

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


def convert_ns_to_readable(time_ns: int) -> str:
    hours: int
    minutes: int
    seconds: int = time_ns // 10 ** 9
    if seconds == 0:
        return "менее секунды"
    if seconds < 60:
        hours = 0
        minutes = 0
    else:
        hours = 0
        minutes = seconds // 60
        seconds %= 60
        if minutes >= 60:
            hours = minutes // 60
            minutes %= 60
    hour_string: str = f"{hours} ч " if hours != 0 else ""
    minutes_string: str = f"{minutes} мин " if minutes != 0 else ""
    seconds_string: str = f"{seconds} с" if seconds != 0 else ""
    return f"{hour_string}{minutes_string}{seconds_string}"


def custom_logging(func: Callable):
    def wrapper(*args, **kwargs):
        if args is None or not args:
            _args_string: str = "none"
        else:
            _args_string: str = ", ".join(f"{arg}" for arg in args)

        if kwargs is None or not kwargs:
            _kwargs_string: str = "none"
        else:
            _kwargs_string: str = ", ".join(f"{k}={v}" for k, v in kwargs.items())

        logger.info(f"\nfunction: {func.__name__}\nargs: {_args_string}\nkwargs: {_kwargs_string}")
        return func(*args, **kwargs)

    return wrapper


def timer(func):
    """Print the runtime of the decorated function"""
    @wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


def logging_faulthandler(func: Callable):
    if not faulthandler.is_enabled():
        faulthandler.enable()

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    if faulthandler.is_enabled():
        faulthandler.disable()
    return wrapper


class LogVerbosity(IntEnum):
    ZERO = 0
    LOW = 10
    MEDIUM = 20
    HIGH = 30

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self._name_}>"

    @classmethod
    def from_string(cls, value: str):
        if value.upper() not in cls.__members__:
            logger.debug(f"Level {value} is not found, so {cls.__name__}.LOW is set")
            return cls.LOW
        else:
            return cls[value.upper()]

    @classmethod
    def from_int(cls, value: int):
        if value not in cls._value2member_map_:
            logger.debug(f"Level {value} is not found, so {cls.__name__}.LOW is set")
            return cls.LOW
        else:
            return cls(value)


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    _log_folder: Path = Path(__file__).parent.parent.joinpath("logs")

    def __init__(
            self,
            file_name: str, *,
            verbosity: int | str | LogVerbosity = LogVerbosity.LOW,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        if isinstance(verbosity, str):
            verbosity: LogVerbosity = LogVerbosity.from_string(verbosity)
        elif isinstance(verbosity, int):
            verbosity: LogVerbosity = LogVerbosity.from_int(verbosity)
        self._file_name: str = file_name
        self._verbosity: LogVerbosity = verbosity
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: Path = self._log_folder.joinpath(f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler


def configure_custom_logging(name: str):
    def inner(func: Callable):
        def wrapper(*args, **kwargs):
            handlers: dict[HandlerType, LoggingLevel] = {
                "stream": "INFO",
                "file_rotating": "DEBUG"
            }

            file_name: str = name

            logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers=handlers)
            stream_handler: dict[str, Any] = logger_configuration.stream_handler()
            rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

            logger.configure(handlers=[stream_handler, rotating_file_handler])

            logging.basicConfig(handlers=[InterceptHandler()], level=0)
            logger.debug("========================================")
            logger.debug("Запуск программы:")

            return func(*args, **kwargs)

        return wrapper

    return inner



def my_logger(verbosity: LogVerbosity):

    def _inner_logger(fun):
        @wraps(fun)
        def _inner_decorator(*args, **kwargs):
            if verbosity >= LogVerbosity.LOW:
                print(f'LOG: Verbosity level: {verbosity}')
                print(f'LOG: {fun.__name__} is being called!')
            if verbosity == LogVerbosity.HIGH:
                print(f'LOG: Scope of the caller is {__name__}.')
                print(f'LOG: Arguments are {args}, {kwargs}')

            return fun(*args, **kwargs)

        return _inner_decorator

    return _inner_logger


@timer
@my_logger(verbosity=LogVerbosity.LOW)
def my_function(n):
    return sum(range(n))


@timer
@my_logger(verbosity=LogVerbosity.HIGH)
def my_unordinary_function(n, m):
    return sum(range(n)) + m


if __name__ == '__main__':
    # print(my_function(10))
    # LOG: Verbosity level: LOW
    # LOG: my_function is being called!
    # 45
    print(LogVerbosity._member_map_.items())
    print(LogVerbosity._value2member_map_.items())
    # print(my_unordinary_function(5, 1))
    # LOG: Verbosity level: HIGH
    # LOG: my_unordinary_function is being called!
    # LOG: Date and time of call is 2023-07-25 19:09:15.954603.
    # LOG: Scope of the caller is __main__.
    # LOG: Arguments are (5, 1), {}
    # 11
