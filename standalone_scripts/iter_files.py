import os
from glob import glob
import os.path as p


def files(root_dir: str):
    return [
        _ for _ in glob("**/*.adoc", root_dir=root_dir, recursive=True)
        if not p.basename(_) == "nav.adoc"]


def sort_files(lines: list[str] = None):
    if lines is not None:
        lines.sort()
    return lines


def modify(line: str, *, prefix: str = "", postfix: str = ""):
    return f"{prefix}{line}{postfix}"


def level(path_file: str):
    return len(path_file.split("/"))


def write_to_file(base_path: str, lines: list[str] = None):
    if lines is None:
        return
    _: str = p.join(base_path, "nav.adoc")
    if p.exists(_):
        os.remove(_)
    with open(_, "x") as f:
        f.write("\n".join(lines))
    print(f"Success")


if __name__ == '__main__':
    path: str = "/Users/user/PycharmProjects/hugo_ascii_doc/docs/modules/pgw/pages/"
    _lines: list[str] = []
    for _ in sort_files(files(path)):
        _prefix: str = level(_) * "*"
        _lines.append(modify(_, prefix=f"{_prefix} xref:", postfix="[]"))
    write_to_file(path, _lines)
