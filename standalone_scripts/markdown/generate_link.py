from argparse import ArgumentError, ArgumentParser, Namespace, OPTIONAL, RawDescriptionHelpFormatter, SUPPRESS
from sys import platform
from textwrap import dedent
from typing import TypeAlias
import os.path as p


OptStr: TypeAlias = str | None
_INDEX_FILES: tuple[str, ...] = (
    "_index.md", "index.md"
)
_MARKDOWN_EXTENSION: str = ".md"
_press_enter_key: str = "Press the ENTER key to close the window ..."

prog: str = p.basename(p.abspath(__file__))
_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = f"{_python_version} {prog} <SOURCE> <DESTINATION> [ -a/--anchor <ANCHOR> ]"

description: str = "Link generator for git files to be correct when the static site is generated"
version: str = f"{description}, 0.1.0 beta"

_note_pwd: str = "Using 'pwd' for current directory and marks '.' and '..' is allowed"

_help_source: str = f"str | Path. Specify the source file to place the link into.\n{_note_pwd}"
_help_destination: str = f"str | Path. Specify the destination file to point with the link.\n{_note_pwd}"
_help_anchor: str = (
    "str. Specify the anchor to add to the end of the link.\n"
    "The octotorp sign # is optional")
_help_version: str = "Show the script version with the full information message and close the window"
_help_help: str = "Show the help message and close the window"

_example_source_1: str = "/Users/user/PycharmProjects/Protei_MME/content/common/config/diameter.md"
_example_destination_1: str = "/Users/user/PycharmProjects/Protei_MME/content/common/config/mme.md --anchor #database"
_example_source_2: str = r"C:\User\tarasov-a\PyCharmProjects\Protei_MME\content\common\basics\internal_architecture.md"
_example_destination_2: str = r"C:\User\tarasov-a\PyCharmProjects\Protei_MME\content\common\basics\images" \
                              r"\MME_INTERNAL_ARCH.svg"

epilog: str = dedent(f"""\
Examples:

python3 {prog} {_example_source_1} {_example_destination_1}

python {prog} pwd/gtp_c.md ../../logging/_index.md --anchor gtp-c

python {prog} {_example_source_2} {_example_destination_2}""")


class BaseError(Exception):
    """Base error class to inherit."""


class InvalidFileError(BaseError):
    def __init__(self, file: str, file_type: str):
        self._message: str = f"{file_type.capitalize()} path {file} is not a file"
        super().__init__(self._message)


class InvalidFileTypeError(BaseError):
    def __init__(self, file: str):
        _: str = p.splitext(file)[1].removeprefix(".")
        self._message: str = f"File {str(file)} is not a Markdown file, extension: {_}"
        super().__init__(self._message)


def validate_anchor(file: str, anchor: str | None):
    if anchor is None:
        return True
    else:
        anchor = anchor.removeprefix("#")

    try:
        with open(file, "r+", encoding="utf-8") as f:
            _content: str = f.read()

        _anchor_templates: tuple[str, ...] = (
            f'<a name="{anchor}">', "{%s}" % anchor
        )

        if any(_anchor_template in _content for _anchor_template in _anchor_templates):
            return True
        else:
            return False

    except PermissionError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        print("Anchor validation failed. Please inspect the anchor exists in the file on your own")
        return True

    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        print("Anchor validation failed. Please inspect the anchor exists in the file on your own")
        return True


def validate_path(path: str, path_type: str):
    if not p.exists(path):
        raise FileNotFoundError(f"Path {path} is not found")

    if not p.isfile(path):
        raise InvalidFileError(path, path_type)

    if path_type.lower() == "source" and p.splitext(path)[1] != _MARKDOWN_EXTENSION:
        raise InvalidFileTypeError(path)


def generate_rel_path(source: str, destination: str):
    try:
        _valid_link: str = p.relpath(destination, source)
    except OSError:
        raise
    else:
        return _valid_link


def generate_single_link(source: str, destination: str, anchor: OptStr = None):
    _path_source: str = p.abspath(source.replace("pwd", p.abspath(p.curdir)))
    _path_destination: str = p.abspath(destination.replace("pwd", p.abspath(p.curdir)))

    try:
        validate_path(_path_source, "source")
        validate_path(_path_destination, "destination")

    except BaseError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        input(_press_enter_key)
        raise

    try:
        _valid_link: str = generate_rel_path(_path_source, _path_destination)

    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        input(_press_enter_key)
        raise

    if p.basename(_path_source) in _INDEX_FILES:
        _valid_link = _valid_link.removeprefix("../")

    if p.basename(_path_destination) in _INDEX_FILES:
        _valid_link = p.dirname(_valid_link)
    elif p.splitext(_path_destination)[1] != _MARKDOWN_EXTENSION:
        pass
    else:
        _valid_link = f"{p.splitext(_valid_link)[0]}/"

    if not validate_anchor(_path_destination, anchor):
        print(f"Anchor {anchor} is not found in the file {_path_destination}, so it is omitted")
        return _valid_link

    if anchor is not None:
        _valid_link = f"{_valid_link}#{anchor.removeprefix('#')}/"

    return _valid_link


def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "source",
        action="store",
        help=_help_source)
    parser.add_argument(
        "destination",
        action="store",
        help=_help_destination)
    parser.add_argument(
        "-a", "--anchor",
        action="store",
        nargs=OPTIONAL,
        default=None,
        required=False,
        help=_help_anchor,
        dest="anchor")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")
        input(_press_enter_key)
        raise

    except ValueError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        input(_press_enter_key)
        raise


def generate():
    args: Namespace = parse_command_line()
    source: str = getattr(args, "source")
    destination: str = getattr(args, "destination")
    anchor: str = getattr(args, "anchor") if hasattr(args, "anchor") else None
    print(generate_single_link(source, destination, anchor))


if __name__ == '__main__':
    generate()
    input(_press_enter_key)
