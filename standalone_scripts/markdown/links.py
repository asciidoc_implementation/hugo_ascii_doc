from __future__ import annotations

import faulthandler
import logging
import os.path as p
from abc import abstractmethod
from collections import Counter
from functools import cached_property
from glob import iglob
from logging import Handler, LogRecord
from pathlib import Path
from re import Match, Pattern, compile, finditer, fullmatch
from sys import platform, stdout as sysout
from types import FrameType
from typing import Any, Callable, Iterable, Iterator, Literal, Type

from loguru import logger

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

_EXTENSION: str = "md"

_PATTERN_ANCHOR_HEADING: str = r"^#+\s+.*\{#([\w_-]+)}\s*$"
_PATTERN_ANCHOR_TEXT: str = r"^.*<a\sname=\"([\w_-]+)\">.*$"
_PATTERN_ANCHOR: Pattern = compile(rf"{_PATTERN_ANCHOR_HEADING}|{_PATTERN_ANCHOR_TEXT}")

_DELIMETER: str = "\\" if platform == "win32" else "/"

_PATTERN_LINK: Pattern = compile(r"\[[^]]+]\(([^)]+)\)")

_START_STOP: tuple[str, ...] = ("http", "mailto", "#")
_IMAGE_EXTENSIONS: tuple[str, ...] = ("jpg", "png", "svg")

_INDEX_FILES: tuple[str, ...] = (f"index.{_EXTENSION}", f"_index.{_EXTENSION}")


class BaseError(Exception):
    """Base class to inherit."""


class LineInvalidTypeError(BaseError):
    """Line index type must be int and its value must be str."""


class DestinationFileNotFoundError(BaseError):
    """Destination file from link is not found or cannot be handled."""


class LinkPartError(BaseError):
    """Link part index must be int in range: 0-1."""


class FileTypeInvalidError(BaseError):
    """File name is neither unique nor non-unique."""


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    _log_folder: Path = Path(__file__).parent.parent.parent.joinpath("logs")

    def __init__(
            self,
            file_name: str,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: str = str(self._log_folder.joinpath(f"{self._file_name}_debug.log"))
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler


def configure_custom_logging(name: str):
    if not faulthandler.is_enabled():
        faulthandler.enable()

    def inner(func: Callable):
        def wrapper(*args, **kwargs):
            handlers: dict[HandlerType, LoggingLevel] = {
                "stream": "INFO",
                "file_rotating": "DEBUG"
            }

            file_name: str = name

            logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
            stream_handler: dict[str, Any] = logger_configuration.stream_handler()
            rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

            logger.configure(handlers=[stream_handler, rotating_file_handler])

            logging.basicConfig(handlers=[InterceptHandler()], level=0)
            logger.debug("========================================")
            logger.debug("Запуск программы:")

            return func(*args, **kwargs)

        return wrapper

    if faulthandler.is_enabled():
        faulthandler.disable()

    return inner


def unique(values: Iterable[Any]) -> tuple[Any]:
    return tuple(k for k, v in Counter(values).items() if v == 1)


def not_unique(values: Iterable[Any]) -> tuple[Any]:
    return tuple(k for k, v in Counter(values).items() if v != 1)


def child(path: Path) -> str:
    return str(path.relative_to(path.parent.parent))


def extension(path: Path):
    return path.suffix[1:]


class LinkStorage:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._internal: tuple[str, ...] = tuple(iglob("**/*", root_dir=self._path, recursive=True))
        self.dir_indexes: dict[str, Path] = dict()
        self.dirindexes: dict[str, Path] = dict()
        self.images: dict[str, Path] = dict()
        self.md_files: dict[str, Path] = dict()

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._path)})>"

    def iter_dir_paths(self) -> Iterator[Path]:
        return iter(self._path.joinpath(_) for _ in self._internal if self._path.joinpath(_).is_dir())

    def iter_file_paths(self) -> Iterator[Path]:
        return iter(self._path.joinpath(_) for _ in self._internal if self._path.joinpath(_).is_file())

    def iter_md_file_paths(self) -> Iterator[Path]:
        return iter(_ for _ in self.iter_file_paths() if extension(_) == _EXTENSION)

    def iter_image_paths(self) -> Iterator[Path]:
        return iter(_ for _ in self.iter_file_paths() if extension(_) in _IMAGE_EXTENSIONS)

    def iter_non_index_paths(self) -> Iterator[Path]:
        return iter(_ for _ in self.iter_md_file_paths() if not _.stem.endswith("index"))

    def _prepare_dirs(self):
        for _ in self.iter_dir_paths():
            if _.joinpath(f"_index.{_EXTENSION}").exists():
                if _.name in self.dir_indexes.keys():
                    self.dir_indexes[_.name] = self._path
                else:
                    self.dir_indexes[_.name] = _.joinpath(f"_index.{_EXTENSION}")

            elif _.joinpath(f"index.{_EXTENSION}").exists():
                if _.name in self.dirindexes.keys():
                    self.dirindexes[_.name] = self._path
                else:
                    self.dirindexes[_.name] = _.joinpath(f"_index.{_EXTENSION}")

            else:
                logger.warning(
                    f"В директории {str(_)} не найдены ни файл index.{_EXTENSION}, ни файл _index.{_EXTENSION}."
                    f"Директория проигнорирована")
        return

    def _prepare_images(self):
        for _ in self.iter_image_paths():
            self.images[_.name] = _
        return

    def _prepare_md_files(self):
        for _ in self.iter_non_index_paths():
            if _.name in self.unique_md_names:
                self.md_files[_.name] = _
            elif _.name in self.not_unique_md_names:
                self.md_files[child(_)] = _
            else:
                logger.error(f"Некорректное значение {_}")
                raise FileTypeInvalidError
        return

    def prepare(self):
        self._prepare_dirs()
        self._prepare_images()
        self._prepare_md_files()

    @property
    def unique_md_names(self) -> tuple[str, ...]:
        return unique(_.name for _ in self.iter_non_index_paths())

    @property
    def not_unique_md_names(self) -> tuple[str, ...]:
        return not_unique(_.name for _ in self.iter_non_index_paths())

    def __iter__(self) -> Iterator[MarkdownFile]:
        return iter(MarkdownFile.identifiers.get(_.name) for _ in self.iter_md_file_paths())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._internal
        else:
            return False

    def __getitem__(self, item):
        if isinstance(item, str) and item in self:
            return MarkdownFile.identifiers.get(self._path.joinpath(item).name)
        else:
            raise KeyError

    def __len__(self):
        return len(tuple(iter(self)))

    def __bool__(self):
        return len(self) > 0


class Link:
    __slots__ = ("_source", "_link", "_link_storage")

    def __init__(
            self,
            source: str,
            link: str,
            link_storage: LinkStorage):
        self._source = source
        self._link: str = link
        self._link_storage: LinkStorage = link_storage

    def __str__(self):
        return f"{self.__class__.__name__}: link {self._link} from {self._source}"

    def __repr__(self):
        _kwargs: dict = {k: getattr(self, k) for k in self.__class__.__slots__}
        _kwargs_string: str = ", ".join([f"{k}={v}" for k, v in _kwargs.items()])
        return f"<{self.__class__.__name__}({_kwargs_string})>"

    def specify(self):
        _dest_path: Path = Path(self[0])
        _dest_name: str = _dest_path.name

        if _dest_name.endswith(_IMAGE_EXTENSIONS):
            link_type: str = "image"
        elif _dest_name.endswith(f"index.{_EXTENSION}") or _dest_name in self._link_storage.dirindexes.keys():
            link_type: str = "index"
        else:
            link_type: str = "md_file"

        _types = {
            "image": ImageLink,
            "index": DirIndexLink,
            "md_file": MarkdownFileLink
        }

        return _types.get(link_type)(self._source, self._link, self._link_storage)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._link.split("#")[item]
        else:
            logger.error(f"Тип должен быть int или slice, но получен {type(item)}")
            raise LinkPartError

    def __bool__(self):
        return self._link == self.link_updated()

    def from_file(self) -> MarkdownFile:
        return MarkdownFile(self._source)

    @property
    @abstractmethod
    def anchor(self): ...

    @property
    @abstractmethod
    def link_to_file(self): ...

    def validate_link(self):
        if self.destination_file() is None:
            raise DestinationFileNotFoundError

    @property
    @abstractmethod
    def destination(self) -> str: ...

    def destination_file(self) -> MarkdownFile:
        return MarkdownFile(self.destination)

    @property
    def valid_link(self):
        return p.relpath(self.destination_file().path, self._source)

    def is_valid_anchor(self):
        if self.anchor and self.anchor in MarkdownFile.anchors.get(self.destination):
            return True
        else:
            return False

    @property
    def valid_anchor(self):
        logger.info(f"anchor = {self.anchor}")
        if not self.anchor:
            return "/"
        elif self.anchor and self.anchor in MarkdownFile.anchors.get(self.destination):
            return f"/#{self.anchor}/"
        else:
            logger.warning(f"{self.anchor} не найден в файле {self.destination_file().path}, поэтому якорь удален")
            return "/"

    @abstractmethod
    def link_updated(self) -> str: ...


class ImageLink(Link):
    @property
    def anchor(self):
        return ""

    @property
    def link_to_file(self):
        return self._link

    @property
    def name(self):
        return self._link.split(_DELIMETER)[-1].removesuffix("/")

    @property
    def destination(self) -> str:
        return str(self._link_storage.images.get(self.name))

    def link_updated(self) -> str:
        if self.from_file().name in _INDEX_FILES:
            _: str = self.valid_link.removeprefix("../")
        else:
            _: str = f"../{self.valid_link}"

        return f"{_}{self.valid_anchor}"


class DirIndexLink(Link):
    @property
    def anchor(self):
        return self[-1].removesuffix("/")

    @property
    def link_to_file(self) -> str:
        return self[0]

    @property
    def name(self) -> str:
        return self[0].removesuffix("/").removesuffix("index.md").removesuffix("/")

    def _is_index(self):
        return self.name in self._link_storage.dir_indexes

    def _isindex(self):
        return self.name in self._link_storage.dirindexes

    @property
    def destination(self) -> str:
        if self._isindex():
            return str(self._link_storage.dirindexes.get(self.name))
        elif self._is_index():
            return str(self._link_storage.dir_indexes.get(self.name))

    def link_updated(self) -> str:
        if self.from_file().name in _INDEX_FILES:
            _: str = self.valid_link.removeprefix("../")
        else:
            _: str = f"../{self.valid_link}"

        if self._isindex():
            return f"{_.removesuffix(f'index.{_EXTENSION}')}{self.valid_anchor}"
        elif self._is_index():
            return f"{_.removesuffix(f'_index.{_EXTENSION}')}{self.valid_anchor}"


class MarkdownFileLink(Link):
    @property
    def anchor(self):
        return self[-1].removesuffix("/")

    @property
    def link_to_file(self):
        return self[0]

    def _options(self):
        return f"../{self.link_to_file}", self.link_to_file, self.link_to_file.removeprefix("../")

    def generate_options(self) -> tuple[str, ...]:
        return self._options()

    @property
    def name(self):
        _name: str = self[0].removesuffix("/").rsplit(_DELIMETER, 1)[-1]

        if _name in self._link_storage.not_unique_md_names:
            for _option in self._options():
                _: Path = Path(self._source).joinpath(_option)
                if _.exists():
                    _name: str = child(_)
        return _name

    @property
    def destination(self) -> str:
        return str(self._link_storage.md_files.get(self.name))

    def link_updated(self) -> str:
        if self.from_file().name in _INDEX_FILES:
            _: str = self.valid_link.removeprefix("../")
        else:
            _: str = f"../{self.valid_link}"

        return f"{_.removesuffix(f'.{_EXTENSION}')}{self.valid_anchor}"


class MarkdownFile:
    identifiers = dict()

    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._content: list[str] = []
        self._links: dict[int, list[Link]] = dict()

        if self._path.stem.endswith("index"):
            self._name: str = self._path.parent.name
        else:
            self._name: str = self._path.name

        self.__class__.identifiers[self._name] = self
        self._anchors: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({str(self._path)})>"

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.writelines("".join(self._content))

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            logger.error(f"Тип должен быть int или slice, но получен {type(item)}")
            raise LineInvalidTypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            logger.error(
                f"Тип ключа должен быть int, а значения - str, "
                f"но получен ключ типа {type(key)} и значение типа {type(value)}")
            raise LineInvalidTypeError

    def get_link(self, key: int):
        if key in self._links:
            return self._links.get(key)
        else:
            logger.error(f"На строке {key} нет ссылок")

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) > 0

    def __iter__(self):
        return iter(self._content)

    def get_anchors(self):
        for line in iter(self):
            _m: Match = fullmatch(_PATTERN_ANCHOR, line.removesuffix("\n"))
            if _m and _m.group(2):
                self._anchors.append(f"{_m.group(2)}")

    # def generate_link(self):
    #     for index, line in enumerate(iter(self)):
    #         for _match in finditer(_PATTERN_LINK, line):
    #             _link: str = _match.group(1)
    #             if _link.startswith(_START_STOP):
    #                 continue
    #             if index not in self._links.keys():
    #                 self._links[index] = []
    #             if Path(_link).suffix in _IMAGE_EXTENSIONS:
    #                 link: ImageLink = Link(self._path, _link, LinkType.IMAGE).specify()
    #             elif CustomPath(_link).stem

    def get_links(self, link_storage: LinkStorage):
        for index, line in enumerate(iter(self)):
            for _match in finditer(_PATTERN_LINK, line):
                _link_item: str = _match.group(2)
                if _link_item.startswith(_START_STOP):
                    continue
                if index not in self._links:
                    self._links[index] = []
                _link: Link = Link(str(self._path), _link_item, link_storage)
                self._links[index].append(_link)
        return

    def repair_links(self):
        for index, line in enumerate(iter(self)):
            for _match in finditer(_PATTERN_LINK, line):
                try:
                    link_item: str = _match.group(1)
                    # logger.info(f"link_item = {link_item}")
                    if link_item.startswith(_START_STOP) or link_item.endswith(_IMAGE_EXTENSIONS):
                        continue
                    _link: Link = Link(self._path, link_item)
                    _link.validate_link()
                    logger.info(f"_link = {_link}")
                    # logger.info(f"_link.correct_link = {_link.valid_link}")
                    if bool(_link):
                        logger.info(f"В файле {self._path} cсылка {link_item} корректна")
                    else:
                        self[index] = line.replace(link_item, _link.link_updated())
                        logger.error(
                            f"В файле {self._path} cсылка {link_item} некорректна.\n"
                            f"Ссылка из файла {self._path} на файл {_link.destination_file().path} заменена на:\n"
                            f"{_link.link_updated()}")
                except DestinationFileNotFoundError as e:
                    print(f"{e.__class__.__name__}, {str(e)}")
                    continue
        # self.write()

    @property
    def path(self) -> Path:
        return self._path

    @property
    def name(self) -> str:
        return self._name

    @property
    def anchors(self) -> list[str]:
        return self._anchors

    @property
    def links(self):
        return self._links


class Directory:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._internal: tuple[str, ...] = tuple(iglob("**/*", root_dir=self._path, recursive=True))

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    @property
    def dirs(self) -> tuple[Path, ...]:
        return tuple(self._path.joinpath(_) for _ in self._internal if self._path.joinpath(_).is_dir())

    @property
    def files(self) -> tuple[Path, ...]:
        return tuple(self._path.joinpath(_) for _ in self._internal if self._path.joinpath(_).is_file())

    @property
    def md_file_paths(self) -> tuple[Path, ...]:
        return tuple(_ for _ in self.files if extension(_) == _EXTENSION)

    @property
    def images_paths(self):
        return tuple(_ for _ in self.files if extension(_) in _IMAGE_EXTENSIONS)

    def __iter__(self) -> Iterator[MarkdownFile]:
        return iter(MarkdownFile.identifiers.get(_.name) for _ in self.md_file_paths)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._internal
        else:
            return False

    def __getitem__(self, item):
        if isinstance(item, str) and item in self:
            return MarkdownFile.identifiers.get(self._path.joinpath(item).name)
        else:
            raise KeyError

    def __len__(self):
        return len(list(iter(self)))

    def __bool__(self):
        return len(self) > 0


@configure_custom_logging("links")
def main():
    # _path: str = "C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\oss"
    _path: str = "/Users/user/PycharmProjects/hugo_ascii_doc/mme/common"
    link_storage: LinkStorage = LinkStorage(Path(_path))
    link_storage.prepare()
    # logger.info("dir_indexes:")
    # logger.info(link_storage.dir_indexes.items())
    # logger.info("dirindexes:")
    # logger.info(link_storage.dirindexes.items())
    # logger.info("images:")
    # logger.info(link_storage.images.items())
    # logger.info("md_files:")
    # logger.info(link_storage.md_files.items())

    directory: Directory = Directory(Path(_path))
    for md_path in directory.md_file_paths:
        markdown_file: MarkdownFile = MarkdownFile(md_path)
        markdown_file.read()
        markdown_file.get_anchors()
        logger.error(markdown_file.anchors)
        markdown_file.get_links(link_storage)
        logger.error(markdown_file.links.items())
    # directory: Directory = Directory(CustomPath(_path))
    # markdown_file: MarkdownFile
    # markdown_files: list[MarkdownFile] = [MarkdownFile(md_path) for md_path in directory.md_file_paths]
    # for markdown_file in markdown_files:
    #     markdown_file.read()
    #     markdown_file.get_anchors()
    #     markdown_file.repair_links()


if __name__ == '__main__':
    main()
