from argparse import ArgumentError, ArgumentParser, Namespace, RawDescriptionHelpFormatter, SUPPRESS
from pathlib import Path
from sys import platform
from typing import Iterable, Iterator, NamedTuple, TypeAlias


PathStr: TypeAlias = str | Path

_conversion_table: dict[str, str] = {
    "с": ", в секундах.",
    "мс": ", в миллисекундах.",
    "мин": ", в минутах.",
    "б": ", в байтах.",
    "Б": ", в битах.",
    "Мб": ", в мегабайтах.",
    "МБ": ", в мегабитах.",
    "Кб": ", в килобайтах.",
    "КБ": ", в килобитах.",
    "Б/с": ", в битах в секунду.",
    "": ""
}

prog: str = Path(__file__).name

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = f"{_python_version} {prog} <DIR>"

description: str = "Script to replace the measurement units and range from the Type column"
version: str = f"{description}, 0.1.0 beta"

_press_any_key: str = "Press the ENTER key to close the window ..."

_help_dir: str = "str | Path. Specify the folder to get all files inside and update"
_help_version: str = "Show the script version with the full information message and close the window"
_help_help: str = "Show the help message and close the window"


def split_line_br(line: str) -> list[str]:
    return [_.strip() for _ in line.replace("<br>", "\n").split("\n") if _]


def bound(items: Iterable[str], char: str) -> str:
    _: str = f" {char} ".join(items)
    return f"{char} {_} {char}"


class File:
    def __init__(self, file_path: PathStr):
        self._path: Path = Path(file_path).resolve()
        self._content: list[str] = []
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError(f"Key {item} must be int or slice")

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError(f"Key {key} must be int and value {value} must be str")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def _validate(self):
        if not self._path.is_file():
            raise TypeError(f"Path {self._path} does not lead to the file")

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    def lines(self):
        return "".join(self._content)

    @property
    def raw_extension(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def path(self):
        return self._path

    @property
    def content(self):
        return self._content


class Directory:
    def __init__(self, dir_path: PathStr):
        self._path: Path = Path(dir_path).resolve()
        self._files: list[Path] = []
        self._subfolders: list[Path] = []
        self._validate()
        self.set_items()

    @property
    def path(self):
        return self._path

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.is_dir():
            raise TypeError(f"Path {self._path} does not lead to the folder")

    def __iter__(self):
        return iter(_ for _ in self._path.iterdir())

    def set_items(self):
        self._files: list[Path] = list(filter(lambda x: x.is_file(), iter(self)))
        self._subfolders: list[Path] = list(filter(lambda x: x.is_dir(), iter(self)))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def iter_file_items(self) -> Iterator[File]:
        return iter(File(_) for _ in self._files)

    def iter_dir_items(self):
        return iter(self.__class__(_) for _ in self._subfolders)


class LineType(NamedTuple):
    value_type: str
    value_measurement: str | None = None
    value_range: str | None = None

    def __str__(self):
        _measurement_string: str = "" if self.value_measurement is None else f", {self.value_measurement}"
        _range_string: str = "" if self.value_range is None else f", {self.value_range}"
        return f"{self.__class__.__name__}: {self.value_type}{_measurement_string}{_range_string}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value_type}, {self.value_measurement}, {self.value_range})>"

    @property
    def range_limits(self) -> list[str]:
        if self.value_range is not None:
            return self.value_range.split("-")

    @property
    def min(self) -> str:
        return self.range_limits[0]

    @property
    def max(self) -> str:
        return self.range_limits[1]

    @property
    def measurement_string(self) -> str:
        if self.value_measurement is not None and self.value_measurement:
            return _conversion_table.get(self.value_measurement, f"NOT_FOUND, {self.value_measurement}")
        else:
            return "."

    @property
    def range_string(self) -> str:
        if self.value_range is not None and self.value_range:
            return f"<br>Диапазон: {self.min}-{self.max}."
        else:
            return ""

    @classmethod
    def from_line(cls, line: str):
        _items: list[str] = []
        _: list[str] = line.split("<br>")
        if not line.startswith("int") or line.startswith("int/") or line == "int":
            _items.extend((line, None, None))
        elif "," in _[0]:
            _values: list[str] = _[0].split(",")
            _items.extend((_values[0], _values[1].strip(), _[1]))
        elif "-" in _[1]:
            _items.extend((_[0], None, _[1]))
        else:
            _items.extend((_[0], _[1], None))
        return cls(*_items)


class TableLine:
    def __init__(self, line: str):
        self._line: str = line
        self._items: list[str] = [_.strip() for _ in line.split("|")[:-1] if _]

    def __str__(self):
        return f"{self.__class__.__name__}: {self._items}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._line})>"

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._items[item]
        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._items[key] = value
        else:
            raise TypeError

    def __len__(self):
        return len(self._items)

    def __iter__(self):
        return iter(self._items)

    def text_line(self):
        return f'{bound(self._items, "|")}\n'

    @property
    def description_value(self):
        return self[1]

    @description_value.setter
    def description_value(self, value):
        self[1] = value

    @property
    def type_value(self):
        return self[2]

    @type_value.setter
    def type_value(self, value):
        self[2] = value

    @property
    def line_type(self):
        return LineType.from_line(self.type_value)

    def update_description_value(self):
        _items: list[str] = [
            _.strip() for _ in self.description_value.replace("<br>", "\n").split("\n") if _]
        if _items:
            _items[0] = (
                f"{_items[0].removesuffix('.')}"
                f"{self.line_type.measurement_string}"
                f"{self.line_type.range_string}")
            self.description_value = "<br>".join(_items)
            self.type_value = self.line_type.value_type
            return

    def update(self):
        if len(self) != 6 or self[0] == "Параметр" or not self[2]:
            return self._line
        self.update_description_value()
        return self.text_line()


class MdFile(File):
    def __init__(self, file_path: PathStr):
        super().__init__(file_path)
        self._table_line_indexes: list[int] = []
        self._table_lines: list[str] = []

    def set_table_items(self):
        for index, line in enumerate(iter(self)):
            if line.startswith("| "):
                self._table_line_indexes.append(index)
                self._table_lines.append(line)

    def iter_table_lines(self) -> Iterator[TableLine]:
        return iter(TableLine(line) for line in self._table_lines)

    def update(self):
        self.set_table_items()
        for index, table_line in enumerate(self.iter_table_lines()):
            self[self._table_line_indexes[index]] = table_line.update()


def update(directory: Directory):
    for file in directory.iter_file_items():
        if file.raw_extension == "md":
            print(f"file = {file.path}")
            md_file: MdFile = MdFile(file.path)
            md_file.read()
            md_file.update()
            md_file.write()
    for subfolder in directory.iter_dir_items():
        md_directory: Directory = Directory(subfolder.path)
        update(md_directory)


def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "dir",
        action="store",
        help=_help_dir)
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")


def update_dir():
    args: Namespace = parse_command_line()
    dir_path: str = getattr(args, "dir")
    try:
        directory: Directory = Directory(dir_path)
        update(directory)
    except FileNotFoundError | NotADirectoryError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
    else:
        print("Success!")
    finally:
        input(_press_any_key)


if __name__ == '__main__':
    path_dir: str = "/Users/user/PycharmProjects/Protei_SS7FW/content/common/config/folder"
    _directory: Directory = Directory(path_dir)
    update(_directory)
