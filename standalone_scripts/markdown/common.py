from enum import Enum
from os import getenv
from pathlib import Path
from subprocess import CalledProcessError, TimeoutExpired, run
from sys import platform
from typing import Iterable, Iterator, TypeAlias

PathStr: TypeAlias = str | Path


def validate_path(base: str, file_or_dir: str) -> bool:
    base: str = base.replace("pwd", str(Path().cwd().resolve()))
    _base_path: Path = Path(base).resolve()
    if not _base_path.exists():
        print(f"Invalid base path: {base}")
        return False

    _full_path: Path = _base_path.joinpath(file_or_dir)
    print(_full_path)

    if not _full_path.exists():
        print(f"Invalid additional part to the base path: {file_or_dir}")
        return False
    print(_full_path)
    return True


def validate_path_no_base(path: PathStr, extension: Iterable[str] = None):
    _: Path = Path(path.replace("pwd", str(Path().cwd().resolve()))).resolve()
    if not path.exists():
        print(f"Invalid file path: {_}")
        return False
    if extension is not None and _.suffix.removeprefix(".") not in extension:
        print(f"Invalid file type: {_}")
        return False
    return True


def add_to_path():
    _path: str | None = getenv("PATH", None)
    if _path is None:
        raise OSError("$PATH variable is not found")

    if platform.startswith("win"):
        _separator: str = ";"
        _local: str = "C:\\Windows\\System32\\"
    else:
        _separator: str = ":"
        _local: str = "/usr/local/bin/"

    _: list[str] = _path.split(_separator)
    if _local not in _:
        run(["export", "PATH", "=", f'"$PATH{_separator}{_local}"'])


def cmd(commands: Iterable[str]):
    if platform.startswith("win"):
        commands: list[str] = ["cmd", *commands]
    return commands


def install(packages: Iterable[str]):
    add_to_path()
    for _package in packages:
        try:
            run(cmd((_package, "-v")), timeout=30, capture_output=True, check=True)
        except CalledProcessError | TimeoutExpired:
            try:
                run(cmd(("gem", "install", _package)), timeout=30, capture_output=True, check=True)
            except CalledProcessError | TimeoutExpired:
                try:
                    run(cmd(("sudo", "-S", "gem", "install", _package)), timeout=30, capture_output=True, check=True)
                except CalledProcessError | TimeoutExpired:
                    print(f"Failed to install {_package}")
                finally:
                    continue
            else:
                print(f"Gem {_package} installed")
                continue
        else:
            print(f"Gem {_package} is already installed")
            continue


class Extension(Enum):
    MARKDOWN = "md"
    ADOC = "adoc"
    ASCIIDOC = "asciidoc"
    YML = "yml"
    YAML = "yaml"
    TOML = "toml"
    INI = "ini"
    CFG = "cfg"
    PROPERTIES = "properties"
    LOG = "log"
    CDR = "cdr"
    JSON = "json"
    HTML = "html"
    XML = "xml"
    PY = "py"
    RB = "rb"
    CSS = "css"
    SCSS = "scss"
    TXT = "txt"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._value_}"

    @classmethod
    def _missing_(cls, value: object):
        print(f"{cls.__name__} {value} is not found")
        return None

    missing = _missing_

    @classmethod
    def reversed(cls):
        return {**cls._value2member_map_.items()}

    @classmethod
    def values(cls):
        return cls.__members__.values()


class File:
    def __init__(self, file_path: PathStr):
        self._path: Path = Path(file_path).resolve()
        self._content: list[str] = []
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError(f"Key {item} must be int or slice")

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError(f"Key {key} must be int and value {value} must be str")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def _validate(self):
        if not self._path.is_file():
            raise TypeError(f"Path {self._path} does not lead to the file")

    def read(self):
        try:
            with open(self._path, "r+", encoding="utf8") as f:
                _: list[str] = f.readlines()
        except UnicodeDecodeError as e:
            print(e)
            _ = []
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    def lines(self):
        return "".join(self._content)

    @property
    def name(self) -> str:
        return self._path.name

    @property
    def raw_extension(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def extension(self):
        if self.raw_extension in Extension.values():
            return Extension.reversed().get(self.raw_extension)
        else:
            return Extension.missing(self.raw_extension)

    @property
    def path_dir(self):
        return self._path.parent

    @property
    def path(self):
        return self._path

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        else:
            return NotImplemented

    def insert_line(self, index: int, line: str):
        if 0 <= index < len(self):
            self._content.insert(index, line)
        elif index == len(self):
            self._content.append(line)
        else:
            print(f"Index {index} is out of bounds")

    def delete_line(self, index: int):
        if 0 <= index < len(self):
            self._content.pop(index)
        else:
            print(f"Index {index} is out of bounds")

    @property
    def content(self):
        return self._content


class Directory:
    def __init__(self, dir_path: PathStr):
        self._path: Path = Path(dir_path).resolve()
        self._files: list[Path] = []
        self._subfolders: list[Path] = []
        self._validate()
        self.set_items()

    @property
    def path(self):
        return self._path

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.is_dir():
            raise TypeError(f"Path {self._path} does not lead to the folder")

    def __iter__(self):
        return iter(_ for _ in self._path.iterdir())

    def set_items(self):
        self._files: list[Path] = list(filter(lambda x: x.is_file(), iter(self)))
        self._subfolders: list[Path] = list(filter(lambda x: x.is_dir(), iter(self)))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def iter_file_items(self) -> Iterator[File]:
        return iter(File(_) for _ in self._files)

    def iter_dir_items(self):
        return iter(self.__class__(_) for _ in self._subfolders)
