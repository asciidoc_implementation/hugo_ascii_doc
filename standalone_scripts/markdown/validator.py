from __future__ import annotations

import faulthandler
import logging
import os.path as p
from enum import IntEnum
from functools import cached_property, wraps
from glob import iglob
from logging import Handler, LogRecord
from re import Pattern, compile, finditer, fullmatch
from sys import platform, stdout as sysout
from types import FrameType
from typing import Any, Callable, Literal, Match, NamedTuple, Type

from loguru import logger

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
_PATTERN_ANCHOR_HEADING: str = r"^#+\s+.*\{#([\w_-]+)}\s*$"
_PATTERN_ANCHOR_TEXT: str = r"^.*<a\sname=\"([\w_-]+)\">.*$"
_PATTERN_ANCHOR: Pattern = compile(rf"{_PATTERN_ANCHOR_HEADING}|{_PATTERN_ANCHOR_TEXT}")

_DELIMETER: str = "\\" if platform == "win32" else "/"

_PATTERN_LINK: Pattern = compile(r"\[[^]]+]\(([^)]+)\)")


def custom_logging(func: Callable):
    def wrapper(*args, **kwargs):
        if args is None or not args:
            _args_string: str = "none"
        else:
            _args_string: str = ", ".join(f"{arg}" for arg in args)

        if kwargs is None or not kwargs:
            _kwargs_string: str = "none"
        else:
            _kwargs_string: str = ", ".join(f"{k}={v}" for k, v in kwargs.items())

        logger.info(f"\nfunction: {func.__name__}\nargs: {_args_string}\nkwargs: {_kwargs_string}")
        return func(*args, **kwargs)

    return wrapper


def logging_faulthandler(func: Callable):
    if not faulthandler.is_enabled():
        faulthandler.enable()

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    if faulthandler.is_enabled():
        faulthandler.disable()
    return wrapper


class LogVerbosity(IntEnum):
    ZERO = 0
    LOW = 10
    MEDIUM = 20
    HIGH = 30

    def __str__(self):
        return f"{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self._name_}>"

    @classmethod
    def from_string(cls, value: str):
        if value.upper() not in cls.__members__:
            logger.debug(f"Level {value} is not found, so {cls.__name__}.LOW is set")
            return cls.LOW
        else:
            return cls[value.upper()]

    @classmethod
    def from_int(cls, value: int):
        if value not in cls._value2member_map_:
            logger.debug(f"Level {value} is not found, so {cls.__name__}.LOW is set")
            return cls.LOW
        else:
            return cls(value)


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    _log_folder: str = p.join(p.dirname(p.dirname(p.dirname(__file__))), "logs")

    def __init__(
            self,
            file_name: str, *,
            verbosity: int | str | LogVerbosity = LogVerbosity.LOW,
            handlers: dict[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers = dict()
        if isinstance(verbosity, str):
            verbosity: LogVerbosity = LogVerbosity.from_string(verbosity)
        elif isinstance(verbosity, int):
            verbosity: LogVerbosity = LogVerbosity.from_int(verbosity)
        self._file_name: str = file_name
        self._verbosity: LogVerbosity = verbosity
        self._handlers: dict[str, str] = handlers

    @staticmethod
    def _no_http(record: dict):
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self):
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self):
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self):
        return "{message}"

    def stream_handler(self):
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError:
            return
        else:
            return _handler

    def rotating_file_handler(self):
        try:
            _log_path: str = p.join(self._log_folder, f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError:
            return
        else:
            return _handler


def configure_custom_logging(name: str):
    def inner(func: Callable):
        def wrapper(*args, **kwargs):
            handlers: dict[HandlerType, LoggingLevel] = {
                "stream": "INFO",
                "file_rotating": "DEBUG"
            }

            file_name: str = name

            logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers=handlers)
            stream_handler: dict[str, Any] = logger_configuration.stream_handler()
            rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

            logger.configure(handlers=[stream_handler, rotating_file_handler])

            logging.basicConfig(handlers=[InterceptHandler()], level=0)
            logger.debug("========================================")
            logger.debug("Запуск программы:")

            return func(*args, **kwargs)

        return wrapper

    return inner


def get_match(m: Match):
    return [_ for _ in m.groups() if _]


def join_path(base: str, path: str):
    return p.normpath(p.join(base, path))


def validate_dir(path: str):
    try:
        return p.isdir(p.realpath(path, strict=True))
    except OSError as e:
        print(f"{e.__class__.__name__}, путь {path} некорректен или не существует")
        raise


def validate_file(path: str):
    try:
        return p.isfile(p.realpath(path, strict=True))
    except OSError as e:
        print(f"{e.__class__.__name__}, путь {path} некорректен или не существует")
        raise


def stem(path: str):
    if validate_file(path):
        return p.basename(path).split(".")[0]
    elif validate_dir(path):
        return p.basename(path).split(_DELIMETER)[-1]
    raise FileNotFoundError


def suffix(path: str):
    if not validate_file(path):
        raise FileNotFoundError
    return p.basename(path).split(".")[1]


class _LinkFile(NamedTuple):
    base_path: str
    path: str
    anchors: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}: {self.full_path}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    @property
    def full_path(self):
        return join_path(self.base_path, self.path)

    def find_anchors(self):
        if bool(self):
            with open(self.full_path, "r+", encoding="utf-8") as f:
                for line in f.readlines():
                    _m: Match = fullmatch(_PATTERN_ANCHOR, line.removesuffix("\n"))
                    if _m:
                        self.anchors.extend(get_match(_m))
        return self

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.anchors
        else:
            return False

    def __len__(self):
        return len(self.anchors)

    def __bool__(self):
        return p.isfile(self.full_path)

    def __iter__(self):
        return iter(self.anchors)


class LinkStorage:
    def __init__(self, root_dir: str = None):
        if root_dir is None:
            root_dir: str = p.dirname(__file__)
        elif not validate_dir(root_dir):
            raise FileNotFoundError

        self._root_dir: str = p.normpath(root_dir)
        self._links: tuple[str, ...] = tuple(iglob("**/*", root_dir=root_dir, recursive=True))

    def __str__(self):
        return f"{self.__class__.__name__}: {self._root_dir}\n{self._links}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._root_dir})>"

    def __iter__(self):
        return iter(self._links)

    def __len__(self):
        return len(self._links)

    def __bool__(self):
        return len(self) > 0

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._links
        else:
            return NotImplemented

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self:
                return join_path(self._root_dir, item)
            else:
                raise KeyError
        elif isinstance(item, int):
            if 0 <= item < len(self):
                return join_path(self._root_dir, self._links[item])
            else:
                raise ValueError
        else:
            raise TypeError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._root_dir == other._root_dir
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._root_dir != other._root_dir
        else:
            return NotImplemented

    def dict_files(self) -> dict[str, str]:
        """Specifies the dictionary of short names and full paths.
            :returns: dict[str, str]
        """
        return {
            f"{p.basename(p.dirname(self[link]))}/{stem(self[link])}": f"{self[link]}"
            for link in self}

    def _set_path(self, path: str):
        return join_path(self._root_dir, path)

    def _is_dir(self, path: str):
        return p.isdir(self._set_path(path))

    def _is_file(self, path: str):
        return p.isfile(self._set_path(path))

    def _get_stem(self, path: str):
        return p.basename(self._set_path(path)).split(".")[0]

    @property
    def root_dir(self):
        return self._root_dir

    @property
    def dirindexes(self):
        def _isindex(path: str):
            return p.exists(self._set_path(f"{path}/index.md"))

        return tuple(map(self._get_stem, filter(self._is_dir and _isindex, self._links)))

    @property
    def dir_indexes(self):
        def _is_index(path: str):
            return p.exists(self._set_path(f"{path}/_index.md"))

        return tuple(map(self._get_stem, filter(self._is_dir and _is_index, self._links)))

    @property
    def file_indexes(self) -> tuple[str, ...]:
        return tuple(filter(self._is_file, self._links))

    @property
    def markdown_files(self):
        return tuple(filter(lambda x: suffix(self._set_path(x)) == "md" and x != "_index.md", self.file_indexes))

    @cached_property
    def link_files(self) -> list[_LinkFile]:
        return [_LinkFile(self._root_dir, link).find_anchors() for link in self.markdown_files]

    def anchors(self) -> dict[str, tuple[str, ...]]:
        return {k.full_path: (*k.anchors,) for k in self.link_files}


class Link(NamedTuple):
    link_storage: LinkStorage
    source: str
    link: str

    def __str__(self):
        return f"{self.__class__.__name__}: link {self.link} from {self.source}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self.source.split("#")[item]
        else:
            raise TypeError

    def __bool__(self):
        return self.link == self.valid_link

    @property
    def anchor(self):
        if "#" in self.link:
            return self[-1].removesuffix("/")
        else:
            return ""

    @property
    def destination(self):
        _split: list[str] = self[0].removesuffix("/").split(_DELIMETER)
        logger.info(f"_split = {_split}")
        _name: str = _split[-1]

        if _name in self.link_storage.dirindexes:
            _file: str = f"{_name}/index"
        elif _name in self.link_storage.dir_indexes:
            _file: str = f"{_name}/_index"
        else:
            _file: str = f"{_split[-2]}/{_name}"

        return self.link_storage.dict_files().get(_file)

    @property
    def valid_anchor(self):
        logger.info(f"anchor = {self.anchor}")
        if not self.anchor:
            return "/"
        elif self.anchor not in self.link_storage.anchors().get(self.destination):
            logger.warning(f"{self.anchor} не найден в файле {self.destination}, поэтому якорь удален")
            return "/"
        else:
            return f"/#{self.anchor}/"

    @property
    def valid_link(self):
        logger.info(f"destination = {self.destination}")
        logger.info(f"Link.link = {self.link}")
        _link: str = p.relpath(self.destination, self.source)
        if p.basename(self.source) not in ("index.md", "_index.md"):
            _link: str = f"../{_link.removesuffix('.md')}"
        return _link

    @property
    def correct_link(self):
        return f"{self.valid_link}{self.valid_anchor}"

    @property
    def final_link(self):
        _correct_link: str = f"{self.valid_link}{self.valid_anchor}"
        if _correct_link != self.link:
            logger.warning(
                f"В файле {self.source} cсылка {self.link} некорректна.\n"
                f"Ссылка из файла {self.source} на файл {self.destination} заменена на:\n"
                f"{_correct_link}")
        return _correct_link


class MarkdownFile:
    def __init__(self, path: str, link_storage: LinkStorage):
        validate_file(path)
        self._path: str = path
        self._link_storage: LinkStorage = link_storage
        self._content: list[str] = []

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.writelines("".join(self._content))

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        return len(self) > 0

    def __iter__(self):
        return iter(self._content)

    def links(self):
        _links: list[Link] = []
        for index, line in enumerate(iter(self)):
            for _match in finditer(_PATTERN_LINK, line):
                link_item: str = f"{_match.group(1).removeprefix('#')}"
                logger.info(f"link_item = {link_item}")
                if link_item.startswith(("http", "#")):
                    continue
                _link: Link = Link(self._link_storage, self._path, link_item)
                _links.append(_link)
                logger.info(f"_link.correct_link = {_link.correct_link}")
                logger.info(f"_link.final_link = {_link.final_link}")

    def repair_links(self):
        for index, line in enumerate(iter(self)):
            for _match in finditer(_PATTERN_LINK, line):
                link_item: str = _match.string.decode("utf-8")
                if link_item.startswith("http"):
                    continue
                _link: Link = Link(self._link_storage, self._path, link_item)
                if _link.correct_link == link_item:
                    continue
                self[index] = line.replace(link_item, _link.correct_link)
                print(
                    f"В файле {self._path} cсылка {link_item} некорректна.\n"
                    f"Ссылка из файла {self._path} на файл {_link.destination} заменена на:\n"
                    f"{_link.correct_link}")


@configure_custom_logging("validator")
def main():
    _path: str = "/Users/user/PycharmProjects/hugo_ascii_doc/Protei_MME/content/common"
    _storage: LinkStorage = LinkStorage(_path)
    for _ in _storage.markdown_files:
        logger.info(f"link = {_}")
        logger.info(f"full_link = {join_path(_storage.root_dir, _)}")
        markdown_file: MarkdownFile = MarkdownFile(join_path(_storage.root_dir, _), _storage)
        markdown_file.read()
        markdown_file.links()


if __name__ == '__main__':
    main()
