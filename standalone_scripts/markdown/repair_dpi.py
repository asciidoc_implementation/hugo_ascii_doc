import faulthandler
import logging
from argparse import ArgumentError, ArgumentParser, Namespace, RawDescriptionHelpFormatter, SUPPRESS
from functools import cached_property, wraps
from importlib import import_module
from logging import Handler, LogRecord
from pathlib import Path
from string import digits
from subprocess import CalledProcessError, TimeoutExpired, run
from sys import platform, stdout as sysout
from textwrap import dedent
from types import FrameType
from typing import Any, Callable, Iterable, Iterator, Literal, Mapping, Type, TypeAlias
from loguru import logger


def import_libs(packages: str | Iterable[str]):
    _successful_packages: list[str] = []
    _failed_packages: list[str] = []

    if isinstance(packages, str):
        packages: tuple[str] = (packages,)
    else:
        packages: tuple[str, ...] = *packages,
    for package in iter(packages):
        try:
            import_module(package)
        except ModuleNotFoundError | ImportError as e:
            print(f"{e.__class__.__name__}, module = {e.name}, message = {e.msg}")
            print(f"Trying to install the module {package}")
            if platform.startswith("win"):
                py: str = "py"
            else:
                py: str = "python3"
            try:
                run([py, "-m", "pip", "install", "loguru"], timeout=30, check=True)
            except CalledProcessError as e:
                print("Sorry, something went wrong.")
                print(f"{e.__class__.__name__}, command = {e.cmd}, return code = {e.returncode}")
                _failed_packages.append(package)
            except TimeoutExpired as e:
                print("Sorry, something went wrong.")
                print(f"{e.__class__.__name__}, command = {e.cmd}, timeout = {e.timeout}")
                _failed_packages.append(package)
            except OSError as e:
                print("Sorry, something went wrong.")
                print(f"{e.__class__.__name__}, error = {e.strerror}")
                _failed_packages.append(package)
            else:
                _successful_packages.append(package)
        else:
            print(f"Package {package} is successfully imported!")
    if _failed_packages:
        _packages_string: str = "\n".join(_failed_packages)
        print(f"The following packages are required but not found and failed to be installed: {_packages_string}")
        input(_press_enter_key)
    elif _successful_packages:
        _packages_string: str = "\n".join(_successful_packages)
        print(f"The following packages have been installed since required but had not been found: {_packages_string}")
        print(f"Everything is ok")

PathStr: TypeAlias = str | Path

_FRONT_MATTER: str = dedent("""\
---
title: "%s"
description: "%s"
weight: 20
type: docs
---
""")
_ALIGNMENT: tuple[str, ...] = ("|:", ":|")
_LIST_NUMBERED: tuple[str, ...] = tuple(f"{digit}." for digit in digits)
_DEFAULT: str = "умолчанию"
_PREFIX: str = ". По умолчанию: "
_POSTFIX: str = "."
_AVOID_START_LINE: tuple[str, ...] = ("| Параметр", "|-", "|:-", "| -", "| :-")
_AVOID_FROM_VALUE: tuple[str, ...] = ("-", "\\-", "")

description: str = "Script to repair DPI"
prog: str = Path(__file__).name
_press_enter_key: str = "Press the ENTER key to close the window ..."

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = (
    f"{_python_version} {prog} \n"
    f"    [ -h/--help | -v/--version ] \n"
    f"    <DIR> [ -a/--api | -c/--columns | -d/--dots | -l/-lists | -s/--side | -t/--types | -r/--prc ]"
    f"    [ --off ]")

version: str = f"{description}, 1.0.1 beta\n"

_help_version: str = (
    "Show the script version with the full information message and close the window")
_help_help: str = "Show the help message and close the window"
_help_api: str = "Flag. Specify to generate files from the api one."
_help_dots: str = "Flag. Specify to add dots to the description lines."
_help_lists: str = "Flag. Specify to convert numbered lists to the unordered ones."
_help_side: str = "Flag. Specify to set the alignment to the left."
_help_types: str = "Flag. Specify to convert the values in the Type column to the proper ones."
_help_prc: str = "Flag. Specify to convert the values in the PR column to the proper ones."
_help_columns: str = "Flag. Specify to merge the columns Description and Default."
_help_off: str = "Flag. Specify to turn off logging."
_help_directory: str = (
    "String. Specify the path to the directory to modify the files.\n"
    "Note that the path can be relative")

epilog = dedent(r"""Examples:

python3 repair_dpi.py ../directory --api --columns --dots --lists --side --types --prc --off
python repair_dpi.py C:\Users\user\PyCharmProjects\project\content\common -a -с -d -l -s -t -r --off
""")

terms_log_folder: Path = Path.home().joinpath("Desktop").joinpath("_repair_dpi_logs")

HandlerType: Type[str] = Literal["stream", "file_rotating"]
LoggingLevel: Type[str] = Literal["TRACE", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


class InterceptHandler(Handler):
    def emit(self, record: LogRecord):
        # Get corresponding loguru level if it exists
        try:
            level: str = logger.level(record.levelname).name
        except ValueError:
            level: int = record.levelno
        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame: FrameType = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


class LoggerConfiguration:
    def __init__(
            self,
            file_name: str,
            handlers: Mapping[HandlerType, LoggingLevel] = None):
        if handlers is None:
            handlers: dict[str, str] = dict()
        self._file_name: str = file_name
        self._handlers: dict[str, str] = {**handlers}

    @staticmethod
    def _no_http(record: dict) -> bool:
        return not record["name"].startswith("urllib3")

    @cached_property
    def _colored_format(self) -> str:
        return " | ".join(
            ("<green>{time:DD-MMM-YYYY HH:mm:ss}</green>::<level>{level.name}</level>",
             "<cyan>{module}</cyan>::<cyan>{function}</cyan>",
             "<cyan>{file.name}</cyan>::<cyan>{name}</cyan>::<cyan>{line}</cyan>",
             "\n<level>{message}</level>")
        )

    @cached_property
    def _wb_format(self) -> str:
        return " | ".join(
            ("{time:DD-MMM-YYYY HH:mm:ss}::{level.name}",
             "{module}::{function}",
             "{file.name}::{name}::{line}",
             "\n{message}")
        )

    @cached_property
    def _user_format(self) -> str:
        return "{message}"

    def stream_handler(self) -> dict[str, Any]:
        try:
            _logging_level: str | None = self._handlers.get("stream")
            _handler: dict[str, Any] = {
                "sink": sysout,
                "level": _logging_level,
                "format": self._user_format,
                "colorize": True,
                "filter": self._no_http,
                "diagnose": True
            }
        except KeyError as e:
            print(f"{e.__class__.__name__}, {str(e)}")
            raise e
        else:
            return _handler

    def rotating_file_handler(self) -> dict[str, Any]:
        try:
            _log_path: Path = Path(terms_log_folder).joinpath(f"{self._file_name}_debug.log")
            _logging_level: str = self._handlers.get("file_rotating")
            _handler: dict[str, Any] = {
                "sink": _log_path,
                "level": _logging_level,
                "format": self._colored_format,
                "colorize": False,
                "diagnose": True,
                "rotation": "2 MB",
                "mode": "a",
                "encoding": "utf8"
            }
        except KeyError as e:
            print(f"{e.__class__.__name__}, {str(e)}")
            raise e
        else:
            return _handler


def complete_configure_custom_logging(name: str):
    handlers: dict[HandlerType, LoggingLevel] = {
        "stream": "INFO",
        "file_rotating": "DEBUG"
    }

    file_name: str = name

    logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)
    stream_handler: dict[str, Any] = logger_configuration.stream_handler()
    rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

    logger.configure(handlers=[stream_handler, rotating_file_handler])

    logging.basicConfig(handlers=[InterceptHandler()], level=0)

    def inner(func: Callable):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner


def logging_faulthandler(func: Callable):
    if not faulthandler.is_enabled():
        faulthandler.enable()

    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    if faulthandler.is_enabled():
        faulthandler.disable()
    return wrapper


def multi_replace(line: str, replace_table: Mapping[str, str] = None):
    if replace_table is not None:
        for k, v in replace_table.items():
            line = line.replace(k, v)
    return line


def get_base_path(file_path: Path):
    for parent_path in tuple(file_path.parents):
        if str(parent_path.resolve()).endswith("content/common"):
            return str(file_path.relative_to(parent_path.parent.parent.resolve()))
    else:
        logger.error(f"File path {file_path} is invalid")
        raise ValueError


def write_file(path: PathStr, content: list[str] = None):
    path: Path = Path(path).resolve()
    if content is None:
        return
    heading: str = content[0]
    try:
        _description, _title = heading.lstrip("#").strip().split(",")
        path_file: Path = path.joinpath(_title)
        path_file.touch(exist_ok=True)
        gitlab_docs_path: str = get_base_path(path_file)
        _front_matter: str = _FRONT_MATTER % (_title.strip(), _description.strip(), gitlab_docs_path)
        with path_file.open("a+") as f:
            f.write(_front_matter)
            f.writelines(content)
    except (PermissionError | ValueError) as e:
        logger.error(f"{e.__class__.__name__}, {str(e)}")
        logger.error("File is not written")
    except OSError as e:
        logger.error(f"{e.__class__.__name__}, {e.strerror}")
        logger.error("File is not written")
    else:
        logger.info(f"File {str(path_file)} is created and written")


def split_table_line(line: str):
    return [_.strip() for _ in line.removeprefix("|").removesuffix("|").split("|")]


def merge_columns(
        line: str,
        from_column: int,
        to_column: int,
        prefix: str,
        postfix: str):
    _items: list[str] = split_table_line(line)
    _max: int = max((from_column, to_column))
    if len(_items) < _max:
        logger.info(f"Number of columns in line {line} is less than required: {_max}")
        return line
    if line.startswith("|-"):
        _line: str = "|---" * (len(_items) - 1)
        return f"{_line}|\n"
    if not line.startswith(_AVOID_START_LINE) and _items[from_column].strip() not in _AVOID_FROM_VALUE:
        _items[to_column] = f"{_items[to_column].removesuffix('.')}{prefix}{_items[from_column]}{postfix}"
    _items.pop(from_column)
    _line: str = " | ".join(_items)
    return f"| {_line} |\n"


class File:
    def __init__(self, file_path: PathStr):
        self._path: Path = Path(file_path).resolve()
        self._content: list[str] = []
        self._table_indexes: list[int] = []
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError(f"Key {item} must be int or slice")

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError(f"Key {key} must be int and value {value} must be str")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def _validate(self):
        if not self._path.is_file():
            raise TypeError(f"Path {self._path} does not lead to the file")

    def read(self):
        try:
            with open(self._path, "r+", encoding="utf8") as f:
                _: list[str] = f.readlines()
        except UnicodeDecodeError as e:
            print(e)
            _ = []
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    def lines(self):
        return "".join(self._content)

    @property
    def name(self) -> str:
        return self._path.name

    @property
    def extension(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def path_dir(self):
        return self._path.parent

    @property
    def path(self):
        return self._path

    @property
    def content(self):
        return self._content

    def execute(self, func_name: str, is_execute: bool = None, *args, **kwargs):
        if is_execute:
            return getattr(self, func_name)(*args, **kwargs)
        else:
            return

    def iter_table_indexes(self) -> Iterator[int]:
        return iter(self._table_indexes)

    def set_table_indexes(self):
        self._table_indexes = [index for index, line in enumerate(iter(self)) if line.startswith("|")]

    def repair_prc(self):
        _replace_table: dict[str, str] = {
            " NR ": " P  ",
            " PR ": " C  "
        }
        logger.debug(f"\nrepair_pr, {str(self._path)}")
        for index in self.iter_table_indexes():
            _line: str = self[index]
            if any(_ in _line for _ in _replace_table):
                logger.debug(f"\nindex= {index}, line = {_line}")
                self[index] = f"{multi_replace(_line, _replace_table)}"
        logger.info(f"PR column values are replaced with proper ones")

    def repair_types(self):
        _replace_table: dict[str, str] = {
            " Bool ": "bool",
            " Int ": "int",
            " Set ": "list",
            " OptionSet ": "object",
            " String ": "string",
            " Number ": " int ",
            " Array ": " list ",
            " Object ": " object ",
            " IPv4 ": "ip"
        }
        logger.debug(f"\nrepair_types, {str(self._path)}")
        for index in self.iter_table_indexes():
            _line: str = self[index]
            if any(_ in _line for _ in _replace_table):
                logger.debug(f"\nindex = {index}, line = {self[index]}")
                self[index] = f"{multi_replace(_line, _replace_table)}"
        logger.info(f"Types are replaced with proper ones")

    def repair_dots(self):
        logger.debug(f"\nrepair_dots, {str(self._path)}")
        for index in self.iter_table_indexes():
            if self[index].startswith(("| Параметр", "|--", "| -", "|:-")):
                continue
            items: list[str] = [_ for _ in self[index].removesuffix("\n")[1:-1].strip().split("|")]
            if len(items) < 2:
                continue
            if items[1].strip():
                items[1] = f"{items[1].strip().removesuffix('.')}."
            self[index] = f"| {' | '.join(items)} |\n"
        logger.info(f"Dots are added to descriptions")

    def repair_lists(self):
        logger.debug(f"\nrepair_lists, {str(self._path)}")
        for index, line in enumerate(iter(self)):
            if line.startswith(_LIST_NUMBERED) and index not in self.iter_table_indexes():
                logger.debug(f"\nindex = {index}, line = {line}")
                self[index] = f"* {line.split('.', 1)[1]}"
        logger.info(f"Numbered lists are converted to unordered ones")

    def _make_dir(self, name: str):
        _: Path = self._path.parent.joinpath(name)
        logger.debug(f"path = {_}")
        _.mkdir(exist_ok=True)
        return _

    def repair_api(self):
        logger.debug(f"\nrepair_api, {str(self._path)}")
        dir_path: Path = self._make_dir("api")
        logger.info(f"Directory {str(dir_path)} is created")
        _heading: str = "###"
        headers: list[int] = [0]
        headers.extend([index for index, line in enumerate(iter(self)) if line.startswith(f"{_heading} ")])
        headers.append(len(self))
        logger.debug(f"\nheaders = {headers}")
        for idx in headers[:-1]:
            logger.debug(f"\nrange = {headers[idx]}, {headers[idx + 1]}")
            _content: list[str] = [self[_] for _ in range(headers[idx], headers[idx + 1])]
            write_file(dir_path)
        logger.info(f"Files for API requests are generated")

    def repair_side(self):
        logger.debug(f"\nrepair_side, {str(self._path)}")
        for index, line in enumerate(iter(self)):
            if any(_ in line for _ in _ALIGNMENT):
                logger.debug(f"\nindex = {index}, line = {line}")
                self[index] = line.replace(":", "-")
        logger.info(f"Alignment is repaired")

    @property
    def _headers(self):
        return [index for index in self.iter_table_indexes() if self[index].startswith(("| Параметр", "| Поле"))]

    def _from_to_header(self, header_from_index: int):
        if self._headers.index(header_from_index) + 1 == len(self._headers):
            _border: int = -1
        else:
            _border: int = self._headers[self._headers.index(header_from_index) + 1]
        _first: int = self._table_indexes[self._headers.index(header_from_index)]
        _last: int = self._table_indexes[_border]
        return [index for index in self._table_indexes if _first <= index <= _last]

    def repair_columns(self):
        logger.debug(f"\nrepair_default, {str(self._path)}")
        for header_index in self._headers:
            _line: str = self[header_index]
            if _DEFAULT not in split_table_line(_line)[3]:
                logger.debug(f"Header {_line} does not contain the word '{_DEFAULT}', so the table is omitted")
                continue
            else:
                for index in self._from_to_header(header_index):
                    _: str = self[index]
                    self[index] = merge_columns(_, 3, 1, _PREFIX, _POSTFIX)
        logger.info("Columns are merged")


class FileDirectory:
    def __init__(self, dir_path: PathStr):
        self._path: Path = Path(dir_path).resolve()
        self._files: list[Path] = []
        self._subfolders: list[Path] = []
        self._validate()
        self.set_items()

    @property
    def path(self):
        return self._path

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.is_dir():
            raise TypeError(f"Path {self._path} does not lead to the folder")

    def __iter__(self):
        return iter(_ for _ in self._path.iterdir())

    def set_items(self):
        self._files: list[Path] = list(filter(lambda x: x.is_file() and x.suffix == ".md", iter(self)))
        self._subfolders: list[Path] = list(filter(lambda x: x.is_dir(), iter(self)))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def iter_file_items(self) -> Iterator[File]:
        return iter(File(_) for _ in self._files)

    def iter_dir_items(self):
        return iter(self.__class__(_) for _ in self._subfolders)


@logging_faulthandler
@complete_configure_custom_logging("repair_dpi")
def generate(
        directory: str,
        api: bool = None,
        dots: bool = None,
        lists: bool = None,
        side: bool = None,
        types: bool = None,
        prc: bool = None,
        columns: bool = None,
        off: bool = None):
    if off:
        logger.disable("__main__")
        print("Logger is off")
    file_directory: FileDirectory = FileDirectory(directory)
    file_directory.set_items()
    for file in file_directory.iter_file_items():
        logger.info(f"\nFile {file.path}")
        logger.info("------------------------")
        file.read()
        file.set_table_indexes()
        file.execute("repair_api", api)
        file.execute("repair_dots", dots)
        file.execute("repair_lists", lists)
        file.execute("repair_side", side)
        file.execute("repair_types", types)
        file.execute("repair_prc", prc)
        file.execute("repair_columns", columns)
        file.write()
    logger.info("\nAll the operations are done")
    input(_press_enter_key)


@logging_faulthandler
@complete_configure_custom_logging("repair_dpi")
def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "directory",
        action="store",
        help=_help_directory)
    parser.add_argument(
        "-a", "--api",
        action="store_true",
        default=SUPPRESS,
        help=_help_api,
        dest="api")
    parser.add_argument(
        "-c", "--columns",
        action="store_true",
        default=SUPPRESS,
        help=_help_columns,
        dest="columns")
    parser.add_argument(
        "-d", "--dots",
        action="store_true",
        default=SUPPRESS,
        help=_help_dots,
        dest="dots")
    parser.add_argument(
        "-l", "--lists",
        action="store_true",
        default=SUPPRESS,
        help=_help_lists,
        dest="lists")
    parser.add_argument(
        "-s", "--side",
        action="store_true",
        default=SUPPRESS,
        help=_help_side,
        dest="side")
    parser.add_argument(
        "-t", "--types",
        action="store_true",
        default=SUPPRESS,
        help=_help_types,
        dest="types")
    parser.add_argument(
        "-r", "--prc",
        action="store_true",
        default=SUPPRESS,
        help=_help_prc,
        dest="prc")
    parser.add_argument(
        "--off",
        action="store_true",
        default=SUPPRESS,
        help=_help_off,
        dest="off")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            logger.debug(args)
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")
        input(_press_enter_key)


@logging_faulthandler
@complete_configure_custom_logging("repair_dpi")
def repair():
    logger.info("================================================================================")
    logger.debug("Запуск программы:")
    args: Namespace = parse_command_line()
    directory: str = getattr(args, "directory")
    api: bool = getattr(args, "api") if hasattr(args, "files") else False
    dots: bool = getattr(args, "dots") if hasattr(args, "dots") else False
    lists: bool = getattr(args, "lists") if hasattr(args, "lists") else False
    side: bool = getattr(args, "side") if hasattr(args, "side") else False
    types: bool = getattr(args, "types") if hasattr(args, "types") else False
    prc: bool = getattr(args, "prc") if hasattr(args, "prc") else False
    columns: bool = getattr(args, "columns") if hasattr(args, "columns") else False
    off: bool = getattr(args, "off") if hasattr(args, "off") else False
    return generate(directory, api, dots, lists, side, types, prc, columns, off)


if __name__ == '__main__':
    import_libs("loguru")
    repair()
