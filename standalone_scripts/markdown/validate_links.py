import os
import os.path as p
from glob import glob
from re import Pattern, compile, findall, finditer
from typing import Iterator, NamedTuple


_INDEX_FILES: tuple[str, ...] = ("_index", "index")
_MD: str = "md"
_LINK_PATTERN: Pattern = compile(r"\[[^(]]\((\S*)\)")
_NOT_INSPECTED_LINKS: tuple[str, ...] = ("http", "ftp", "mailto", "www", "///")


class BaseError(Exception):
    """Base error class to inherit."""


class LinkInitError(BaseError):
    """Non-Markdown file cannot have an anchor."""


class FileKeyGetError(BaseError):
    """Key must be int or slice."""


class FileKeySetError(BaseError):
    """Key must be int and value must be str."""


class FileInvalidItemError(BaseError):
    """Path does not lead to the file."""


class LinkInvalidError(BaseError):
    """Link destination file is not found."""


class LineWithoutLinkError(BaseError):
    """Line does not have any links."""


class Link(NamedTuple):
    from_file: str
    to_file: str
    anchor: str | None
    extension: str = _MD

    def _validate(self):
        if self.extension != _MD and self.anchor is not None:
            raise LinkInitError

    def __str__(self):
        _anchor = f", #{self.anchor}" if self.anchor is not None else ""
        return f"{self.__class__.__name__}: {self.from_file} -> {self.to_file}{_anchor}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    @property
    def source_file(self) -> str:
        return p.abspath(f"{self.from_file}.md")

    @property
    def dest_file(self) -> str:
        return p.abspath(f"{self.to_file}{self.extension}")

    @property
    def is_from_index(self) -> bool:
        return p.basename(self.source_file).split(".")[0] in _INDEX_FILES

    @property
    def is_to_index(self) -> bool:
        return p.basename(self.dest_file).split(".")[0] in _INDEX_FILES

    @property
    def _valid_link(self):
        return p.relpath(self.dest_file, self.source_file)

    @property
    def link(self):
        _prefix: str = "../" if not self.is_from_index else ""
        if self.extension != _MD:
            _: str = self._valid_link
        elif self.is_to_index:
            _: str = p.dirname(self._valid_link)
        else:
            _: str = self._valid_link.removesuffix(_MD)
        _anchor: str = f"#{self.anchor}/" if self.anchor is not None else ""
        return f"{_prefix}{_}/{_anchor}"

    @classmethod
    def from_string(cls, file: str, line: str):
        if "](" not in line:
            raise LineWithoutLinkError
        for _m in finditer(_LINK_PATTERN, line):
            _: str
            link: str
            _, link = _m.string[1:-1].split("](")
            if link.startswith(_NOT_INSPECTED_LINKS):
                continue
            elif link.startswith("/"):
                link = f".{link}"
            from_file: str = file
            to_file: str

class LinkValidator:
    def __init__(self, base_path: str | None = None):
        if base_path is None:
            base_path: str = p.curdir
        self._base_path: str = base_path
        self._source: str | None = None
        self._link: str | None = None

    @property
    def link(self):
        return self._link

    @link.setter
    def link(self, value):
        self._link = value

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, value):
        self._source = value

    @property
    def from_file(self):
        return self._source

    @property
    def anchor(self):
        return self._link.split("#")[1] if "#" in self._link else ""

    @property
    def md_files(self):
        return [f"content/common/{_}" for _ in glob(f"**/*{_MD}", root_dir=self._base_path, recursive=True)]

    @property
    def subfolders(self):
        return [f"content/common/{_}" for _ in glob("**", root_dir=self._base_path, recursive=True)]

    @property
    def files(self):
        return [
            f"content/common/{_}" for _ in glob("**/*", root_dir=self._base_path, recursive=True)
            if p.splitext(_)[1] != _MD]

    @property
    def base_link(self) -> str:
        return self._link.split("#")[0]

    @property
    def to_file(self):
        if p.basename(self._source) not in _INDEX_FILES:
            _link: str = self.base_link.removeprefix("../").removesuffix("/")
        else:
            _link: str = self.base_link.removesuffix("/")
        if _link in self.files:
            return _link
        elif _link in self.subfolders:
            if f"{_link}/_index{_MD}" in self.md_files:
                return f"{_link}/_index{_MD}"
            elif f"{_link}/index{_MD}" in self.md_files:
                return f"{_link}/index{_MD}"
        elif f"{_link.removesuffix('/')}{_MD}" in self.md_files:
            return f"{_link.removesuffix('/')}{_MD}"
        elif _link in self.md_files:
            return _link
        else:
            print(f"Link {self._link} does not point to the file")
            return

    @property
    def link_item(self) -> Link:
        return Link(self.from_file, self.to_file, self.anchor)

    @property
    def valid_link(self):
        return self.link_item.link


class File:
    def __init__(self, file_path: str):
        self._path: str = p.abspath(file_path)
        self._content: list[str] = []
        self._link_indexes: list[int] = []
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise FileKeyGetError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise FileKeySetError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def _validate(self):
        if not p.exists(self._path):
            raise FileInvalidItemError

    def read(self):
        try:
            with open(self._path, "r+", encoding="utf8") as f:
                _: list[str] = f.readlines()
        except UnicodeDecodeError as e:
            print(e)
            _ = []
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    def set_link_line_indexes(self):
        self._link_indexes = [index for index, line in enumerate(iter(self)) if "](" in line]

    @property
    def link_lines(self) -> list[str]:
        return [self[index] for index in self._link_indexes]


class FileDirectory:
    def __init__(self, dir_path: str):
        self._path: str = p.abspath(dir_path)
        self._files: list[str] = []
        self._subfolders: list[str] = []
        self._validate()
        self.set_items()

    @property
    def path(self):
        return self._path

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not p.isdir(self._path):
            raise TypeError(f"Path {self._path} does not lead to the folder")

    def __iter__(self):
        return iter(_ for _ in os.listdir(self._path))

    def set_items(self):
        self._files: list[str] = list(filter(lambda x: p.isfile(x) and p.splitext(self._path)[1] == _MD, iter(self)))
        self._subfolders: list[str] = list(filter(lambda x: p.isdir(x), iter(self)))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def iter_file_items(self) -> Iterator[File]:
        return iter(File(_) for _ in self._files)

    def iter_dir_items(self):
        return iter(self.__class__(_) for _ in self._subfolders)


link_validator: LinkValidator = LinkValidator()


def generate(path: str):
    lv: LinkValidator = LinkValidator(path)
    directory: FileDirectory = FileDirectory(path)
    for file in directory.iter_file_items():
        file.read()
        file.set_link_line_indexes()
        for index in file.link_indexes:
            pass




if __name__ == '__main__':
    _pattern: Pattern = compile(r"text")
    _line: str = "example_line"
    print(bool(findall(_pattern, _line)))
    _path: str = p.join(p.dirname(p.dirname(p.dirname(p.abspath(__file__)))), "markdown", "content", "common")
    print(_path)
    print(glob("**/", root_dir=_path, recursive=True))
