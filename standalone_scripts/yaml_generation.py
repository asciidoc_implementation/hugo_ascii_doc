from pathlib import Path
from typing import Any, Iterator, NamedTuple, TypeAlias
from loguru import logger

PathStr: TypeAlias = str | Path
_INDEX_FILES: tuple[str, ...] = ("_index.md", "index.md")
_NO_WEIGHT: int = 1000
_MD_EXTENSION: str = ".md"


def level(path: PathStr, base_path: PathStr):
    return len(Path(path).relative_to(base_path).parts)


class MarkdownFile:
    _index: int = 0
    elements: dict[int, Any] = None

    def __init__(self, file_path: PathStr, base_path: PathStr):
        self._path: Path = Path(file_path).resolve()
        self._base_path: Path = Path(base_path).resolve()
        self.index: int = self.__class__._index
        self._front_matter: dict[str, str] = dict()
        self._front_matter_indexes: list[int] = []
        self._content: list[str] = []
        self._level: int | None = None
        self._validate()

        self.__class__.elements[self.__class__._index] = self
        self.__class__._index += 1

    @classmethod
    def get_item(cls, index: int):
        if not isinstance(index, int):
            raise TypeError(f"Index must be int, but {type(index)} received")
        elif index not in cls.elements:
            raise KeyError(f"Index {index} is not found")
        else:
            return cls.elements.get(index)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __bool__(self):
        return self.name not in _INDEX_FILES

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError(f"Key {item} must be int or slice")

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError(f"Key {key} must be int and value {value} must be str")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def _validate(self):
        if not self._path.is_file():
            raise TypeError(f"Path {self._path} does not lead to the file")

    def read(self):
        try:
            with open(self._path, "r+", encoding="utf8") as f:
                _: list[str] = f.readlines()
        except UnicodeDecodeError as e:
            print(e)
            _ = []
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    def lines(self):
        return "".join(self._content)

    @property
    def name(self) -> str:
        return self._path.name

    @property
    def extension(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def path_dir(self):
        return self._path.parent

    @property
    def path(self):
        return self._path

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        else:
            return NotImplemented

    def insert_line(self, index: int, line: str):
        if 0 <= index < len(self):
            self._content.insert(index, line)
        elif index == len(self):
            self._content.append(line)
        else:
            print(f"Index {index} is out of bounds")

    def delete_line(self, index: int):
        if 0 <= index < len(self):
            self._content.pop(index)
        else:
            print(f"Index {index} is out of bounds")

    @property
    def content(self):
        return self._content

    @property
    def relpath(self):
        return self._path.relative_to(self._base_path)

    def set_front_matter(self):
        _front_matter_index: int = -1
        for index, line in enumerate(iter(self)):
            if index == 0:
                continue
            elif line.removesuffix("\n") == "---":
                _front_matter_index = index
                break
        self._front_matter_indexes = [_ for _ in range(_front_matter_index + 1)]
        for line in iter([self[_] for _ in range(1, _front_matter_index)]):
            k, v = line.split(":")[0], line.split(":")[1].strip()
            self._front_matter[k] = v

    @property
    def title(self):
        return self._front_matter.get("title")

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        self._level = value

    def set_level(self):
        _addend: int = int(not bool(self)) * (-1)
        self._level = level(self._path, self._base_path) + _addend

    @property
    def weight(self):
        logger.info(f"self.path = {self.path}")
        logger.warning(f"self._front_matter = {self._front_matter}")
        return int(self._front_matter.get("weight"))


class MarkdownDirectory:
    _index: int = 0
    elements: dict[int, Any] = dict()

    def __init__(self, dir_path: PathStr, base_path: PathStr):
        self._path: Path = Path(dir_path).resolve()
        self._base_path: Path = Path(base_path).resolve()
        self.index = self.__class__._index
        self._md_files: list[int] = []
        self._subfolders: list[int] = []
        self._file_index: int | None = None
        self._output: dict[str, Any] = dict()
        self._validate()

        self.__class__.elements[self.__class__._index] = self
        self.__class__._index += 1

    @classmethod
    def get_item(cls, index: int):
        if not isinstance(index, int):
            raise TypeError(f"Index must be int, but {type(index)} received")
        elif index not in cls.elements:
            raise KeyError(f"Index {index} is not found")
        else:
            return cls.elements.get(index)

    @property
    def path(self):
        return self._path

    @property
    def relpath(self):
        return self._path.relative_to(self._base_path)

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.is_dir():
            raise TypeError(f"The path '{self._path}' does not lead to the folder")

    def __iter__(self):
        return iter(_ for _ in self._path.iterdir())

    def __getitem__(self, item) -> MarkdownFile:
        return MarkdownFile.get_item(item)

    @property
    def index_file(self):
        return self[self._file_index] if self._file_index is not None else None

    def set_md_items(self):
        for _ in filter(lambda x: x.is_file(), iter(self)):
            if _.suffix == _MD_EXTENSION:
                _md: MarkdownFile = MarkdownFile(_, self._base_path)
                _md.read()
                _md.set_front_matter()
                self._md_files.append(_md.index)

    def set_subfolders(self):
        for _ in filter(lambda x: x.is_dir(), iter(self)):
            md_directory: MarkdownDirectory = MarkdownDirectory(_, self._base_path)
            md_directory.set_md_items()
            md_directory.set_index_file()
            md_directory.set_subfolders()
            self._subfolders.append(md_directory.index)

    def iter_md_files(self) -> Iterator[MarkdownFile]:
        return iter(self[_] for _ in self._md_files)

    def sort(self):
        return sorted(self.iter_md_files(), key=lambda x: (x.weight, x.name))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def __bool__(self):
        return len(self) > 0

    def set_index_file(self):
        for _ in self._md_files:
            if self[_].name in _INDEX_FILES:
                self._file_index = _

    def __len__(self):
        return len([_ for _ in self.iter_md_files() if _.name not in _INDEX_FILES])

    def iter_subfolders(self):
        return iter(self.__class__.get_item(_) for _ in self._subfolders)

    def level(self) -> int:
        return level(self._path, self._base_path)

    @property
    def weight(self) -> int:
        if self.index_file is not None:
            return self.index_file.weight
        else:
            return _NO_WEIGHT

    def set_output(self):
        _: list[MarkdownFile] = []
        self._output[f"{self.relpath}"] = dict()
        if self.index_file is not None:
            self._output[f"{self.relpath}"]["index"] = [self.index_file.relpath]
        if len(self) == 1:
            pass
        self._output[f"{self.relpath}"]["level"] = self.level
        self._output[f"{self.relpath}"]["files"] = [_file.relpath for _file in self.sort()]

    def set_subfolders_output(self):
        for subfolder in self.iter_subfolders():
            pass


class FileParams(NamedTuple):
    name: str
    weight: int
    level: int
    parent: str

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __key(self) -> tuple[str, int, int, str]:
        return self.parent, self.level, self.weight, self.name

    @property
    def attrs(self):
        return self.level, self.weight

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        try:
            if not isinstance(other, self.__class__) or self.parent != other.parent:
                return NotImplemented
            if self.attrs == other.attrs:
                return self.name < other.name
            else:
                return self.attrs < other.attrs
        except TypeError as e:
            logger.error(f"{e.__class__.__name__}, {e.args}")
            logger.error(f"first = {repr(self)}\nsecond = {repr(other)}")
            raise

    def __gt__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name > other.name
        else:
            return self.attrs > other.attrs

    def __le__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name <= other.name
        else:
            return self.attrs < other.attrs

    def __ge__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name >= other.name
        else:
            return self.attrs > other.attrs


class FilesTree:
    def __init__(self):
        self._dict_files: dict[str, Any] = dict()
