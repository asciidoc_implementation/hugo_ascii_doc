import json


def prettify(file: str):
    with open(file, "r+") as f:
        _ = json.load(f)

    with open(file, "w+") as f:
        json.dump(_, f, ensure_ascii=False, indent=2, sort_keys=False)


if __name__ == '__main__':
    _path = "C:\\Users\\tarasov-a\\PycharmProjects\\Protei_SS7FW\\content\\common\\desc\\tgpp.json"
    prettify(_path)
