from argparse import ArgumentError, ArgumentParser, Namespace, ONE_OR_MORE, \
    RawDescriptionHelpFormatter, SUPPRESS
from pathlib import Path
from re import Match, Pattern, compile, match, search, sub
from sys import platform
from textwrap import dedent
from typing import Iterable, Mapping

from yaml import BaseLoader, dump, load

from common import File, PathStr, validate_path


pattern_heading_attr: Pattern = compile(r'^(=+\s)\+\+\+<a name="([^"]+)">\+\+\+([^+]*)\+\+\+</a>\+\+\+')
sub_heading_attr: str = r"[[\2]]\n\1\3"

pattern_attr: Pattern = compile(r'\+\+\+<a name="([^"]+)">\+\+\+([^+]*)\+\+\+</a>\+\+\+')
sub_attr: str = r"[[\1]]\2"

pattern_abbr: Pattern = compile(r'\+\+\+<abbr title="([^"]*)">\+\+\+([^+]*)\+\+\+</abbr>\+\+\+')
sub_abbr: str = r"{\2}"

pattern_source: Pattern = compile(r"^\[,(.*)]$")
sub_source: str = r"[source,\1]"

pattern_code: Pattern = compile(r"`([^`]+)`")
sub_code: str = r"*_\1_*"

pattern_list: Pattern = compile(r"^\*(.*)\s[-\u2014]\s(.*)[;.]$")
sub_list: str = r"\1:: \2"

pattern_link: Pattern = compile(r"link:(\.*/)(.*)\[(.*[^\\])]")
sub_link: str = r"xref:\2[\3]"

pattern_note: Pattern = compile(r"[*_`]*Примечание\.?[*_`]*")
sub_note: str = "NOTE:"

pattern_caution: Pattern = compile(r"[*_`]*Внимание\.?[*_`]*")
sub_caution: str = "CAUTION:"

pattern_downlevel_straight: Pattern = compile(r"(xref|link)(:\.\./)(\.\./)*([^\[]*)/\[")
sub_downlevel_straight: str = r"\1:\3\4.adoc["

pattern_downlevel_part: Pattern = compile(r"(xref|link)(:\.\./)(\.\./)*([^\[]*)/#")
sub_downlevel_part: str = r"\1:\3\4.adoc#"

pattern_anchor: Pattern = compile(r"<<([^,]+),([^>]*)>>")

pattern_xref: Pattern = compile(r"\[\[([^]]+)]]")

p_pattern_adoc: Pattern = compile(r":(.*)/#(.*).adoc\[")
sub_p_adoc: str = r":\1.adoc#\2["

p_pattern_sunder: Pattern = compile(r"\[\[(.*)]](\[?)_(.*)(]?)")
sub_p_sunder: str = r"[[\1]]\2\3\4"

pp_pattern: Pattern = compile(r"=*\s+([^\n]*)")
sub_pp: str = r".\1"

pattern_description: Pattern = compile(r"^=*\s(.*)\s?=*")
sub__description: str = r'.\1\n[cols="1,6"]'

pattern_logging_parameters: Pattern = compile(r"^=*\sОписание параметров.*")
sub_logging_parameters: str = r'.Описание параметров\n[options="header",cols="1,4,4,2"]'

pattern_dlist: Pattern = compile(r"^=*\sСекции.*")
sub_dlist: str = r".Секции\n[horizontal]"

pattern_sample: Pattern = compile(r"^=*\sПример.*")
sub_sample: str = r".Пример"

pattern_group: Pattern = compile(r"^=*\sГруппа")
sub_group: str = r'.Группа\n[cols="1,6,2"]'

pattern_tag: Pattern = compile(r"^\[#(.*)]")
sub_tag: str = r"[[_\1]]"

pattern_hw: Pattern = compile(r"^=*\s(Требования\sк\sаппаратному\sобеспечению).*")
sub_hw: str = r".\1"

pattern_sw: Pattern = compile(r"^=*\s(Требования к программному обеспечению).*")
sub_sw: str = r".\1"

pattern_header: Pattern = compile(
    r"^=*\s(Запрос|Ответ|Параметры запроса|Параметры ответа|Пример успешного ответа|Параметры запуска|Запуск "
    r"скрипта).*")
sub_header: str = r".\1"

_replace: Mapping[Pattern, str] = {
    pattern_heading_attr: sub_heading_attr,
    pattern_downlevel_straight: sub_downlevel_straight,
    pattern_downlevel_part: sub_downlevel_part,
    pattern_attr: sub_attr,
    pattern_abbr: sub_abbr,
    pattern_source: sub_source,
    pattern_code: sub_code,
    pattern_list: sub_list,
    pattern_link: sub_link,
    pattern_caution: sub_caution,
    pattern_note: sub_note
}

_replace_post: Mapping[Pattern, str] = {
    p_pattern_adoc: sub_p_adoc,
    p_pattern_sunder: sub_p_sunder,
    pattern_logging_parameters: sub_logging_parameters,
    pattern_dlist: sub_dlist,
    pattern_sample: sub_sample,
    pattern_group: sub_group,
    pattern_tag: sub_tag,
    pattern_hw: sub_hw,
    pattern_sw: sub_sw,
    pattern_header: sub_header
}

# _replace_ppost: Mapping[Pattern, str] = {
#     pp_pattern_code: sub_pp_code,
#     pp_pattern_table: sub_pp_table
# }

_front_matter_tags: tuple[str, ...] = (
    "title",
    "description",
    "weight",
    "type",
    "gitlab_docs_enable_new_issue",
    "gitlab_docs_enable_edit_ide",
    "gitlab_docs_path",
    "gitlab_docs_branch",
    "gitlab_project_name",
    "gitlab_project_path",
    "gitlab_docs_enable_edit"
)

_note_rel_path: str = "Note that should be relative to {base}"
_note_exclusive: str = "Mutually exclusive with"
_note_yml_yaml: str = "Note that should be a *.yml or *.yaml file"

_help_base: str = "Path. Specify the file or directory to use as a base\n"
_help_file: str = (
    "str | Path | list[str] | list[Path]. Specify the files to add for conversion.\n"
    "May be used multiple times.\n"
    f"{_note_rel_path}.\n"
    f"{_note_exclusive} -d/--dir")
_help_dir: str = (
    "str | Path. Specify the folder to get all files inside and add for conversion.\n"
    f"{_note_rel_path}.\n"
    f"{_note_exclusive} -f/--file")
_help_recursive: str = (
    "Flag. Specify to get files from the directory recursively.\n"
    "Note that used only with the -d/--dir option, otherwise ignored")
_help_attributes: str = (
    "str | Path. Specify the file with AsciiDoc attributes to add to the files.\n"
    f"{_note_rel_path}.\nSee more information about possible attributes here: "
    f"https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes/")
_help_version: str = (
    "Show the script version with the full information message and close the window")
_help_help: str = "Show the help message and close the window"

description: str = "Repair some flaws in automatic conversion from Markdown to AsciiDoc"

prog: str = Path(__file__).name

_press_any_key: str = "Press any key to close..."

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = (
    f"{_python_version} {prog} \n"
    f"    [ -h/--help | -v/--version ] \n"
    f"    ( -f/--file FILES... | -d/--dir DIR [ -r/--recursive ] ) "
    f"-a/--attributes FILE <BASE>")

epilog: str = dedent(f"""\
The script repairs:
* +++<a name="">+++...+++</a>+++;
* +++<abbr title="">+++...+++</abbr>+++;
* <<anchor,text>>;
* issues with links and anchors;
* transforms some file headings to list or table ones.
""")

_text_info: str = dedent(f"""\
The files can be set explicitly or received as content from the directory and/or its subfolders.
Note that only *.adoc files are searched and processed. Other file types are ignored.

----
The file with AsciiDoc attributes (option -a/--attributes) must have the following structure:

pdf:
  :<attribute_name>: <attribute_value>
  ...
  :<attribute_name>: <attribute_value>
html5:
  :<attribute_name>: <attribute_value>
  ...
  :<attribute_name>: <attribute_value>
  
File example:

pdf:
  :show-title:
  :toc-title: Содержание
  :toclevels: 5
  :epc: EPC
html5:
  :notitle:
  :epc: pass:[<abbr title="Evolved Packet Core">EPC</abbr>]
----

Examples:

python modify.py content/common/oam -f installation.md --files api.md cli.md
python modify.py content/common/config --dir component --attributes ../../attributes.yaml
python modify.py content/common -d logging --recursive -a ascii_attributes.yml""")

version: str = f"{description}, 0.1.0 beta\n{_text_info}"


def to_lower(line: str):
    return "{%s}" % (line.lower())


def multiple_sub(line: str, replace: Mapping[Pattern, str] = None) -> str:
    if replace is None:
        return line
    _: dict[Pattern, str] = {k: v for k, v in replace.items()}
    for pattern, string in _.items():
        if search(pattern, line):
            line: str = sub(pattern, string, line)
        else:
            continue
    return line.removesuffix("\n")


def prepare_value(value: str | bool | None) -> str:
    if value is None:
        return ""
    elif isinstance(value, bool):
        return f" {str(value).lower()}"
    else:
        return f" {value}"


class FileYaml(File):
    def __init__(self, file_path: PathStr):
        super().__init__(file_path)
        self._params: dict[str, dict] = dict()
        self._validate()

    def _validate(self):
        super()._validate()
        if self.raw_extension not in ("yml", "yaml"):
            raise ValueError(f"Invalid path file {self._path}")

    def read(self):
        with self._path.open("r+", encoding="utf-8") as f:
            content: dict[str, dict] = load(f, BaseLoader)
        self._params = content

    def write(self):
        with self._path.open("w+", encoding="utf-8") as f:
            f.write(dump(self._params))
        return

    @property
    def html5s_params(self) -> list[str]:
        return [f"{k}:{prepare_value(v)}" for k, v in self._params.get("html5s").items()]

    @property
    def pdf_params(self) -> list[str]:
        return [f"{k}:{prepare_value(v)}" for k, v in self._params.get("pdf").items()]

    def set_content(self):
        self._content = [
            'ifeval::["{backend}" == "html5"]',
            *self.html5s_params,
            "endif::[]",
            "\n",
            'ifeval::["{backend}" == "pdf"]',
            *self.pdf_params,
            "endif::[]"]


class AdocFile(File):
    _separator_front_matter: str = "---"

    def __init__(self, file_path: PathStr, attributes: Iterable[str] = None):
        if attributes is None:
            attributes: list[str] = []
        super().__init__(file_path)
        self._front_matter_indexes: set[int] = set()
        self._attributes: list[str] = [*attributes]

    def _ignored_indexes(self):
        _ignored: list[int] = [*self._front_matter_indexes]
        _ignored.extend(
            [index for index, line in enumerate(iter(self))
             if line.startswith(("|===", "[", "----", "....", ":"))])
        return _ignored

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __bool__(self):
        return self[0].startswith("---")

    def set_front_matter(self):
        if not self:
            self._front_matter_indexes = {
                index for index, line in enumerate(iter(self))
                if match(compile(r":.+:.*"), line) or line == "---\n" or line.startswith("= ")}
        else:
            self._front_matter_indexes = {index for index in range(13)}

    @property
    def _text_lines(self):
        _: list[int] = [
            index for index in range(len(self))
            if index not in self._front_matter_indexes]
        _.sort()
        return [self[index].removesuffix("\n") for index in _]

    def modify_front_matter(self):
        if self:
            return

        front_matter_lines: list[str] = [self._separator_front_matter]
        attributes_lines: list[str] = []

        for index in iter(self._front_matter_indexes):
            line: str = self[index]
            if line.startswith("= "):
                _: str = line.removeprefix("= ").removesuffix("\n")
                front_matter_lines.append(f"title: {_}")
                attributes_lines.append(line.removesuffix("\n"))
            elif line.removeprefix(":").startswith(_front_matter_tags):
                front_matter_lines.append(line.removeprefix(':').removesuffix('\n'))
            else:
                attributes_lines.append(line.removesuffix("\n"))
        front_matter_lines.append(self._separator_front_matter)

        self._content = [
            *front_matter_lines, *attributes_lines, "\n", *self._attributes, *self._text_lines
        ]
        self.set_front_matter()

    def _validate(self):
        super()._validate()
        if self.raw_extension not in ("adoc", "asciidoc"):
            raise TypeError(
                f"File must have an extension 'adoc' or 'asciidoc' "
                f"but {self.raw_extension} received")

    def write(self):
        try:
            with self._path.open("w+", encoding="utf-8") as f:
                f.write(self.lines())
        except PermissionError as e:
            print(f"{e.__class__.__name__}, {e.strerror}")
        except UnicodeEncodeError as e:
            print(
                f"{e.__class__.__name__}\ncause = {e.reason}\n"
                f"encoding = {e.encoding}, start = {e.start}, end = {e.end}")

    def modify(self):
        for index, line in enumerate(iter(self)):
            if index in self._ignored_indexes():
                self[index] = line.removesuffix("\n")
            else:
                line = multiple_sub(line, _replace)

                _m_anchor: Match = search(pattern_anchor, line)
                if _m_anchor:
                    line = line.replace(_m_anchor.group(1), sub("-", "_", f"{_m_anchor.group(1).lower()}"))

                _m_xref: Match = search(pattern_xref, line)
                if _m_xref:
                    line = line.replace(_m_xref.group(1), sub("-", "_", f"{_m_xref.group(1)}"))

                line = multiple_sub(line, _replace_post)
                self[index] = line.removesuffix("\n")

    def post_processing(self):
        for index, line in enumerate(self._content[:-2]):
            try:
                if index not in self._ignored_indexes():
                    _m: Match = match(pp_pattern, line)
                    if _m and self[index + 1] == "\n" and self[index + 2].startswith("["):
                        self[index] = sub(pp_pattern, sub_pp, line)
                        del self._content[index + 1]
                    elif _m and self[index + 1] == "\n" and self[index + 2].startswith("|==="):
                        self[index] = sub(pp_pattern, sub_pp, line)
                        self[index + 1] = '[options="header"]'
                    else:
                        continue
            except IndexError:
                pass
            except OSError as e:
                print(f"{e.__class__.__name__}, {e.strerror}")
        return

    def processing(self):
        self.read()
        self.set_front_matter()
        self.modify()
        self.modify_front_matter()
        self.post_processing()
        self.write()

    @property
    def front_matter_indexes(self):
        return self._front_matter_indexes

    @front_matter_indexes.setter
    def front_matter_indexes(self, value):
        self._front_matter_indexes = value


def generate(
        base: str,
        files: Iterable[str] = None,
        folder: str = None,
        is_recursive: bool = False,
        file_attributes: str = None):
    if files is None and folder is None:
        print(f"At least one of the -f/--file and -d/--dir must be set")
        input(_press_any_key)
        raise AttributeError

    if files is None and not validate_path(base, folder):
        print(f"Folder is not found")
        input(_press_any_key)
        raise ValueError

    base: str = base.replace("pwd", str(Path.cwd().resolve()))
    _base_path: Path = Path(base).resolve()

    if files is None:
        _folder_path: Path = _base_path.joinpath(folder)
        if is_recursive:
            _paths: list[Path] = list(_folder_path.rglob("*.adoc"))
        else:
            _paths: list[Path] = [
                _child for _child in _folder_path.iterdir()
                if _child.is_file() and _child.suffix == ".adoc"]
    else:
        _paths: list[Path] = [
            Path(file) for file in files if validate_path(base, file)]

        if not _paths:
            print("All provided files are inappropriate to convert")
            input(_press_any_key)
            return

    def get_attributes():
        if file_attributes is None:
            return None

        _file_yaml_path: Path = _base_path.joinpath(file_attributes).resolve()

        if not validate_path(base, file_attributes):
            print(f"File {file_attributes} is not found")
            return None
        elif _file_yaml_path.suffix not in (".yml", ".yaml"):
            print(
                f"File {file_attributes} must have an extension 'yml' or 'yaml' "
                f"but {_file_yaml_path.suffix.removeprefix('.')} received")
            return None
        else:
            _file_yaml: FileYaml = FileYaml(_file_yaml_path)
            _file_yaml.read()
            _file_yaml.set_content()
            return _file_yaml.content

    attributes: list[str] | None = get_attributes()
    for _path in _paths:
        _adoc_file: AdocFile = AdocFile(_base_path.joinpath(_path), attributes)
        _adoc_file.processing()


def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "base",
        action="store",
        help=_help_base)
    exclusive_group = parser.add_mutually_exclusive_group(required=True)
    exclusive_group.add_argument(
        "-f", "--file",
        action='extend',
        nargs=ONE_OR_MORE,
        default=SUPPRESS,
        help=_help_file,
        dest="files")
    exclusive_group.add_argument(
        "-d", "--dir",
        action="store",
        default=SUPPRESS,
        help=_help_dir,
        dest="folder")
    parser.add_argument(
        "-r", "--recursive",
        action="store_true",
        help=_help_recursive,
        dest="is_recursive")
    parser.add_argument(
        "-a", "--attributes",
        action="store",
        default=SUPPRESS,
        help=_help_attributes,
        dest="file_attributes")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args
    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")


def modify():
    args: Namespace = parse_command_line()
    base: str = getattr(args, "base")
    files: list[str] = getattr(args, "files") if hasattr(args, "files") else None
    folder: str = getattr(args, "folder") if hasattr(args, "folder") else None
    is_recursive: bool = getattr(args, "is_recursive") if hasattr(args, "is_recursive") else None
    file_attributes: str = getattr(args, "file_attributes") if hasattr(args, "file_attributes") else None
    return generate(base, files, folder, is_recursive, file_attributes)


if __name__ == '__main__':
    modify()
