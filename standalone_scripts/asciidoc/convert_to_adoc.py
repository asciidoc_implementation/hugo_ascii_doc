from argparse import ArgumentError, ArgumentParser, Namespace, ONE_OR_MORE, OPTIONAL, \
    RawDescriptionHelpFormatter, SUPPRESS
from os import getenv
from pathlib import Path
from subprocess import CalledProcessError, run
from sys import platform
from textwrap import dedent
from typing import Iterable

from common import validate_path


_default_output: str = "bash.sh"

_note_rel_path: str = "Note that should be relative to {base}"
_note_exclusive: str = "Mutually exclusive with"
_note_pwd: str = "Using 'pwd' for current directory and marks '.' and '..' is allowed"

_help_base: str = f"Path. Specify the file or directory to use as a base.\n{_note_pwd}"
_help_file: str = (
    "str | Path | list[str] | list[Path]. Specify the files to add for conversion.\n"
    "May be used multiple times.\n"
    f"{_note_rel_path}.\n{_note_pwd}.\n"
    f"{_note_exclusive} -d/--dir")
_help_dir: str = (
    "str | Path. Specify the folder to get all files inside and add for conversion.\n"
    f"{_note_rel_path}.\n{_note_pwd}.\n"
    f"{_note_exclusive} -f/--file")
_help_recursive: str = (
    "Flag. Specify to get files from the directory recursively.\n"
    "Note that used only with the -d/--dir option, otherwise ignored")
_help_output: str = (
    "str | Path. Specify the path to output file.\n"
    f"{_note_rel_path}.\n{_note_pwd}.\n"
    "The default path is {base}/bash.sh")
_help_run: str = (
    "Flag. Specify to run the script when its generation is over")
_help_version: str = (
    "Show the script version with the full information message and close the window")
_help_help: str = "Show the help message and close the window"

prefix_content: str = (
    "kramdoc --auto-id-prefix=_ --auto-id-separator=- --html-to-native --lazy-ids "
    "-a caution-caption=Внимание -a experimental -a image-caption=Рисунок "
    "-a important-caption=Внимание -a note-caption=Примечание -a table-caption=Таблица")

_attributes: str = ("""
API attributes:
    --auto-id-prefix (=_):
        Specify the prefix for identifiers
    --auto-id-separator (=-):
        Specify the character to use for replacing spaces and marks
    --html-to-native:
        Flag to process html tags
    --lazy-ids:
        Autogenerate identifiers for sections and blocks when they are not equal to their headings 

AsciiDoc attributes (-a):
    caution-capture (=Внимание):
        Specify the phrase to replace the CAUTION admonition
    experimental:
        Flag to activate some features
    image-caption (=Рисунок):
        Specify the phrase to use in image captions
    important-caption (=Внимание):
        Specify the phrase to replace the IMPORTANT admonition
    note-caption (=Примечание):
        Specify the phrase to replace the NOTE admonition
    table-caption (=Таблица):
        Specify the phrase to use in table captions
""")

description: str = "Bash script generator for conversion from Markdown to AsciiDoc"

prog: str = Path(__file__).name

_press_any_key: str = "Press the ENTER key to close the window ..."

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = (
    f"{_python_version} {prog} \n"
    f"    [ -h/--help | -v/--version ] \n"
    f"    ( -f/--file FILES... | -d/--dir DIR [ -r/--recursive ] ) "
    f"[ -o/--output OUTPUT ] [ --run ] <BASE>")

epilog: str = dedent(f"""\
The script generates bash commands to convert files from Markdown to AsciiDoc using the 
`kramdown-asciidoc` library, https://github.com/asciidoctor/kramdown-asciidoc""")

_text_info: str = dedent(f"""
The following attributes are added:
{_attributes}

The files can be set explicitly or received as content from the directory and/or its subfolders.
Note that only *.md files are searched and processed. Other file types are ignored.

Examples:

python convert_to_adoc.py content/common/oam -f installation.md --files api.md cli.md
python convert_to_adoc.py content/common/config --dir component --output config_bash.sh
python convert_to_adoc.py content/common -d logging --recursive -o logging_bash
python convert_to_adoc.py content/common --dir basics --recursive -o basics/basics.sh --run 
""")

version: str = f"{description}, 0.1.0 beta\n{_text_info}"


def generate(
        base: str,
        files: Iterable[str] = None,
        folder: str = None,
        is_recursive: bool = False,
        output: str = None,
        is_run: bool = False):
    if files is None and not validate_path(base, folder):
        print(f"Folder is not found")
        input(_press_any_key)
        raise ValueError

    base: str = base.replace("pwd", str(Path.cwd().resolve()))
    _base_path: Path = Path(base).resolve()

    if files is None:
        _folder_path: Path = _base_path.joinpath(folder)
        if is_recursive:
            _final: list[Path] = list(_folder_path.rglob("*.md"))
        else:
            _final: list[Path] = [
                _child for _child in _folder_path.iterdir()
                if _child.is_file() and _child.suffix == ".md"]
    else:
        _final: list[Path] = [
            Path(file) for file in files if validate_path(base, file)]

        if not _final:
            print("All provided files are inappropriate to convert")
            input(_press_any_key)
            return

    _: str = getenv("SHELL", None)

    if _ is None:
        print("Environment variable $SHELL is not found!\n")
        _bash_version: str = ""
    else:
        _bash_version: str = f"#!{_}\n\n"

    _lines = [_base_path.joinpath(_f).resolve().relative_to(_base_path) for _f in _final]
    _content: list[str] = [_bash_version]
    _content.extend([f"{prefix_content} {_line}" for _line in _lines])

    if not output:
        output: str = _default_output

    try:
        _output_path: Path = _base_path.joinpath(output).resolve()
        _output_path.touch(exist_ok=True)
    except (OSError | PermissionError) as e:
        print(e.__class__.__name__)
        print(
            f"Invalid output file path {output}. The value is changed to the default one: "
            f"{_base_path.joinpath('bash.sh')}")
        _output_path: Path = _base_path.joinpath("bash.sh").resolve()

    try:
        with _output_path.open("w+", encoding="utf-8") as f:
            f.write("\n".join(_content))

        print(f"The file is generated. You can find it here:\n{_output_path}")
    except (OSError | PermissionError) as e:
        print(e.__class__.__name__)
        print("Error occurred")
        input(_press_any_key)
        raise

    if is_run:
        try:
            run(["sh", f"{_output_path}"], capture_output=True)
        except CalledProcessError as e:
            print(f"{e.__class__.__name__}, {e.output}")


def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "base",
        action="store",
        help=_help_base)
    exclusive_group = parser.add_mutually_exclusive_group(required=True)
    exclusive_group.add_argument(
        "-f", "--file",
        action='extend',
        nargs=ONE_OR_MORE,
        default=SUPPRESS,
        help=_help_file,
        dest="files")
    exclusive_group.add_argument(
        "-d", "--dir",
        action="store",
        default=SUPPRESS,
        help=_help_dir,
        dest="folder")
    parser.add_argument(
        "-r", "--recursive",
        action="store_true",
        help=_help_recursive,
        dest="is_recursive")
    parser.add_argument(
        "-o", "--output",
        action="store",
        nargs=OPTIONAL,
        default=_default_output,
        required=False,
        help=_help_output,
        dest="output")
    parser.add_argument(
        "--run",
        action="store_true",
        help=_help_run,
        dest="is_run")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")


def to_adoc():
    args: Namespace = parse_command_line()
    base: str = getattr(args, "base")
    files: list[str] = getattr(args, "files") if hasattr(args, "files") else None
    folder: str = getattr(args, "folder") if hasattr(args, "folder") else None
    is_recursive: bool = getattr(args, "is_recursive") if hasattr(args, "is_recursive") else None
    output: str = getattr(args, "output") if hasattr(args, "output") else None
    is_run: bool = getattr(args, "is_run") if hasattr(args, "is_run") else False
    return generate(base, files, folder, is_recursive, output, is_run)


if __name__ == '__main__':
    to_adoc()
