from argparse import ArgumentError, ArgumentParser, Namespace, OPTIONAL, \
    RawDescriptionHelpFormatter, SUPPRESS
from pathlib import Path
from subprocess import CalledProcessError, TimeoutExpired, run
from sys import platform
from textwrap import dedent

from loguru import logger

from standalone_scripts.init_logger import configure_custom_logging

prefix_content: str = (
    "asciidoctor-pdf -a imagesdir=../content/images -a pdf-fontsdir=../fonts "
    "-a pdf-theme=base-theme.yml -a pdf-themesdir=../themes -a skip-front-matter")

_attributes: str = ("""\
AsciiDoc attributes (-a):
    imagesdir (=../content/images):
        Specify the path to the folder containing images
    pdf-fontsdir (=../fonts):
        Specify the path to the folder containing PDF fonts
    pdf-theme (=base-theme.yml):
        Specify the PDF theme basing on the pdf-themesdir
    pdf-themesdir (=../themes):
        Specify the path to the folder containing PDF themes
    skip-font-matter:
        Skip the front matter at the beginning of the file
""")

description: str = "Bash script generator for conversion from AsciiDoc to PDF"
prog: str = Path(__file__).name

_press_any_key: str = "Press the ENTER key to close the window ..."

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = (
    f"{_python_version} {prog} \n"
    f"    [ -h/--help | -v/--version ] \n"
    f"    [ -s/--skip ] [ -d/--themesdir THEMESDIR ] [ -t/--theme THEME ] "
    f"[ -i/--images IMAGEDIR ] [ -f/--fonts FONTS ] <FILE>")

epilog: str = dedent(f"""\
The script generates the bash command to convert the file from AsciiDoc to PDF using the `asciidoctor-pdf` library, 
https://github.com/asciidoctor/asciidoctor-pdf""")

_text_info: str = dedent(f"""
The following attributes are added:
{_attributes}

Note that only the *.adoc file is processed. Other file types are ignored.

Examples:

python convert_to_pdf.py _index.adoc
python convert_to_pdf.py --skip -d themes -t base-theme.yml -i ../images -f ../fonts
python convert_to_pdf.py content/common -d logging --recursive -o logging_bash""")

version: str = f"{description}, 0.1.0 beta\n{_text_info}"

_note_rel_path: str = "Note that the relative path is based on the {pdf-themesdir} path if specified"
_note_exclusive: str = "Mutually exclusive with"
_note_pwd: str = "Using 'pwd' for current directory and marks '.' and '..' is allowed"

_help_skip_font_matter: str = "Flag. Specify to skip the front matter at the beginning"
_help_pagenums: str = "Flag. Specify to add page numbers to the bottom of the page"
_help_imagesdir: str = f"str | Path. Specify the folder to get images.\n{_note_rel_path}"
_help_pdf_themesdir: str = "str | Path. Specify the folder to get files with the PDF themes"
_help_pdf_theme: str = f"str | Path. Specify the file to get the PDF theme.\n{_note_rel_path}"
_help_pdf_fontsdir: str = f"str | Path. Specify the path to output file.\n{_note_rel_path}"
_help_version: str = "Show the script version with the full information message and close the window"
_help_help: str = "Show the help message and close the window"

_help_file: str = "str | Path | list[str] | list[Path]. Specify the file to convert"
_help_output: str = (
    "str | Path. Specify the path to output file.\n{_note_pwd}.\nThe default path is {file_name}.pdf")


def generate_attribute_string(name: str, value: str | bool | None):
    if isinstance(value, str):
        return f" -a {name.replace('_', '-')}={value}"
    elif isinstance(value, bool) and value:
        return f" -a {name.replace('_', '-')}"
    else:
        return ""


def process_output(file: str, output: str | None):
    if output is None:
        return
    if not output.endswith("pdf"):
        output: str = f"{output}.pdf"
    try:
        _: Path = Path(file).parent.joinpath(output).resolve()
        _.touch(exist_ok=True)
    except FileNotFoundError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        print(f"Default name {Path(file).with_suffix('.pdf')} is set up")
        return
    except RuntimeError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        print(f"Default name {Path(file).with_suffix('.pdf')} is set up")
        return
    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        print(f"Default name {Path(file).with_suffix('.pdf')} is set up")
        return
    else:
        return _


def generate(
        file: str,
        output: str | None = None,
        imagesdir: str | None = None,
        pdf_themesdir: str | None = None,
        pdf_theme: str | None = None,
        pdf_fontsdir: str | None = None,
        skip_front_matter: bool | None = None,
        pagenums: bool | None = None):
    try:
        _file: Path = Path(file).resolve(strict=True)
    except FileNotFoundError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        input(_press_any_key)
        raise

    _file_default: Path = Path(file).resolve().with_suffix(".pdf")
    _pdf_themesdir: str = generate_attribute_string("pdf-themesdir", pdf_themesdir)
    _pdf_theme: str = generate_attribute_string("pdf-theme", pdf_theme)
    _imagesdir: str = generate_attribute_string("imagesdir", imagesdir)
    _pdf_fontsdir: str = generate_attribute_string("pdf-fontsdir", pdf_fontsdir)
    _skip_front_matter: str = generate_attribute_string("skip-font-matter", skip_front_matter)
    _pagenums: str = generate_attribute_string("pagenums", pagenums)

    _command: str = (
        f"asciidoctor-pdf{_pdf_themesdir}{_pdf_theme}{_pdf_fontsdir}"
        f"{_imagesdir}{_skip_front_matter}{_pagenums} {file}")

    try:
        run(_command.split(), check=True, timeout=300)
    except CalledProcessError | TimeoutExpired as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        print("Install asciidoctor, asciidoctor-pdf, asciidoctor-kroki")
        input(_press_any_key)
        raise

    _updated_file: Path | None = process_output(file, output)
    if _updated_file is not None:
        _: Path = _file_default.replace(_updated_file)
    else:
        _: Path = _file_default

    print(f"The file is generated. You can find it there: {_}")


def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    parser.add_argument(
        "file",
        action='store',
        help=_help_file)
    parser.add_argument(
        "-o", "--output",
        action="store",
        nargs=OPTIONAL,
        default=None,
        required=False,
        help=_help_output,
        dest="output")
    parser.add_argument(
        "-i", "--imagesdir",
        action="store",
        nargs=SUPPRESS,
        default=None,
        required=False,
        help=_help_imagesdir,
        dest="imagesdir")
    parser.add_argument(
        "-d", "--themesdir",
        action="store",
        nargs=SUPPRESS,
        default=None,
        required=False,
        help=_help_pdf_themesdir,
        dest="pdf_themesdir")
    parser.add_argument(
        "-f", "--fonts",
        action="store",
        nargs=SUPPRESS,
        default=None,
        required=False,
        help=_help_pdf_fontsdir,
        dest="pdf_fontsdir")
    parser.add_argument(
        "-t", "--theme",
        action="store",
        nargs=SUPPRESS,
        default=None,
        required=False,
        help=_help_pdf_theme,
        dest="pdf_theme")
    parser.add_argument(
        "-s", "--skip",
        action="store_true",
        help=_help_skip_font_matter,
        dest="skip_front_matter")
    parser.add_argument(
        "-p", "--pagenums",
        action="store_true",
        help=_help_pagenums,
        dest="pagenums")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")


@configure_custom_logging("modify")
def convert_to_pdf():
    args: Namespace = parse_command_line()
    file: str = getattr(args, "file")
    output: str = getattr(args, "output")
    imagesdir: str = getattr(args, "imagesdir") if hasattr(args, "imagesdir") else None
    pdf_themesdir: str = getattr(args, "pdf_themesdir") if hasattr(args, "pdf_themesdir") else None
    pdf_theme: str = getattr(args, "pdf_theme") if hasattr(args, "pdf_theme") else None
    pdf_fontsdir: str = getattr(args, "pdf_fontsdir") if hasattr(args, "pdf_fontsdir") else None
    skip_front_matter: bool = getattr(args, "skip_front_matter") if hasattr(args, "skip_front_matter") else None
    pagenums: bool = getattr(args, "pagenums") if hasattr(args, "pagenums") else None
    logger.info(args)
    return generate(
        file,
        output,
        imagesdir,
        pdf_themesdir,
        pdf_theme,
        pdf_fontsdir,
        skip_front_matter,
        pagenums)


if __name__ == '__main__':
    convert_to_pdf()
    # path: str = "C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\common\\config\\diameter.adoc"
    # file_adoc: AdocFile = AdocFile(path)
    # file_adoc.read()
    # for _line in file_adoc._content:
    #     _line = multiple_sub(_line, {pattern_heading_attr: sub_heading_attr})
    # logger.error(f"{str(file_adoc)}")
    # file_adoc.write()
