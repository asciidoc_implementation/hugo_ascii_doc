from __future__ import annotations

import json
from operator import attrgetter
from pathlib import Path
from textwrap import dedent
from typing import Any, NamedTuple

from loguru import logger
from yaml import dump

from standalone_scripts.asciidoc.common import Directory, File, PathStr
from standalone_scripts.init_logger import configure_custom_logging

predefined: str = dedent("""\
settings:,
  title-page: {product},
  doctype: "book",
  toc: true,
  figure-caption: "Рисунок",
  chapter-signifier: false,
  toc-title: "Содержание",
  outlinelevels: 2,
  title-logo-image: "images/logo.svg[top=4%,align=right,pdfwidth=3cm]",
  version: 1.0.0

Rights:
  title:
    value: "Юридическая информация"
    title-files: false
  index:
    - content/common/_index.md""")

# _core_path: Path = Path(__file__).parent.resolve()
_core_path: Path = Path("C:\\Users\\tarasov-a\\PycharmProjects\\Protei_STP\\content")

_index_files: tuple[str, ...] = (
    "_index.md", "index.md"
)


class ProjectFile(File):
    def __init__(self, file_path: PathStr, base_path: PathStr):
        super().__init__(file_path)
        self._base_path: Path = Path(base_path).resolve()
        self._front_matter: dict[str, str] = dict()
        self._front_matter_indexes: list[int] = []
        self.specify_front_matter()

    @property
    def relpath(self):
        return self._path.relative_to(self._base_path)

    def specify_front_matter(self):
        _front_matter_index: int = -1
        for index, line in enumerate(iter(self)):
            if index == 0:
                continue
            elif line.removesuffix("\n") == "---":
                _front_matter_index = index
                break
        self._front_matter_indexes = [_ for _ in range(_front_matter_index + 1)]
        for line in iter([self[_] for _ in range(1, _front_matter_index)]):
            k, v = line.split(":")[0], line.split(":")[1].strip()
            self._front_matter[k] = v

    @property
    def title(self):
        return self._front_matter.get("title")

    @property
    def level(self):
        return len(self.relpath.parts)

    @property
    def weight(self):
        logger.info(f"self.path = {self.path}")
        logger.warning(f"self._front_matter = {self._front_matter}")
        return int(self._front_matter.get("weight"))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.params == other.params
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.params != other.params
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.params < other.params
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.params > other.params
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.params <= other.params
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.params >= other.params
        else:
            return NotImplemented

    def __len__(self):
        return len(self._content)

    def __bool__(self):
        for index, line in enumerate(iter(self)):
            if index in self._front_matter_indexes:
                continue
            elif not line:
                continue
            else:
                return True
        else:
            return False

    def predefined(self):
        return predefined.format(product=self.title)

    @property
    def params(self) -> FileParams:
        return FileParams(self.name, self.weight, self.level, str(self.relpath))


class FileParams(NamedTuple):
    name: str
    weight: int
    level: int
    parent: str

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    @classmethod
    def from_project_file(cls, project_file: ProjectFile):
        return cls(project_file.name, project_file.weight, project_file.level, str(project_file.relpath))

    def __key(self) -> tuple[str, int, int, str]:
        return self.parent, self.level, self.weight, self.name

    @property
    def attrs(self):
        return self.level, self.weight

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __lt__(self, other):
        try:
            if not isinstance(other, self.__class__) or self.parent != other.parent:
                return NotImplemented
            if self.attrs == other.attrs:
                return self.name < other.name
            else:
                return self.attrs < other.attrs
        except TypeError as e:
            logger.error(f"{e.__class__.__name__}, {e.args}")
            logger.error(f"first = {repr(self)}\nsecond = {repr(other)}")
            raise

    def __gt__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name > other.name
        else:
            return self.attrs > other.attrs

    def __le__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name <= other.name
        else:
            return self.attrs < other.attrs

    def __ge__(self, other):
        if not isinstance(other, self.__class__) or self.parent != other.parent:
            return NotImplemented
        if self.attrs == other.attrs:
            return self.name >= other.name
        else:
            return self.attrs > other.attrs


class ProjectDirectory(Directory):
    def __init__(self, dir_path: PathStr):
        super().__init__(dir_path)
        self._index_file: ProjectFile | None = None
        self._sorted_files: list[ProjectFile] = [
            ProjectFile(_, self._path) for _ in self._files
            if _.suffix == ".md" and _.name not in _index_files]
        self.read_all()
        self.set_index_file()

    def read_all(self):
        for file in self._sorted_files:
            file.read()
            file.specify_front_matter()

    @property
    def parent(self) -> str:
        return str(self._path.relative_to(_core_path))

    @property
    def level_value(self) -> int:
        return len(self.parent.split("/"))

    def set_index_file(self):
        if self._path.joinpath("_index.md").exists():
            _: ProjectFile = ProjectFile(self._path.joinpath("_index.md"), self._path)
        elif self._path.joinpath("index.md").exists():
            _: ProjectFile = ProjectFile(self._path.joinpath("index.md"), self._path)
        else:
            raise ValueError
        self._index_file = _
        self._index_file.read()
        self._index_file.specify_front_matter()

    @property
    def title_value(self):
        return self._index_file.title

    def iter_files(self):
        return iter(filter(lambda x: x.suffix == ".md" and bool(x), self._files))

    def iter_project_files(self):
        return iter(ProjectFile(_, self._path) for _ in self.iter_files())

    @property
    def files_value(self):
        return list(filter(lambda x: x.name not in _index_files, self._sorted_files))

    def sort(self):
        self.read_all()
        logger.error(f"{self._sorted_files}")
        _: dict[FileParams, ProjectFile] = {
            _file.params: _file for _file in self._sorted_files
        }
        _params: list[FileParams] = sorted(_.keys(), key=attrgetter("attrs"))
        _params: list[FileParams] = sorted(_params, key=attrgetter("name"))
        self._sorted_files = [_[k] for k in _params]

    def output_subfolders(self):
        _output_values: list[dict] = []
        for subfolder in self._subfolders:
            logger.info(f"subfolder = {subfolder}")
            project_dir = self.__class__(subfolder)
            project_dir.set_items()
            project_dir.set_index_file()
            project_dir.read_all()
            project_dir.sort()
            _output_values.append(project_dir.output())
        return _output_values

    def output(self):
        _ = {
            self.parent: {
                "title": {
                    "value": self.title_value,
                    "level": self.level_value
                },
                "index": [str(self._index_file.path)],
                "files": [str(_.path) for _ in self.files_value]
            }
        }
        if self.output_subfolders() is not None:
            for item in self.output_subfolders():
                _.update(item)
        return _

    def relpath(self, file: ProjectFile):
        return file.path.relative_to(self._path)

    def iter_dir_items(self):
        return iter(self.__class__(_) for _ in self._subfolders)

    @property
    def weight(self):
        return self._index_file.weight


class YamlFile:
    def __init__(self, file: PathStr):
        self._file: Path = Path(file).resolve()
        self._dict_attributes: dict[str, Any] = dict()

    def __getitem__(self, item):
        return self._dict_attributes[item]

    def __setitem__(self, key, value):
        self._dict_attributes[key] = value

    @property
    def title(self):
        _: ProjectFile = ProjectFile(_core_path.joinpath("_index.md"), _core_path)
        _.read()
        _.specify_front_matter()
        return _.title

    def set_attributes(self):
        _settings: dict[str, str | bool | int] = {
            "title-page": self.title,
            "doctype": "book",
            "toc": True,
            "figure-caption": "Рисунок",
            "toc-title": "Содержание",
            "outlinelevels": 2,
            "title-logo-image": "images/logo.svg[top=4%,align=right,pdfwidth=3cm]",
            "version": "1.0.0"
        }
        _rights: dict[str, dict[str, str | bool] | list[str]] = {
            "title": {
                "value": "Юридическая информация",
                "title-files": False
            },
            "index": ["content/common/_index.md"]
        }
        self["settings"] = _settings
        self["Rights"] = _rights

    def add(self, dir_path: PathStr):
        project_directory: ProjectDirectory = ProjectDirectory(dir_path)
        project_directory.sort()
        for k, v in project_directory.output().items():
            self[k] = v

    def output(self):
        print(type(dump(self._dict_attributes)))
        return dump(self._dict_attributes)

    def write(self):
        with self._file.open("w+", encoding="utf-8") as f:
            f.write(str(dump(self._dict_attributes, indent=2, allow_unicode=True)))


@configure_custom_logging("generate")
def generate(path: PathStr):
    # _dir = ProjectDirectory(path)
    yaml_file: YamlFile = YamlFile(_core_path.with_name("yaml_file.yml"))
    yaml_file.set_attributes()
    yaml_file.add(path)
    logger.info(yaml_file._dict_attributes)
    print(yaml_file.output())
    yaml_file.write()


if __name__ == '__main__':
    _path = "C:\\Users\\tarasov-a\\PycharmProjects\\Protei_STP\\content\\common"
    generate(_path)

    # _file = "/Users/user/PycharmProjects/hugo_ascii_doc/markdown/content/_index.md"
    # project_file: ProjectFile = ProjectFile(_file)
    # project_file.read()
    # project_file.specify_front_matter()
    # print(project_file.predefined())
