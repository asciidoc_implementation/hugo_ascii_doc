from os import getenv
from subprocess import CalledProcessError, TimeoutExpired, run
from sys import platform
from typing import Iterable

_packages: tuple[str, ...] = (
    "asciidoctor",
    "asciidoctor-pdf",
    "rghost",
    "asciidoctor-kroki",
    "kramdown-asciidoc",
    "asciidoc-diagrams",
    "asciidoctor-kroki")


def add_to_path():
    _path: str | None = getenv("PATH", None)
    if _path is None:
        raise OSError("$PATH variable is not found")

    if platform.startswith("win"):
        _separator: str = ";"
        _local: str = "C:\\Windows\\System32\\"
    else:
        _separator: str = ":"
        _local: str = "/usr/local/bin/"

    _: list[str] = _path.split(_separator)
    if _local not in _:
        run(["export", "PATH", "=", f'"$PATH{_separator}{_local}"'])


def cmd(commands: Iterable[str]):
    if platform.startswith("win"):
        commands: list[str] = ["cmd", *commands]
    return commands


def install(packages: Iterable[str]):
    add_to_path()
    for _package in packages:
        try:
            run(cmd((_package, "-v")), timeout=30, capture_output=True, check=True)
        except CalledProcessError | TimeoutExpired:
            try:
                run(cmd(("gem", "install", _package)), timeout=30, capture_output=True, check=True)
            except CalledProcessError | TimeoutExpired:
                try:
                    run(cmd(("sudo", "-S", "gem", "install", _package)), timeout=30, capture_output=True, check=True)
                except CalledProcessError | TimeoutExpired:
                    print(f"Failed to install {_package}")
                finally:
                    continue
            else:
                print(f"Gem {_package} installed")
                continue
        else:
            print(f"Gem {_package} is already installed")
            continue
