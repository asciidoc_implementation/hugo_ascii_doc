from __future__ import annotations

from argparse import ArgumentError, ArgumentParser, Namespace, ONE_OR_MORE, RawDescriptionHelpFormatter, SUPPRESS
from collections import deque
from itertools import zip_longest
from pathlib import Path
from re import Match, fullmatch
from sys import platform
from textwrap import dedent
from typing import Iterable, Iterator

from loguru import logger

from ascii_doc.common import PathStr
from init_logger import configure_custom_logging

_headers: tuple[str, ...] = (
    "Параметр", "Описание", "Тип", "По умолчанию", "O/M", "P/R", "Версия"
)


def inspect_line(line: str):
    _: str = line.strip().removesuffix("\n")
    return not _ or _ in ("-", "\-") or line.startswith((*_headers, "----")) or line == "|==="


def bound_line(lines: Iterable[str], boundary: str = ""):
    _: str = boundary.join(lines)
    return f"{boundary}{_}{''.join(reversed(boundary))}\n"


def adoc_format(items: Iterable[str]):
    _ = "\n|".join(items)
    return f"|{_}\n\n"


def joined_line(line: str):
    return True if fullmatch(r"\d\+\|.*", line) else False


def value_joined(line: str):
    if joined_line(line):
        return fullmatch(r"(\d).*", line).group(1)


class DocFile:
    def __init__(self, file: PathStr):
        self._file: Path = Path(file).resolve()
        self._content: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}:\n{self._content}"

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def read(self):
        with self._file.open("r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with self._file.open("w+", encoding="utf-8") as f:
            f.writelines(self._content)
        print(f"File {self._file}")

    def __getitem__(self, item):
        return self._content.__getitem__(item)

    def __setitem__(self, key, value):
        self._content.__setitem__(key, value)

    @property
    def extension(self):
        _table: dict[str, str] = {
            ".md": "Markdown",
            ".adoc": "AsciiDoc"
        }
        return _table.get(self._file.suffix, None)


class MarkdownDocFile(DocFile):
    @classmethod
    def from_parent(cls, doc_file: DocFile):
        return cls(doc_file._file)

    @property
    def table_lines(self):
        return list(filter(lambda x: x.startswith("|"), self._content))

    @property
    def table_indexes(self):
        return [index for index, line in enumerate(self._content) if line.startswith("|")]


class AsciiDocFile(DocFile):
    @classmethod
    def from_parent(cls, doc_file: DocFile):
        return cls(doc_file._file)

    def replace_decorators(self):
        for index, line in enumerate(iter(self)):
            _m: Match = fullmatch(r"(\w|\d[\^*+]{,3})\|(.*)", line)
            if _m:
                self[index] = line.removeprefix(_m.group(1))

    @property
    def table_line_indexes(self) -> list[tuple[int, int]]:
        _indexes: list[int] = [index for index, line in enumerate(iter(self)) if line == "|===\n"]
        _indexes.sort()
        return [(x, y) for x, y in zip(_indexes[::2], _indexes[1::2])]

    @property
    def ascii_doc_tables(self) -> list[AsciiDocTable]:
        return list(self.iter_ascii_doc_tables())

    def _ascii_doc_lines(self, index: int):
        if index > len(self.table_line_indexes):
            raise ValueError
        return self._content[self.table_line_indexes[index][0]:self.table_line_indexes[index][1] + 1]

    def iter_ascii_doc_tables(self) -> Iterator[AsciiDocTable]:
        for index in range(len(self.table_line_indexes)):
            yield AsciiDocTable(self._ascii_doc_lines(index))

    @property
    def _non_table_lines(self):
        _non_table: deque[int] = deque()
        for x, y in self.table_line_indexes:
            _non_table.extend((x, y))
        _non_table.appendleft(0)
        _non_table.append(len(self))
        _non_table_indexes: list[tuple[int, int]] = [
            (x, y) for x, y in zip([*_non_table][::2], [*_non_table][1::2])]
        return [self._content[x:y + 1] for x, y in _non_table_indexes]

    # @property
    # def ascii_doc_table_lines(self):
    #     return [ascii_doc_table.lines for ascii_doc_table in self.iter_ascii_doc_tables()]

    def update(self, ascii_doc_table_lines: Iterable[Iterable[str]]):
        x: list[str]
        y: list[str]
        _lines: list[str] = []
        # logger.error(f"self.ascii_doc_table_lines = {ascii_doc_table_lines}")
        for x, y in zip_longest(self._non_table_lines, ascii_doc_table_lines, fillvalue=[]):
            _lines.extend(x)
            _lines.extend(y)
            # _lines.append("\n|===")
        self._content = _lines


class AsciiDocTable:
    def __init__(self, raw_lines: Iterable[str], num_cols: int = 7):
        self._raw_lines: list[str] = [*raw_lines]
        self._num_cols: int = num_cols
        self._lines: list[str] = []
        self._adoc_lines: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}:\n{self._lines}"

    def set_lines(self):
        not_empty_lines: list[str] = list(filter(lambda x: x and not x.startswith("|==="), self._raw_lines))
        join_lines: str = "".join(not_empty_lines)
        self._lines = [_.strip() for _ in join_lines.split("|") if _ != "\n"]

    def num_rows(self):
        return len(self._lines) // self._num_cols

    def adoc_table_lines(self):
        _lines = []
        for index in range(self.num_rows()):
            _items: list[str] = [
                self._lines[index * self._num_cols + col_index]
                for col_index in range(self._num_cols)]
            _lines.append(adoc_format(_items))
        self._adoc_lines = _lines

    def __len__(self):
        return self._num_cols

    def __getitem__(self, item):
        if not isinstance(item, int):
            raise KeyError
        if item >= self.num_rows():
            raise ValueError
        return self._adoc_lines[item]

    def __setitem__(self, key, value):
        self._lines[key] = value

    @property
    def lines(self):
        return self._lines

    @lines.setter
    def lines(self, value):
        self._lines = value

    @property
    def adoc_lines(self):
        return self._adoc_lines

    @adoc_lines.setter
    def adoc_lines(self, value):
        self._adoc_lines = value


class TableRow:
    def __init__(self, line: str, separator: str):
        self._raw_line: str = line
        self._separator: str = separator
        self._line_items: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}: {self.combine_line()}"

    def __getitem__(self, item):
        return self._line_items[item]

    def __setitem__(self, key, value):
        self._line_items[key] = value

    def set_line_items(self):
        raise NotImplementedError

    def combine_line(self):
        raise NotImplementedError

    def __len__(self):
        return len(self._line_items)

    def merge_columns(
            self,
            from_col: int,
            to_col: int,
            prefix_info: str | None = None,
            suffix_info: str | None = None):
        if prefix_info is None:
            prefix_info: str = ""
        if suffix_info is None:
            suffix_info: str = ""
        try:
            _from_text: str = self[from_col]
            _to_text: str = self[to_col]
            # logger.error(_from_text)
            # logger.error(_to_text)
        except (IndexError | KeyError) as e:
            logger.error(f"{e.__class__.__name__}: from {from_col} : to {to_col}: len {len(self)}")
        else:
            if not inspect_line(_from_text):
                self[to_col] = f"{_to_text}{self._separator}{prefix_info}{_from_text}{suffix_info}"
        finally:
            # logger.error(f"self._line_items = {self._line_items}")
            self._line_items.pop(from_col)


class AsciiDocTableRow(TableRow):
    def __init__(self, line: str):
        separator: str = " +\n"
        super().__init__(line, separator)

    def set_line_items(self):
        self._line_items = [_.strip() for _ in self._raw_line.split("|")[1:]]

    def combine_line(self):
        return adoc_format(self._line_items)


class TableRowMarkdown(TableRow):
    def __init__(self, line: str):
        separator: str = "<br>"
        super().__init__(line, separator)

    def set_line_items(self):
        self._line_items = [_.strip() for _ in self._raw_line.split("|")[1:-1]]

    def combine_line(self):
        return bound_line(self._line_items, "| ")


def get_child(doc_file: DocFile):
    if doc_file.extension == "Markdown":
        return MarkdownDocFile.from_parent(doc_file)
    elif doc_file.extension == "AsciiDoc":
        return AsciiDocFile.from_parent(doc_file)


@configure_custom_logging("join_columns")
def join_ascii_doc_columns(
        file: PathStr,
        from_col: int,
        to_col: int,
        prefix_info: str | None = None,
        suffix_info: str | None = None):
    doc_file: DocFile = DocFile(file)
    ascii_doc_file: AsciiDocFile = AsciiDocFile.from_parent(doc_file)
    ascii_doc_file.read()
    ascii_doc_file.replace_decorators()
    ascii_doc_table_lines: list[list[str]] = []
    for ascii_doc_table in ascii_doc_file.ascii_doc_tables:
        ascii_doc_table.set_lines()
        ascii_doc_table.adoc_table_lines()
        for index, ascii_doc_line in enumerate(ascii_doc_table.adoc_lines):
            ascii_doc_row: AsciiDocTableRow = AsciiDocTableRow(ascii_doc_line)
            ascii_doc_row.set_line_items()
            ascii_doc_row.merge_columns(from_col, to_col, prefix_info, suffix_info)
            ascii_doc_table.adoc_lines[index] = ascii_doc_row.combine_line()
        ascii_doc_table_lines.append(ascii_doc_table.adoc_lines)

    ascii_doc_file.update(ascii_doc_table_lines)
    ascii_doc_file.write()


@configure_custom_logging("join_columns")
def join_md_columns(
        file: PathStr,
        from_col: int,
        to_col: int,
        prefix_info: str | None = None,
        suffix_info: str | None = None):
    doc_file: DocFile = DocFile(file)
    doc_file.read()
    markdown_doc_file: MarkdownDocFile = MarkdownDocFile.from_parent(doc_file)
    markdown_doc_file.read()
    for index, line in enumerate(iter(markdown_doc_file.table_lines)):
        table_row: TableRowMarkdown = TableRowMarkdown(line)
        table_row.set_line_items()
        if len(table_row) < 7:
            continue
        table_row.merge_columns(from_col, to_col, prefix_info, suffix_info)
        # logger.error(f"index = {index}")
        markdown_doc_file[markdown_doc_file.table_indexes[index]] = table_row.combine_line()

    markdown_doc_file.write()


@configure_custom_logging("join_columns")
def merge_files(directory: PathStr):
    _from_col: int = 3
    _to_col: int = 1
    _prefix_info: str = "По умолчанию: "
    _suffix_info: str = "."
    for _ in Path(directory).iterdir():
        if _.is_file() and _.suffix == ".md":
            join_md_columns(_, _from_col, _to_col, _prefix_info, _suffix_info)


_default_output: str = "bash.sh"

_note_rel_path: str = "Note that should be relative to {base}"
_note_exclusive: str = "Mutually exclusive with"
_note_pwd: str = "Using 'pwd' for current directory and marks '.' and '..' is allowed"

_help_base: str = f"Path. Specify the file or directory to use as a base.\n{_note_pwd}"
_help_file: str = (
    "str | Path | list[str] | list[Path]. Specify the files to tranform the tables.\n"
    "May be used multiple times.\n"
    f"{_note_rel_path}.\n{_note_pwd}.\n"
    f"{_note_exclusive} -d/--dir")
_help_dir: str = (
    "str | Path. Specify the folder to get all files inside and add to tranform the tables.\n"
    f"{_note_rel_path}.\n{_note_pwd}.\n"
    f"{_note_exclusive} -f/--file")
_help_recursive: str = (
    "Flag. Specify to get files from the directory recursively.\n"
    "Note that used only with the -d/--dir option, otherwise ignored")
_help_version: str = (
    "Show the script version with the full information message and close the window")
_help_help: str = "Show the help message and close the window"

description: str = "Script to merge table columns"

prog: str = Path(__file__).name

_press_any_key: str = "Press any key to close..."

_python_version: str = "python" if platform.startswith("win") else "python3"
usage: str = (
    f"{_python_version} {prog} \n"
    f"    [ -h/--help | -v/--version ] \n"
    f"    ( -f/--file FILES... | -d/--dir DIR [ -r/--recursive ] ) <BASE>")

epilog: str = dedent(f"""\
The script rewrites the files with tables having the merged columns""")

_text_info: str = dedent(f"""
The files can be set explicitly or received as content from the directory and/or its subfolders.
Note that only *.md or *.adoc files are searched and processed. Other file types are ignored.

Examples:

python merge_columns.py content/common/oam -f installation.md --files api.md cli.md
python merge_columns.py content/common -d logging --recursive
python merge_columns.py content/common --dir basics --recursive 
""")

version: str = f"{description}, 0.1.0 beta\n{_text_info}"


def validate_path(base: str, file_or_dir: str) -> bool:
    base: str = base.replace("pwd", str(Path().cwd().resolve()))
    _base_path: Path = Path(base).resolve()
    if not _base_path.exists():
        print(f"Invalid base path: {base}")
        return False

    _full_path: Path = _base_path.joinpath(file_or_dir)

    if not _full_path.exists():
        print(f"Invalid additional part to the base path: {file_or_dir}")
        return False
    print(_full_path)
    return True


@configure_custom_logging("convert_to_adoc")
def generate(
        base: str,
        files: Iterable[str] = None,
        folder: str = None,
        is_recursive: bool = False):
    if not base:
        logger.error(f"Base path must be set")
        input(_press_any_key)
        raise ValueError

    if files is None and folder is None:
        logger.error(f"At least one of the -f/--file and -d/--dir must be set")
        input(_press_any_key)
        raise AttributeError

    if files is None and not validate_path(base, folder):
        logger.error(f"Folder is not found")
        input(_press_any_key)
        raise ValueError

    base: str = base.replace("pwd", str(Path.cwd().resolve()))
    _base_path: Path = Path(base).resolve()

    if files is None:
        _folder_path: Path = _base_path.joinpath(folder)
        if is_recursive:
            _final: list[Path] = list(_folder_path.rglob("*.md|*.adoc"))
        else:
            _final: list[Path] = [
                _child for _child in _folder_path.iterdir()
                if _child.is_file() and _child.suffix == ".md"]
    else:
        _final: list[Path] = [
            Path(file) for file in files if validate_path(base, file)]

        if not _final:
            logger.error("All provided files are inappropriate to convert")
            input(_press_any_key)
            return


@configure_custom_logging("convert_to_adoc")
def parse_command_line():
    parser: ArgumentParser = ArgumentParser(
        prog=prog,
        usage=usage,
        description=description,
        epilog=epilog,
        formatter_class=RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False)
    exclusive_group = parser.add_mutually_exclusive_group(required=True)
    exclusive_group.add_argument(
        "-f", "--file",
        action='extend',
        nargs=ONE_OR_MORE,
        default=SUPPRESS,
        help=_help_file,
        dest="files")
    exclusive_group.add_argument(
        "-d", "--dir",
        action="store",
        default=SUPPRESS,
        help=_help_dir,
        dest="folder")
    parser.add_argument(
        "-r", "--recursive",
        action="store_true",
        help=_help_recursive,
        dest="is_recursive")
    parser.add_argument(
        "-v", "--version",
        action="version",
        version=version,
        help=_help_version)
    parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help=_help_help)

    try:
        args: Namespace = parser.parse_args()
        if hasattr(args, "help") or hasattr(args, "version"):
            return
        else:
            return args

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")


@configure_custom_logging("convert_to_adoc")
def update():
    args: Namespace = parse_command_line()
    base: str = getattr(args, "base")
    files: list[str] = getattr(args, "files") if hasattr(args, "files") else None
    folder: str = getattr(args, "folder") if hasattr(args, "folder") else None
    is_recursive: bool = getattr(args, "is_recursive") if hasattr(args, "is_recursive") else None
    output: str = getattr(args, "output") if hasattr(args, "output") else None
    is_run: bool = getattr(args, "is_run") if hasattr(args, "is_run") else False
    logger.info(args)
    return generate(base, files, folder, is_recursive, output, is_run)


if __name__ == '__main__':
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\basics")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\config")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\config\\component")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\logging")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\logging\\edr")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\logging\\stat")
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\oam")
