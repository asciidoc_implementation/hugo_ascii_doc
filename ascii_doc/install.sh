export PATH="/usr/local/bin/:$PATH"
gem install --bindir /usr/local/bin/ asciidoctor
gem install --bindir /usr/local/bin/ asciidoctor-pdf
gem install --bindir /usr/local/bin/ rghost
gem install --bindir /usr/local/bin/ kramdown-asciidoc
gem install --bindir /usr/local/bin/ asciidoctor-kroki