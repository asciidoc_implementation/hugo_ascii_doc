from typing import TypeAlias
from pathlib import Path
from itertools import zip_longest

PathStr: TypeAlias = str | Path
OptPath: TypeAlias = Path | None
OptStr: TypeAlias = str | None
OptPathStr: TypeAlias = str | Path | None


_press_any_key: str = "Press the ENTER key to close the window:"
_index_file_md: tuple[str, ...] = ("_index.md", "index.md")


class InvalidExtensionError(Exception):
    """The file has an invalid extension."""


class InvalidLinkTypeError(Exception):
    """The link does not lead to the file."""


def validate_path(base: PathStr, file: PathStr, extension: str = "md"):
    try:
        _: Path = Path(base).joinpath(file).resolve(strict=True)
        if not _.exists():
            raise FileNotFoundError
        if not _.is_file():
            raise InvalidLinkTypeError
        if not _.suffix.removeprefix(".") == extension:
            raise InvalidExtensionError
    except FileNotFoundError as e:
        print(f"Invalid path {str(Path(base).resolve())}/{file}")
        print(f"{e.__class__.__name__}: {e.strerror}")
        input(_press_any_key)
        raise
    except InvalidExtensionError | InvalidLinkTypeError as e:
        print(f"{e.__class__.__name__}: {str(e)}")
        input(_press_any_key)
        raise


def is_index(path: PathStr):
    return Path(path).name in _index_file_md


class LinkValidator:
    __slots__ = ("_base", "_from_file", "_to_file", "_anchor")

    def __init__(
            self, *,
            from_file: OptPathStr = None,
            to_file: OptPathStr = None,
            anchor: OptStr = None,
            base: OptPathStr = None):
        if base is None:
            base: Path = Path(__file__).resolve().parent
        self._base: Path = Path(base).resolve()
        self._from_file: OptPathStr = from_file
        self._to_file: OptPathStr = to_file
        self._anchor: OptStr = anchor

    def __bool__(self):
        return self._base, self._from_file, self._to_file is not None, None, None

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"({self._base}, {self._from_file}, {self._to_file}, {self._anchor})")

    def __str__(self):
        return (
            f"{self.__class__.__name__}({self._base}): "
            f"{self._from_file} -> {self._to_file}{self.anchor_string}")

    @property
    def anchor_string(self):
        return f"#{self._anchor}" if self._anchor else ""

    def _from_path(self):
        if self._from_file is not None:
            return self._base.joinpath(self._from_file)
        else:
            return self._base

    def _to_path(self):
        if self._to_file is not None:
            return self._base.joinpath(self._to_file)
        else:
            return self._base

    @property
    def _rel_from_path(self):
        return self._from_path().relative_to(self._base)

    @property
    def _rel_to_path(self):
        return self._to_path().relative_to(self._base)

    def _validate(self):
        if not self:
            print(
                f"Either source file or destination one is not specified.\n"
                f"source: {self._from_file}\ndestination: {self._to_file}")

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self._base == other._base:
            return self._from_path(), self._to_path() == other._from_path(), other._to_path()
        else:
            return False

    def __ne__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self._base == other._base:
            return self._from_path(), self._to_path() != other._from_path(), other._to_path()
        else:
            return True

    def _valid_link(self):
        return str(self._rel_from_path.joinpath(self._rel_to_path))

    def site_link(self):
        if is_index(self._from_path()):
            _: str = self._valid_link().removeprefix("../")
        else:
            _: str = f"../{self._valid_link()}"
        if is_index(self._to_path()):
            return f"{_.removesuffix(self._to_path().name)}/{self.anchor_string}"
        else:
            return f"{_.removesuffix(self._to_path().suffix)}/{self.anchor_string}"

    @classmethod
    def generate_link(cls, link: str, file: str):
        # [\w\.//-]*#*[\w\.//-]//+
        _from_file: Path = Path(file).resolve()
        _to_file: Path = _from_file.joinpath(link).resolve()
        for _ in _from_file.parents[:]:
            if _ in _to_file.parents[:]:
                _common = _
                break
        print(_common)

    @classmethod
    def generate(cls, link: str, file: str):
        _from_file: Path = Path(file).resolve()
        _link: str = link.split("#")[0].removesuffix("/").removesuffix(".md")
        print(_link)
        _ = f"{Path(_link).stem}.md"
        for _parent in _from_file.parents[:]:
            if _parent.parts[-1].endswith()
        __path = "/".join(link.split("#")[0].split("/")[:-1])
        _path = f"{__path}/{_}"
        print(_path)







def proper_link(from_file: PathStr, to_file: PathStr):
    _base_path: Path = Path(__file__).parent


if __name__ == '__main__':
    _from = "/Users/user/PycharmProjects/hugo_ascii_doc/markdown/content/common/config/ap.md"
    _to = "../../logging/edr/connect.md"
    _ = LinkValidator(from_file=_from, to_file=_to)
    # print(str(_))
    LinkValidator.generate(_to, _from)

