from functools import cached_property
from pathlib import Path
from typing import Iterable, Iterator, NamedTuple, TypeAlias


PathStr: TypeAlias = str | Path


def get_index_md(path: Path):
    _path_parent_index = path
    try:
        _path_parent_index: Path = path.joinpath("_index.md").resolve(strict=True)
    except FileNotFoundError:
        _path_parent_index: Path = path.joinpath("index.md").resolve()
    finally:
        return _path_parent_index


def get_title(value: PathStr, base: PathStr):
    _: ProjectFile = ProjectFile(value, base)
    _.read()
    return _.title


class Level(NamedTuple):
    level: int
    parent: PathStr
    name: str
    base: PathStr

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict()}"

    @cached_property
    def full_path(self) -> object:
        return Path(self.base).joinpath(self.parent).joinpath(self.name).resolve()

    def __eq__(self, other):
        if isinstance(other, self.__class__) and self.full_path == other.full_path:
            return self.level == other.level
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__) and self.full_path == other.full_path:
            return self.level != other.level
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.level > other.level
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.level < other.level
        else:
            return NotImplemented

    def __le__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.full_path == other.full_path:
            return self.level >= other.level
        else:
            return self.level > other.level

    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.full_path == other.full_path:
            return self.level <= other.level
        else:
            return self.level < other.level


class ProjectFile:
    def __init__(self, file_path: PathStr, base: PathStr):
        self._path: Path = Path(file_path).resolve()
        self._content: list[str] = []
        self._base: Path = base
        self._front_matter_indexes: list[int] = [index for index in range(1, 12)]
        self._validate()

    def _validate(self):
        if not self._path.is_file():
            raise TypeError(f"Path {self._path} does not lead to the file")

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._content[item]
        else:
            raise TypeError(f"Key {item} must be int or slice")

    @property
    def raw_extension(self) -> str:
        return self._path.suffix.removeprefix(".")

    @property
    def front_matter(self) -> list[str]:
        return [self[index] for index in self._front_matter_indexes]

    @cached_property
    def level(self) -> int:
        return len(self._path.resolve().parts) - len(self._base.resolve().parts) - 1

    @cached_property
    def _front_matter_dict(self) -> dict[str, str]:
        _front_matter_items: tuple[tuple[str, ...], ...] = tuple((*_.split(":"),) for _ in self.front_matter)
        return {
            _key.strip(): _value.strip().removesuffix("\n")
            for _key, _value in _front_matter_items
        }

    @cached_property
    def weight(self) -> int:
        return self._front_matter_dict.get("weight", -1)

    @cached_property
    def title(self) -> str:
        return self._front_matter_dict.get("title", "")

    @property
    def path_dir(self) -> Path:
        return self._path.parent

    @property
    def path(self) -> Path:
        return self._path

    def read(self):
        with open(self._path, "r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._path, "w+", encoding="utf-8") as f:
            f.write("".join(self._content))
        return

    @property
    def name(self) -> str:
        return self._path.name

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.weight == other.weight
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.weight != other.weight
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.weight <= other.weight
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.weight < other.weight
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.weight >= other.weight
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.weight > other.weight
        else:
            return NotImplemented

    def __bool__(self):
        return self.weight > -1

    @cached_property
    def level_instance(self) -> Level:
        return Level(
            self.level,
            self.path_dir.relative_to(self._base),
            self.name,
            self._base)


class ProjectFileContainer:
    def __init__(self, project_files: Iterable[ProjectFile] = None):
        if project_files is None:
            project_files = []
        self._project_files: tuple[ProjectFile] = *project_files,

    def __len__(self):
        return len(self._project_files)

    def __iter__(self):
        return iter(self._project_files)

    @property
    def max_level(self) -> int:
        return max(_.level_instance.level for _ in iter(self))

    @cached_property
    def parents(self) -> set[str]:
        return {str(_.level_instance.parent) for _ in iter(self)}

    @cached_property
    def names(self) -> tuple[str, ...]:
        return tuple(_.level_instance.name for _ in iter(self))

    def sorted_parents(self, base: PathStr) -> tuple[tuple[int, list[str]], ...]:
        _weight: dict[int, dict[str, int]] = dict()
        for level in range(self.max_level + 1):
            _weight[level] = dict()
        for _parent in self.parents:
            _path_parent: Path = Path(base).joinpath(_parent)
            _path_parent_index: Path = get_index_md(_path_parent)
            _file: ProjectFile = ProjectFile(_path_parent_index, base)
            _file.read()
            _weight[_file.level][_parent] = _file.weight
        _res: dict[int, list[str]] = {}
        for _ in _weight:
            if _ == 0:
                continue
            _parents: list[str] = sorted(_weight.get(_).keys(), key=lambda x: _weight.get(_).get(x))
            _res[_] = _parents
        return tuple((k, v) for k, v in _res.items())

    def output_parents(self, base: PathStr) -> list[str]:
        lines: list[str] = []
        for k, v in self.sorted_parents(base):
            for value in v:
                _path_base: Path = Path(base).joinpath(value)
                print(f"_path_base = {_path_base}")
                lines.append(f"{value}:")
                lines.append(
                    f"  index: {_path_base.joinpath('_index.md').relative_to(base.parent.parent)}")
                if k > 1:
                    _file_index: Path = get_index_md(_path_base)
                    print(_file_index)
                    lines.append(f"  title:")
                    lines.append(f"    value: {get_title(_file_index, '/')}")
                _files: list[Path] = [
                    _ for _ in _path_base.iterdir()
                    if _.suffix.removeprefix(".") == "md" and not str(_).endswith("index.md")]
                if _files:
                    lines.append(f"  files:")
                    lines.extend(
                        [f"    - {str(_.relative_to(base.parent.parent))}" for _ in _files])
        return lines

    def __getitem__(self, item):
        if isinstance(item, int):
            if 0 <= item < self.max_level:
                return list(filter(lambda x: x.level == item, iter(self)))
            else:
                print(f"Key {item} is out of bounds, min is 0, max is {self.max_level}")
                raise KeyError
        elif isinstance(item, str):
            if item in self.parents:
                return list(filter(lambda x: x.level_instance.parent == item, iter(self)))
            elif item in self.names:
                return list(filter(lambda x: x.level_instance.name == item, iter(self)))
            else:
                print(f"Key {item} must be a parent or a name")
                raise KeyError
        else:
            print(f"Key must be int or str but {type(item)} received")
            raise TypeError


class ProjectDirectory:
    def __init__(self, dir_path: PathStr):
        self._path: Path = Path(dir_path).resolve()
        self._project_files: list[ProjectFile] = []
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def _validate(self):
        if not self._path.is_dir():
            raise TypeError(f"Path {self._path} does not lead to the folder")

    def __iter__(self):
        return iter(self._path.iterdir())

    @property
    def contents(self) -> tuple[Path, ...]:
        return tuple(iter(self))

    def set_project_files(self, path: PathStr = None):
        if path is None:
            path: Path = self._path
        for _p in path.iterdir():
            if _p.is_file() and _p.suffix.removeprefix(".") == "md":
                _file: ProjectFile = ProjectFile(_p, self._path)
                self._project_files.append(_file)
            elif _p.is_dir():
                self.set_project_files(_p)
            else:
                continue
        return

    @property
    def project_files(self) -> list[ProjectFile]:
        return self._project_files

    def sort_project_files(self):
        self._project_files.sort(key=lambda project_file: project_file.level_instance)

    def iter_files(self) -> Iterator[Path]:
        return iter(filter(lambda x: x.is_file(), self._path.iterdir()))

    def iter_subfolders(self) -> Iterator[Path]:
        return iter(filter(lambda x: x.is_dir(), self._path.iterdir()))

    def __contains__(self, item):
        if isinstance(item, Path):
            return item in iter(self)
        elif isinstance(item, str):
            _path_item: Path = Path(item).resolve()
            if _path_item.exists():
                return _path_item in iter(self)
            else:
                return False
        else:
            return NotImplemented

    @property
    def path(self) -> Path:
        return self._path


if __name__ == '__main__':
    _path: str = "/Users/user/Desktop/pgw-develop_pages/content/common"
    _path_dir: Path = Path(_path)
    _project_dir: ProjectDirectory = ProjectDirectory(_path_dir)
    print(_project_dir.path)
    _project_dir.set_project_files()
    for _project_file in _project_dir.project_files:
        _project_file.read()
    _project_dir.sort_project_files()
    _project_container: ProjectFileContainer = ProjectFileContainer(_project_dir.project_files)
    print(_project_container.parents)
    print(_project_container.sorted_parents(_project_dir.path))
    print("\n".join(_project_container.output_parents(_project_dir.path)))
