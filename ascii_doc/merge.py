from pathlib import Path
from typing import TypeAlias, Iterable

PathStr: TypeAlias = str | Path

_headers: tuple[str, ...] = (
    "Параметр", "Описание", "Тип", "По умолчанию", "O/M", "P/R", "Версия"
)


def bound_line(lines: Iterable[str], boundary: str = ""):
    _: str = boundary.join(lines)
    return f"{boundary}{_}{''.join(reversed(boundary))}\n"


def inspect_line(line: str):
    _: str = line.strip().removesuffix("\n")
    return not _ or _ in ("-", "\-") or line.startswith((*_headers, "----")) or line == "|==="


class DocFile:
    def __init__(self, file: PathStr):
        self._file: Path = Path(file).resolve()
        self._content: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}:\n{self._content}"

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def read(self):
        with self._file.open("r+", encoding="utf-8") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with self._file.open("w+", encoding="utf-8") as f:
            f.writelines(self._content)
        print(f"File {self._file}")

    def __getitem__(self, item):
        return self._content.__getitem__(item)

    def __setitem__(self, key, value):
        self._content.__setitem__(key, value)

    @property
    def extension(self):
        _table: dict[str, str] = {
            ".md": "Markdown",
            ".adoc": "AsciiDoc"
        }
        return _table.get(self._file.suffix, None)


class MarkdownDocFile(DocFile):
    @classmethod
    def from_parent(cls, doc_file: DocFile):
        return cls(doc_file._file)

    @property
    def table_lines(self):
        return list(filter(lambda x: x.startswith("|"), self._content))

    @property
    def table_indexes(self):
        return [index for index, line in enumerate(self._content) if line.startswith("|")]


class TableRow:
    def __init__(self, line: str, separator: str):
        self._raw_line: str = line
        self._separator: str = separator
        self._line_items: list[str] = []

    def __str__(self):
        return f"{self.__class__.__name__}: {self.combine_line()}"

    def __getitem__(self, item):
        return self._line_items[item]

    def __setitem__(self, key, value):
        self._line_items[key] = value

    def set_line_items(self):
        raise NotImplementedError

    def combine_line(self):
        raise NotImplementedError

    def __len__(self):
        return len(self._line_items)

    def merge_columns(
            self,
            from_col: int,
            to_col: int,
            prefix_info: str | None = None,
            suffix_info: str | None = None):
        if prefix_info is None:
            prefix_info: str = ""
        if suffix_info is None:
            suffix_info: str = ""
        try:
            _from_text: str = self[from_col]
            _to_text: str = self[to_col]
        except (IndexError | KeyError) as e:
            print(f"{e.__class__.__name__}: from {from_col} : to {to_col}: len {len(self)}")
        else:
            if not inspect_line(_from_text):
                self[to_col] = f"{_to_text}{self._separator}{prefix_info}{_from_text}{suffix_info}"
        finally:
            self._line_items.pop(from_col)


class TableRowMarkdown(TableRow):
    def __init__(self, line: str):
        separator: str = "<br>"
        super().__init__(line, separator)

    def set_line_items(self):
        self._line_items = [_.strip() for _ in self._raw_line.split("|")[1:-1]]

    def combine_line(self):
        return bound_line(self._line_items, "| ")


def join_md_columns(
        file: PathStr,
        from_col: int,
        to_col: int,
        prefix_info: str | None = None,
        suffix_info: str | None = None):
    doc_file: DocFile = DocFile(file)
    doc_file.read()
    markdown_doc_file: MarkdownDocFile = MarkdownDocFile.from_parent(doc_file)
    markdown_doc_file.read()
    for index, line in enumerate(iter(markdown_doc_file.table_lines)):
        table_row: TableRowMarkdown = TableRowMarkdown(line)
        table_row.set_line_items()
        if len(table_row) < 7:
            continue
        table_row.merge_columns(from_col, to_col, prefix_info, suffix_info)
        markdown_doc_file[markdown_doc_file.table_indexes[index]] = table_row.combine_line()

    markdown_doc_file.write()


def merge_files(directory: PathStr):
    _from_col: int = 3
    _to_col: int = 1
    _prefix_info: str = "По умолчанию: "
    _suffix_info: str = "."

    for _ in Path(directory).resolve().rglob("*.md"):
        join_md_columns(_, _from_col, _to_col, _prefix_info, _suffix_info)

if __name__ == '__main__':
    # _file: str = "C:\\Users\\tarasov-a\\PycharmProjects\\hugo_ascii_doc\\markdown\\content\\common\\config\\component\\diameter.md"
    # _from_col: int = 3
    # _to_col: int = 1
    # _prefix_info: str = "По умолчанию: "
    # _suffix_info: str = "."
    # join_md_columns(_file, _from_col, _to_col, _prefix_info, _suffix_info)
    merge_files("C:\\Users\\tarasov-a\\PycharmProjects\\Protei_STP\\content\\common\\config")
