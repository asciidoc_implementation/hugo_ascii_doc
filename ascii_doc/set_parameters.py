from yaml import BaseLoader, load, dump

from common import File, PathStr


def prepare_value(value: str | bool | None) -> str:
    if value is None:
        return ""
    elif isinstance(value, bool):
        return f" {str(value).lower()}"
    else:
        return f" {value}"


class FileYaml(File):
    def __init__(self, file_path: PathStr):
        super().__init__(file_path)
        self._params: dict[str, dict] = dict()
        self._validate()

    def _validate(self):
        super()._validate()
        if self.raw_extension not in ("yml", "yaml"):
            raise ValueError(f"Invalid path file {self._path}")

    def read(self):
        with self._path.open("r+", encoding="utf-8") as f:
            content: dict[str, dict] = load(f, BaseLoader)
        self._params = content

    def write(self):
        with self._path.open("w+", encoding="utf-8") as f:
            f.write(dump(self._params))
        return

    @property
    def html5_params(self) -> list[str]:
        return [f"{k}:{prepare_value(v)}" for k, v in self._params.get("html5").items()]

    @property
    def pdf_params(self) -> list[str]:
        return [f"{k}:{prepare_value(v)}" for k, v in self._params.get("pdf").items()]

    def set_content(self):
        self._content = [
            'ifeval::["{backend}" == "html5"]',
            *self.html5_params,
            "endif::[]",
            "\n",
            'ifeval::["{backend}" == "pdf"]',
            *self.pdf_params,
            "endif::[]"]


if __name__ == '__main__':
    path: str = "/Users/user/PycharmProjects/hugo_ascii_doc/ascii_doc/attributes.yaml"
    file_yaml: FileYaml = FileYaml(path)
    file_yaml.read()
    print(file_yaml)
