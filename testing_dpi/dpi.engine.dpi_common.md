---
title: "dpi/engine/dpi_common.cfg"
description: "Файл конфигурации общих параметров приложения."
weight: 11
type: docs

---

Ключ для reload - `dpi_common.cfg`.

## Используемые секции

+ [Common](#dpi-common-common) - общие параметры конфигурации
+ [External Filter](#dpi-ext-filter) - параметры конфигурации подключения к `DPI.MED`
+ [BandwidthControl](#dpi-common-bandwidthcontrol) - параметры управления полосой пропускания на `DPI.TF`
+ [TcpReassembler](#dpi-common-tcpreassembler) - параметры настройки TCP-reassembler (не является внутренним функционалом ixEngine) 
+ [GTP](#dpi-common-gtp) - параметры обработки GTP-трафика Gn/Gp и S5/S8 интерфейсов мобильной сети
+ [HighAvailability](#dpi-common-highavailability) - параметры конфигурации механизмов определения состояния `DPI.CS`
+ [CongestionControl](#dpi-common-congestioncontrol) - параметры конфигурации управления перегрузками
+ [AccountingControl](#dpi-common-accountingcontrol) - параметры обработки RADIUS-запросов
+ [Handlers](#dpi-common-handlers) - параметры конфигурации логик-обработчиков
+ [Hotline](#dpi-common-hotline) - параметры управления функцией HTTP Redirect
+ [DnsWhitelist](#dpi-common-dnswhitelist) - параметры управления функцией DNS White List
+ [Timers](#dpi-common-timers) - параметры конфигурации логик-обработчиков
+ [Trace](#dpi-common-trace) - дополнительные параметры журналирования работы приложения (статистика по таблицам потоков и контекстов)
+ [Steering](#dpi-common-steering) - параметры прозрачного перенаправления трафика (стиринг)
+ [Snmp](#dpi-common-snmp) - параметры работы SNMP модуля приложения 

## Описание параметров

| Параметр                                                           | Описание                                                                                                                                           | Тип    | Значение по умолчанию | Допустимые значения                                                                                 | O/M | R      |
|--------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----------------------|-----------------------------------------------------------------------------------------------------|-----|--------|
| **<a name="dpi-common-common"></a>[Common]**                       |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| DB_Mode                                                            | Флаг необходимости использования БД для сохранения счетчиков локальных политик по трафику абонентов.                                               | int    | 0                     | 0 (не использовать БД), 1 (использовать локальную БД), 2 (использовать sqlite)                      | O   | P      |
| DB_Path                                                            | Путь к файлам БД sqlite при ее использовании.                                                                                                      | string | "../statistic.db"     | Любой валидный путь                                                                                 | O   | P      |
| ReopenIfApnChanged                                                 | Флаг необходимости повторного открытия gx, gy сессий при смене APN.                                                                                | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| QuotaThreshold                                                     | Минимальное израсходованное количество байт абонентов сервиса DPI.CS для посылки уведомления в абонентскую логику.                                 | int    | 1000                  | > 0                                                                                                 | O   | R      |
| UseMultipliers                                                     | Флаг использования множителей израсходованного трафика, полученных по Gx от PCRF в VSA, для отправки потребления по RG по Gy в OCS.                | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| EnableSyncPolicyManager                                            | Флаг использования логики SyncPolicyManager (блокировка потоков на старте без ожидания финальной классификации).                                   | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| PersistentMappings                                                 | Флаг сохранения информации об абонентских контекстах в LevelDB на диск.                                                                            | bool   | 0                     | 0, 1                                                                                                | O   | P      |
| LevelDBAsync                                                       | Флаг запуска работы c LevelDB в отдельных потоках со настраиваемой очередью.                                                                       | bool   | 0                     | 0, 1                                                                                                | O   | P      |
| LevelDBWriterQueueSize                                             | Размер очереди выделенного потока для работы с LevelDB.                                                                                            | int    | 4096                  | > 0                                                                                                 | O   | P      |
| LevelDBWriterIdleSleepMsec                                         | Время сна выделенного потока в мс для работы с LevelDB при отсутствии необходимости записи.                                                        | int    | 1                     | > 0                                                                                                 | O   | P      |
| LevelDBWriterInsertSleepMsec                                       | Время попытки записи информации в LevelDB выделенным потоком при переполнении очереди в мс.                                                        | int    | 10                    | > 0                                                                                                 | O   | P      |
| FragmentedWhiteList                                                | Список CIDR абонентов, фрагментированный трафик которых необходимо пропускать не смотря на выключенный флаг FragmentedPacketsPass.                 | list   | Пустой список         | Формат: { { IPv4/mask; }; { IPv4/mask; }; ... };                                                    | O   | R      |
| L2Accounting                                                       | Флаг для учета L2 заголовков пакетов при учете объема переданного трафика в IP-потоке.                                                             | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| UpdateFlowsQueueSize                                               | Максимальный размер очереди событий обновления потоков сервиса абонента.                                                                           | int    | 20000                 | > 0                                                                                                 | O   | P      |
| EventFlowsQueueSize                                                | Максимальный размер очереди событий применения event-based политики.                                                                               | int    | 10000                 | > 0                                                                                                 | O   | P      |
| ChangeServiceQueueSize                                             | Максимальный размер очереди CDR для событий обновления сервиса при изменении профиля абонента.                                                     | int    | 10000                 | > 0                                                                                                 | O   | P      |
| DefaultToS                                                         | Значение заголовкаipToS по умолчанию выставляемое IP-потокам.                                                                                      | int    | 255                   | 0-254, 255 (оставлять исходное значение в IP-пакете)                                                | O   | R      |
| ResetByLogicKey                                                    | Флаг использования ключа абонентской логики при сбросе абонента.                                                                                   | bool   | 1                     | 0, 1                                                                                                | O   | R      |
| SkipProcessIdle                                                    | Флаг необходимости проверки наличия обновлений для потоков трафика при отсутствии пакетов (только для тестирования).                               | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| InstanceName                                                       | Уникальное имя экземпляра DPI.CS.                                                                                                                  | string | \-                    | Любая валидная строка                                                                               | M   | R      |
| GyPlusMode                                                         | Флаг включения функции Ericsson Gy+ (Rule-Space-* AVP).                                                                                            | bool   | 0                     | 0, 1                                                                                                | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-ext-filter"></a>[External Filter]**                 |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| ExternalRedirectURL                                                | URL сервера для перенаправления HTTP трафика при попытке доступа к запрещенному ресурсу.                                                           | string | Пустая строка         | Любой валидный URL с указанием протокола, например "http://example.com/"                            | O   | R      |
| ExternalRedirectIP                                                 | ipадрес сервера перенаправления.                                                                                                                   | string | Пустая строка         | Любой валидныйipадрес, например "10.123.2.1"                                                        | O   | R      |
| ExternalMediatorURL                                                | URL микросервиса DPI.MED.                                                                                                                          | string | Пустая строка         | Любой валидный URL с указанием протокола, например "http://example.com/"                            | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-bandwidthcontrol"></a>[BandwidthControl]**   |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Enabled                                                            | Флаг включения управления полосой пропускания на DPI.TF.                                                                                           | bool   | \-                    | 0, 1                                                                                                | M   | PR[^1] |
| BandwidthEnumerator                                                | Флаг использования новой таблицы скоростей, значения которой начинаются с 1 Kbps.                                                                  | bool   | \-                    | 0, 1                                                                                                | O   | R      |
| StartEIR                                                           | Идентификатор значения EIR таблицы скоростей DPI.TF для стартовой корзины.                                                                         | int    | 18 (300Kbps)          | 0-255                                                                                               | O   | PR[^2] |
| BestEffortUplinkEIR                                                | Идентификатор значения EIR таблицы скоростей DPI.TF для BestEffort uplink корзины. Актуально только для DPIv5.                                     | int    | 255 (1,024 Gbps)      | 0-255                                                                                               | O   | PR[^2] |
| BestEffortDownlinkEIR                                              | Идентификатор значения EIR таблицы скоростей DPI.TF для BestEffort downlink корзины. Актуально только для DPIv5.                                   | int    | 255 (1,024 Gbps)      | 0-255                                                                                               | O   | PR[^2] |
| LimitEIR                                                           | Минимальное значение идентификатора EIR абонента в политике, по достижению которого трафик будет отправлен во внешнюю петлю.                       | int    | 255 (1,024 Gbps)      | 0-255                                                                                               | O   | R      |
| BalancerEIR                                                        | Максимальная полоса пропускания BALANCER DPI.TF в Mbps.                                                                                            | int    | \-                    | Пропускная способность каналов DPI.TF                                                               | M   | PR[^2] |
| BalancerCIRs                                                       | Список из 8 значений CIR в Mbps для инициализации ячеек BALANCNER DPI.TF.                                                                          | list   | \-                    | Формат: {int(1);int(2) ...int(8) };                                                                 | M   | PR[^2] |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-tcpreassembler"></a>[TcpReassembler]**       |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Enabled                                                            | Флаг включения внутреннего TCP-reassmebler.                                                                                                        | bool   | 0                     | 0, 1                                                                                                | O   | P      |
| MaxSegments                                                        | Максимальное количество сегментов для переупорядочивания для 1 IPv4-потока.                                                                        | int    | 200                   | N > 0                                                                                               | O   | P      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-gtp"></a>[GTP]**                             |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| GTP_Traffic                                                        | Флаг работы с GTP-трафиком.                                                                                                                        | bool   | 0                     | 0, 1                                                                                                | O   | P      |
| SubscriberIdType                                                   | Идентификатор абонента для извлечения из GTP.                                                                                                      | int    | 0                     | 0 (IMSI), 1 (MSISDN), 2 (IMEI)                                                                      | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-highavailability"></a>[HighAvailability]**   |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| StartAsMaster                                                      | Флаг начального режима работы экземпляра DPI.CS после запуска.                                                                                     | bool   | 0                     | 0 (slave), 1 (master)                                                                               | O   | P      |
| IcmpFailoverDetection                                              | Флаг необходимости использования мониторинга ICMP сообщений в User Plane для определения состояния DPI.CS.                                         | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| MasterIpAddresses                                                  | Список sourceipв User Plane, при получении ICMP сообщений от которых DPI.CS перейдет в состояние master.                                           | list   | Пустой список         | Формат: { "IPv4"; "IPv4" ... };                                                                     | O   | R      |
| SlaveIpAddresses                                                   | Список sourceipв User Plane, при получении ICMP сообщений от которых DPI.CS перейдет в состояние slave.                                            | list   | Пустой список         | Формат: { "IPv4"; "IPv4" ... };                                                                     | O   | R      |
| RequiredMasterCount                                                | Минимальное количество master IPv4, ICMP сообщения от которых должен получать DPI.CS для перехода в состояние master.                              | int    | 1                     | > 0                                                                                                 | O   | R      |
| PingTimeout                                                        | Время в мс, в течение которого могут отсутствовать ICMP сообщения с masteripадресов.                                                               | int    | 20000                 | > 0                                                                                                 | O   | R      |
| IcmpType                                                           | Необходимый тип ICMP сообщения, который DPI.CS должен получить от masteripадресов для перехода в состояние master.                                 | int    | 8                     | 0 (echo-reply), 8 (echo-request)                                                                    | O   | R      |
| SendRequests                                                       | Посылать ICMP-запросы самостоятельно. Запросы будут отправляться на первый адрес из списка `MasterIpAddresses`.                                    | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| SendRequestsToNW                                                   | Флаг отправки ICMP-запросов в сторону сети или абонента.                                                                                           | bool   | 1                     | 1 - посылать пинги в сторону сети, 0 - в сторону абонента                                           | O   | R      |
| SenderIP                                                           | IP с которого отправлять ICMP запросы.                                                                                                             | string | 0.0.0.0               | Любой валидный IPv4-адрес                                                                           | O   | R      |
| MasterMAC                                                          | MAC-адрес на который отправлять ICMP запросы.                                                                                                      | string | 00:00:00:00:00:00     | Любой валидный MAC-адрес                                                                            | O   | R      |
| SenderMAC                                                          | MAC-адрес с которого отправлять ICMP запросы.                                                                                                      | string | 00:00:00:00:00:00     | Любой валидный MAC-адрес                                                                            | O   | R      |
| VLAN                                                               | VLAN который нужно добавлять к отправляемым ICMP запросам.                                                                                         | int    | -                     | 2-4094                                                                                              | O   | R      |
| MaximumPingsMissed                                                 | Количество отсутствующих ответов на ICMP сообщения, после которого произойдет смена состояния.                                                     | int    | 10                    | > 0                                                                                                 | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-congestioncontrol"></a>[CongestionControl]** |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| NormalLevelStartPolicy                                             | Идентификатор значения EIR таблицы скоростей DPI.TF стартовой корзины при уровне загрузки приложения Normal.                                       | int    | 255 (1,024 Gbps)      | 0-255                                                                                               | O   | R      |
| MinorLevelStartPolicy                                              | Идентификатор значения EIR таблицы скоростей DPI.TF стартовой корзины при уровне загрузки приложения Minor.                                        | int    | 200 (96 Mbps)         | 0-255                                                                                               | O   | R      |
| MajorLevelStartPolicy                                              | Идентификатор значения EIR таблицы скоростей DPI.TF стартовой корзины при уровне загрузки приложения Major.                                        | int    | 100 (8,6 Mbps)        | 0-255                                                                                               | O   | R      |
| CriticalLevelStartPolicy                                           | Идентификатор значения EIR таблицы скоростей DPI.TF стартовой корзины при уровне загрузки приложения Critical.                                     | int    | 15 (128 Kbps)         | 0-255                                                                                               | O   | R      |
| MaxAllocatedPrimitives                                             | Максимальное количество примитивов в общей очереди приложения, при котором начинают игнорироваться новые примитивы с данными.                      | int    | 1000000               | > 0                                                                                                 | O   | P      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-accountingcontrol"></a>[AccountingControl]** |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Enabled                                                            | Флаг необходимости наличия активного контекста абонента.                                                                                           | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| WaitAccountingStop                                                 | Флаг необходимости получения RADIUS Accounting-Stop для закрытия Gx/Gy логик, соответствующих данной абонентской сессии.                           | bool   | 0                     | 0, 1                                                                                                | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-handlers"></a>[Handlers]**                   |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Subscriber_Handlers                                                | Количество обработчиков абонентов (на каждый поток обработки будет Subscriber_Handlers/количество потоков или минимум 1 обработчик ).              | int    | 10000                 | > 0                                                                                                 | O   | P      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-hotline"></a>[Hotline]**                     |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Enabled                                                            | Флаг включения функции.                                                                                                                            | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| EnableDNSRedirect                                                  | Флаг использования DNS spoofing для перенаправления трафика.                                                                                       | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| InjectedMAC                                                        | MAC-адрес источника для пакетов, вставляемых в канал DPI.CS данной функцией.                                                                       | string | 00:00:00:00:00:00     | Формат: aa:bb:cc:ee:dd:ff                                                                           | O   | R      |
| MappingRedirectHost2Ip                                             | Флаг необходимости проверкиipадреса назначения, в случае совпадения http.server + http.url с целевым ресурсом для перенаправления.                 | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| RedirectHost                                                       | URL-адрес, на который необходимо перенаправлять HTTP трафик.                                                                                       | string | \-                    | Формат: "http://example.com/aoc/"                                                                   | O   | R      |
| RedirectHostIP                                                     | Списокipадресов, принадлежащих RedirectHost.                                                                                                       | list   | \-                    | Формат: { "IPv4"; "IPv4"; ... };                                                                    | O   | R      |
| URL2IPMapping                                                      | Список сопоставления URL:IPv4 для перенаправления по A записям DNS.                                                                                | object | \-                    | Соответствуют ограничениям элементов списка                                                         | O   | R      |
| **{**                                                              |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| ...                                                                |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **{**                                                              |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| URL                                                                | URL для перенаправления, заданный в политике абонента.                                                                                             | string | \-                    | Формат: "http://example.com/aoc/"                                                                   | M   | R      |
| IP                                                                 | ipадрес, соответствующий URL для перенаправления, который необходимо подставлять в DNS ответы.                                                     | ip     | \-                    | ip                                                                                                  | M   | R      |
| **};**                                                             |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| ...                                                                |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **};**                                                             |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| BlackList                                                          | Списокipадресов, HTTP трафик на которые не перенаправляется при включенной функции.                                                                | list   | \-                    | Формат: { "IPv4"; "IPv4" ... "IPv4" };                                                              | O   | R      |
| Selfcare                                                           | Список параметров шифрования значений при обогащении HTTP-запросов идентификатором абонента при использовании функции в глобальном сервисе DPI.CS. | object | \-                    | Соответствуют ограничениям элементов списка                                                         | O   | R      |
| **{**                                                              |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| EnableSubscriberIdEncryption                                       | Флаг использования алгоритма шифрования RC4 с представлением в base64 значения идентификатора абонента.                                            | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| SubscriberIdEncryptionKey                                          | Ключ для алгоритма шифрования RC4.                                                                                                                 | string | \-                    | Произвольная строка без спец. символов                                                              | O   | R      |
| **};**                                                             |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-dnswhitelist"></a>[DnsWhitelist]**           |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Enabled                                                            | Флаг включения функции.                                                                                                                            | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| Whitelist                                                          | Списокipадресов на стороне сети Интернет, на которое разрешены DNS-запросы.                                                                        | list   | Пустой список         | Формат: { IPv4; IPv4; ... IPv4; };                                                                  | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-timers"></a>[Timers]**                       |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| WaitStartTimeout                                                   | Время ожидания инициализации diameter-стека в секундах.                                                                                            | int    | 60000                 | > 0                                                                                                 | O   | R      |
| SubscriberRefreshTimeout                                           | Интервал обновления политики абонентов в мc.                                                                                                       | int    | 60                    | > 0                                                                                                 | O   | P      |
| SubscriberInactiveTimeout                                          | Максимальное время ожидания трафика после открытия абонентской сессии в мс.                                                                        | int    | 0                     | 0 (не ограничено), > 0                                                                              | O   | R      |
| ServiceIdleTimeout                                                 | Максимальное время бездействия сервисной логики после завершения всех потоков в мс.                                                                | int    | 60000                 | ≥ 0                                                                                                 | O   | R      |
| StatisticOutTimeout                                                | Интервал вывода статистики в statistic.log в мc.                                                                                                   | int    | 10000                 | > 0                                                                                                 | O   | R      |
| SubscriberIdleTimeout                                              | Максимальное время бездействия абонентской логики после завершения абонентской сессии в мс.                                                        | int    | 10000                 | > 0                                                                                                 | O   | R      |
| PacketEngineStatisticsRate                                         | Интервал вывода статистики работы PacketEngine в мс.                                                                                               | int    | 10000                 | 1000-100000                                                                                         | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-trace"></a>[Trace]**                         |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| ExtFlowTableStatistic                                              | Флаг включения вывода расширенной статистике по таблицам потоков.                                                                                  | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| ExtFlowContextTableStatistic                                       | Флаг включения вывода расширенной статистике по таблицам контекстов.                                                                               | bool   | 0                     | 0, 1                                                                                                | O   | R      |
| **<a name="dpi-common-steering"></a>[Steering]**                   |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| Protocols                                                          | Список идентификаторов протоколов Qosmos ixEngine.                                                                                                 | list   | \-                    | Любые протоколы из заявленных в поддержке классификатором, Формат: { ProtocolID; ProtocolID; ... }; | O   | R      |
|                                                                    |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| **<a name="dpi-common-snmp"></a>[Snmp]**                           |                                                                                                                                                    |        |                       |                                                                                                     |     |        |
| SnmpQueueSize                                                      | Размер очереди для отправки трапов.                                                                                                                | int    | 1000                  | > 0                                                                                                 | O   | P      |
| SendNoMappingAlarm                                                 | Флаг необходимости отправки трапа по причине отсутствия связки IP-ID.                                                                              | bool   | 0                     | 0, 1                                                                                                | O   | P      |
| LogicCountSendPeriod                                               | Интервал отправки трапов в мс.                                                                                                                     | int    | 10000                 | > 0                                                                                                 | O   | R      |


[^1]: Если отключено на старте, то NR
[^2]: R при рестарте DPI.TF


## Пример конфигурации

```
[Common]
UseMultipliers = 0;
EnableSyncPolicyManager = 0; 
QuotaThreshold = 500;
EnableLocalServiceList = 1;
PersistentMappings = 1;
LevelDBAsync = 1;
LevelDBWriterQueueSize = 8192;
LevelDBWriterIdleSleepMsec = 2;
LevelDBWriterInsertSleepMsec = 3;

FragmentedWhiteList = { 
    "10.0.10.0/24"
    "10.0.12.0/24"
};

L2Accounting = 0;
ResendTimeStep = 1;

InstanceName = "pcef01.dc1";

UpdateFlowsQueueSize = 200000;
EventFlowsQueueSize = 10000;
ChangeServiceQueueSize = 10000;

DefaultToS = 255;

ResetByLogicKey = 1;

[ExternalFilter]
ExternalRedirectURL = "http://example.com/";
ExternalRedirectIP = "10.10.1.23";
ExternalMediatorURL = "http://10.123.3.1:8080";


[BandwidthControl]
Enabled = 1;
UpdateBucketMode = 1;
BandwidthEnumerator = 1;
StartEIR = 164;
BestEffortUplinkEIR = 255;
BestEffortDownlinkEIR = 255;
LimitEIR = 255;
BalancerEIR = 20000;
BalancerCIRs = { 20000; 8000; 4000; 2000; 1000; 500; 200; 50;}

[TcpReassembler]
Enabled = 1;
MaxSegments = 25;

[GTP]
GTP_Traffic = 0;
SubscriberIdType = 1;

[HighAvailability]
StartAsMaster = 1;
IcmpFailoverDetection = 1;

MasterIpAddresses = {
            "10.77.177.221";
            "10.77.177.241";
};

SlaveIpAddresses = "172.16.64.173";
PingTimeout = 3000;
IcmpType = 0;
SendRequests = 0;
SendRequestsToNW = 0;
MaximumPingsMissed = 3;

[CongestionControl]
MinorLevelStartPolicy = 255;
NormalLevelStartPolicy = 200;
MajorLevelStartPolicy = 100;
CriticalLevelStartPolicy = 15;
MaxAllocatedPrimitives = 1000000;

[AccountingControl]
Enabled = 0;
WaitAccountingStop = 1;

[Handlers]
Service_Handlers = 500000;
Subscriber_Handlers = 200000;

[Hotline]
Enabled = 1;
RedirectHost = "http://welcomepage.example.com";
RedirectHostIP = "192.168.100.63";
BlackList = { "192.168.100.203"; "192.168.100.205";};

URL2IPMapping = {
    {
        URL="https://example.com/";
        IP ="192.168.100.63";
    };
    {
        URL="https://example.net/";
        IP ="192.168.100.64";
    };
};

Selfcare =
{
    EnableSubscriberIdEncryption = 0;
    SubscriberIdEncryptionKey = "test_key";
};

[DnsWhitelist]
Enabled = 1;
Whitelist = { 217.11.190.2; 193.111.11.2; 168.95.1.1; 109.74.69.254; 8.8.8.8; 217.11.177.155; };

[Handlers]
Service_Handlers = 500000;
Subscriber_Handlers = 200000;

[CongestionControl]
MinorLevelStartPolicy = 255;
NormalLevelStartPolicy = 200;
MajorLevelStartPolicy = 100;
CriticalLevelStartPolicy = 15;
MaxAllocatedPrimitives = 1000000;

[Timers]
SubscriberRefreshTimeout = 60000;
SubscriberInactiveTimeout = 0;
SubscriberIdleTimeout = 2000;
ServiceIdleTimeout = 600000;
StatisticOutTimeout = 10000;
PacketEngineStatisticsRate = 10000;

[Trace]
ExtFlowTableStatistic = 1;
ExtFlowContextTableStatistic = 1;

[Snmp]
SnmpQueueSize = 100;
SendNoMappingAlarm = 0;
LogicCountSendPeriod = 100000
;
```








