---
title: "diameter/diameter.cfg"
description: "Файл конфигурации локального DIAMETER клиента."
weight: 7
type: docs

---

## Используемые секции

+ [Common](#diameter-common) - описание общих параметров клиента
+ [LocalPeerCapabilities](#diameter-localpeerpapabilities) - общее описание конфигурации всех DIAMETER соединений клиента 
+ [EgressPool](#diameter-egresspool) - настройки предаллоцированного буфера для отправки сообщений
+ [Timers](#diameter-timers) - описание таймеров клиента
+ [Failover](#diameter-failover) - настройки переотправки запросов в кластере

## Описание параметров

| Параметр                                                                 | Описание                                                                                                                |    Тип    |      Значение по умолчанию       |                                             Допустимые значения                                             | O/M |  R  |
|--------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|-----------|----------------------------------|-------------------------------------------------------------------------------------------------------------|-----|-----|
| **<a name="diameter-common"></a>[Common]**                                        |                                                                                                                                           |                             |                                                    |                                                                                                                               |                       |  |
| RequestQueueLimit                                                                 | Максимальное количество ожидающих отправки запросов в сокете. |            int            |                 0 (не ограничено)                  |                                                              ≥ 0                                                              |          O            |           R |
|
| **<a name="diameter-localpeerpapabilities"></a>[LocalPeerCapabilities]**          |                                                                                                                                           |                             |                                                    |                                                                                                                               |                       |  |
| Product-Name                                                                      | Название платформы. |          string           |                         \-                         |                                            Произвольная строка без спец. символов                                             |          M            |          P |
| Vendor-ID                                                                         | Идентификатор производителя. |            int            |                         \-                         |                                                             20873                                                             |          M            |          P |
| Firmware-Revision                                                                 | Версия программного обеспечения. |            int            |                         \-                         |                                                              \-                                                               |          M            |          P |
| Origin-State-Id                                                                   | Идентификатор состояния клиента. |            int            |          Timestamp времени запуска DPI.CS          |                                                              > 0                                                              |          O            |          P |
| Origin-Host                                                                       | Origin-Host клиента для данного соединения. |          string           |                         \-                         |                                            Произвольная строка без спец. символов                                             |          M            |          P |
| Origin-Realm                                                                      | Origin-Realm клиента для данного соединения. |          string           |                         \-                         |                                            Произвольная строка без спец. символов                                             |          M            |          P |
| Auth-Application-Id                                                               | Список идентификаторов поддерживаемых приложений авторизации и аутентификации верхнего уровня. |            list            |                         \-                         |                                                  0, 3, 4, 16777238, 16777303                                                  |          M            |          P |
| Acct-Application-Id                                                               | Список идентификаторов поддерживаемых приложений тарификации верхнего уровня. |            list            |                         \-                         |                                                            0, 3, 4                                                            |          M            |          P |
| Vendor-Specific-Application-Id                                                    | Список идентификаторов поддерживаемых приложений других производителей. |         object         |                         \-                         |                                          Соответствуют ограничениям элементов списка                                          |          O            |          P |
| **{**                                                                             |                  |                  |                  |                  |                                                                                                                                           |                             |                       |                       |                       |  |
| Vendor-Id                                                                         | Идентификатор производителя. |            int            |                         \-                         |          Любое значение, заявленное в [IANA](https://www.iana.org/assignments/enterprise-numbers/enterprise-numbers)          |          M            |          P |
| Auth-Application-Id                                                               | Список идентификаторов поддерживаемых приложений авторизации и аутентификации верхнего уровня. |            list            |                         \-                         |                                                           int> 0                                                            |          M            |          P |
| Acct-Application-Id                                                               | Список идентификаторов поддерживаемых приложений тарификации верхнего уровня. |            list            |                         \-                         |                                                           int> 0                                                            |          M            |          P |
| **};**                                                                            |                  |                  |                  |                  |                                                                                                                                           |                             |                       |                       |                       |  |
| Inband-Security-Id                                                                | Список поддерживаемых механизмов обеспечения безопасности. |            list            |                         \-                         |                                                    0 (NO_INBAND_SECURITY)                                                     |          M            |          P |
| Supported-Vendor-Id                                                               | Список поддерживаемых производителей. |            list            |                         \-                         |                                                           int> 0                                                            |          O            |          P |
|  |                  |                  |                  |                  |                  |                                                                                            |                       |                       |                       |                       |                       |  |
| **<a name="diameter-timers"></a>[Timers]**                                        |                  |                  |                  |                  |                  |                                                                                                                                           |                       |                       |                       |                       |  |
| Appl_Timeout                                                                      | Максимальное время в мс между посылкой TCP SYN или SCTP INIT и получением CEA. |            int            |                       40000                        |                                                         Не ограничено                                                         |          O            |          P |
| Watchdog_Timeout                                                                  | Интервал посылки DWR в мс. |            int            |                       10000                        |                                                         Не ограничено                                                         |          O            |          P |
| Reconnect_Timeout                                                                 | Интервал времени в мс между попытками открыть новое TCP/SCTP соединение с удаленным узлом. |            int            |                       30000                        |                                                         Не ограничено                                                         |          O            |          P |
| Statistic_Timeout                                                                 | Интервал вывода статистики работы в мс. |            int            |                       60000                        |                                                              > 0                                                              |          O            |           R |
| Tx_Timeout                                                                        | Таймаут получения ответа на запрос в мс. |            int            |                       30000                        |                                                              > 0                                                              |          O            |           R |
| Startup_Timeout                                                                   | Таймаут перехода компоненты Sg.DIAM в активное состояние в мс(все компоненты Sg.PCSM должны создаться до этого момента). |            int            |                        1000                        |                                                              > 0                                                              |          O            |          P |
|  |                  |                  |                  |                  |                  |                                                                                            |                       |                       |                       |                       |                       |  |
| **<a name="diameter-egresspool"></a>[EgressPool]**                                |                  |                  |                  |                  |                  |                                                                                                                                           |                       |                       |                       |                       |  |
| Count                                                                             | Максимальное количество сообщений в пуле, для которых предаллоцируется память. |            int            |                         \-                         |                                                              > 0                                                              |          M            |          P |
| Length                                                                            | Максимальная длина в байтах одного DIAMETER сообщения в пуле. |            int            |                         \-                         |                                                              > 0                                                              |          M            |          P |
| **<a name="diameter-failover"></a>[Failover]**                                    |                  |                  |                  |                  |                  |                                                                                                                                           |                       |                       |                       |                       |  |
| **{**                                                                             |                  |                  |                  |                  |                                                                                                                                           |                             |                       |                       |                       |  |
| Route                                                                             | Название маршрута. |          string           |                         \-                         |                                            Произвольная строка без спец. символов                                             |          M            |           R |
| Codes                                                                             | Список diameter ResultCode, при получении которых запрос необходимо переотправить на другую ноду в кластере. |            list            |                   Пустой список                    |                                                           int> 0                                                            |          O            |           R |
| **};**                                                                            |                  |                  |                  |                  |                                                                                                                                           |                             |                       |                       |                       |  |

## Пример конфигурации

```
        [LocalPeerCapabilities]
        Origin-Host                     = "pcef01.epc.mnc001.mcc123.3gppnetwork.org";
        Origin-Realm                    = "epc.mnc001.mcc123.3gppnetwork.org";
        Vendor-ID                       = 20873;
        Product-Name                    = "PROTEI-DPI";
        Firmware-Revision               = 21;
        Auth-Application-Id             = { 0; 3; 4; 16777238; 16777303; };
        Acct-Application-Id             = { 0; 3; 4 };
        Inband-Security-Id              = { 0; };
        Supported-Vendor-Id             = { 20873; 10415 };
        Vendor-Specific-Application-Id  = {
                {
                    Vendor-Id = 10415;
                    Auth-Application-Id = { 16777238; 16777303; };
                }
        };

        [EgressPool]
        Count = 4096;
        Length = 16384;

        [Timers]
        Appl_Timeout        = 5000;
        Watchdog_Timeout    = 15000;
        Reconnect_Timeout   = 30000;
        Statistic_Timeout   = 60000:
        Tx_Timeout          = 5000;
        Startup_Timeout     = 1000;

        [Failover]
        {
            Route = "pcrf";
            Codes = {3002;3005;};
        };
        {
            Route = "ocs";
            Codes = {3002;3005;};
        };
```